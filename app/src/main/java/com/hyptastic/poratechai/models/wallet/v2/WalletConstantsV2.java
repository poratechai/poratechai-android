package com.hyptastic.poratechai.models.wallet.v2;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hyptastic.poratechai.models.wallet.WalletConstants;

/**
 * Created by ISHRAK on 7/18/2018.
 */

public class WalletConstantsV2 extends WalletConstants implements Parcelable {
    @SerializedName("DURATION_NONE")
    @Expose
    private int DURATION_NONE;
    @SerializedName("DURATION_ONE_MONTH")
    @Expose
    private int DURATION_ONE_MONTH;
    @SerializedName("DURATION_THREE_MONTHS")
    @Expose
    private int DURATION_THREE_MONTHS;
    @SerializedName("COST_ONE_MONTH")
    @Expose
    private double COST_ONE_MONTH;
    @SerializedName("COST_THREE_MONTHS")
    @Expose
    private double COST_THREE_MONTHS;

    public WalletConstantsV2(int DURATION_NONE, int DURATION_ONE_MONTH, int DURATION_THREE_MONTHS, double COST_ONE_MONTH, double COST_THREE_MONTHS) {
        this.DURATION_NONE = DURATION_NONE;
        this.DURATION_ONE_MONTH = DURATION_ONE_MONTH;
        this.DURATION_THREE_MONTHS = DURATION_THREE_MONTHS;

        this.COST_ONE_MONTH = COST_ONE_MONTH;
        this.COST_THREE_MONTHS = COST_THREE_MONTHS;
    }

    public int getDURATION_NONE() {
        return DURATION_NONE;
    }

    public void setDURATION_NONE(int DURATION_NONE) {
        this.DURATION_NONE = DURATION_NONE;
    }

    public int getDURATION_ONE_MONTH() {
        return DURATION_ONE_MONTH;
    }

    public void setDURATION_ONE_MONTH(int DURATION_ONE_MONTH) {
        this.DURATION_ONE_MONTH = DURATION_ONE_MONTH;
    }

    public int getDURATION_THREE_MONTHS() {
        return DURATION_THREE_MONTHS;
    }

    public void setDURATION_THREE_MONTHS(int DURATION_THREE_MONTHS) {
        this.DURATION_THREE_MONTHS = DURATION_THREE_MONTHS;
    }

    public double getCOST_ONE_MONTH() {
        return COST_ONE_MONTH;
    }

    public void setCOST_ONE_MONTH(double COST_ONE_MONTH) {
        this.COST_ONE_MONTH = COST_ONE_MONTH;
    }

    public double getCOST_THREE_MONTHS() {
        return COST_THREE_MONTHS;
    }

    public void setCOST_THREE_MONTHS(double COST_THREE_MONTHS) {
        this.COST_THREE_MONTHS = COST_THREE_MONTHS;
    }

    //Parcelable - start

    public static final Parcelable.Creator<WalletConstantsV2> CREATOR = new Creator<WalletConstantsV2>() {
        @Override
        public WalletConstantsV2 createFromParcel(Parcel parcel) {
            return new WalletConstantsV2(parcel);
        }

        @Override
        public WalletConstantsV2[] newArray(int i) {
            return new WalletConstantsV2[i];
        }
    };

    public WalletConstantsV2(Parcel parcel){
        DURATION_NONE = parcel.readInt();
        DURATION_ONE_MONTH = parcel.readInt();
        DURATION_THREE_MONTHS = parcel.readInt();
        COST_ONE_MONTH = parcel.readDouble();
        COST_THREE_MONTHS = parcel.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(DURATION_NONE);
        parcel.writeInt(DURATION_ONE_MONTH);
        parcel.writeInt(DURATION_THREE_MONTHS);
        parcel.writeDouble(COST_ONE_MONTH);
        parcel.writeDouble(COST_THREE_MONTHS);
    }

    //Parcelable - end
}
