
package com.hyptastic.poratechai.models.wallet;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InvoiceResponse implements Parcelable
{
    @Expose
    @SerializedName("status")
    private int status;

    @Expose
    @SerializedName("data")
    private InvoiceResponseData data;

    public InvoiceResponse(int status, InvoiceResponseData data) {
        this.status = status;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public InvoiceResponseData getData() {
        return data;
    }

    public void setData(InvoiceResponseData data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "InvoiceResponse{" +
                "status=" + status +
                ", data=" + data +
                '}';
    }

    //Parcelable

    public InvoiceResponse(Parcel parcel){
        status = parcel.readInt();
        data = parcel.readParcelable(InvoiceResponse.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(status);
        parcel.writeParcelable(data, i);
    }

    public static final Parcelable.Creator<InvoiceResponse> CREATOR = new Parcelable.Creator<InvoiceResponse>(){
        @Override
        public InvoiceResponse createFromParcel(Parcel parcel) {
            return new InvoiceResponse(parcel);
        }

        @Override
        public InvoiceResponse[] newArray(int i) {
            return new InvoiceResponse[0];
        }
    };
}
