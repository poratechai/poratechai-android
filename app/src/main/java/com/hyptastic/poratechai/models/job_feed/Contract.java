package com.hyptastic.poratechai.models.job_feed;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Created by ISHRAK on 3/18/2018.
 */

public class Contract implements Parcelable {
    @SerializedName("daysPerWeek")
    @Expose
    private int daysPerWeek;
    @SerializedName("salaryOffer")
    @Expose
    private int salaryOffer;
    @SerializedName("message")
    @Expose
    private String message;

    public Contract(int daysPerWeek, int salaryOffer, String message) {
        this.daysPerWeek = daysPerWeek;
        this.salaryOffer = salaryOffer;
        this.message = message;
    }

    public int getDaysPerWeek() {
        return daysPerWeek;
    }

    public void setDaysPerWeek(int daysPerWeek) {
        this.daysPerWeek = daysPerWeek;
    }

    public int getSalaryOffer() {
        return salaryOffer;
    }

    public void setSalaryOffer(int salaryOffer) {
        this.salaryOffer = salaryOffer;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this).append("daysPerWeek", daysPerWeek).append("salaryOffer", salaryOffer).append("message", message).toString();
    }

    //Parcelable - definitions

    public Contract(Parcel parcel)
    {
        daysPerWeek = parcel.readInt();
        salaryOffer = parcel.readInt();
        message     = parcel.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(daysPerWeek);
        parcel.writeInt(salaryOffer);
        parcel.writeString(message);
    }

    public static final Parcelable.Creator<Contract> CREATOR = new Parcelable.Creator<Contract>(){
        @Override
        public Contract createFromParcel(Parcel parcel) {
            return new Contract(parcel);
        }

        @Override
        public Contract[] newArray(int i) {
            return new Contract[i];
        }
    };
}
