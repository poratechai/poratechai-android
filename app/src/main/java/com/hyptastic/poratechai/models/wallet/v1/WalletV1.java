
package com.hyptastic.poratechai.models.wallet.v1;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

public class WalletV1 implements Parcelable
{
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("contractBalance")
    @Expose
    private double contractBalance;
    @SerializedName("transactions")
    @Expose
    private String[] transactions = null;
    @SerializedName("lastPaidDate")
    @Expose
    private String lastPaidDate;
    public final static Parcelable.Creator<WalletV1> CREATOR = new Creator<WalletV1>() {


        @SuppressWarnings({
            "unchecked"
        })
        public WalletV1 createFromParcel(Parcel in) {
            return new WalletV1(in);
        }

        public WalletV1[] newArray(int size) {
            return (new WalletV1[size]);
        }

    }
    ;

    protected WalletV1(Parcel in) {
        this.userId = in.readString();
        this.contractBalance = in.readDouble();
        transactions = in.createStringArray();
        this.lastPaidDate = in.readString();
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public WalletV1() {
    }

    /**
     * 
     * @param transactions
     * @param lastPaidDate
     * @param userId
     * @param contractBalance
     */
    public WalletV1(String userId, double contractBalance, String[] transactions, String lastPaidDate) {
        super();
        this.userId = userId;
        this.contractBalance = contractBalance;
        this.transactions = transactions;
        this.lastPaidDate = lastPaidDate;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public double getContractBalance() {
        return contractBalance;
    }

    public void setContractBalance(double contractBalance) {
        this.contractBalance = contractBalance;
    }

    public String[] getTransactions() {
        return transactions;
    }

    public void setTransactions(String[] transactions) {
        this.transactions = transactions;
    }

    public String getLastPaidDate() {
        return lastPaidDate;
    }

    public void setLastPaidDate(String lastPaidDate) {
        this.lastPaidDate = lastPaidDate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("userId", userId).append("contractBalance", contractBalance).append("transactions", transactions).append("lastPaidDate", lastPaidDate).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userId);
        dest.writeDouble(contractBalance);
        dest.writeStringArray(transactions);
        dest.writeString(lastPaidDate);
    }

    public int describeContents() {
        return  0;
    }

}
