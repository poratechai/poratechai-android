package com.hyptastic.poratechai.models.users;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 5/21/18.
 */

public class ParentStat {
    @SerializedName("postedJobs")
    @Expose
    private int postedJobs;
    @SerializedName("contractRequests")
    @Expose
    private int contractRequests;
    @SerializedName("rating")
    @Expose
    private float rating;
    @SerializedName("tutorsEmployed")
    @Expose
    private int tutorsEmployed;

    public ParentStat(){};

    public ParentStat(int postedJobs, int contractRequests, float rating, int tutorsEmployed) {
        super();
        this.postedJobs = postedJobs;
        this.contractRequests = contractRequests;
        this.rating = rating;
        this.tutorsEmployed = tutorsEmployed;
    }

    public int getpostedJobs() {
        return postedJobs;
    }

    public void setpostedJobs(int postedJobs) {
        this.postedJobs = postedJobs;
    }

    public int getcontractRequests() {
        return contractRequests;
    }

    public void setcontractRequests(int contractRequests) {
        this.contractRequests = contractRequests;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public int gettutorsEmployed() {
        return tutorsEmployed;
    }

    public void settutorsEmployed(int tutorsEmployed) {
        this.tutorsEmployed = tutorsEmployed;
    }

    @Override
    public String toString() {
        return "TutorStat{" +
                "postedJobs=" + postedJobs +
                ", contractRequests=" + contractRequests +
                ", rating=" + rating +
                ", tutorsEmployed=" + tutorsEmployed +
                '}';
    }
}
