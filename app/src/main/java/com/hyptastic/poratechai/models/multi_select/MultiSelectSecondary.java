package com.hyptastic.poratechai.models.multi_select;

import android.support.v7.widget.RecyclerView;

/**
 * Created by ISHRAK on 1/19/2019.
 */

public class MultiSelectSecondary {
    public boolean isChecked;
    public String item;

    public MultiSelectSecondary(boolean isChecked, String item) {
        this.isChecked = isChecked;
        this.item = item;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    @Override
    public String toString() {
        return "MultiSelectSecondary{" +
                "isChecked=" + isChecked +
                ", item='" + item + '\'' +
                '}';
    }
}
