package com.hyptastic.poratechai.models.users;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 5/11/18.
 */

public class TutorStat {
    @SerializedName("activeContracts")
    @Expose
    private int activecontracts;
    @SerializedName("currentIncome")
    @Expose
    private int currentIncome;
    @SerializedName("rating")
    @Expose
    private float rating;
    @SerializedName("matches")
    @Expose
    private int matches;

    public TutorStat(){};

    public TutorStat(int activecontracts, int currentIncome, float rating, int matches) {
        super();
        this.activecontracts = activecontracts;
        this.currentIncome = currentIncome;
        this.rating = rating;
        this.matches = matches;
    }

    public int getActivecontracts() {
        return activecontracts;
    }

    public void setActivecontracts(int activecontracts) {
        this.activecontracts = activecontracts;
    }

    public int getCurrentIncome() {
        return currentIncome;
    }

    public void setCurrentIncome(int currentIncome) {
        this.currentIncome = currentIncome;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public int getMatches() {
        return matches;
    }

    public void setMatches(int matches) {
        this.matches = matches;
    }

    @Override
    public String toString() {
        return "TutorStat{" +
                "activecontracts=" + activecontracts +
                ", currentIncome=" + currentIncome +
                ", rating=" + rating +
                ", matches=" + matches +
                '}';
    }
}

