package com.hyptastic.poratechai.models.multi_select;

import java.util.ArrayList;

/**
 * Created by ISHRAK on 1/19/2019.
 */

public class MultiSelectPrimary {
    private String title;
    private ArrayList<MultiSelectSecondary> multiSelectSecondaryItem;

    public MultiSelectPrimary(String title, ArrayList<MultiSelectSecondary> multiSelectSecondaryItem) {
        this.title = title;
        this.multiSelectSecondaryItem = multiSelectSecondaryItem;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<MultiSelectSecondary> getMultiSelectSecondaryItem() {
        return multiSelectSecondaryItem;
    }

    public void setItems(ArrayList<MultiSelectSecondary> multiSelectSecondaryItem) {
        this.multiSelectSecondaryItem = multiSelectSecondaryItem;
    }

    @Override
    public String toString() {
        return "MultiSelectPrimary{" +
                "title='" + title + '\'' +
                ", multiSelectSecondaryItem=" + multiSelectSecondaryItem +
                '}';
    }
}
