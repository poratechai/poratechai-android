
package com.hyptastic.poratechai.models.review;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Review implements Parcelable
{
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("reviewerName")
    @Expose
    private String reviewerName;
    @SerializedName("rating")
    @Expose
    private double rating;
    @SerializedName("reviewTags")
    @Expose
    private String[] reviewTags;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;

    private final static long serialVersionUID = 8828150563838755654L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Review() {
    }

    /**
     * @param userId
     * @param rating
     * @param reviewTags
     */
    public Review(String userId, String reviewerName, double rating, String[] reviewTags)
    {
        super();
        this.userId = userId;
        this.reviewerName = reviewerName;
        this.rating = rating;
        this.reviewTags = reviewTags;
    }

    /**
     * @param userId
     * @param rating
     * @param reviewTags
     * @param updatedAt
     */
    public Review(String userId, String reviewerName, double rating, String[] reviewTags, String updatedAt) {
        super();
        this.userId = userId;
        this.reviewerName = reviewerName;
        this.rating = rating;
        this.reviewTags = reviewTags;
        this.updatedAt = updatedAt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getReviewerName() {
        return reviewerName;
    }

    public void setReviewerName(String reviewerName) {
        this.reviewerName = reviewerName;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String[] getReviewTags() {
        return reviewTags;
    }

    public void setReviewTags(String[] reviewTags) {
        this.reviewTags = reviewTags;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("userId", userId).append("rating", rating).append("reviewTags", reviewTags).append("updatedAt", updatedAt).toString();
    }

    //Parcelable - start

    public Review(Parcel parcel)
    {
        userId = parcel.readString();
        rating = parcel.readDouble();
        reviewTags = parcel.createStringArray();
        updatedAt = parcel.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(userId);
        parcel.writeDouble(rating);
        parcel.writeStringArray(reviewTags);
        parcel.writeString(updatedAt);
    }

    public static final Parcelable.Creator<Review> CREATOR = new Parcelable.Creator<Review>(){
        @Override
        public Review createFromParcel(Parcel parcel) {
            return new Review(parcel);
        }

        @Override
        public Review[] newArray(int i) {
            return new Review[i];
        }
    };

    //Parcelable - end
}
