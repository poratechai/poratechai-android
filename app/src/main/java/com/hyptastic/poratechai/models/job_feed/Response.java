package com.hyptastic.poratechai.models.job_feed;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;

/**
 * Created by ISHRAK on 3/19/2018.
 */

public class Response implements Serializable {
    @SerializedName("Response")
    @Expose
    private String Response;

    public Response(String response) {
        Response = response;
    }

    public String getResponse() {
        return Response;
    }

    public void setResponse(String response) {
        Response = response;
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this).append("Response", Response).toString();
    }
}
