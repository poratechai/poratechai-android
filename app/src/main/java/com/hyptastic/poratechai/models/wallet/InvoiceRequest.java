
package com.hyptastic.poratechai.models.wallet;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

public class InvoiceRequest implements Parcelable
{
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("product_description")
    @Expose
    private String productDescription;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("zipcode")
    @Expose
    private String zipcode;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("oneMonthCost")
    @Expose
    private double oneMonthCost;
    @SerializedName("threeMonthsCost")
    @Expose
    private double threeMonthsCost;
    public final static Parcelable.Creator<InvoiceRequest> CREATOR = new Creator<InvoiceRequest>() {


        @SuppressWarnings({
            "unchecked"
        })
        public InvoiceRequest createFromParcel(Parcel in) {
            return new InvoiceRequest(in);
        }

        public InvoiceRequest[] newArray(int size) {
            return (new InvoiceRequest[size]);
        }

    }
    ;

    protected InvoiceRequest(Parcel in) {
        this.amount = ((String) in.readValue((String.class.getClassLoader())));
        this.productName = ((String) in.readValue((String.class.getClassLoader())));
        this.productDescription = ((String) in.readValue((String.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.email = ((String) in.readValue((String.class.getClassLoader())));
        this.phone = ((String) in.readValue((String.class.getClassLoader())));
        this.address = ((String) in.readValue((String.class.getClassLoader())));
        this.city = ((String) in.readValue((String.class.getClassLoader())));
        this.state = ((String) in.readValue((String.class.getClassLoader())));
        this.zipcode = ((String) in.readValue((String.class.getClassLoader())));
        this.country = ((String) in.readValue((String.class.getClassLoader())));
        this.userId = ((String) in.readValue((String.class.getClassLoader())));
        this.oneMonthCost = in.readDouble();
        this.threeMonthsCost = in.readDouble();
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public InvoiceRequest() {
    }

    /**
     * 
     * @param amount
     * @param phone
     * @param address
     * @param email
     * @param productDescription
     * @param userId
     * @param zipcode
     * @param name
     * @param state
     * @param productName
     * @param country
     * @param city
     */
    public InvoiceRequest(String amount, String productName, String productDescription, String name, String email, String phone, String address, String city, String state, String zipcode, String country, String userId, double oneMonthCost, double threeMonthsCost) {
        super();
        this.amount = amount;
        this.productName = productName;
        this.productDescription = productDescription;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.city = city;
        this.state = state;
        this.zipcode = zipcode;
        this.country = country;
        this.userId = userId;
        this.oneMonthCost = oneMonthCost;
        this.threeMonthsCost = threeMonthsCost;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public double getOneMonthCost() {
        return oneMonthCost;
    }

    public void setOneMonthCost(double oneMonthCost) {
        this.oneMonthCost = oneMonthCost;
    }

    public double getThreeMonthsCost() {
        return threeMonthsCost;
    }

    public void setThreeMonthsCost(double threeMonthsCost) {
        this.threeMonthsCost = threeMonthsCost;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("amount", amount).append("productName", productName).append("productDescription", productDescription).append("name", name).append("email", email).append("phone", phone).append("address", address).append("city", city).append("state", state).append("zipcode", zipcode).append("country", country).append("userId", userId).append("oneMonthCost", oneMonthCost).append("threeMonthsCost", threeMonthsCost).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(amount);
        dest.writeValue(productName);
        dest.writeValue(productDescription);
        dest.writeValue(name);
        dest.writeValue(email);
        dest.writeValue(phone);
        dest.writeValue(address);
        dest.writeValue(city);
        dest.writeValue(state);
        dest.writeValue(zipcode);
        dest.writeValue(country);
        dest.writeValue(userId);
        dest.writeDouble(oneMonthCost);
        dest.writeDouble(threeMonthsCost);
    }

    public int describeContents() {
        return  0;
    }

}
