package com.hyptastic.poratechai.models.users;

import java.util.List;

/**
 * Created by User on 01-Feb-18.
 */

public class Response {
    private String message;
    private String token;
    private String userId;
    private String path;
    private String fullName;
    private String isTutor;
    private String institution;
    private List<String> paths;
    private String isVerified;
    private String tutorUpdateFlag;
    private int contactNumber;

    public String getMessage() {
        return message;
    }

    public String getToken() {
        return token;
    }

    public String getUserId(){
        return userId;
    }

    public String getProfilePicturePath() {
        return path;
    }

    public String getFullName(){
        return fullName;
    }

    public String getIsTutor() { return isTutor; }

    public String getInstitution() { return institution; }

    public List<String> getCredentialPicturePaths() { return paths; }

    public String getIsVerified() { return isVerified; }

    public String getTutorUpdateFlag() {
        return tutorUpdateFlag;
    }

    public void setTutorUpdateFlag(String tutorUpdateFlag) {
        this.tutorUpdateFlag = tutorUpdateFlag;
    }

    public int getContactNumber() {
        return contactNumber;
    }
}


