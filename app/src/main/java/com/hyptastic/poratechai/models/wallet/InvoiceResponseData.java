package com.hyptastic.poratechai.models.wallet;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Created by ISHRAK on 6/22/2018.
 */

public class InvoiceResponseData implements Parcelable {
    @SerializedName("invoice_id")
    @Expose
    private String invoiceId;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("buyer_email")
    @Expose
    private String buyerEmail;
    @SerializedName("buyer_name")
    @Expose
    private String buyerName;
    @SerializedName("buyer_phone")
    @Expose
    private String buyerPhone;
    @SerializedName("buyer_address")
    @Expose
    private String buyerAddress;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("product_description")
    @Expose
    private String productDescription;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("payer_account")
    @Expose
    private String payerAccount;
    @SerializedName("t_status")
    @Expose
    private String tStatus;
    @SerializedName("payer_name")
    @Expose
    private String payerName;
    @SerializedName("ip_address")
    @Expose
    private String ipAddress;
    @SerializedName("user_agent")
    @Expose
    private String userAgent;
    @SerializedName("reason")
    @Expose
    private String reason;
    @SerializedName("gateway_txn_id")
    @Expose
    private String gatewayTxnId;
    @SerializedName("gateway_name")
    @Expose
    private String gatewayName;
    @SerializedName("gateway_url")
    @Expose
    private String gatewayUrl;
    @SerializedName("gateway_category")
    @Expose
    private String gatewayCategory;
    @SerializedName("gateway_network")
    @Expose
    private String gatewayNetwork;
    @SerializedName("issuer_name")
    @Expose
    private String issuerName;
    @SerializedName("issuer_phone")
    @Expose
    private String issuerPhone;
    @SerializedName("issuer_website")
    @Expose
    private String issuerWebsite;
    @SerializedName("card_brand")
    @Expose
    private String cardBrand;
    @SerializedName("card_type")
    @Expose
    private String cardType;
    @SerializedName("card_category")
    @Expose
    private String cardCategory;
    @SerializedName("issuer_country_iso2")
    @Expose
    private String issuerCountryIso2;
    @SerializedName("issuer_country")
    @Expose
    private String issuerCountry;
    @SerializedName("transactions")
    @Expose
    private String[] transactions = null;
    @SerializedName("emi_used")
    @Expose
    private int emiUsed;
    @SerializedName("emi_tenor")
    @Expose
    private String emiTenor;
    @SerializedName("emi_partner")
    @Expose
    private String emiPartner;
    @SerializedName("emi_form")
    @Expose
    private String emiForm;

    /**
     * No args constructor for use in serialization
     *
     */
    public InvoiceResponseData() {
    }

    /**
     *
     * @param transactions
     * @param productDescription
     * @param userAgent
     * @param reason
     * @param cardCategory
     * @param buyerAddress
     * @param gatewayTxnId
     * @param currency
     * @param amount
     * @param gatewayName
     * @param emiForm
     * @param issuerCountry
     * @param issuerCountryIso2
     * @param emiPartner
     * @param issuerPhone
     * @param emiTenor
     * @param ipAddress
     * @param status
     * @param invoiceId
     * @param payerAccount
     * @param buyerEmail
     * @param tStatus
     * @param cardType
     * @param gatewayCategory
     * @param cardBrand
     * @param buyerName
     * @param gatewayUrl
     * @param buyerPhone
     * @param emiUsed
     * @param gatewayNetwork
     * @param issuerName
     * @param issuerWebsite
     * @param productName
     * @param payerName
     */
    public InvoiceResponseData(String invoiceId, String amount, String buyerEmail, String buyerName, String buyerPhone, String buyerAddress, String productName, String productDescription, String status, String currency, String payerAccount, String tStatus, String payerName, String ipAddress, String userAgent, String reason, String gatewayTxnId, String gatewayName, String gatewayUrl, String gatewayCategory, String gatewayNetwork, String issuerName, String issuerPhone, String issuerWebsite, String cardBrand, String cardType, String cardCategory, String issuerCountryIso2, String issuerCountry, String[] transactions, int emiUsed, String emiTenor, String emiPartner, String emiForm) {
        super();
        this.invoiceId = invoiceId;
        this.amount = amount;
        this.buyerEmail = buyerEmail;
        this.buyerName = buyerName;
        this.buyerPhone = buyerPhone;
        this.buyerAddress = buyerAddress;
        this.productName = productName;
        this.productDescription = productDescription;
        this.status = status;
        this.currency = currency;
        this.payerAccount = payerAccount;
        this.tStatus = tStatus;
        this.payerName = payerName;
        this.ipAddress = ipAddress;
        this.userAgent = userAgent;
        this.reason = reason;
        this.gatewayTxnId = gatewayTxnId;
        this.gatewayName = gatewayName;
        this.gatewayUrl = gatewayUrl;
        this.gatewayCategory = gatewayCategory;
        this.gatewayNetwork = gatewayNetwork;
        this.issuerName = issuerName;
        this.issuerPhone = issuerPhone;
        this.issuerWebsite = issuerWebsite;
        this.cardBrand = cardBrand;
        this.cardType = cardType;
        this.cardCategory = cardCategory;
        this.issuerCountryIso2 = issuerCountryIso2;
        this.issuerCountry = issuerCountry;
        this.transactions = transactions==null?new String[0]:transactions;
        this.emiUsed = emiUsed;
        this.emiTenor = emiTenor;
        this.emiPartner = emiPartner;
        this.emiForm = emiForm;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBuyerEmail() {
        return buyerEmail;
    }

    public void setBuyerEmail(String buyerEmail) {
        this.buyerEmail = buyerEmail;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getBuyerPhone() {
        return buyerPhone;
    }

    public void setBuyerPhone(String buyerPhone) {
        this.buyerPhone = buyerPhone;
    }

    public String getBuyerAddress() {
        return buyerAddress;
    }

    public void setBuyerAddress(String buyerAddress) {
        this.buyerAddress = buyerAddress;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPayerAccount() {
        return payerAccount;
    }

    public void setPayerAccount(String payerAccount) {
        this.payerAccount = payerAccount;
    }

    public String getTStatus() {
        return tStatus;
    }

    public void setTStatus(String tStatus) {
        this.tStatus = tStatus;
    }

    public String getPayerName() {
        return payerName;
    }

    public void setPayerName(String payerName) {
        this.payerName = payerName;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getGatewayTxnId() {
        return gatewayTxnId;
    }

    public void setGatewayTxnId(String gatewayTxnId) {
        this.gatewayTxnId = gatewayTxnId;
    }

    public String getGatewayName() {
        return gatewayName;
    }

    public void setGatewayName(String gatewayName) {
        this.gatewayName = gatewayName;
    }

    public String getGatewayUrl() {
        return gatewayUrl;
    }

    public void setGatewayUrl(String gatewayUrl) {
        this.gatewayUrl = gatewayUrl;
    }

    public String getGatewayCategory() {
        return gatewayCategory;
    }

    public void setGatewayCategory(String gatewayCategory) {
        this.gatewayCategory = gatewayCategory;
    }

    public String getGatewayNetwork() {
        return gatewayNetwork;
    }

    public void setGatewayNetwork(String gatewayNetwork) {
        this.gatewayNetwork = gatewayNetwork;
    }

    public String getIssuerName() {
        return issuerName;
    }

    public void setIssuerName(String issuerName) {
        this.issuerName = issuerName;
    }

    public String getIssuerPhone() {
        return issuerPhone;
    }

    public void setIssuerPhone(String issuerPhone) {
        this.issuerPhone = issuerPhone;
    }

    public String getIssuerWebsite() {
        return issuerWebsite;
    }

    public void setIssuerWebsite(String issuerWebsite) {
        this.issuerWebsite = issuerWebsite;
    }

    public String getCardBrand() {
        return cardBrand;
    }

    public void setCardBrand(String cardBrand) {
        this.cardBrand = cardBrand;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardCategory() {
        return cardCategory;
    }

    public void setCardCategory(String cardCategory) {
        this.cardCategory = cardCategory;
    }

    public String getIssuerCountryIso2() {
        return issuerCountryIso2;
    }

    public void setIssuerCountryIso2(String issuerCountryIso2) {
        this.issuerCountryIso2 = issuerCountryIso2;
    }

    public String getIssuerCountry() {
        return issuerCountry;
    }

    public void setIssuerCountry(String issuerCountry) {
        this.issuerCountry = issuerCountry;
    }

    public String[] getTransactions() {
        return transactions;
    }

    public void setTransactions(String[] transactions) {
        this.transactions = transactions;
    }

    public int getEmiUsed() {
        return emiUsed;
    }

    public void setEmiUsed(int emiUsed) {
        this.emiUsed = emiUsed;
    }

    public String getEmiTenor() {
        return emiTenor;
    }

    public void setEmiTenor(String emiTenor) {
        this.emiTenor = emiTenor;
    }

    public String getEmiPartner() {
        return emiPartner;
    }

    public void setEmiPartner(String emiPartner) {
        this.emiPartner = emiPartner;
    }

    public String getEmiForm() {
        return emiForm;
    }

    public void setEmiForm(String emiForm) {
        this.emiForm = emiForm;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("invoiceId", invoiceId).append("amount", amount).append("buyerEmail", buyerEmail).append("buyerName", buyerName).append("buyerPhone", buyerPhone).append("buyerAddress", buyerAddress).append("productName", productName).append("productDescription", productDescription).append("status", status).append("currency", currency).append("payerAccount", payerAccount).append("tStatus", tStatus).append("payerName", payerName).append("ipAddress", ipAddress).append("userAgent", userAgent).append("reason", reason).append("gatewayTxnId", gatewayTxnId).append("gatewayName", gatewayName).append("gatewayUrl", gatewayUrl).append("gatewayCategory", gatewayCategory).append("gatewayNetwork", gatewayNetwork).append("issuerName", issuerName).append("issuerPhone", issuerPhone).append("issuerWebsite", issuerWebsite).append("cardBrand", cardBrand).append("cardType", cardType).append("cardCategory", cardCategory).append("issuerCountryIso2", issuerCountryIso2).append("issuerCountry", issuerCountry).append("transactions", transactions).append("emiUsed", emiUsed).append("emiTenor", emiTenor).append("emiPartner", emiPartner).append("emiForm", emiForm).toString();
    }

    //Parcelable - start

    protected InvoiceResponseData(Parcel in) {
        this.invoiceId = in.readString();
        this.amount = in.readString();
        this.buyerEmail = in.readString();
        this.buyerName = in.readString();
        this.buyerPhone = in.readString();
        this.buyerAddress = in.readString();
        this.productName = in.readString();
        this.productDescription = in.readString();
        this.status = in.readString();
        this.currency = in.readString();
        this.payerAccount = in.readString();
        this.tStatus = in.readString();
        this.payerName = in.readString();
        this.ipAddress = in.readString();
        this.userAgent = in.readString();
        this.reason = in.readString();
        this.gatewayTxnId = in.readString();
        this.gatewayName = in.readString();
        this.gatewayUrl = in.readString();
        this.gatewayCategory = in.readString();
        this.gatewayNetwork = in.readString();
        this.issuerName = in.readString();
        this.issuerPhone = in.readString();
        this.issuerWebsite = in.readString();
        this.cardBrand = in.readString();
        this.cardType = in.readString();
        this.cardCategory = in.readString();
        this.issuerCountryIso2 = in.readString();
        this.issuerCountry = in.readString();
        transactions = in.createStringArray();
        this.emiUsed = in.readInt();
        this.emiTenor = in.readString();
        this.emiPartner = in.readString();
        this.emiForm = in.readString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(invoiceId);
        dest.writeString(amount);
        dest.writeString(buyerEmail);
        dest.writeString(buyerName);
        dest.writeString(buyerPhone);
        dest.writeString(buyerAddress);
        dest.writeString(productName);
        dest.writeString(productDescription);
        dest.writeString(status);
        dest.writeString(currency);
        dest.writeString(payerAccount);
        dest.writeString(tStatus);
        dest.writeString(payerName);
        dest.writeString(ipAddress);
        dest.writeString(userAgent);
        dest.writeString(reason);
        dest.writeString(gatewayTxnId);
        dest.writeString(gatewayName);
        dest.writeString(gatewayUrl);
        dest.writeString(gatewayCategory);
        dest.writeString(gatewayNetwork);
        dest.writeString(issuerName);
        dest.writeString(issuerPhone);
        dest.writeString(issuerWebsite);
        dest.writeString(cardBrand);
        dest.writeString(cardType);
        dest.writeString(cardCategory);
        dest.writeString(issuerCountryIso2);
        dest.writeString(issuerCountry);
        dest.writeArray(transactions);
        dest.writeInt(emiUsed);
        dest.writeString(emiTenor);
        dest.writeString(emiPartner);
        dest.writeString(emiForm);
    }

    public int describeContents() {
        return  0;
    }

    public static final Parcelable.Creator<InvoiceResponseData> CREATOR = new Parcelable.Creator<InvoiceResponseData>(){
        @Override
        public InvoiceResponseData createFromParcel(Parcel parcel) {
            return new InvoiceResponseData(parcel);
        }

        @Override
        public InvoiceResponseData[] newArray(int i) {
            return new InvoiceResponseData[0];
        }
    };
}
