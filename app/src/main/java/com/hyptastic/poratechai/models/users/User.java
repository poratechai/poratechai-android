package com.hyptastic.poratechai.models.users;

/**
 * Created by User on 31-Jan-18.
 */
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;

public class User implements Parcelable
{
    @SerializedName("_id")
    @Expose
    private String _id;
    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("profilePicture")
    @Expose
    private String profilePicture;
    @SerializedName("institution")
    @Expose
    private String[] institution;
    @SerializedName("isVerified")
    @Expose
    private String isVerified;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("newPassword")
    @Expose
    private String newPassword;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("tutorUpdateFlag")
    @Expose
    private String tutorUpdateFlag;
    @SerializedName("contactNumber")
    @Expose
    private Integer contactNumber;
    @SerializedName("minPreferredSalary")
    @Expose
    private Integer minPreferredSalary;
    @SerializedName("preferredClassToTeach")
    @Expose
    private String[] preferredClassToTeach;
    @SerializedName("preferredSubjects")
    @Expose
    private String[] preferredSubjects;
    @SerializedName("preferredSubcategory")
    @Expose
    private String[] preferredSubcategory;
    @SerializedName("preferredDaysPerWeek")
    @Expose
    private Integer preferredDaysPerWeek;
    @SerializedName("locationLat")
    @Expose
    private Double locationLat;
    @SerializedName("locationLong")
    @Expose
    private Double locationLong;
    @SerializedName("radius")
    @Expose
    private Double radius;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("credentialPictures")
    @Expose
    private String[] verificationImages;

    private final static long serialVersionUID = -5736664147968774081L;

    /**
     * No args constructor for use in serialization
     *
     */
    public User() {
    }

    /**
     *
     * @param contactNumber
     * @param profilePicture
     * @param gender
     * @param fullName
     */
    public User(String fullName, String gender, String profilePicture, int contactNumber) {
        super();
        this.fullName = fullName;
        this.gender = gender;
        this.email = email;
        this.password = password;
        this.profilePicture = profilePicture;
        this.contactNumber = contactNumber;
    }

    public User(String fullName) {
        super();
        this.fullName = fullName;
    }

    public User(String _id, String fullName, String tutorUpdateFlag, int minPreferredSalary, String [] institution, String[] preferredClassToTeach, String[] preferredSubcategory, String[] preferredSubjects, int preferredDaysPerWeek, Double latitude, Double longitude, Double radius, String location, String[] verificationImages)
    {
        super();
        this._id = _id;
        this.fullName = fullName;
        this.tutorUpdateFlag = tutorUpdateFlag;
        //this.profilePicture = profilePicture;
        this.institution = institution;
        this.contactNumber = contactNumber;
        this.minPreferredSalary = minPreferredSalary;
        this.preferredClassToTeach = preferredClassToTeach;
        this.preferredSubcategory = preferredSubcategory;
        this.preferredSubjects = preferredSubjects;
        this.preferredDaysPerWeek = preferredDaysPerWeek;
        this.locationLat = latitude;
        this.locationLong = longitude;
        this.radius = radius;
        this.location = location;
        this.verificationImages = verificationImages;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String[] getInstitution() {
        return institution;
    }

    public void setInstitution(String[] institution) {
        this.institution = institution;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) { this.message = message; }

    public String getTutorUpdateFlag() {
        return tutorUpdateFlag;
    }

    public void setTutorUpdateFlag(String tutorUpdateFlag) { this.tutorUpdateFlag = tutorUpdateFlag; }

    public Integer getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(Integer contactNumber) {
        this.contactNumber = contactNumber;
    }

    public void setPreferredSubjects(String [] preferredSubjects) { this.preferredSubjects = preferredSubjects; }

    public String[] getPreferredSubjects(){ return preferredSubjects; }

    public void setPreferredClassToTeach(String [] preferredClassToTeach) { this.preferredClassToTeach = preferredClassToTeach; }

    public String[] getPreferredClassToTeach(){ return preferredClassToTeach; }

    public void setPreferredSubcategory(String [] preferredSubcategory) { this.preferredSubcategory = preferredSubcategory; }

    public String[] getPreferredSubcategory(){ return preferredSubcategory; }

    public void setMinPreferredSalary (Integer minPreferredSalary) { this.minPreferredSalary = minPreferredSalary; }

    public Integer getMinPreferredSalary() { return minPreferredSalary; }

    public void setPreferredDaysPerWeek (Integer preferredDaysPerWeek) { this.preferredDaysPerWeek = preferredDaysPerWeek; }

    public Integer getPreferredDaysPerWeek() { return preferredDaysPerWeek; }

    public Double getLocationLat() { return locationLat; }

    public Double getLocationLong() { return locationLong; }

    public Double getRadius() { return radius; }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String[] getVerificationImages() {
        return verificationImages;
    }

    public void setVerificationImages(String[] verificationImages) {
        this.verificationImages = verificationImages;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("fullName", fullName).append("gender", gender).append("email", email).append("password", password).append("profilePicture", profilePicture).append("createdAt", createdAt).append("newPassword", newPassword).append("token", token).append("message", message).append("contactNumber", contactNumber).append("minPreferredSalary", minPreferredSalary).append("preferredSubjects", preferredSubjects).append("preferredSubcategory", preferredSubcategory).append("preferredClassToTeach", preferredClassToTeach).append("preferredDaysPerWeek", preferredDaysPerWeek).append("locationLat", locationLat).append("locationLong", locationLong).append("radius", radius).append("verificationImages", verificationImages).toString();
    }

    public User(Parcel parcel)
    {
        _id = parcel.readString();
        fullName = parcel.readString();
        gender = parcel.readString();
        email = parcel.readString();
        password = parcel.readString();
        profilePicture = parcel.readString();
        institution = parcel.createStringArray();
        createdAt = parcel.readString();
        newPassword = parcel.readString();
        token = parcel.readString();
        message = parcel.readString();
        contactNumber = parcel.readInt();
        locationLat = parcel.readDouble();
        locationLong = parcel.readDouble();
        radius = parcel.readDouble();
        location = parcel.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(_id);
        parcel.writeString(fullName);
        parcel.writeString(gender);
        parcel.writeString(email);
        parcel.writeString(password);
        parcel.writeString(profilePicture);
        parcel.writeStringArray(institution);
        parcel.writeString(createdAt);
        parcel.writeString(newPassword);
        parcel.writeString(token);
        parcel.writeString(message);
        parcel.writeInt(contactNumber);
        parcel.writeDouble(locationLat);
        parcel.writeDouble(locationLong);
        parcel.writeDouble(radius);
        parcel.writeString(location);
    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>(){
        @Override
        public User createFromParcel(Parcel parcel) {
            return new User(parcel);
        }

        @Override
        public User[] newArray(int i) {
            return new User[i];
        }
    };
}
