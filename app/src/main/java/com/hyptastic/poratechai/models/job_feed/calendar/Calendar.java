package com.hyptastic.poratechai.models.job_feed.calendar;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Arrays;
import java.util.Date;

/**
 * Created by ISHRAK on 6/20/2019.
 */

public class Calendar implements Parcelable {
    @SerializedName("_id")
    @Expose
    private String _id;
    @SerializedName("jobId")
    @Expose
    private String jobId;
    @SerializedName("dates")
    @Expose
    private Dates[] dates;
    @SerializedName("createdAt")
    @Expose
    private Date createdAt;

    public Calendar(String _id, String jobId, Dates[] dates, Date createdAt) {
        this._id = _id;
        this.jobId = jobId;
        this.dates = dates;
        this.createdAt = createdAt;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public Dates[] getDates() {
        return dates;
    }

    public void setDates(Dates[] dates) {
        this.dates = dates;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "Calendar{" +
                "_id='" + _id + '\'' +
                ", jobId='" + jobId + '\'' +
                ", dates=" + Arrays.toString(dates) +
                ", createdAt=" + createdAt +
                '}';
    }

    //-------------------------------------------------------------------------------------------------------------------------
//Parcelable - start
@Override
public int describeContents() {
    return 0;
}

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this._id);
        dest.writeString(this.jobId);
        dest.writeTypedArray(this.dates, flags);
        dest.writeLong(this.createdAt != null ? this.createdAt.getTime() : -1);
    }

    protected Calendar(Parcel in) {
        this._id = in.readString();
        this.jobId = in.readString();
        this.dates = in.createTypedArray(Dates.CREATOR);
        long tmpCreatedAt = in.readLong();
        this.createdAt = tmpCreatedAt == -1 ? null : new Date(tmpCreatedAt);
    }

    public static final Creator<Calendar> CREATOR = new Creator<Calendar>() {
        @Override
        public Calendar createFromParcel(Parcel source) {
            return new Calendar(source);
        }

        @Override
        public Calendar[] newArray(int size) {
            return new Calendar[size];
        }
    };
//Parcelable - end
//-------------------------------------------------------------------------------------------------------------------------
}
