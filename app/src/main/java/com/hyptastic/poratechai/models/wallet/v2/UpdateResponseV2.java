package com.hyptastic.poratechai.models.wallet.v2;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hyptastic.poratechai.models.wallet.InvoiceResponse;
import com.hyptastic.poratechai.models.wallet.UpdateResponse;

/**
 * Created by ISHRAK on 6/27/2018.
 */

public class UpdateResponseV2 extends UpdateResponse {
    @SerializedName("subscriptionType")
    @Expose
    private int subscriptionType;
    @SerializedName("validityInDays")
    @Expose
    private int validityInDays;
    @SerializedName("pendingInvoice")
    @Expose
    private InvoiceResponse pendingInvoice;

    public UpdateResponseV2(int subscriptionType, int validityInDays, InvoiceResponse pendingInvoice) {
        this.subscriptionType = subscriptionType;
        this.validityInDays = validityInDays;
        this.pendingInvoice = pendingInvoice;
    }

    public int getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(int subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

    public int getValidityInDays() {
        return validityInDays>0?validityInDays:0;
    }

    public void setValidityInDays(int validityInDays) {
        this.validityInDays = validityInDays;
    }

    public InvoiceResponse getPendingInvoice() {
        return pendingInvoice;
    }

    public void setPendingInvoice(InvoiceResponse pendingInvoiceNumber) {
        this.pendingInvoice = pendingInvoiceNumber;
    }

    @Override
    public String toString(){
        return subscriptionType + " " + validityInDays + " " + pendingInvoice.toString();
    }
}
