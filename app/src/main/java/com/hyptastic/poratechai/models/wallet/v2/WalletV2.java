
package com.hyptastic.poratechai.models.wallet.v2;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

public class WalletV2 implements Parcelable
{
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("subscriptionMonths")
    @Expose
    private double subscriptionMonths;
    @SerializedName("transactions")
    @Expose
    private String[] transactions = null;
    @SerializedName("lastPaidDate")
    @Expose
    private String lastPaidDate;

    public final static Parcelable.Creator<WalletV2> CREATOR = new Creator<WalletV2>() {
        @SuppressWarnings({
                "unchecked"
        })
        public WalletV2 createFromParcel(Parcel in) {
            return new WalletV2(in);
        }

        public WalletV2[] newArray(int size) {
            return (new WalletV2[size]);
        }
    };

    protected WalletV2(Parcel in) {
        this.userId = in.readString();
        this.subscriptionMonths = in.readDouble();
        transactions = in.createStringArray();
        this.lastPaidDate = in.readString();
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public WalletV2() {
    }

    /**
     *
     * @param transactions
     * @param lastPaidDate
     * @param userId
     * @param subscriptionMonths
     */
    public WalletV2(String userId, double subscriptionMonths, String[] transactions, String lastPaidDate) {
        super();
        this.userId = userId;
        this.subscriptionMonths = subscriptionMonths;
        this.transactions = transactions;
        this.lastPaidDate = lastPaidDate;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public double getSubscriptionMonths() {
        return subscriptionMonths;
    }

    public void setSubscriptionMonths(double subscriptionMonths) {
        this.subscriptionMonths = subscriptionMonths;
    }

    public String[] getTransactions() {
        return transactions;
    }

    public void setTransactions(String[] transactions) {
        this.transactions = transactions;
    }

    public String getLastPaidDate() {
        return lastPaidDate;
    }

    public void setLastPaidDate(String lastPaidDate) {
        this.lastPaidDate = lastPaidDate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("userId", userId).append("subscriptionMonths", subscriptionMonths).append("transactions", transactions).append("lastPaidDate", lastPaidDate).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userId);
        dest.writeDouble(subscriptionMonths);
        dest.writeStringArray(transactions);
        dest.writeString(lastPaidDate);
    }

    public int describeContents() {
        return  0;
    }

}
