
package com.hyptastic.poratechai.models.job_feed;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang.builder.ToStringBuilder;

public class JobPost implements Parcelable
{
    @SerializedName("jobTitle")
    @Expose
    protected String jobTitle;
    @SerializedName("category")
    @Expose
    protected String category;
    @SerializedName("subcategory")
    @Expose
    protected String subcategory;
    @SerializedName("class")
    @Expose
    protected String classValue;
    @SerializedName("subjects")
    @Expose
    protected String[] subjects;
    @SerializedName("tutorGenderPreference")
    @Expose
    protected String tutorGenderPreference;
    @SerializedName("locationLat")
    @Expose
    protected double locationLat;
    @SerializedName("locationLong")
    @Expose
    protected double locationLong;
    @SerializedName("location")
    @Expose
    protected String location;
    @SerializedName("salary")
    @Expose
    protected int salary;
    @SerializedName("status")
    @Expose
    protected int status;
    @SerializedName("daysPerWeek")
    @Expose
    protected int daysPerWeek;
    @SerializedName("userId")
    @Expose
    protected String userId;
    @SerializedName("createdAt")
    @Expose
    protected String createdAt;
    @SerializedName("updatedAt")
    @Expose
    protected String updatedAt;
    //private final static long serialVersionUID = 4478319051236079936L;

    public JobPost(){}
        
    /**
     * @param jobTitle
     * @param category
     * @param subcategory
     * @param classValue
     * @param subjects
     * @param tutorGenderPreference
     * @param locationLat
     * @param locationLong
     * @param location
     * @param salary
     * @param daysPerWeek
     * @param userId
     */
    
    public JobPost(String jobTitle, String category, String subcategory, String classValue, String[] subjects, String tutorGenderPreference, double locationLat, double locationLong, String location, int salary, int status, int daysPerWeek, String userId) {
        super();
        this.jobTitle = jobTitle;
        this.category = category;
        this.subcategory = subcategory;
        this.classValue = classValue;
        this.subjects = subjects;
        this.tutorGenderPreference = tutorGenderPreference;
        this.locationLat = locationLat;
        this.locationLong = locationLong;
        this.location = location;
        this.salary = salary;
        this.status = status;
        this.daysPerWeek = daysPerWeek;
        this.userId = userId;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public String getClassValue() {
        return classValue;
    }

    public void setClassValue(String classValue) {
        this.classValue = classValue;
    }

    public String[] getSubjects() {
        return subjects;
    }

    public void setSubjects(String[] subjects) {
        this.subjects = subjects;
    }

    public String getTutorGenderPreference() {
        return tutorGenderPreference;
    }

    public void setTutorGenderPreference(String tutorGenderPreference) {
        this.tutorGenderPreference = tutorGenderPreference;
    }

    public double getLocationLat() {
        return locationLat;
    }

    public void setLocationLat(double locationLat) {
        this.locationLat = locationLat;
    }

    public double getLocationLong() {
        return locationLong;
    }

    public void setLocationLong(double locationLong) {
        this.locationLong = locationLong;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getDaysPerWeek() {
        return daysPerWeek;
    }

    public void setDaysPerWeek(int daysPerWeek) {
        this.daysPerWeek = daysPerWeek;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("jobTitle", jobTitle).append("category", category).append("subcategory", subcategory).append("class value", classValue).append("subjects", subjects).append("tutorGenderPreference", tutorGenderPreference).append("locationLat", locationLat).append("locationLong", locationLong).append("salary", salary).append("daysPerWeek", daysPerWeek).append("status", status).append("userId", userId).toString();
    }

    //Parcelable - definitions

    public JobPost(Parcel parcel)
    {
        setJobTitle(parcel.readString());
        setCategory(parcel.readString());
        setSubcategory(parcel.readString());
        setClassValue(parcel.readString());
        setSubjects(parcel.createStringArray());
        setTutorGenderPreference(parcel.readString());
        setLocationLat(parcel.readDouble());
        setLocationLong(parcel.readDouble());
        setLocation(parcel.readString());
        setSalary(parcel.readInt());
        setStatus(parcel.readInt());
        setDaysPerWeek(parcel.readInt());
        setUserId(parcel.readString());
        setCreatedAt(parcel.readString());
        setUpdatedAt(parcel.readString());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(jobTitle);
        parcel.writeString(category);
        parcel.writeString(subcategory);
        parcel.writeString(classValue);
        parcel.writeStringArray(subjects);
        parcel.writeString(tutorGenderPreference);
        parcel.writeDouble(locationLat);
        parcel.writeDouble(locationLong);
        parcel.writeString(location);
        parcel.writeInt(salary);
        parcel.writeInt(status);
        parcel.writeInt(daysPerWeek);
        parcel.writeString(userId);
        parcel.writeString(createdAt);
        parcel.writeString(updatedAt);
    }

    public static final Parcelable.Creator<JobPost> CREATOR = new Parcelable.Creator<JobPost>(){
        @Override
        public JobPost createFromParcel(Parcel parcel) {
            return new JobPost(parcel);
        }

        @Override
        public JobPost[] newArray(int i) {
            return new JobPost[i];
        }
    };
}

