package com.hyptastic.poratechai.models.job_feed;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hyptastic.poratechai.models.review.Review;
import com.hyptastic.poratechai.models.users.User;

import org.apache.commons.lang.builder.ToStringBuilder;

public class JobFeedResponse extends JobPost implements Parcelable
{
    @SerializedName("_id")
    @Expose
    private String _id;
    @SerializedName("tutorId")
    @Expose
    private String tutorId;
    @SerializedName("reviewIdParent")
    @Expose
    private String reviewIdParent;
    @SerializedName("reviewIdTutor")
    @Expose
    private String reviewIdTutor;
    @SerializedName("userProfile")
    @Expose
    private User[] userProfile;
    @SerializedName("tutorProfile")
    @Expose
    private User[] tutorProfile;
    @SerializedName("matchedTutors")
    @Expose
    private List<Match> matchedTutors = null;
    @SerializedName("reviewTutor")
    @Expose
    private Review[] reviewTutor;
    @SerializedName("reviewParent")
    @Expose
    private Review[] reviewParent;
    //private final static long serialVersionUID = 1987692097569524428L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public JobFeedResponse() {
    }

    /**
     *
     * @param id
     * @param tutorId
     * @param reviewIdParent
     * @param reviewIdTutor
     * @param userProfile
     * @param tutorProfile
     * @param matchedTutors
     * @param reviewTutor
     * @param reviewParent
     */
    public JobFeedResponse(String id, String tutorId, String reviewIdParent, String reviewIdTutor, User[] userProfile, User[] tutorProfile, List<Match> matchedTutors, Review[] reviewTutor, Review[] reviewParent) {
        super();
        this._id = id;
        this.tutorId = tutorId;
        this.reviewIdParent = reviewIdParent;
        this.reviewIdTutor = reviewIdTutor;
        this.userProfile = userProfile;
        this.tutorProfile = tutorProfile;
        this.matchedTutors = matchedTutors;
        this.reviewTutor = reviewTutor;
        this.reviewParent = reviewParent;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getTutorId() {
        return tutorId;
    }

    public void setTutorId(String tutorId) {
        this.tutorId = tutorId;
    }

    public String getReviewIdParent() {
        return reviewIdParent;
    }

    public void setReviewIdParent(String reviewIdParent) {
        this.reviewIdParent = reviewIdParent;
    }

    public String getReviewIdTutor() {
        return reviewIdTutor;
    }

    public void setReviewIdTutor(String reviewIdTutor) {
        this.reviewIdTutor = reviewIdTutor;
    }

    public User[] getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(User[] userProfile) {
        this.userProfile = userProfile;
    }

    public User[] getTutorProfile() {
        return tutorProfile;
    }

    public void setTutorProfile(User[] tutorProfile) {
        this.tutorProfile = tutorProfile;
    }

    public List<Match> getMatchedTutors() {
        return matchedTutors;
    }

    public void setMatchedTutors(List<Match> matchedTutors) {
        this.matchedTutors = matchedTutors;
    }

    public Review[] getReviewTutor() {
        return reviewTutor;
    }

    public void setReviewTutor(Review[] reviewTutor) {
        this.reviewTutor = reviewTutor;
    }

    public Review[] getReviewParent() {
        return reviewParent;
    }

    public void setReviewParent(Review[] reviewParent) {
        this.reviewParent = reviewParent;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("_id", _id).append(super.toString()).append("tutorId", tutorId).append("reviewIdParent", reviewIdParent).append("reviewIdTutor", reviewIdTutor).append("userProfile", userProfile).append("tutorProfile", tutorProfile).append("matchedTutors", matchedTutors).append("reviewTutor", reviewTutor).append("reviewParent", reviewParent).toString();
    }

    //Parcelable - definitions

    private JobFeedResponse(Parcel parcel)
    {
        super(parcel);

        set_id(parcel.readString());
        setTutorId(parcel.readString());
        setReviewIdParent(parcel.readString());
        setReviewIdTutor(parcel.readString());
        setUserProfile(parcel.createTypedArray(User.CREATOR));
        setTutorProfile(parcel.createTypedArray(User.CREATOR));
        setMatchedTutors(parcel.createTypedArrayList(Match.CREATOR));
        setReviewTutor(parcel.createTypedArray(Review.CREATOR));
        setReviewParent(parcel.createTypedArray(Review.CREATOR));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);

        parcel.writeString(_id);
        parcel.writeString(tutorId);
        parcel.writeString(reviewIdParent);
        parcel.writeString(reviewIdTutor);
        parcel.writeTypedArray(userProfile, i);
        parcel.writeTypedArray(tutorProfile, i);
        parcel.writeTypedList(matchedTutors);
        parcel.writeTypedArray(reviewTutor, i);
        parcel.writeTypedArray(reviewParent, i);
    }

    public static final Parcelable.Creator<JobFeedResponse> CREATOR = new Parcelable.Creator<JobFeedResponse>(){
        @Override
        public JobFeedResponse createFromParcel(Parcel parcel) {
            return new JobFeedResponse(parcel);
        }

        @Override
        public JobFeedResponse[] newArray(int i) {
            return new JobFeedResponse[i];
        }
    };
}
