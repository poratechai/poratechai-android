package com.hyptastic.poratechai.models.job_feed.calendar;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by ISHRAK on 6/22/2019.
 */

public class Dates implements Parcelable {
    @SerializedName("parentStudentSignature")
    @Expose
    private boolean parentStudentSignature;
    @SerializedName("tutorSignature")
    @Expose
    private boolean tutorSignature;
    @SerializedName("parentStudentNote")
    @Expose
    private String parentStudentNote;
    @SerializedName("tutorNote")
    @Expose
    private String tutorNote;
    @SerializedName("year")
    @Expose
    private int year;
    @SerializedName("month")
    @Expose
    private int month;
    @SerializedName("day")
    @Expose
    private int day;

    public Dates(boolean parentStudentSignature, boolean tutorSignature, String parentStudentNote, String tutorNote, int year, int month, int day) {
        this.parentStudentSignature = parentStudentSignature;
        this.tutorSignature = tutorSignature;
        this.parentStudentNote = parentStudentNote;
        this.tutorNote = tutorNote;
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public boolean isParentStudentSignature() {
        return parentStudentSignature;
    }

    public void setParentStudentSignature(boolean parentStudentSignature) {
        this.parentStudentSignature = parentStudentSignature;
    }

    public boolean isTutorSignature() {
        return tutorSignature;
    }

    public void setTutorSignature(boolean tutorSignature) {
        this.tutorSignature = tutorSignature;
    }

    public String getParentStudentNote() {
        return parentStudentNote;
    }

    public void setParentStudentNote(String parentStudentNote) {
        this.parentStudentNote = parentStudentNote;
    }

    public String getTutorNote() {
        return tutorNote;
    }

    public void setTutorNote(String tutorNote) {
        this.tutorNote = tutorNote;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    @Override
    public String toString() {
        return "Dates{" +
                "parentStudentSignature=" + parentStudentSignature +
                ", tutorSignature=" + tutorSignature +
                ", parentStudentNote='" + parentStudentNote + '\'' +
                ", tutorNote='" + tutorNote + '\'' +
                ", year=" + year +
                ", month=" + month +
                ", day=" + day +
                '}';
    }

//---------------------------------------------------------------------------------------------------------------------------------
// Parcelable - start
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.parentStudentSignature ? (byte) 1 : (byte) 0);
        dest.writeByte(this.tutorSignature ? (byte) 1 : (byte) 0);
        dest.writeString(this.parentStudentNote);
        dest.writeString(this.tutorNote);
        dest.writeInt(this.year);
        dest.writeInt(this.month);
        dest.writeInt(this.day);
    }

    protected Dates(Parcel in) {
        this.parentStudentSignature = in.readByte() != 0;
        this.tutorSignature = in.readByte() != 0;
        this.parentStudentNote = in.readString();
        this.tutorNote = in.readString();
        this.year = in.readInt();
        this.month = in.readInt();
        this.day = in.readInt();
    }

    public static final Creator<Dates> CREATOR = new Creator<Dates>() {
        @Override
        public Dates createFromParcel(Parcel source) {
            return new Dates(source);
        }

        @Override
        public Dates[] newArray(int size) {
            return new Dates[size];
        }
    };
//---------------------------------------------------------------------------------------------------------------------------------
//Parcelable - end
}
