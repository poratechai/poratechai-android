package com.hyptastic.poratechai.models.wallet.v1;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hyptastic.poratechai.models.wallet.WalletConstants;

/**
 * Created by ISHRAK on 7/18/2018.
 */

public class WalletConstantsV1 extends WalletConstants implements Parcelable {
    @SerializedName("UNIT_PRICE")
    @Expose
    private double UNIT_PRICE;
    @SerializedName("UNIT_SALARY_STEP")
    @Expose
    private double UNIT_SALARY_STEP;

    public WalletConstantsV1(double UNIT_PRICE, double UNIT_SALARY_STEP) {
        this.UNIT_PRICE = UNIT_PRICE;
        this.UNIT_SALARY_STEP = UNIT_SALARY_STEP;
    }

    public double getUNIT_PRICE() {
        return UNIT_PRICE;
    }

    public void setUNIT_PRICE(double UNIT_PRICE) {
        this.UNIT_PRICE = UNIT_PRICE;
    }

    public double getUNIT_SALARY_STEP() {
        return UNIT_SALARY_STEP;
    }

    public void setUNIT_SALARY_STEP(double UNIT_SALARY_STEP) {
        this.UNIT_SALARY_STEP = UNIT_SALARY_STEP;
    }

    //Parcelable - start

    public static final Parcelable.Creator<WalletConstantsV1> CREATOR = new Creator<WalletConstantsV1>() {
        @Override
        public WalletConstantsV1 createFromParcel(Parcel parcel) {
            return new WalletConstantsV1(parcel);
        }

        @Override
        public WalletConstantsV1[] newArray(int i) {
            return new WalletConstantsV1[i];
        }
    };

    public WalletConstantsV1(Parcel parcel){
        UNIT_PRICE = parcel.readDouble();
        UNIT_SALARY_STEP = parcel.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeDouble(UNIT_PRICE);
        parcel.writeDouble(UNIT_SALARY_STEP);
    }

    //Parcelable - end
}
