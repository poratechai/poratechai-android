
package com.hyptastic.poratechai.models.job_feed;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hyptastic.poratechai.models.users.User;

import org.apache.commons.lang.builder.ToStringBuilder;

public class Match implements Parcelable
{
    @SerializedName("_id")
    @Expose
    private String _id;
    @SerializedName("tutorId")
    @Expose
    private String tutorId;
    @SerializedName("jobId")
    @Expose
    private String jobId;
    @SerializedName("previousStatus")
    @Expose
    private int previousStatus;
    @SerializedName("currentStatus")
    @Expose
    private int currentStatus;
    @SerializedName("daysPerWeek")
    @Expose
    private int daysPerWeek;
    @SerializedName("salaryOffer")
    @Expose
    private int salaryOffer;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("dateOfStatusChange")
    @Expose
    private String dateOfStatusChange;
    @SerializedName("tutorProfile")
    @Expose
    private User[] tutorProfile;
    @SerializedName("jobData")
    @Expose
    private JobFeedResponse[] jobData;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Match() {
    }

    /**
     *
     * @param _id
     * @param jobId
     * @param tutorId
     * @param previousStatus
     * @param jobData
     * @param tutorProfile
     * @param currentStatus
     * @param dateOfStatusChange
     */
    public Match(String _id, String tutorId, String jobId, int previousStatus, int currentStatus, int daysPerWeek, int salaryOffer, String message, String dateOfStatusChange, User[] tutorProfile, JobFeedResponse[] jobData) {
        super();
        this._id = _id;
        this.tutorId = tutorId;
        this.jobId = jobId;
        this.previousStatus = previousStatus;
        this.currentStatus = currentStatus;
        this.daysPerWeek = daysPerWeek;
        this.salaryOffer = salaryOffer;
        this.message = message;
        this.dateOfStatusChange = dateOfStatusChange;
        this.tutorProfile = tutorProfile;
        this.jobData = jobData;
    }

    public String get_id()
    {
        return _id;
    }

    public void set_id(String _id)
    {
        this._id = _id;
    }

    public String getTutorId() {
        return tutorId;
    }

    public void setTutorId(String tutorId) {
        this.tutorId = tutorId;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public int getPreviousStatus() {
        return previousStatus;
    }

    public void setPreviousStatus(int previousStatus) {
        this.previousStatus = previousStatus;
    }

    public int getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(int currentStatus) {
        this.currentStatus = currentStatus;
    }

    public int getDaysPerWeek() {
        return daysPerWeek;
    }

    public void setDaysPerWeek(int daysPerWeek) {
        this.daysPerWeek = daysPerWeek;
    }

    public int getSalaryOffer() {
        return salaryOffer;
    }

    public void setSalaryOffer(int salaryOffer) {
        this.salaryOffer = salaryOffer;
    }

    public String getTutorMessage() {
        return message;
    }

    public void setTutorMessage(String message) {
        this.message = message;
    }

    public String getDateOfStatusChange() {
        return dateOfStatusChange;
    }

    public void setDateOfStatusChange(String dateOfStatusChange) {
        this.dateOfStatusChange = dateOfStatusChange;
    }

    public User[] getTutorProfile() {
        return tutorProfile;
    }

    public void setTutorProfile(User[] tutorProfile) {
        this.tutorProfile = tutorProfile;
    }

    public JobFeedResponse[] getJobData() {
        return jobData;
    }

    public void setJobData(JobFeedResponse[] jobData) {
        this.jobData = jobData;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("_id", _id).append("tutorId", tutorId).append("jobId", jobId).append("previousStatus", previousStatus).append("currentStatus", currentStatus).append("dateOfStatusChange", dateOfStatusChange).append("tutorProfile", tutorProfile).append("jobData", jobData).toString();
    }

    //Parcelable - definitions

    public Match(Parcel parcel)
    {
        _id                 = parcel.readString();
        tutorId             = parcel.readString();
        jobId               = parcel.readString();
        previousStatus      = parcel.readInt();
        currentStatus       = parcel.readInt();
        daysPerWeek         = parcel.readInt();
        salaryOffer         = parcel.readInt();
        message             = parcel.readString();
        dateOfStatusChange  = parcel.readString();
        tutorProfile        = parcel.createTypedArray(User.CREATOR);
        jobData             = parcel.createTypedArray(JobFeedResponse.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(_id);
        parcel.writeString(tutorId);
        parcel.writeString(jobId);
        parcel.writeInt(previousStatus);
        parcel.writeInt(currentStatus);
        parcel.writeInt(daysPerWeek);
        parcel.writeInt(salaryOffer);
        parcel.writeString(message);
        parcel.writeString(dateOfStatusChange);
        parcel.writeTypedArray(tutorProfile, i);
        parcel.writeTypedArray(jobData, i);
    }

    public static final Parcelable.Creator<Match> CREATOR = new Parcelable.Creator<Match>(){
        @Override
        public Match createFromParcel(Parcel parcel) {
            return new Match(parcel);
        }

        @Override
        public Match[] newArray(int i) {
            return new Match[i];
        }
    };
}
