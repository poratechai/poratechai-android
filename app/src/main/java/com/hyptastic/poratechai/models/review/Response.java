package com.hyptastic.poratechai.models.review;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;

/**
 * Created by ISHRAK on 3/19/2018.
 */

public class Response implements Serializable {
    @SerializedName("Response")
    @Expose
    private String Response;
    @SerializedName("_id")
    @Expose
    private String _id;

    public Response(String response, String _id) {
        Response = response;
        this._id = _id;
    }

    public String getResponse() {
        return Response;
    }

    public void setResponse(String response) {
        Response = response;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this).append("Response", Response).append("_id", _id).toString();
    }
}
