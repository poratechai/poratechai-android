package com.hyptastic.poratechai.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ISHRAK on 7/1/2018.
 */

public class GeneralResponse {
    @SerializedName("message")
    @Expose
    private String message;

    public GeneralResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
