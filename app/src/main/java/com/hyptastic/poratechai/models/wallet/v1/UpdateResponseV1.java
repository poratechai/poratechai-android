package com.hyptastic.poratechai.models.wallet.v1;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hyptastic.poratechai.models.wallet.InvoiceResponse;
import com.hyptastic.poratechai.models.wallet.UpdateResponse;

/**
 * Created by ISHRAK on 6/27/2018.
 */

public class UpdateResponseV1 extends UpdateResponse {
    @SerializedName("balance")
    @Expose
    private double balance;
    @SerializedName("pendingInvoice")
    @Expose
    private InvoiceResponse pendingInvoice;

    public UpdateResponseV1(double balance, InvoiceResponse pendingInvoice) {
        this.balance = balance;
        this.pendingInvoice = pendingInvoice;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public InvoiceResponse getPendingInvoice() {
        return pendingInvoice;
    }

    public void setPendingInvoice(InvoiceResponse pendingInvoiceNumber) {
        this.pendingInvoice = pendingInvoiceNumber;
    }

    @Override
    public String toString(){
        return balance + " " + pendingInvoice.toString();
    }
}
