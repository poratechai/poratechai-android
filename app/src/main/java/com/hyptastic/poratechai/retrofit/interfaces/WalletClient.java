package com.hyptastic.poratechai.retrofit.interfaces;

import com.hyptastic.poratechai.models.wallet.InvoiceRequest;
import com.hyptastic.poratechai.models.wallet.InvoiceResponse;
import com.hyptastic.poratechai.models.wallet.v1.UpdateResponseV1;
import com.hyptastic.poratechai.models.wallet.v2.UpdateResponseV2;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

/**
 * Created by ISHRAK on 5/14/2018.
 */

public interface WalletClient {
    @GET("/wallet/updateWallet")
    Observable<UpdateResponseV1> updateWalletV1( );

    @GET("/wallet/updateWallet")
    Observable<UpdateResponseV2> updateWalletV2( );

    @POST("/wallet/createInvoice")
    Observable<InvoiceResponse> createInvoice( @Body InvoiceRequest invoiceRequest );

    @GET("/wallet/getInvoice")
    Observable<InvoiceResponse> getInvoice( @Query("invoice_id") String invoiceId );

    @DELETE("/wallet/deleteInvoice")
    Observable<Object> deleteInvoice( @Query("invoiceNumber") String invoiceNumber );
}
