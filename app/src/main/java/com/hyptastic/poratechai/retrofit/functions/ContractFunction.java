package com.hyptastic.poratechai.retrofit.functions;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.widget.Toast;

import com.hyptastic.poratechai.activities.profile.ProfileViewActivity;
import com.hyptastic.poratechai.adapters.job_feed.JobFeedAdapter;
import com.hyptastic.poratechai.adapters.job_feed.parent_student.ContractRequestsAdapter;
import com.hyptastic.poratechai.fragments.job_feed.tutor.ContractFragment;
import com.hyptastic.poratechai.models.GeneralResponse;
import com.hyptastic.poratechai.models.job_feed.Contract;
import com.hyptastic.poratechai.models.job_feed.JobFeedResponse;
import com.hyptastic.poratechai.models.job_feed.Match;
import com.hyptastic.poratechai.models.job_feed.Response;
import com.hyptastic.poratechai.retrofit.interfaces.ContractClient;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

/**
 * Created by ISHRAK on 3/18/2018.
 */

public class ContractFunction implements Parcelable {
    public ContractClient contractClient;
    public Context context;

    public ContractFunction(ContractClient contractClient, Context context)
    {
        this.contractClient = contractClient;
        this.context = context;
    }

    public void sendContractReviewRequest(final ContractFragment contractFragment, String matchId, Contract contract)
    {
        final DisposableObserver<GeneralResponse> disposable = contractClient.sendContractReviewRequest(matchId, contract)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<GeneralResponse>(){
                    @Override
                    public void onNext(GeneralResponse s)
                    {
                        Log.e("Send Contract: ", s.toString());
                        contractFragment.dismissDialog();
                        Toast.makeText(context, s.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    @Override
                    public void onError(Throwable e)
                    {
                        if(e instanceof HttpException && ((HttpException)e).code()==403){
                            Toast.makeText(context, "Upload verification images and get verified to send contract request", Toast.LENGTH_LONG).show();
                        }else if(e instanceof HttpException && ((HttpException)e).code()==402){
                            Toast.makeText(context, "Subscribe to send contract request", Toast.LENGTH_LONG).show();
                        }
                        e.printStackTrace();
                    }
                    @Override
                    public void onComplete()
                    {

                    }
                });
    }

    public void acceptContractReviewRequest(String matchId)
    {
        final DisposableObserver<GeneralResponse> disposable = contractClient.acceptContractReviewRequest(matchId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<GeneralResponse>(){
                    @Override
                    public void onNext(GeneralResponse s) {
                        Log.e("Accept Contract: ", s.getMessage());
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void acceptTutor(final ContractRequestsAdapter contractRequestsAdapter, String matchId)
    {
        final DisposableObserver<Response> disposable = contractClient.acceptTutor(matchId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<Response>(){
                    @Override
                    public void onNext(Response s) {
                        Log.e("Accept Tutor: ", s.toString());

                        contractRequestsAdapter.refresh();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void acceptTutor(final ProfileViewActivity profileViewActivity, String matchId)
    {
        final DisposableObserver<Response> disposable = contractClient.acceptTutor(matchId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<Response>(){
                    @Override
                    public void onNext(Response s) {
                        Log.e("Accept Tutor: ", s.toString());

                        profileViewActivity.tutorAccepted(true);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void cancelJobinPhase1(final JobFeedAdapter<Match> jobFeedAdapter, final int position, String matchId)
    {
        final DisposableObserver<Response> disposable = contractClient.cancelJobinPhase1(matchId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<Response>(){
                    @Override
                    public void onNext(Response s) {
                        Log.e("Cancel Phase 1: ", s.toString());
                        jobFeedAdapter.removeItem(position);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    //Called from adapters->job_feed->parent_student->ContractRequestsAdapter
    public void cancelJobinPhase2(final ContractRequestsAdapter contractRequestsAdapter, final int position, String matchId)
    {
        final DisposableObserver<Response> disposable = contractClient.cancelJobinPhase2(matchId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<Response>(){
                    @Override
                    public void onNext(Response s) {
                        Log.e("Cancel Phase 2: ", s.toString());
                        contractRequestsAdapter.removeItemFromList(position);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    //Called from adapters->job_feed->tutor->MatchesAdapter
    public void cancelJobinPhase2(final JobFeedAdapter<Match> jobFeedAdapter, final int position, String matchId)
    {
        final DisposableObserver<Response> disposable = contractClient.cancelJobinPhase2(matchId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<Response>(){
                    @Override
                    public void onNext(Response s) {
                        Log.e("Cancel Phase 2: ", s.toString());
                        jobFeedAdapter.removeItem(position);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    //Called from adapters->job_feed->parent_student->ContractRequestsAdapter
    public void cancelJobinPhase3(final ContractRequestsAdapter contractRequestsAdapter, String matchId)
    {
        final DisposableObserver<Response> disposable = contractClient.cancelJobinPhase3(matchId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<Response>(){
                    @Override
                    public void onNext(Response s) {
                        Log.e("Cancel Phase 3: ", s.toString());

                        contractRequestsAdapter.refresh();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    //Called from adapters->job_feed->tutor->MatchesAdapter
    public void cancelJobinPhase3(final JobFeedAdapter<Match> jobFeedAdapter, final int position, String matchId)
    {
        final DisposableObserver<Response> disposable = contractClient.cancelJobinPhase3(matchId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<Response>(){
                    @Override
                    public void onNext(Response s) {
                        Log.e("Cancel Phase 3: ", s.toString());

                        jobFeedAdapter.removeItem(position);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    //Called from adapters->job_feed->parent_student/tutor->ActiveJobsAdapter
    public void cancelJobinPhase4(final JobFeedAdapter<JobFeedResponse> jobFeedAdapter, String jobId)
    {
        final DisposableObserver<Response> disposable = contractClient.cancelJobinPhase4(jobId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<Response>(){
                    @Override
                    public void onNext(Response s) {
                        Log.e("Cancel Phase 4: ", s.toString());
                        jobFeedAdapter.loadData();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    //Called from adapters->job_feed->tutor->MatchesAdapter
    public void cancelJobinPhase0(final JobFeedAdapter<Match> jobFeedAdapter, final int position, String matchId)
    {
        final DisposableObserver<Response> disposable = contractClient.cancelJobinPhase0(matchId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<Response>(){
                    @Override
                    public void onNext(Response s) {
                        Log.e("Cancel Phase 0: ", s.toString());
                        jobFeedAdapter.removeItem(position);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void parentCancelsJob(String jobId)
    {
        final DisposableObserver<Response> disposable = contractClient.parentCancelsJob(jobId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<Response>(){
                    @Override
                    public void onNext(Response s) {
                        Log.e("Parent Cancels Job: ", s.toString());
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public ContractFunction(Parcel parcel)
    {

    }

    //Parcelable - definitions

    public static final Parcelable.Creator<ContractFunction> CREATOR = new Parcelable.Creator<ContractFunction>(){
        @Override
        public ContractFunction createFromParcel(Parcel parcel) {
            return new ContractFunction(parcel);
        }

        @Override
        public ContractFunction[] newArray(int i) {
            return new ContractFunction[i];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }
}
