package com.hyptastic.poratechai.retrofit.interfaces;

import com.hyptastic.poratechai.models.GeneralResponse;
import com.hyptastic.poratechai.models.wallet.v1.WalletConstantsV1;
import com.hyptastic.poratechai.models.wallet.v2.WalletConstantsV2;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by ISHRAK on 7/1/2018.
 */

public interface SystemClient {
    @GET("/system/systemArrayConstants")
    Observable<GeneralResponse> getSystemArrayConstants();

    @GET("/system/systemWalletConstants")
    Observable<WalletConstantsV1> getSystemWalletConstantsV1();

    @GET("/system/systemWalletConstants")
    Observable<WalletConstantsV2> getSystemWalletConstantsV2();

    @GET("/coupon/getPrice")
    Observable<WalletConstantsV2> getPrice();

    @GET("/system/getPeekResults")
    Observable<GeneralResponse> getPeekResults();

    @GET("/system/supportNumber")
    Observable<String> getSupportNumber();

    @GET("/system/androidVersionCode")
    Observable<String> getVersionCode();

    @GET("/system/checkConnectivity")
    Observable<String> getConnectionStatus();
}
