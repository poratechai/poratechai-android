package com.hyptastic.poratechai.retrofit.functions;

/**
 * Created by User on 31-Jan-18.
 */

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.hyptastic.poratechai.activities.PorateChaiActivity;
import com.hyptastic.poratechai.activities.HireOrTeachActivity;
import com.hyptastic.poratechai.activities.profile.ProfileViewActivity;
import com.hyptastic.poratechai.activities.profile.TutorMapActivity;
import com.hyptastic.poratechai.fragments.dashboard.BlockingFragment;
import com.hyptastic.poratechai.fragments.login_registration.ChangePasswordDialog;
import com.hyptastic.poratechai.fragments.login_registration.ConfirmCodeDialog;
import com.hyptastic.poratechai.fragments.login_registration.LoginFragment;
import com.hyptastic.poratechai.fragments.login_registration.RegisterFragment;
import com.hyptastic.poratechai.fragments.login_registration.ResetPasswordDialog;
import com.hyptastic.poratechai.fragments.profile.tutor.profile.DialogFragmentClasses;
import com.hyptastic.poratechai.fragments.profile.tutor.profile.DialogFragmentDaysPerWeek;
import com.hyptastic.poratechai.fragments.profile.tutor.profile.DialogFragmentInstitutions;
import com.hyptastic.poratechai.fragments.profile.tutor.profile.DialogFragmentSalary;
import com.hyptastic.poratechai.fragments.profile.tutor.profile.DialogFragmentSkills;
import com.hyptastic.poratechai.fragments.profile.tutor.profile.DialogFragmentSubjects;
import com.hyptastic.poratechai.fragments.profile.DialogFragmentVerificationImages;
import com.hyptastic.poratechai.models.GeneralResponse;
import com.hyptastic.poratechai.models.users.ChangePassword;
import com.hyptastic.poratechai.models.users.ImageResponse;
import com.hyptastic.poratechai.models.users.Response;
import com.hyptastic.poratechai.models.users.User;
import com.hyptastic.poratechai.retrofit.interfaces.UserClient;
import com.hyptastic.poratechai.utils.Constants;
import com.hyptastic.poratechai.utils.Functions;

import java.io.Serializable;
import java.util.ArrayList;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.HttpException;

public class UserFunctions implements Serializable {
    UserClient userClient;
    Context context;

    private String userId;

    public UserFunctions(UserClient userClient,Context context){
        this.userClient = userClient;
        this.context = context;
    }

    public void registerUser(final RegisterFragment registerFragment, String fullName, int contactNumber, String gender, String profilePicture){
        try {
            User newUser = new User(fullName, gender, profilePicture, contactNumber);

            userClient.register(newUser)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            CompositeDisposable compositeDisposable =new CompositeDisposable();
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onNext(Response response) {
                            Functions.storeLoginDataToSharedPreferences(null,null,response.getUserId(),null, null, null, null, null, "false");
                            registerFragment.login(response.getContactNumber());
                        }

                        @Override
                        public void onError(Throwable e) {
                            try
                            {
                                Log.e("User Creation Failed ", e.getMessage());
                                if(e.getMessage().equals("HTTP 409 Conflict")){
//                                    Toast.makeText(context,"Contact number already in use",Toast.LENGTH_LONG).show();
                                    registerFragment.login(contactNumber);
                                }
                                else{
                                    Toast.makeText(context,"Network error",Toast.LENGTH_LONG).show();
                                }
                            }
                            catch(Exception exception)
                            {
                                Log.e("User Creation EXCEPTION", exception.getMessage());
                            }
                        }

                        @Override
                        public void onComplete() {
                            Log.v("Register", "Successful");
                            Toast.makeText(context,"Registration Successful",Toast.LENGTH_LONG).show();
                        }
                    });
        }
        catch (Exception e){
            Log.v("Error", e.getMessage());
        }

    }

    public void getVerificationStatus(final com.hyptastic.poratechai.fragments.profile.NavigationFragment navigationFragment, String userId)
    {
        try
        {
            final DisposableObserver<GeneralResponse> disposable = userClient.getVerificationStatus(userId)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<GeneralResponse>()
                    {
                        @Override
                        public void onNext(GeneralResponse response) {
                            navigationFragment.verificationStatusGotten(true, response.getMessage().equals("true"));
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();

                            navigationFragment.verificationStatusGotten(false, false);
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getVerificationStatus(final ProfileViewActivity profileViewActivity, String userId)
    {
        try
        {
            final DisposableObserver<GeneralResponse> disposable = userClient.getVerificationStatus(userId)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<GeneralResponse>()
                    {
                        @Override
                        public void onNext(GeneralResponse response) {
                            profileViewActivity.verificationStatusGotten(true, response.getMessage().equals("true"));
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();

                            profileViewActivity.verificationStatusGotten(false, false);
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getParentStudentProfileForViewSelf(final com.hyptastic.poratechai.fragments.profile.parent_student.profile.ProfileFragment profileFragment){
        try
        {
            final DisposableObserver<User> disposable = userClient.getProfile()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<User>()
                    {
                        @Override
                        public void onNext(User user) {
                            profileFragment.parentStudentProfileGotten(user);
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getParentStudentProfileForViewOthers(final com.hyptastic.poratechai.fragments.profile.parent_student.profile.ProfileFragment profileFragment, String userId){
        try
        {
            final DisposableObserver<User> disposable = userClient.getOthersProfile(userId)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<User>()
                    {
                        @Override
                        public void onNext(User user) {
                            profileFragment.parentStudentProfileGotten(user);
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getTutorProfileForViewSelf(final com.hyptastic.poratechai.fragments.profile.tutor.profile.ProfileFragment profileFragment){
        try
        {
            final DisposableObserver<User> disposable = userClient.getProfile()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<User>()
                    {
                        @Override
                        public void onNext(User user) {
                            profileFragment.tutorProfileGotten(user);
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getTutorProfileForViewOthers(final com.hyptastic.poratechai.fragments.profile.tutor.profile.ProfileFragment profileFragment, String userId){
        try
        {
            final DisposableObserver<User> disposable = userClient.getOthersProfile(userId)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<User>()
                    {
                        @Override
                        public void onNext(User user) {
                            profileFragment.tutorProfileGotten(user);
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void verifyCode(final ConfirmCodeDialog confirmCodeDialog, String code){
        try
        {
            final DisposableObserver<String> disposable = userClient.verifyCode(code)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<String>()
                    {
                        @Override
                        public void onNext(String str) {
                            Toast.makeText(confirmCodeDialog.getActivity(), str, Toast.LENGTH_LONG).show();

                            confirmCodeDialog.verificationSuccess(true);
                        }

                        @Override
                        public void onError(Throwable e) {
                            if(e instanceof HttpException && ((HttpException)e).code()==401){
                                Toast.makeText(confirmCodeDialog.getActivity(), "Please try again!", Toast.LENGTH_LONG).show();

                                confirmCodeDialog.verificationSuccess(false);
                            }
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void resendCode(final ConfirmCodeDialog confirmCodeDialog){
        try
        {
            final DisposableObserver<String> disposable = userClient.resendCode()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<String>()
                    {
                        @Override
                        public void onNext(String str) {
                            Log.v("Response from par edit", str);
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            Toast.makeText(confirmCodeDialog.getActivity(), "Code Resent", Toast.LENGTH_LONG).show();
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void logoutUser(final com.hyptastic.poratechai.fragments.profile.ProfileFragment profileFragment){
        try
        {
            final DisposableObserver<String> disposable = userClient.logout()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<String>()
                    {
                        @Override
                        public void onNext(String str) {
                            profileFragment.logout();
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
//                            Toast.makeText(context, "Verified", Toast.LENGTH_SHORT).show();
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void uploadProfilePicture(final com.hyptastic.poratechai.fragments.profile.NavigationFragment fragment, byte[] profilePicture){
        try
        {
            RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), profilePicture);
            MultipartBody.Part body = MultipartBody.Part.createFormData("image", "image.jpg", requestFile);
            String imageUrl;

            final DisposableObserver<Response> disposable = userClient.updateProfilePicture(body)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<Response>()
                    {
                        @Override
                        public void onNext(Response response) {
                            Functions.storeProfilePictureToSharedPreferences(response.getProfilePicturePath());
                            fragment.profilePictureUploaded(response.getProfilePicturePath());
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            Toast.makeText(context, "Successfully Uploaded Profile Picture", Toast.LENGTH_SHORT).show();
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getProfilePicture(final Constants.PICTURE_PACKET picturePacket, String userId){
        try
        {
            final DisposableObserver<Response> disposable = userClient.getProfilePicture(userId)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<Response>()
                    {
                        @Override
                        public void onNext(Response response) {
                            try{
                                Log.v("Get Prof Pic Resp", response.getProfilePicturePath());
                                picturePacket.path = response.getProfilePicturePath();
                                Functions.setProfilePicture(picturePacket);
                            }catch(Exception e){
                                Log.e("Profile Picture", e.toString());
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void changePassword(ChangePasswordDialog changePasswordDialog, ChangePassword changePassword){
        try
        {
            final DisposableObserver<Response> disposable = userClient.changePassword(changePassword)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<Response>()
                    {
                        @Override
                        public void onNext(Response response) {
                            Log.v("Pass resp", response.getMessage());
                        }

                        @Override
                        public void onError(Throwable e)
                        {
                            e.printStackTrace();
                            Toast.makeText(context, "Invalid old password", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onComplete() {
                            Toast.makeText(context, "Successfully Changed password", Toast.LENGTH_SHORT).show();
                            changePasswordDialog.closeDialog();
                        }
                    });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    //Called from HireOrTeachActivity
    public void setTutorOrParent(final HireOrTeachActivity hireOrTeachActivity, String flagValue)
    {
        try
        {
            final DisposableObserver<GeneralResponse> disposable = userClient.setTutorOrParent(flagValue)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<GeneralResponse>()
                    {
                        @Override
                        public void onNext(GeneralResponse response) {
                            Functions.setTutorUpdated(response.getMessage());

                            if(flagValue.equals("true")){
                                hireOrTeachActivity.teach();
                            }
                            else{
                                hireOrTeachActivity.hire();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
//                            Toast.makeText(context, "Verified", Toast.LENGTH_SHORT).show();
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    //Called from Switch Change
    public void setTutorOrParent(final PorateChaiActivity porateChaiActivity, String flagValue){
        Functions.showBlockingFragment(porateChaiActivity, true);
        try
        {
            final DisposableObserver<GeneralResponse> disposable = userClient.setTutorOrParent(flagValue)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<GeneralResponse>()
                    {
                        @Override
                        public void onNext(GeneralResponse response) {
                            Toast.makeText(context, "Switched to " + (flagValue.equals("true")?"Tutor":"Parent/Student"), Toast.LENGTH_LONG).show();

                            Functions.setTutorUpdated(response.getMessage());

                            porateChaiActivity.onTutorParentSwitchedOverNetwork((flagValue.equals("true")?true:false), true);
                        }

                        @Override
                        public void onError(Throwable e) {
                            porateChaiActivity.onTutorParentSwitchedOverNetwork((flagValue.equals("true")?true:false), false);

                            Toast.makeText(context, "Please check internet connection", Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            Functions.showBlockingFragment(porateChaiActivity, false);
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void uploadCredentialPicture(final DialogFragmentVerificationImages dialogFragmentVerificationImages, byte[] credentialPicture){
        try
        {
            RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), credentialPicture);
            MultipartBody.Part body = MultipartBody.Part.createFormData("image", "image.jpg", requestFile);
            String imageUrl;

            final DisposableObserver<ImageResponse> disposable = userClient.updateCredentialPicture(body)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<ImageResponse>()
                    {
                        @Override
                        public void onNext(ImageResponse imageResponse) {
                            Toast.makeText(context, imageResponse.getMessage(), Toast.LENGTH_SHORT).show();

                            dialogFragmentVerificationImages.imageUploaded(imageResponse.getUrl(), true);
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();

                            dialogFragmentVerificationImages.imageUploaded(null, false);
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void deleteCredentialPicture(final DialogFragmentVerificationImages dialogFragmentVerificationImages, String url){
        try
        {
            final DisposableObserver<ImageResponse> disposable = userClient.deleteCredentialPicture(url)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<ImageResponse>()
                    {
                        @Override
                        public void onNext(ImageResponse imageResponse) {
                            Toast.makeText(context, imageResponse.getMessage(), Toast.LENGTH_SHORT).show();

                            dialogFragmentVerificationImages.imageDeletedSuccessfully(imageResponse.getUrl());
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getCredentialPictures(final com.hyptastic.poratechai.fragments.profile.ProfileFragment profileFragment){
        try
        {
            final DisposableObserver<Response> disposable = userClient.getCredentialPictures()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<Response>()
                    {
                        @Override
                        public void onNext(Response response) {
                            try{
                                profileFragment.setVerificationImagesGallery(response.getCredentialPicturePaths());
                            }catch(Exception e){
                                Log.e("Profile Picture", e.toString());
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            Toast.makeText(profileFragment.getActivity(), "Could not fetch verification images", Toast.LENGTH_SHORT).show();

                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void resetPassword(ResetPasswordDialog resetPasswordDialog, User user){
        try
        {
            final DisposableObserver<Response> disposable = userClient.resetPassword(user)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<Response>()
                    {
                        @Override
                        public void onNext(Response response) {
                            Toast.makeText(context, response.getMessage().toString(), Toast.LENGTH_SHORT).show();
                            Log.v("Pass resp", response.getMessage());
                        }

                        @Override
                        public void onError(Throwable e)
                        {
                            e.printStackTrace();
                            resetPasswordDialog.resetPasswordError();
                        }

                        @Override
                        public void onComplete() {
                            resetPasswordDialog.closeDialog();
                        }
                    });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void updateInstitutions(DialogFragmentInstitutions dialogFragmentInstitutions, String school, String college, String university){
        try
        {
            final DisposableObserver<GeneralResponse> disposable = userClient.updateInstitutions(school, college, university)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<GeneralResponse>()
                    {
                        @Override
                        public void onNext(GeneralResponse response) {
                            System.out.println("Response received");
                            Toast.makeText(dialogFragmentInstitutions.getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();

                            dialogFragmentInstitutions.institutionsUpdatedOverNetwork();
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void updateSkills(DialogFragmentSkills dialogFragmentSkills, ArrayList<String> selectedSkills){
        try
        {
            final DisposableObserver<GeneralResponse> disposable = userClient.updateSkills(selectedSkills)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<GeneralResponse>()
                    {
                        @Override
                        public void onNext(GeneralResponse response) {
                            System.out.println("Response received");
                            Toast.makeText(dialogFragmentSkills.getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();

                            dialogFragmentSkills.skillsUpdated(true);
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();

                            dialogFragmentSkills.skillsUpdated(false);
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void updateClasses(DialogFragmentClasses dialogFragmentClasses, ArrayList<String> selectedClasses){
        try
        {
            final DisposableObserver<GeneralResponse> disposable = userClient.updateClasses(selectedClasses)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<GeneralResponse>()
                    {
                        @Override
                        public void onNext(GeneralResponse response) {
                            System.out.println("Response received");
                            Toast.makeText(dialogFragmentClasses.getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();

                            dialogFragmentClasses.classesUpdated(true);
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();

                            dialogFragmentClasses.classesUpdated(false);
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void updateSubjects(DialogFragmentSubjects dialogFragmentSubjects, ArrayList<String> selectedSubjects){
        try
        {
            final DisposableObserver<GeneralResponse> disposable = userClient.updateSubjects(selectedSubjects)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<GeneralResponse>()
                    {
                        @Override
                        public void onNext(GeneralResponse response) {
                            System.out.println("Response received");
                            Toast.makeText(dialogFragmentSubjects.getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();

                            dialogFragmentSubjects.subjectsUpdated(true);
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();

                            dialogFragmentSubjects.subjectsUpdated(false);
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void updateLocation(TutorMapActivity tutorMapActivity, double locationLat, double locationLong, double radius, String address){
        try
        {
            final DisposableObserver<GeneralResponse> disposable = userClient.updateLocation(locationLat, locationLong, radius, address)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<GeneralResponse>()
                    {
                        @Override
                        public void onNext(GeneralResponse response) {
                            System.out.println("Response received");
                            Toast.makeText(tutorMapActivity, response.getMessage(), Toast.LENGTH_SHORT).show();

                            tutorMapActivity.locationUpdatedOverNetwork(true);
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();

                            tutorMapActivity.locationUpdatedOverNetwork(false);
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void updateDaysPerWeek(DialogFragmentDaysPerWeek dialogFragmentDaysPerWeek, int daysPerWeek){
        try
        {
            final DisposableObserver<GeneralResponse> disposable = userClient.updateDaysPerWeek(daysPerWeek)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<GeneralResponse>()
                    {
                        @Override
                        public void onNext(GeneralResponse response) {
                            System.out.println("Response received");
                            Toast.makeText(dialogFragmentDaysPerWeek.getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();

                            dialogFragmentDaysPerWeek.daysPerWeekUpdatedOverNetwork(true);
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();

                            dialogFragmentDaysPerWeek.daysPerWeekUpdatedOverNetwork(false);
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void updateSalary(DialogFragmentSalary dialogFragmentSalary, int salary){
        try
        {
            final DisposableObserver<GeneralResponse> disposable = userClient.updateSalary(salary)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<GeneralResponse>()
                    {
                        @Override
                        public void onNext(GeneralResponse response) {
                            System.out.println("Response received");
                            Toast.makeText(dialogFragmentSalary.getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();

                            dialogFragmentSalary.salaryUpdatedOverNetwork(true);
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();

                            dialogFragmentSalary.salaryUpdatedOverNetwork(false);
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void userExists(LoginFragment loginFragment, int contactNumber){
        try
        {
            final DisposableObserver<GeneralResponse> disposable = userClient.userExists(contactNumber)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<GeneralResponse>()
                    {
                        @Override
                        public void onNext(GeneralResponse response) {
                            System.out.println("Response received");
                            if(response.getMessage().equals("True")){
                                loginFragment.showConfirmCodeDialog(contactNumber);
                            }
                            else {
                                loginFragment.stopLoader(response.getMessage());
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();

                            Toast.makeText(loginFragment.getActivity(), "Error! Try again", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
