package com.hyptastic.poratechai.retrofit.interfaces;

import com.hyptastic.poratechai.models.users.ParentStat;
import com.hyptastic.poratechai.models.users.TutorStat;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by root on 5/11/18.
 */

public interface StatClient {
    @GET("/stats/tutor")
    Observable<TutorStat> getTutorStats();

    @GET("/stats/parent")
    Observable<ParentStat> getParentStats();
}
