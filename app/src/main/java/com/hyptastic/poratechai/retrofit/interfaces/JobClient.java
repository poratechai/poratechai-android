package com.hyptastic.poratechai.retrofit.interfaces;

import com.hyptastic.poratechai.models.job_feed.JobFeedResponse;
import com.hyptastic.poratechai.models.job_feed.JobPost;
import com.hyptastic.poratechai.models.job_feed.Match;

import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import io.reactivex.Observable;

/**
 * Created by ISHRAK on 12/5/2017.
 */

//JobPost Feed API calls
public interface JobClient {
    @POST("/feed/job/")
    Observable<String> postJob( @Body JobPost jobPostDetails );

    @PUT("/feed/job/")
    Observable<String> updateJob( @Query("jobId") String jobId, @Body JobPost updatedJobPost );

    @DELETE("/feed/job/")
    Observable<String> deleteJob( @Query("jobId") String jobId );

    @GET("/feed/job/singlePostedJob/")
    Observable<JobFeedResponse> getSinglePostedJob( @Query("jobId") String jobId );

    @GET("/feed/job/postedJobs/")
    Observable<JobFeedResponse[]> getPostedJobs();

    @GET("/feed/job/singleActivePostedJob/")
    Observable<JobFeedResponse> getSingleActivePostedJob( @Query("jobId") String jobId );

    @GET("/feed/job/activePostedJobs/")
    Observable<JobFeedResponse[]> getActivePostedJobs();

    @GET("/feed/job/singlePostedJobHistory/")
    Observable<JobFeedResponse> getSinglePostedJobHistory( @Query("jobId") String jobId );

    @GET("/feed/job/postedJobHistory/")
    Observable<JobFeedResponse[]> getPostedJobHistory();

    @GET("/feed/job/singleMatchedJob/")
    Observable<Match> getSingleMatchedJob( @Query("matchId") String matchId );

    @GET("/feed/job/matchedJobs/")
    Observable<Match[]> getMatchedJobs();

    @GET("/feed/job/singleAppliedJob")
    Observable<Match> getSingleAppliedJob( @Query("matchId") String matchId );

    @GET("/feed/job/appliedJobs")
    Observable<Match[]> getAppliedJobs();

    @GET("/feed/job/singleActiveJob/")
    Observable<JobFeedResponse> getSingleActiveJob( @Query("jobId") String jobId );

    @GET("/feed/job/activeJobs/")
    Observable<JobFeedResponse[]> getActiveJobs();

    @GET("/feed/job/singleJobHistory/")
    Observable<JobFeedResponse> getSingleJobHistory( @Query("jobId") String jobId );

    @GET("/feed/job/jobHistory/")
    Observable<JobFeedResponse[]> getJobHistory();
}
