package com.hyptastic.poratechai.retrofit.interfaces;

/**
 * Created by User on 30-Jan-18.
 */

import com.hyptastic.poratechai.models.GeneralResponse;
import com.hyptastic.poratechai.models.users.ChangePassword;
import com.hyptastic.poratechai.models.users.ImageResponse;
import com.hyptastic.poratechai.models.users.Response;
import com.hyptastic.poratechai.models.users.User;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;

import io.reactivex.Observable;
import retrofit2.http.Query;

public interface UserClient {
    @POST("/authentication/register")
    Observable<Response> register( @Body User user );

    @GET("/authentication/userExists")
    Observable<GeneralResponse> userExists( @Query("contactNumber") Integer contactNumber);

    @POST("/authentication/login")
    Observable<Response> login();

    @PUT("/authentication/verifyCode")
    Observable<String> verifyCode( @Query("code") String code );

    @POST("/authentication/resendCode")
    Observable<String> resendCode( );

    @PUT("/authentication/logout")
    Observable<String> logout( );

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    @PUT("/profile/setTutorOrParent")
    Observable<GeneralResponse> setTutorOrParent( @Query("flagValue") String flagValue );

    @GET("/profile/isVerifiedByVerificationImages")
    Observable<GeneralResponse> getVerificationStatus( @Query("userId") String userId );

    @GET("/profile/")
    Observable<User> getProfile();

    @GET("/profile/others")
    Observable<User> getOthersProfile( @Query("userId") String userId );

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    @PUT("/profile/password/changePassword")
    Observable<Response> changePassword( @Body ChangePassword changePassword );

    @POST("/profile/password/resetPassword")
    Observable<Response> resetPassword( @Body User user );

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    @PUT("/profile/updateProfile")
    Observable<String> updateProfile( @Body User user );

    @Multipart
    @PUT("/profile/picture/uploadProfilePicture")
    Observable<Response> updateProfilePicture( @Part MultipartBody.Part image);

    @Multipart
    @PUT("/profile/picture/uploadCredentialPicture")
    Observable<ImageResponse> updateCredentialPicture( @Part MultipartBody.Part image);

    @DELETE("/profile/picture/deleteCredentialPicture")
    Observable<ImageResponse> deleteCredentialPicture( @Query("path") String path );

    @GET("/profile/picture/getProfilePicture")
    Observable<Response> getProfilePicture( @Query("userId") String userId );

    @GET("/profile/picture/getCredentialPictures")
    Observable<Response> getCredentialPictures( );

    @FormUrlEncoded
    @PUT("/profile/tutor/updateInstitutions")
    Observable<GeneralResponse> updateInstitutions( @Field("school") String school,
                                                    @Field("college") String college, @Field("university") String university );

    @FormUrlEncoded
    @PUT("/profile/tutor/updateSkills")
    Observable<GeneralResponse> updateSkills( @Field("skills[]") ArrayList<String> selectedSkills );

    @FormUrlEncoded
    @PUT("/profile/tutor/updateClasses")
    Observable<GeneralResponse> updateClasses( @Field("classes[]") ArrayList<String> selectedClasses );

    @FormUrlEncoded
    @PUT("/profile/tutor/updateSubjects")
    Observable<GeneralResponse> updateSubjects( @Field("subjects[]") ArrayList<String> selectedSubjects );

    @FormUrlEncoded
    @PUT("/profile/tutor/updateLocation")
    Observable<GeneralResponse> updateLocation( @Field("locationLat") double locationLat,
                                                @Field("locationLong") double locationLong, @Field("radius") double radius,
                                                @Field("location") String location );

    @FormUrlEncoded
    @PUT("/profile/tutor/updateDaysPerWeek")
    Observable<GeneralResponse> updateDaysPerWeek( @Field("daysPerWeek") int daysPerWeek );

    @FormUrlEncoded
    @PUT("/profile/tutor/updateSalary")
    Observable<GeneralResponse> updateSalary( @Field("salary") int salary );
}
