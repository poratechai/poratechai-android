package com.hyptastic.poratechai.retrofit.functions;

import android.content.Context;
import android.widget.Toast;

import com.hyptastic.poratechai.adapters.profile.ReviewsAdapter;
import com.hyptastic.poratechai.fragments.job_feed.DialogFragmentReviews;
import com.hyptastic.poratechai.models.review.Response;
import com.hyptastic.poratechai.models.review.Review;
import com.hyptastic.poratechai.retrofit.interfaces.ReviewClient;

import java.util.ArrayList;
import java.util.Arrays;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by ISHRAK on 4/8/2018.
 */

public class ReviewFunction {
    private ReviewClient reviewClient = null;
    private Context context;

    public ReviewFunction(ReviewClient reviewClient, Context context)
    {
        this.reviewClient = reviewClient;
        this.context = context;
    }

    public void getReviewsForParent(final ReviewsAdapter reviewsAdapter, String userId){
        try
        {
            final DisposableObserver<Review[]> disposable = reviewClient.getReviewsForParent(userId)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<Review[]>() {
                        @Override
                        public void onNext(Review[] reviews) {
                            reviewsAdapter.setReviewList(new ArrayList(Arrays.asList(reviews)));
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getReviewsForTutor(final ReviewsAdapter reviewsAdapter, String userId){
        try
        {
            final DisposableObserver<Review[]> disposable = reviewClient.getReviewsForTutor(userId)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<Review[]>() {
                        @Override
                        public void onNext(Review[] reviews) {
                            reviewsAdapter.setReviewList(new ArrayList(Arrays.asList(reviews)));
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void reviewByParent(final DialogFragmentReviews dialogFragmentReviews, final String jobId, Review reviewDetails)
    {
        try
        {
            final DisposableObserver<Response> disposable = reviewClient.reviewByParent(jobId, reviewDetails)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<Response>() {
                        @Override
                        public void onNext(Response response) {
                            Toast.makeText(context, "Review submitted successfully", Toast.LENGTH_LONG).show();
                            dialogFragmentReviews.reviewSubmittedSuccessfullyOverNetwork();
                        }

                        @Override
                        public void onError(Throwable e) {
                            Toast.makeText(context, "Could not post review", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Review posting complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void reviewByTutor(final DialogFragmentReviews dialogFragmentReviews, final String jobId, Review reviewDetails)
    {
        try
        {
            final DisposableObserver<Response> disposable = reviewClient.reviewByTutor(jobId, reviewDetails)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<Response>() {
                        @Override
                        public void onNext(Response response) {
                            Toast.makeText(context, "Review submitted successfully", Toast.LENGTH_LONG).show();
                            dialogFragmentReviews.reviewSubmittedSuccessfullyOverNetwork();
                        }

                        @Override
                        public void onError(Throwable e) {
                            Toast.makeText(context, "Could not post review", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Review posting complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void updateReviewByParent(final DialogFragmentReviews dialogFragmentReviews, final String reviewId, Review reviewDetails)
    {
        try
        {
            final DisposableObserver<Response> disposable = reviewClient.updateReviewByParent(reviewId, reviewDetails)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<Response>() {
                        @Override
                        public void onNext(Response response) {
                            Toast.makeText(context, "Review submitted successfully", Toast.LENGTH_LONG).show();
                            dialogFragmentReviews.reviewSubmittedSuccessfullyOverNetwork();
                        }

                        @Override
                        public void onError(Throwable e) {
                            Toast.makeText(context, "Could not submit review", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Review editing complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void updateReviewByTutor(final DialogFragmentReviews dialogFragmentReviews, final String reviewId, Review reviewDetails)
    {
        try
        {
            final DisposableObserver<Response> disposable = reviewClient.updateReviewByTutor(reviewId, reviewDetails)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<Response>() {
                        @Override
                        public void onNext(Response response) {
                            Toast.makeText(context, "Review submitted successfully", Toast.LENGTH_LONG).show();
                            dialogFragmentReviews.reviewSubmittedSuccessfullyOverNetwork();
                        }

                        @Override
                        public void onError(Throwable e) {
                            Toast.makeText(context, "Could not submit review", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Review editing complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
