package com.hyptastic.poratechai.retrofit.functions;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.hyptastic.poratechai.fragments.job_feed.tutor.ContractFragment;
import com.hyptastic.poratechai.fragments.payment.PaymentInterface;
import com.hyptastic.poratechai.fragments.payment.tutor.InvoiceFragment;
import com.hyptastic.poratechai.fragments.payment.tutor.PaymentProcessingFragment;
import com.hyptastic.poratechai.models.wallet.InvoiceRequest;
import com.hyptastic.poratechai.models.wallet.InvoiceResponse;
import com.hyptastic.poratechai.models.wallet.v1.UpdateResponseV1;
import com.hyptastic.poratechai.models.wallet.v2.UpdateResponseV2;
import com.hyptastic.poratechai.retrofit.interfaces.WalletClient;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by ISHRAK on 5/14/2018.
 */

public class WalletFunction {
    private WalletClient walletClient = null;
    private Context context;

    public WalletFunction(WalletClient walletClient, Context context) {
        this.walletClient = walletClient;
        this.context = context;
    }

    //After successful network response, createInvoice method calls loadInvoice with reference to paymentInterface
    public void createInvoice(final PaymentInterface paymentInterface, InvoiceRequest invoiceRequest) {
        final DisposableObserver<InvoiceResponse> disposable = walletClient.createInvoice(invoiceRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<InvoiceResponse>() {
                    @Override
                    public void onNext(InvoiceResponse invoiceResponse) {
                        Log.e("Invoice Response", invoiceResponse.toString());
                        paymentInterface.loadInvoice(invoiceResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("Create Invoice", "Error");
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        System.out.println("Complete");
                    }
                });
    }

    //After successful network response, getInvoice method calls loadInvoice with reference to paymentInterface
    public void getInvoice(final PaymentInterface paymentInterface, String invoiceNumber) {
        final DisposableObserver<InvoiceResponse> disposable = walletClient.getInvoice(invoiceNumber)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<InvoiceResponse>() {
                    @Override
                    public void onNext(InvoiceResponse invoiceResponse) {
                        Log.e("Invoice Response", invoiceResponse.toString());
                        paymentInterface.loadInvoice(invoiceResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("Get Invoice", "Error");
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        System.out.println("Complete");
                    }
                });
    }

    //After successful network response, updateWallet method calls refreshBalance with reference to paymentInterface
    //Token - Payment - V1
    public void updateWalletV1(final PaymentInterface paymentInterface) {
        final DisposableObserver<UpdateResponseV1> disposable = walletClient.updateWalletV1()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<UpdateResponseV1>() {
                    @Override
                    public void onNext(UpdateResponseV1 updateResponse) {
                        paymentInterface.refreshBalance(updateResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        System.out.println("Complete");
                    }
                });
    }

    //After successful network response, updateWallet method calls refreshBalance with reference to paymentInterface
    //Subscription - Payment - V2
    public void updateWalletV2(final PaymentInterface paymentInterface) {
        final DisposableObserver<UpdateResponseV2> disposable = walletClient.updateWalletV2()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<UpdateResponseV2>() {
                    @Override
                    public void onNext(UpdateResponseV2 updateResponse) {
                        paymentInterface.refreshBalance(updateResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        System.out.println("Complete");
                    }
                });
    }

    //Token - Payment - V1
//    public void updateWalletV1(final ContractFragment contractFragment) {
//        final DisposableObserver<UpdateResponseV1> disposable = walletClient.updateWalletV1()
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribeOn(Schedulers.io())
//                .subscribeWith(new DisposableObserver<UpdateResponseV1>() {
//                    @Override
//                    public void onNext(UpdateResponseV1 updateResponse) {
//                        contractFragment.balanceUpdated(updateResponse);
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        e.printStackTrace();
//                    }
//
//                    @Override
//                    public void onComplete() {
//                        System.out.println("Complete");
//                    }
//                });
//    }

    //Checking subscription status during sending contract
    //After successful network response, updateWallet method calls balanceUpdated with reference to contractFragment
    //Subscription - Payment - V2
    public void updateWalletV2(final ContractFragment contractFragment) {
        final DisposableObserver<UpdateResponseV2> disposable = walletClient.updateWalletV2()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<UpdateResponseV2>() {
                    @Override
                    public void onNext(UpdateResponseV2 updateResponse) {
                        contractFragment.balanceUpdated(updateResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        System.out.println("Complete");
                    }
                });
    }

    public void deleteInvoice(final InvoiceFragment invoiceFragment, String invoiceNumber){
        final DisposableObserver<Object> disposable = walletClient.deleteInvoice(invoiceNumber)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<Object>() {
                    @Override
                    public void onNext(Object object) {
                        Toast.makeText(invoiceFragment.getActivity(), "Deleted invoice successfully", Toast.LENGTH_LONG).show();
                        invoiceFragment.invoiceDeleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        System.out.println("Complete");
                    }
                });
    }

    public void deleteInvoice(final PaymentProcessingFragment paymentProcessingFragment, String invoiceNumber){
        final DisposableObserver<Object> disposable = walletClient.deleteInvoice(invoiceNumber)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<Object>() {
                    @Override
                    public void onNext(Object object) {
                        Toast.makeText(paymentProcessingFragment.getActivity(), "Deleted invoice successfully", Toast.LENGTH_LONG).show();
                        paymentProcessingFragment.invoiceDeleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        System.out.println("Complete");
                    }
                });
    }
}
