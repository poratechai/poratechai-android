package com.hyptastic.poratechai.retrofit.functions;

import android.content.Context;
import android.util.Log;

import com.hyptastic.poratechai.models.users.ParentStat;
import com.hyptastic.poratechai.models.users.TutorStat;
import com.hyptastic.poratechai.retrofit.interfaces.StatClient;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by root on 5/11/18.
 */

public class StatsFunction {
    private StatClient statClient = null;
    private Context context;

    public StatsFunction(StatClient statClient, Context context){
        this.statClient = statClient;
        this.context = context;
    }
}
