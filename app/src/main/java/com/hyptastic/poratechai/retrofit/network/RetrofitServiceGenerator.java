package com.hyptastic.poratechai.retrofit.network;

/**
 * Created by User on 05-Dec-17.
 */

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.hyptastic.poratechai.retrofit.interfaces.UserClient;
import com.hyptastic.poratechai.utils.Constants;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import retrofit2.converter.gson.GsonConverterFactory;
import io.reactivex.schedulers.Schedulers;

public class RetrofitServiceGenerator {

    private static RxJava2CallAdapterFactory rxAdapter = RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io());

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    //Only used in createService method
    private static OkHttpClient okHttpClient = null;

    public static UserClient getRetrofit(){
        httpClient.addInterceptor(new Interceptor(){
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder builder = original.newBuilder()
                        .header("user-id", Constants.USER_ID_VALUE)
                        .method(original.method(),original.body());
                return  chain.proceed(builder.build());
            }
        });

        return new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .client(httpClient.build())
                .addCallAdapterFactory(rxAdapter)
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(UserClient.class);
    }

    public static UserClient getRetrofitForAuthentication(int contactNumber) {

//        final String basic = Credentials.basic(email,password);
        final String deviceIdToken = FirebaseInstanceId.getInstance().getToken();

        Log.e("DeviceIdToken", deviceIdToken);

        httpClient.addInterceptor(new Interceptor(){
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder builder = original.newBuilder()
                        .header("contact-number", String.valueOf(contactNumber))
                        .header("device-id", deviceIdToken)
                        .method(original.method(),original.body());
                return  chain.proceed(builder.build());
            }
        });

        return new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .client(httpClient.build())
                .addCallAdapterFactory(rxAdapter)
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(UserClient.class);
    }

    public static <S> S createService(final String email, final String token, Class<S> serviceClass) {

        httpClient.addInterceptor(new Interceptor(){
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {

                Log.e("Create Service - Token", token);

                Request original = chain.request();
                Request.Builder builder = original.newBuilder()
                        .header("x-access-token", token)
                        .header("email-address", email)
                        .header("user-id", Constants.USER_ID_VALUE)
                        .method(original.method(), original.body());
                return chain.proceed(builder.build());
            }
        });

        okHttpClient = httpClient.readTimeout(15, TimeUnit.SECONDS).build();

        return new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .client(okHttpClient)
                .addCallAdapterFactory(rxAdapter)
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(serviceClass);
    }

    public static void cancelRequests(){
        if(okHttpClient!=null){
            okHttpClient.dispatcher().cancelAll();
        }
    }
}