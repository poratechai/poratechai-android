package com.hyptastic.poratechai.retrofit.interfaces;

import com.hyptastic.poratechai.models.GeneralResponse;
import com.hyptastic.poratechai.models.job_feed.Contract;
import com.hyptastic.poratechai.models.job_feed.Response;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.PUT;
import retrofit2.http.Query;

/**
 * Created by ISHRAK on 3/18/2018.
 */

public interface ContractClient  {
    @PUT("/feed/contract/sendContractReviewRequest")
    Observable<GeneralResponse> sendContractReviewRequest( @Query("matchId") String matchId, @Body Contract contract );

    @PUT("/feed/contract/acceptContractReviewRequest")
    Observable<GeneralResponse> acceptContractReviewRequest( @Query("matchId") String matchId );

    @PUT("/feed/contract/acceptTutor")
    Observable<Response> acceptTutor( @Query("matchId") String matchId );

    @PUT("/feed/contract/cancelJobinPhase0")
    Observable<Response> cancelJobinPhase0( @Query("matchId") String matchId );

    @PUT("/feed/contract/cancelJobinPhase1")
    Observable<Response> cancelJobinPhase1( @Query("matchId") String matchId );

    @PUT("/feed/contract/cancelJobinPhase2")
    Observable<Response> cancelJobinPhase2( @Query("matchId") String matchId );

    @PUT("/feed/contract/cancelJobinPhase3")
    Observable<Response> cancelJobinPhase3( @Query("matchId") String matchId );

    @PUT("/feed/contract/cancelJobinPhase4")
    Observable<Response> cancelJobinPhase4( @Query("jobId") String jobId );

    @PUT("/feed/contract/parentCancelsJob")
    Observable<Response> parentCancelsJob( @Query("jobId") String jobId );
}
