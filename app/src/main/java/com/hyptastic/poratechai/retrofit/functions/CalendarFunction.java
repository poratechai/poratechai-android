package com.hyptastic.poratechai.retrofit.functions;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.hyptastic.poratechai.fragments.job_feed.utilities.UtilitiesCalendarFragment;
import com.hyptastic.poratechai.models.GeneralResponse;
import com.hyptastic.poratechai.models.job_feed.calendar.Calendar;
import com.hyptastic.poratechai.models.job_feed.calendar.Dates;
import com.hyptastic.poratechai.retrofit.interfaces.CalendarClient;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

/**
 * Created by ISHRAK on 6/24/2019.
 */

public class CalendarFunction {
    public CalendarClient calendarClient;
    public Context context;

    public CalendarFunction(CalendarClient calendarClient, Context context){
        this.calendarClient = calendarClient;
        this.context = context;
    }

    public void getJobCalendarMonth(final UtilitiesCalendarFragment utilitiesCalendarFragment, String jobId, int year, int month){
        final DisposableObserver<Calendar> disposable = calendarClient.getJobCalendarMonth(jobId, year, month)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<Calendar>(){
                    @Override
                    public void onNext(Calendar jobCalendarMonth){
                        utilitiesCalendarFragment.jobCalendarMonthGotten(jobCalendarMonth);
                    }
                    @Override
                    public void onError(Throwable e){

                    }
                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void parentStudentSignature(final UtilitiesCalendarFragment utilitiesCalendarFragment, String jobId, int year, int month, int day, boolean sign){
        final DisposableObserver<Dates> disposable = calendarClient.parentStudentSignature(jobId, year, month, day, sign)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<Dates>(){
                    @Override
                    public void onNext(Dates d) {
                        Toast.makeText(utilitiesCalendarFragment.getActivity(), "Attendance updated successfully", Toast.LENGTH_LONG).show();
                        utilitiesCalendarFragment.studentSignatureCompleted(true, d);
                    }
                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(utilitiesCalendarFragment.getActivity(), "Please try again.", Toast.LENGTH_LONG).show();
                        utilitiesCalendarFragment.studentSignatureCompleted(false, null);
                    }
                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void tutorSignature(final UtilitiesCalendarFragment utilitiesCalendarFragment, String jobId, int year, int month, int day, boolean sign){
        final DisposableObserver<Dates> disposable = calendarClient.tutorSignature(jobId, year, month, day, sign)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<Dates>(){
                    @Override
                    public void onNext(Dates d) {
                        Toast.makeText(utilitiesCalendarFragment.getActivity(), "Attendance updated successfully", Toast.LENGTH_LONG).show();
                        utilitiesCalendarFragment.tutorSignatureCompleted(true, d);
                    }
                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(utilitiesCalendarFragment.getActivity(), "Please try again.", Toast.LENGTH_LONG).show();
                        utilitiesCalendarFragment.tutorSignatureCompleted(false, null);
                    }
                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void parentStudentNote(final UtilitiesCalendarFragment utilitiesCalendarFragment, String jobId, int year, int month, int day, String note){
        final DisposableObserver<Dates> disposable = calendarClient.parentStudentNote(jobId, year, month, day, note)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<Dates>(){
                    @Override
                    public void onNext(Dates d) {
                        utilitiesCalendarFragment.noteCompleted(true, d);
                    }
                    @Override
                    public void onError(Throwable e) {
                        utilitiesCalendarFragment.noteCompleted(false, null);
                    }
                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void tutorNote(final UtilitiesCalendarFragment utilitiesCalendarFragment, String jobId, int year, int month, int day, String note){
        final DisposableObserver<Dates> disposable = calendarClient.tutorNote(jobId, year, month, day, note)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<Dates>(){
                    @Override
                    public void onNext(Dates d) {
                        utilitiesCalendarFragment.noteCompleted(true, d);
                    }
                    @Override
                    public void onError(Throwable e) {
                        utilitiesCalendarFragment.noteCompleted(false, null);
                    }
                    @Override
                    public void onComplete() {

                    }
                });
    }
}
