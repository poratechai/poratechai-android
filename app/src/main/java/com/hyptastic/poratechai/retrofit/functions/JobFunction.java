package com.hyptastic.poratechai.retrofit.functions;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.Toast;
import android.util.Log;

import com.hyptastic.poratechai.activities.job_feed.EditJobActivity;
import com.hyptastic.poratechai.activities.job_feed.PostJobActivity;
import com.hyptastic.poratechai.adapters.job_feed.JobFeedAdapter;
import com.hyptastic.poratechai.adapters.job_feed.parent_student.ContractRequestsAdapter;
import com.hyptastic.poratechai.models.job_feed.JobFeedResponse;
import com.hyptastic.poratechai.models.job_feed.JobPost;
import com.hyptastic.poratechai.models.job_feed.Match;
import com.hyptastic.poratechai.retrofit.interfaces.JobClient;
import com.hyptastic.poratechai.utils.Constants;
import com.hyptastic.poratechai.utils.Functions;

import java.util.ArrayList;
import java.util.Arrays;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

import io.reactivex.Observer;

/**
 * Created by ISHRAK on 2/4/2018.
 */

public class JobFunction implements Parcelable {
    private JobClient jobClient = null;
    private Context context;

    public JobFunction(JobClient jobClient, Context context)
    {
        this.jobClient = jobClient;
        this.context = context;
    }

    public void postJob(final PostJobActivity postJobActivity, JobPost newJobPost)
    {
        jobClient.postJob(newJobPost)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<String>(){
                    @Override
                    public void onSubscribe(Disposable d) {

                    }
                    @Override
                    public void onNext(String s) {
                        Toast.makeText(context, "Job Posted Successfully", Toast.LENGTH_LONG).show();
                        postJobActivity.jobPostResponse(true);
                    }
                    @Override
                    public void onError(Throwable e) {
                        try
                        {
                            Log.e("POST Failed ", e.getMessage());
                            Toast.makeText(context, "Job Post Not Successful", Toast.LENGTH_SHORT).show();

                            postJobActivity.jobPostResponse(false);
                        }
                        catch(Exception exception)
                        {
                            Log.e("POST EXCEPTION", exception.getMessage());
                        }
                    }
                    @Override
                    public void onComplete() {
                    }
                });
    }

    public void getSinglePostedJob(final ContractRequestsAdapter contractRequestsAdapter, String jobId)
    {
        final DisposableObserver<JobFeedResponse> disposable = jobClient.getSinglePostedJob(jobId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<JobFeedResponse>() {
                    @Override
                    public void onNext(JobFeedResponse jobFeedResponse) {
                        contractRequestsAdapter.setJobDetails(jobFeedResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        System.out.println("Complete");
                    }
                });
    }

    public void getSinglePostedJob(final JobFeedAdapter<JobFeedResponse> jobFeedAdapter, String jobId)
    {
        final DisposableObserver<JobFeedResponse> disposable = jobClient.getSinglePostedJob(jobId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<JobFeedResponse>() {
                    @Override
                    public void onNext(JobFeedResponse jobFeedResponse) {
                        Log.e("Single Posted Job", jobFeedResponse.toString());

                        jobFeedAdapter.refreshOneItemFromServer(jobFeedResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        System.out.println("Complete");
                    }
                });
    }

    public void getPostedJobs(final JobFeedAdapter<JobFeedResponse> jobFeedAdapter)
    {
        try
        {
            final DisposableObserver<JobFeedResponse[]> disposable = jobClient.getPostedJobs()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<JobFeedResponse[]>() {
                        @Override
                        public void onNext(JobFeedResponse[] jobFeedResponses) {
                            for(JobFeedResponse response : jobFeedResponses)
                            {
                                System.out.println(response.toString());
                            }

                            jobFeedAdapter.setArrayList(new ArrayList<JobFeedResponse>(Arrays.asList(jobFeedResponses)));
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getSingleActivePostedJob(final JobFeedAdapter<JobFeedResponse> jobFeedAdapter, String jobId)
    {
        try
        {
            final DisposableObserver<JobFeedResponse> disposable = jobClient.getSingleActivePostedJob(jobId)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<JobFeedResponse>() {
                        @Override
                        public void onNext(JobFeedResponse jobFeedResponse) {
                            jobFeedAdapter.refreshOneItemFromServer(jobFeedResponse);
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getActivePostedJobs(final JobFeedAdapter<JobFeedResponse> jobFeedAdapter)
    {
        try
        {
            DisposableObserver<JobFeedResponse[]> disposable = jobClient.getActivePostedJobs()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<JobFeedResponse[]>()
                    {
                        @Override
                        public void onNext(JobFeedResponse[] jobFeedResponses) {
                            jobFeedAdapter.setArrayList(new ArrayList(Arrays.asList(jobFeedResponses)));
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getSinglePostedJobHistory(final JobFeedAdapter<JobFeedResponse> jobFeedAdapter, String jobId)
    {
        try
        {
            final DisposableObserver<JobFeedResponse> disposable = jobClient.getSinglePostedJobHistory(jobId)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<JobFeedResponse>() {
                        @Override
                        public void onNext(JobFeedResponse jobFeedResponse) {
                            jobFeedAdapter.refreshOneItemFromServer(jobFeedResponse);
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getPostedJobHistory(final JobFeedAdapter<JobFeedResponse> jobFeedAdapter)
    {
        try
        {
            DisposableObserver<JobFeedResponse[]> disposable = jobClient.getPostedJobHistory()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<JobFeedResponse[]>()
                    {
                        @Override
                        public void onNext(JobFeedResponse[] jobFeedResponses) {
                            jobFeedAdapter.setArrayList(new ArrayList(Arrays.asList(jobFeedResponses)));
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getSingleMatchedJob(final JobFeedAdapter<Match> jobFeedAdapter, String matchId)
    {
        try
        {
            final DisposableObserver<Match> disposable = jobClient.getSingleMatchedJob(matchId)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<Match>() {
                        @Override
                        public void onNext(Match match) {
                            jobFeedAdapter.refreshOneItemFromServer(match);
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getMatchedJobs(final JobFeedAdapter<Match> jobFeedAdapter)
    {
        try
        {
            DisposableObserver<Match[]> disposable = jobClient.getMatchedJobs()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<Match[]>()
                    {
                        @Override
                        public void onNext(Match[] jobFeedResponses) {
                            for(Match _m : jobFeedResponses)
                            {
                                System.out.println(_m.toString());
                            }
                            jobFeedAdapter.setArrayList(new ArrayList<Match>(Arrays.asList(jobFeedResponses)));
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getSingleAppliedJob(final JobFeedAdapter<Match> jobFeedAdapter, String matchId)
    {
        try
        {
            final DisposableObserver<Match> disposable = jobClient.getSingleAppliedJob(matchId)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<Match>() {
                        @Override
                        public void onNext(Match match) {
                            jobFeedAdapter.refreshOneItemFromServer(match);
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getAppliedJobs(final JobFeedAdapter<Match> jobFeedAdapter)
    {
        try
        {
            DisposableObserver<Match[]> disposable = jobClient.getAppliedJobs()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<Match[]>()
                    {
                        @Override
                        public void onNext(Match[] jobFeedResponses) {
                            for(Match _m : jobFeedResponses)
                            {
                                System.out.println(_m.toString());
                            }
                            jobFeedAdapter.setArrayList(new ArrayList<Match>(Arrays.asList(jobFeedResponses)));
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getSingleActiveJob(final JobFeedAdapter<JobFeedResponse> jobFeedAdapter, String jobId)
    {
        try
        {
            final DisposableObserver<JobFeedResponse> disposable = jobClient.getSingleActiveJob(jobId)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<JobFeedResponse>() {
                        @Override
                        public void onNext(JobFeedResponse jobFeedResponse) {
                            jobFeedAdapter.refreshOneItemFromServer(jobFeedResponse);
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getActiveJobs(final JobFeedAdapter<JobFeedResponse> jobFeedAdapter)
    {
        try
        {
            DisposableObserver<JobFeedResponse[]> disposable = jobClient.getActiveJobs()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<JobFeedResponse[]>()
                    {
                        @Override
                        public void onNext(JobFeedResponse[] jobFeedResponses) {
                            jobFeedAdapter.setArrayList(new ArrayList<JobFeedResponse>(Arrays.asList(jobFeedResponses)));
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getSingleJobHistory(final JobFeedAdapter<JobFeedResponse> jobFeedAdapter, String jobId)
    {
        try
        {
            final DisposableObserver<JobFeedResponse> disposable = jobClient.getSingleJobHistory(jobId)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<JobFeedResponse>() {
                        @Override
                        public void onNext(JobFeedResponse jobFeedResponse) {
                            jobFeedAdapter.refreshOneItemFromServer(jobFeedResponse);
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getJobHistory(final JobFeedAdapter<JobFeedResponse> jobFeedAdapter)
    {
        try
        {
            DisposableObserver<JobFeedResponse[]> disposable = jobClient.getJobHistory()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<JobFeedResponse[]>()
                    {
                        @Override
                        public void onNext(JobFeedResponse[] jobFeedResponses) {
                            jobFeedAdapter.setArrayList(new ArrayList<JobFeedResponse>(Arrays.asList(jobFeedResponses)));
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void update(final EditJobActivity editJobActivity, String jobId, JobPost updatedJobPostDetails)
    {
        try {
            jobClient.updateJob(jobId, updatedJobPostDetails)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<String>(){
                        @Override
                        public void onNext(String s) {
                            editJobActivity.jobUpdateResponse(true);
                            Toast.makeText(context, "Job Updated successfully", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            Toast.makeText(context, "Internal Error, Try again later", Toast.LENGTH_SHORT).show();
                            editJobActivity.jobUpdateResponse(false);
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void delete(final String jobId, final int position, final JobFeedAdapter<JobFeedResponse> jobFeedAdapter)
    {
        try{
            DisposableObserver<String> disposableObserver = jobClient.deleteJob(jobId)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<String>(){
                        @Override
                        public void onNext(String s) {
                            Log.v("Response from JOB del:", s);
                            Toast.makeText(context, "Job Deleted Successfully", Toast.LENGTH_LONG).show();

                            jobFeedAdapter.removeItem(position);
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            Toast.makeText(context, "Could Not Delete Job", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    //Parcelable - definitions

    public JobFunction(Parcel parcel){}

    public static final Parcelable.Creator<JobFunction> CREATOR = new Parcelable.Creator<JobFunction>(){
        @Override
        public JobFunction createFromParcel(Parcel parcel) {
            return new JobFunction(parcel);
        }

        @Override
        public JobFunction[] newArray(int i) {
            return new JobFunction[i];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }
}