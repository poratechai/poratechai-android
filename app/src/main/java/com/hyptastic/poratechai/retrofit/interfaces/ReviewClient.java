package com.hyptastic.poratechai.retrofit.interfaces;

import com.hyptastic.poratechai.models.review.Response;
import com.hyptastic.poratechai.models.review.Review;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

/**
 * Created by ISHRAK on 4/6/2018.
 */

public interface ReviewClient {
    @GET("/review/getReviewsByUserIdForParent")
    Observable<Review[]> getReviewsForParent(
            @Query("userId") String userId
    );

    @GET("/review/getReviewsByUserIdForTutor")
    Observable<Review[]> getReviewsForTutor(
            @Query("userId") String userId
    );

    @POST("/review/reviewByParent")
    Observable<Response> reviewByParent(
            @Query("jobId") String jobId, @Body Review reviewDetails
    );

    @POST("/review/reviewByTutor")
    Observable<Response> reviewByTutor(
            @Query("jobId") String jobId, @Body Review reviewDetails
    );

    @PUT("/review/updateReviewByParent")
    Observable<Response> updateReviewByParent(
            @Query("reviewId") String reviewId, @Body Review reviewDetails
    );

    @PUT("/review/updateReviewByTutor")
    Observable<Response> updateReviewByTutor(
            @Query("reviewId") String reviewId, @Body Review reviewDetails
    );
}
