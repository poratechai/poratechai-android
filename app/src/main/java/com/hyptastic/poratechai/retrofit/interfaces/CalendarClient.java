package com.hyptastic.poratechai.retrofit.interfaces;

import com.hyptastic.poratechai.models.GeneralResponse;
import com.hyptastic.poratechai.models.job_feed.calendar.Calendar;
import com.hyptastic.poratechai.models.job_feed.calendar.Dates;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by ISHRAK on 6/22/2019.
 */

public interface CalendarClient {
    @GET("/feed/calendar/getJobCalendarMonth")
    Observable<Calendar> getJobCalendarMonth(@Query("jobId") String jobId,
                                             @Query("year") int year, @Query("month") int month);

    @FormUrlEncoded
    @POST("/feed/calendar/parentStudentSignature")
    Observable<Dates> parentStudentSignature(@Query("jobId") String jobId,
                                             @Query("year") int year, @Query("month") int month, @Query("day") int day,
                                             @Field("sign") boolean sign);

    @FormUrlEncoded
    @POST("/feed/calendar/tutorSignature")
    Observable<Dates> tutorSignature(@Query("jobId") String jobId,
                                               @Query("year") int year, @Query("month") int month, @Query("day") int day,
                                               @Field("sign") boolean sign);

    @FormUrlEncoded
    @POST("/feed/calendar/parentStudentNote")
    Observable<Dates> parentStudentNote(@Query("jobId") String jobId,
                                                  @Query("year") int year, @Query("month") int month, @Query("day") int day,
                                                  @Field("notes") String notes);

    @FormUrlEncoded
    @POST("/feed/calendar/tutorNote")
    Observable<Dates> tutorNote(@Query("jobId") String jobId,
                                          @Query("year") int year, @Query("month") int month, @Query("day") int day,
                                          @Field("notes") String notes);
}
