package com.hyptastic.poratechai.retrofit.functions;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.hyptastic.poratechai.activities.job_feed.EditJobActivity;
import com.hyptastic.poratechai.activities.splash.NoConnectionActivity;
import com.hyptastic.poratechai.activities.job_feed.PostJobActivity;
import com.hyptastic.poratechai.activities.splash.SplashActivity;
import com.hyptastic.poratechai.fragments.job_feed.tutor.ContractFragment;
import com.hyptastic.poratechai.fragments.payment.PaymentInterface;
import com.hyptastic.poratechai.fragments.peek.NavigationFragment;
import com.hyptastic.poratechai.models.GeneralResponse;
import com.hyptastic.poratechai.models.wallet.v1.WalletConstantsV1;
import com.hyptastic.poratechai.models.wallet.v2.WalletConstantsV2;
import com.hyptastic.poratechai.retrofit.interfaces.SystemClient;
import com.hyptastic.poratechai.utils.Functions;

import java.io.Serializable;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by ISHRAK on 7/1/2018.
 */

public class SystemFunction implements Serializable {
    public SystemClient systemClient;
    public Context context;

    public SystemFunction(SystemClient systemClient, Context context){
        this.context = context;
        this.systemClient = systemClient;
    }

    public void getSystemArrayConstants(final PostJobActivity postJobActivity){
        try
        {
            final DisposableObserver<GeneralResponse> disposable = systemClient.getSystemArrayConstants()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<GeneralResponse>()
                    {
                        @Override
                        public void onNext(GeneralResponse generalResponse) {
                            Functions.setSystemArray(generalResponse.getMessage());
                            postJobActivity.loadJobActivity();
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getSystemArrayConstants(final EditJobActivity editJobActivity){
        try
        {
            final DisposableObserver<GeneralResponse> disposable = systemClient.getSystemArrayConstants()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<GeneralResponse>()
                    {
                        @Override
                        public void onNext(GeneralResponse generalResponse) {
                           Functions.setSystemArray(generalResponse.getMessage());
                            editJobActivity.loadJobActivity();
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    //    public void getSystemArrayConstants(final ProfileActivity profileActivity){
//        try
//        {
//            final DisposableObserver<GeneralResponse> disposable = systemClient.getSystemArrayConstants()
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribeOn(Schedulers.io())
//                    .subscribeWith(new DisposableObserver<GeneralResponse>()
//                    {
//                        @Override
//                        public void onNext(GeneralResponse generalResponse) {
//                            try{
//                                Functions.setSystemArray(generalResponse.getMessage());
////                                profileActivity.loadFragment();
//                            }catch(Exception e){
//                                e.printStackTrace();
//                            }
//                        }
//
//                        @Override
//                        public void onError(Throwable e) {
//                            e.printStackTrace();
//                        }
//
//                        @Override
//                        public void onComplete() {
//                            System.out.println("Complete");
//                        }
//                    });
//        }
//        catch(Exception e)
//        {
//            e.printStackTrace();
//        }
//    }

    public void getSystemArrayConstants(final com.hyptastic.poratechai.fragments.profile.tutor.profile.ProfileFragment profileFragment){
        try
        {
            final DisposableObserver<GeneralResponse> disposable = systemClient.getSystemArrayConstants()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<GeneralResponse>()
                    {
                        @Override
                        public void onNext(GeneralResponse generalResponse) {
                            try{
                                Functions.setSystemArray(generalResponse.getMessage());
                                profileFragment.systemArrayConstantsGotten();
                            }catch(Exception e){
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getSystemArrayConstants(final com.hyptastic.poratechai.fragments.profile.parent_student.profile.ProfileFragment profileFragment){
        try
        {
            final DisposableObserver<GeneralResponse> disposable = systemClient.getSystemArrayConstants()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<GeneralResponse>()
                    {
                        @Override
                        public void onNext(GeneralResponse generalResponse) {
                            try{
                                Functions.setSystemArray(generalResponse.getMessage());
                                profileFragment.systemArrayConstantsGotten();
                            }catch(Exception e){
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    //Token - Payment - V1
    public void getSystemWalletConstantsV1(final PaymentInterface paymentInterface){
        try
        {
            final DisposableObserver<WalletConstantsV1> disposable = systemClient.getSystemWalletConstantsV1()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<WalletConstantsV1>()
                    {
                        @Override
                        public void onNext(WalletConstantsV1 walletConstants) {
                            try{
                                paymentInterface.walletConstantsGotten(walletConstants);
                            }catch(Exception e){
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    //Subscription - Payment - V2
    public void getSystemWalletConstantsV2(final PaymentInterface paymentInterface){
        try
        {
            final DisposableObserver<WalletConstantsV2> disposable = systemClient.getSystemWalletConstantsV2()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<WalletConstantsV2>()
                    {
                        @Override
                        public void onNext(WalletConstantsV2 walletConstants) {
                            try{
                                paymentInterface.walletConstantsGotten(walletConstants);
                            }catch(Exception e){
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getPrice(final PaymentInterface paymentInterface){
        final DisposableObserver<WalletConstantsV2> disposable = systemClient.getPrice()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<WalletConstantsV2>() {
                    @Override
                    public void onNext(WalletConstantsV2 walletConstants) {
                        paymentInterface.walletConstantsGotten(walletConstants);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        System.out.println("Complete");
                    }
                });
    }

    //Token - Payment - V1
//    public void getSystemWalletConstantsV1(final ContractFragment contractFragment){
//        try
//        {
//            final DisposableObserver<WalletConstantsV1> disposable = systemClient.getSystemWalletConstantsV1()
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribeOn(Schedulers.io())
//                    .subscribeWith(new DisposableObserver<WalletConstantsV1>()
//                    {
//                        @Override
//                        public void onNext(WalletConstantsV1 walletConstants) {
//                            try{
//                                contractFragment.walletConstantsGotten(walletConstants);
//                            }catch(Exception e){
//                                e.printStackTrace();
//                            }
//                        }
//
//                        @Override
//                        public void onError(Throwable e) {
//                            e.printStackTrace();
//                        }
//
//                        @Override
//                        public void onComplete() {
//                            System.out.println("Complete");
//                        }
//                    });
//        }
//        catch(Exception e)
//        {
//            e.printStackTrace();
//        }
//    }

    //Subscription - Payment - V2
    public void getSystemWalletConstantsV2(final ContractFragment contractFragment){
        try
        {
            final DisposableObserver<WalletConstantsV2> disposable = systemClient.getSystemWalletConstantsV2()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<WalletConstantsV2>()
                    {
                        @Override
                        public void onNext(WalletConstantsV2 walletConstants) {
                            try{
                                contractFragment.walletConstantsGotten(walletConstants);
                            }catch(Exception e){
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getPeekResults(final NavigationFragment peekFragment){
        try
        {
            final DisposableObserver<GeneralResponse> disposable = systemClient.getPeekResults()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<GeneralResponse>()
                    {
                        @Override
                        public void onNext(GeneralResponse peekResults){
                            try{
                                peekFragment.parsePeekResults(peekResults.getMessage());
                            }catch(Exception e){
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getSupportNumber(final com.hyptastic.poratechai.fragments.support.NavigationFragment supportFragment){
        try
        {
            final DisposableObserver<String> disposable = systemClient.getSupportNumber()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<String>()
                    {
                        @Override
                        public void onNext(String supportNumber) {
                            try{
                                supportFragment.supportNumberGotten(supportNumber);
                            }catch(Exception e){
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getVersionCode(final SplashActivity splashActivity){
        try
        {
            final DisposableObserver<String> disposable = systemClient.getVersionCode()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<String>()
                    {
                        @Override
                        public void onNext(String versionCode) {
                            try{
                                splashActivity.checkVersionUpdate(versionCode);
                            }catch(Exception e){
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();

                            splashActivity.connectionNegative();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getConnectionStatus(final NoConnectionActivity noConnectionActivity){
        try
        {
            final DisposableObserver<String> disposable = systemClient.getConnectionStatus()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<String>()
                    {
                        @Override
                        public void onNext(String connectionStatus) {
                            try{
                                Toast.makeText(noConnectionActivity, connectionStatus, Toast.LENGTH_SHORT).show();

                                noConnectionActivity.connectedSuccessfully();
                            }catch(Exception e){
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();

                            Toast.makeText(noConnectionActivity, "Could not connect", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onComplete() {
                            System.out.println("Complete");
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
