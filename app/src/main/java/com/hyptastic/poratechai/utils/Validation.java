package com.hyptastic.poratechai.utils;

/**
 * Created by User on 31-Jan-18.
 */

import android.text.TextUtils;
import android.util.Patterns;

public class Validation {

    public static boolean validateFields(String name){

        if (TextUtils.isEmpty(name)) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean validateFields(Integer number){

        if (TextUtils.isEmpty(number.toString())) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean validateEmail(String string) {

        if (TextUtils.isEmpty(string) || !Patterns.EMAIL_ADDRESS.matcher(string).matches()) {
            return false;
        } else {
            return  true;
        }
    }

    public static boolean validatePasswordLength(Integer number){
        if(number < 3){
            return false;
        }
        else{
            return true;
        }
    }
}
