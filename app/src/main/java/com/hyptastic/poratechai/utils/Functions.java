package com.hyptastic.poratechai.utils;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.hyptastic.poratechai.activities.PorateChaiActivity;
import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.activities.profile.ProfileViewActivity;
import com.hyptastic.poratechai.fragments.dashboard.BlockingFragment;
import com.hyptastic.poratechai.models.job_feed.JobFeedResponse;
import com.hyptastic.poratechai.models.job_feed.Match;
import com.hyptastic.poratechai.retrofit.functions.UserFunctions;
import com.hyptastic.poratechai.retrofit.interfaces.UserClient;
import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ISHRAK on 3/5/2018.
 */

public abstract class Functions extends AppCompatActivity{
    private static SharedPreferences mSharedPreferences = null;
    private static BlockingFragment blockingFragment = null;

    public static void getScreenDimensions(WindowManager windowManager) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);

        Constants.SCREEN_HEIGHT = displayMetrics.heightPixels;
        Constants.SCREEN_WIDTH = displayMetrics.widthPixels;
    }

    public static void initSharedPreferences(Context context) {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static SharedPreferences getMSharedPreferences(){
        return mSharedPreferences;
    }

    public static void storeLoginDataToSharedPreferences(String email, String token, String userId, String profilePicture, String name, String institution, String isTutor, String isVerified, String tutorUpdateFlag)
    {
        Boolean status = true;
        if(email == null && token == null){
            status = false;
        }

        mSharedPreferences.edit()
                .putString(Constants.EMAIL, email)
                .putString(Constants.TOKEN, token)
                .putString(Constants.USER_ID, userId)
                .putString(Constants.USER_PROFILE_PICTURE, Constants.BASE_URL+profilePicture)
                .putString(Constants.USER_NAME, name)
                .putString(Constants.INSTITUTION, institution)
                .putString(Constants.IS_VERIFIED, isVerified)
                .putBoolean(Constants.STATUS, status)
                .putBoolean(Constants.IS_TUTOR, Boolean.valueOf(isTutor))
                .putString(Constants.TUTOR_UPDATE_FLAG, tutorUpdateFlag)
                .apply();

        loadLoginDataFromSharedPreferences();
    }

    public static void storeProfilePictureToSharedPreferences(String profilePicture){
        mSharedPreferences.edit()
                .putString(Constants.USER_PROFILE_PICTURE, profilePicture)
                .apply();
        Constants.USER_PROFILE_PICTURE_VALUE = mSharedPreferences.getString(Constants.USER_PROFILE_PICTURE,"");
    }

    public static void storeSmsVerificationStatus(String isVerified){
        mSharedPreferences.edit()
                .putString(Constants.IS_VERIFIED, isVerified)
                .apply();
        Constants.IS_VERIFIED_VALUE = mSharedPreferences.getString(Constants.IS_VERIFIED, "");
    }

    public static void loadLoginDataFromSharedPreferences()
    {
        Constants.EMAIL_VALUE = mSharedPreferences.getString(Constants.EMAIL, "");
        Constants.TOKEN_VALUE = mSharedPreferences.getString(Constants.TOKEN, "");
        Constants.USER_ID_VALUE = mSharedPreferences.getString(Constants.USER_ID, "");
        Constants.USER_PROFILE_PICTURE_VALUE = mSharedPreferences.getString(Constants.USER_PROFILE_PICTURE,"");
        Constants.USER_NAME_VALUE = mSharedPreferences.getString(Constants.USER_NAME, "");
        Constants.INSTITUTION_VALUE = mSharedPreferences.getString(Constants.INSTITUTION, "");
        Constants.IS_VERIFIED_VALUE = mSharedPreferences.getString(Constants.IS_VERIFIED,"");
    }

    public static boolean isLoggedIn()
    {
        return mSharedPreferences.getBoolean(Constants.STATUS, false);
    }

    public static void clearLoginDataFromSharedPreferences()
    {
        try
        {
            mSharedPreferences.edit().remove(Constants.EMAIL).apply();
            mSharedPreferences.edit().remove(Constants.TOKEN).apply();
            mSharedPreferences.edit().remove(Constants.USER_ID).apply();
            mSharedPreferences.edit().remove(Constants.USER_PROFILE_PICTURE).apply();
            mSharedPreferences.edit().putBoolean(Constants.STATUS, false).apply();
            mSharedPreferences.edit().remove(Constants.INSTITUTION).apply();
            mSharedPreferences.edit().remove(Constants.IS_TUTOR).apply();
            mSharedPreferences.edit().remove(Constants.IS_VERIFIED).apply();
            mSharedPreferences.edit().remove(Constants.TUTOR_UPDATE_FLAG).apply();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void showBlockingFragment(AppCompatActivity activity, boolean isVisible)
    {
        if(blockingFragment == null){
            blockingFragment = new BlockingFragment();
        }

        if(isVisible){
            blockingFragment.show(activity.getSupportFragmentManager(), "BlockingFragment");
        }else{
            blockingFragment.dismiss();
        }
    }

    public static boolean isTutor()
    {
        return mSharedPreferences.getBoolean(Constants.IS_TUTOR, false);
    }

    //Tutor or Parent switch action - start
    public static void setIsTutorOnNetwork(boolean isTutor, final PorateChaiActivity porateChaiActivity)
    {
        UserFunctions userFunctions = new UserFunctions(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, UserClient.class), porateChaiActivity);

        if(!isTutor){
            Toast.makeText(porateChaiActivity, "Changing to Parent/Student", Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(porateChaiActivity, "Changing to Tutor", Toast.LENGTH_LONG).show();
        }

        userFunctions.setTutorOrParent(porateChaiActivity, String.valueOf(isTutor));
    }

    public static void setIsTutor(boolean isTutor){
        mSharedPreferences.edit()
                .putBoolean(Constants.IS_TUTOR, isTutor)
                .apply();
    }
    //Tutor or Parent switch action - end

    //Check if tutor has updated his/her profile - start
    public static String isTutorUpdated(){
        return mSharedPreferences.getString(Constants.TUTOR_UPDATE_FLAG, "");
    }

    public static void setTutorUpdated(String isTutorUpdated){
        mSharedPreferences.edit()
                .putString(Constants.TUTOR_UPDATE_FLAG, isTutorUpdated)
                .apply();
    }
    //Check if tutor has updated his/her profile - end

    //JobFeed
    public static Match checkInspectionPhase(JobFeedResponse jobFeedResponse)
    {
        List<Match> matchedTutors = jobFeedResponse.getMatchedTutors();

        for(Match m : matchedTutors)
        {
            if(m.getCurrentStatus()==3)
            {
                return m;
            }
        }

        return null;
    }

    //Image Upload - start
    public static byte[] getBytes(InputStream is){
        try{
            ByteArrayOutputStream byteBuff = new ByteArrayOutputStream();

            int buffSize = 1024;
            byte[] buff = new byte[buffSize];

            int len = 0;
            while ((len = is.read(buff)) != -1) {
                byteBuff.write(buff, 0, len);
            }

            return byteBuff.toByteArray();
        }catch(Exception e){
            e.printStackTrace();
        }

        return null;
    }
    //Image Upload - end

    //Profile Picture - start
    public static boolean loadProfilePicture(Context context, String userId, Constants.PICTURE_PACKET picturePacket)
    {
        Log.e("ProfilePicture", "Loading");

        if(picturePacket.path == null){
            Log.e("ProfilePicture", "Getting");

            UserFunctions userFunctions = new UserFunctions(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, UserClient.class), context);

            userFunctions.getProfilePicture(picturePacket, userId);

            return true;
        }else{
            Log.e("ProfilePicture", "Setting");

            return setProfilePicture(picturePacket);
        }
    }

    public static boolean setProfilePicture(Constants.PICTURE_PACKET picturePacket)
    {
        try{
            if(picturePacket.activity == null || picturePacket.activity.isDestroyed()){
                return false;
            }

            Glide.with(picturePacket.activity)
                    .load(picturePacket.path)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.ic_blank_profile_picture)
                            .override(picturePacket.width, picturePacket.height))
                    .apply(RequestOptions.circleCropTransform())
                    .into(picturePacket.imageView);

            return true;
        }catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }
    //Profile Picture - end

    //Profile - start
    public static void loadOthersProfile(Context context, boolean isOtherTutor, String name, String userId, int contractStatus, String matchId){
        Intent intent = new Intent((AppCompatActivity)context, ProfileViewActivity.class);
        intent.putExtra("IS_TUTOR", isOtherTutor);
        intent.putExtra("USER_NAME", name);
        intent.putExtra("USER_ID", userId);
        intent.putExtra("CONTRACT_STATUS", contractStatus);
        intent.putExtra("MATCH_ID", matchId);
        ((AppCompatActivity)context).startActivity(intent);
    }
    //Profile - end

    //Keyboard - start
    public static void hideKeyboard(Context context, View view) //View can be an EditText
    {
        InputMethodManager inputManager = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }

    public static void showKeyboard(Context context, View view) //View can be an EditText
    {
        InputMethodManager inputManager = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.toggleSoftInputFromWindow(view.getWindowToken(), InputMethodManager.SHOW_FORCED, InputMethodManager.RESULT_HIDDEN);
    }

    public static void hideKeyboardWhenFocusLost(Context context, View view, MotionEvent motionEvent)
    {
        if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
            if(view.isFocused()){
                Rect outRect = new Rect();
                view.getGlobalVisibleRect(outRect);
                if(!outRect.contains((int)motionEvent.getRawX(), (int)motionEvent.getRawY())){
                    view.clearFocus();
                    hideKeyboard(context, view);
                }
            }
        }
    }
    //Keyboard - end

    //Change tint of a view
    public static void setTint(View view, int tint){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            view.getBackground().setColorFilter(tint, PorterDuff.Mode.SRC_IN);
        }else{
            Drawable wrapDrawable = DrawableCompat.wrap(view.getBackground());
            DrawableCompat.setTint(wrapDrawable, tint);
            view.setBackground(DrawableCompat.unwrap(wrapDrawable));
        }
    }

    //Dynamic Selectable Background
    public static void setSelectableBackground(Context context, View view)
    {
        int[] attrs = new int[]{R.attr.selectableItemBackground};
        TypedArray typedArray =  context.obtainStyledAttributes(attrs);

        int backgroundResource = typedArray.getResourceId(0, 0);
        view.setBackgroundResource(backgroundResource);

        typedArray.recycle();
    }

    //Parsing System Array which are got from the server
    //classesToTeach, categories, subcategories, subjects
    public static void setSystemArray(String systemConstants){
        try{
            JSONObject object = new JSONObject(systemConstants);

            JSONArray classesToTeachJSONArray = (JSONArray)object.get("classesToTeach");
            JSONArray categoriesJSONArray = (((JSONObject)object.get("subcategories")).names());
            JSONArray subjectsJSONArray = (JSONArray)object.get("subjects");

            String[] categories = new String[categoriesJSONArray.length()];
            String[] classesToTeach = new String[classesToTeachJSONArray.length()];
            String[] subjects = new String[subjectsJSONArray.length()];

            Constants.SYSTEM_ARRAY.classesToTeach = new HashMap<String, String[]>();
            Constants.SYSTEM_ARRAY.categories = new HashMap<String, String[]>();
            Constants.SYSTEM_ARRAY.subjects = new HashMap<String, String[]>();
            Constants.SYSTEM_ARRAY.subcategories = new HashMap<String, String[]>();

            for(int i=0;i<classesToTeachJSONArray.length();i++){
                classesToTeach[i] = classesToTeachJSONArray.get(i).toString();
            }

            for(int i=0;i<categoriesJSONArray.length();i++){
                categories[i] = categoriesJSONArray.get(i).toString();

                JSONArray subcategoriesJSONArray = ((JSONObject)object.get("subcategories")).getJSONArray(categoriesJSONArray.get(i).toString());
                String[] subcategories = new String[subcategoriesJSONArray.length()];

                for(int j=0;j<subcategoriesJSONArray.length();j++){
                    subcategories[j] = subcategoriesJSONArray.get(j).toString();
                }

                Constants.SYSTEM_ARRAY.subcategories.put(categoriesJSONArray.get(i).toString(), subcategories);
            }

            for(int i=0;i<subjectsJSONArray.length();i++){
                subjects[i] = subjectsJSONArray.get(i).toString();
            }

            Constants.SYSTEM_ARRAY.categories.put(Constants.SYSTEM_ARRAY.CATEGORIES_KEY, categories);
            Constants.SYSTEM_ARRAY.classesToTeach.put(Constants.SYSTEM_ARRAY.CLASSES_TO_TEACH_KEY, classesToTeach);
            Constants.SYSTEM_ARRAY.subjects.put(Constants.SYSTEM_ARRAY.SUBJECTS_KEY, subjects);
        }catch(Exception e){
            Log.e("SystemArray", e.toString());
        }
    }

    //Loading Bitmaps efficiently - Resizing - Start
    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight) {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }
    //Loading Bitmaps efficiently - Resizing - End

    //Non-swipeable View.OnTouchListener
    public static View.OnTouchListener getNonSwipeableViewTouchListener(){
        return new View.OnTouchListener(){
            float xDown, yDown;
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction()==MotionEvent.ACTION_DOWN){
                    xDown = motionEvent.getX();
                    yDown = motionEvent.getY();
                }else if(motionEvent.getAction()==MotionEvent.ACTION_UP){
                    if(Math.abs(xDown-motionEvent.getX()) < 5 && Math.abs(yDown-motionEvent.getY()) < 5){
                        view.getParent().requestDisallowInterceptTouchEvent(true);
                        return false;
                    }
                }
                return true;
            }
        };
    }
    //Non-swipeable View.OnTouchListener

    //Get specific fragment - start
    public static Fragment getFragment(FragmentActivity activity, int hashCodeToCheck){
        List<Fragment> fragments = activity.getSupportFragmentManager().getFragments();

        for(Fragment fragment: fragments){
            if(fragment.hashCode() == hashCodeToCheck){
                return fragment;
            }
        }

        return null;
    }
    //Get specific fragment - end
}
