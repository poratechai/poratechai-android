package com.hyptastic.poratechai.utils;

import android.app.Activity;
import android.widget.ImageView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;

/**
 * Created by User on 05-Dec-17.
 */

public abstract class Constants {
//    public static final String BASE_URL = "https://projectxoxo.herokuapp.com";    //us server
    public static final String BASE_URL = "https://poratechaiserver.herokuapp.com"; //eu server
//    public static final String BASE_URL = "http://192.168.2.4:8900";

    public static int SCREEN_HEIGHT = 0;
    public static int SCREEN_WIDTH = 0;

    public static final String TOKEN = "token";
    public static String TOKEN_VALUE = "token";
    public static final String EMAIL = "email";
    public static String EMAIL_VALUE = "email";
    public static final String STATUS = "status";

    public static final String USER_ID = "user_id";
    public static String USER_ID_VALUE = "user_id";

    public static final String USER_NAME = "user_name";
    public static String USER_NAME_VALUE = "user_name";

    public static final String INSTITUTION = "institution";
    public static String INSTITUTION_VALUE = "institution";

    public static final String IS_VERIFIED = "is_verified";
    public static String IS_VERIFIED_VALUE = "is_verified";

    public static final String USER_PROFILE_PICTURE = "user_profile_picture";
    public static String USER_PROFILE_PICTURE_VALUE = "user_profile_picture";

    public static final String IS_TUTOR = "is_tutor";
    public static final String TUTOR_UPDATE_FLAG = "tutor_update_flag";

    public static final String FIRE_BASE_PATH_USER_TOKEN = "userToken";
//    public static final String UID = "5a8143da23beef000f19b18a";

    public static class PICTURE_PACKET{
        public static final int PROFILE_PICTURE_WIDTH = 64;
        public static final int PROFILE_PICTURE_HEIGHT = 64;

        public Activity activity;
        public ImageView imageView;
        public String path;
        public int width;
        public int height;

        public PICTURE_PACKET(Activity activity, ImageView imageView, String path, int width, int height){
            this.activity = activity;
            this.imageView= imageView;
            this.path = path;
            this.width = width;
            this.height = height;
        }
    }

    public static abstract class JOB_STATUS{
        public static final int JOB_STATUS_MATCH_POOL = 0;
        public static final int JOB_STATUS_CONTRACT_REQUEST_SENT = 1;
        public static final int JOB_STATUS_CONTRACT_REVIEW = 2;
        public static final int JOB_STATUS_INSPECTION = 3;
        public static final int JOB_STATUS_ACTIVE = 4;
        public static final int JOB_STATUS_ARCHIVED = 5;
    }

    public static abstract class VIEW_TYPE{
        public static final int JOB_DETAILS = 0;
        public static final int TUTOR_CONTRACT = 1;
    }

    public static abstract class NOTIFICATION_DATA_FRAGMENT_TYPE{
        public static final int PROFILE = 1;
        public static final int JOB_FEED = 2;
        public static final int SUBSCRIPTION = 3;
        public static final int SUPPORT = 4;
    }

    public static abstract class NOTIFICATION_DATA_OPERATION_TYPE{
        public static final int ADD = 1;
        public static final int REMOVE = 2;
        public static final int UPDATE = 3;
    }

    public static abstract class NOTIFICATION_DATA_FEED_TYPE{
        public static final int POSTED_JOB = 1;
        public static final int ACTIVE_POSTED_JOB = 2;
        public static final int POSTED_JOB_HISTORY = 3;
        public static final int MATCH = 4;
        public static final int ACTIVE_JOB = 5;
        public static final int JOB_HISTORY = 6;
    }

    public static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    public static DateFormat outputDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    public static int SALARY_DIFFERENCE = 3000;
    public static double TOKEN_UNIT_PRICE = 200;
    public static double SALARY_STEP_UNIT = 5000;

    public static abstract class SYSTEM_ARRAY{
        public static HashMap<String, String[]> categories;
        public static HashMap<String, String[]> subjects;
        public static HashMap<String, String[]> classesToTeach;
        public static HashMap<String, String[]> subcategories;

        public static String CATEGORIES_KEY = "Categories";
        public static String SUBJECTS_KEY = "Subjects";
        public static String CLASSES_TO_TEACH_KEY = "Classes To Teach";
    }
}
