//package com.hyptastic.poratechai.functions.binders;
//
///**
// * Created by User on 30-Jun-18.
// */
//
//import android.content.Context;
//import android.widget.ImageButton;
//import android.widget.ImageView;
//
//import com.bumptech.glide.Glide;
//import com.hyptastic.poratechai.R;
//import com.hyptastic.poratechai.activities.profile.ProfileActivity;
//import com.mindorks.placeholderview.PlaceHolderView;
//import com.mindorks.placeholderview.annotations.Click;
//import com.mindorks.placeholderview.annotations.Layout;
//import com.mindorks.placeholderview.annotations.NonReusable;
//import com.mindorks.placeholderview.annotations.Resolve;
//import com.mindorks.placeholderview.annotations.View;
//
//@NonReusable
//@Layout(R.layout.item_small)
//public class ImageTypeSmall {
//
//    @View(R.id.imageView)
//    public ImageView imageView;
//
//    @View(R.id.crossButton)
//    public ImageButton imageButton;
//
//    private String mUlr;
//    private Context mContext;
//    private PlaceHolderView mPlaceHolderView;
//
//    public ImageTypeSmall(Context context, PlaceHolderView placeHolderView, String ulr) {
//        mContext = context;
//        mPlaceHolderView = placeHolderView;
//        mUlr = ulr;
//    }
//
//    @Resolve
//    public void onResolved() {
//        Glide.with(mContext).load(mUlr).into(imageView);
//    }
//
//    @Click(R.id.crossButton)
//    public void onClick(){
//        mPlaceHolderView.removeView(this);
//        ProfileActivity profileActivity = new ProfileActivity();
////        profileActivity.deleteCredentialPicture(mUlr);
//    }
//
//}