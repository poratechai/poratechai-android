//package com.hyptastic.poratechai.functions.binders;
//
//import android.content.Context;
//import android.support.v7.widget.LinearLayoutManager;
//
//import com.hyptastic.poratechai.R;
//import com.mindorks.placeholderview.PlaceHolderView;
//import com.mindorks.placeholderview.annotations.Layout;
//import com.mindorks.placeholderview.annotations.NonReusable;
//import com.mindorks.placeholderview.annotations.Resolve;
//import com.mindorks.placeholderview.annotations.View;
//
//import java.util.List;
//
///**
// * Created by User on 30-Jun-18.
// */
//
//@NonReusable
//@Layout(R.layout.item_small_list)
//public class ImageTypeSmallList {
//
//    @View(R.id.placeholderview)
//    public PlaceHolderView mPlaceHolderView;
//
//    private Context mContext;
//    private List<String> mImageList;
//
//    public ImageTypeSmallList(Context context, List<String> imageList) {
//        mContext = context;
//        mImageList = imageList;
//    }
//
//    @Resolve
//    public void onResolved() {
//        mPlaceHolderView.getBuilder()
//                .setHasFixedSize(false)
//                .setItemViewCacheSize(10)
//                .setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
//
//        for(String image : mImageList) {
//            mPlaceHolderView.addView(new ImageTypeSmall(mContext, mPlaceHolderView, image));
//        }
//    }
//}