package com.hyptastic.poratechai.layout;

import android.app.Activity;
import android.support.v4.content.res.ResourcesCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hyptastic.poratechai.R;

import java.util.ArrayList;

/**
 * Created by ISHRAK on 1/24/2019.
 */

public class FlexboxLayout {
    Activity activity;
    int leftSpace = 3;
    ArrayList<String> items;

    TextView addOrEdit;

    public FlexboxLayout(Activity activity){
        this.activity = activity;

        addOrEdit = new TextView(activity);
    }

    public FlexboxLayout(Activity activity, int leftSpace){
        this.activity = activity;
        this.leftSpace = leftSpace;
    }

    public FlexboxLayout(Activity activity, ArrayList<String> items, LinearLayout container, int backgroundLength, boolean addOrEditVisibility){
        this.activity = activity;
        this.items = items;

        if(addOrEditVisibility){
            addOrEdit = new TextView(activity);
        }

        refreshFlexbox(container, items, backgroundLength, addOrEditVisibility);
    }

    public void refreshFlexbox(LinearLayout container, ArrayList<String> items, int backgroundLength, boolean addOrEditVisibility){
        TextView item       = new TextView(activity);
        item.setBackgroundResource(R.drawable.background_grey_flexbox_item);
        item.setTextSize(12);
        item.setTextColor(ResourcesCompat.getColor(activity.getResources(), R.color.black, null));

        LinearLayout row    = new LinearLayout(activity);
        row.setOrientation(LinearLayout.HORIZONTAL);

        container.removeAllViews();

        int previousLength = leftSpace;

        if(items!=null && items.size()>0){
            item.setText(items.get(0));
            row.addView(item, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            ((LinearLayout.LayoutParams)item.getLayoutParams()).setMargins(leftSpace, 0, 3, 5);

            if(items.size()==1){
                container.addView(row, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            }

            item.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            previousLength = item.getMeasuredWidth();

            for(int i=1; i<items.size(); i++){
                item = new TextView(activity);
                item.setBackgroundResource(R.drawable.background_grey_flexbox_item);
                item.setTextSize(12);
                item.setTextColor(ResourcesCompat.getColor(activity.getResources(), R.color.black, null));

                item.setText(items.get(i));
                item.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                //Adding a row to the container - for complete rows
                if((previousLength + item.getMeasuredWidth())>(0.8*backgroundLength)){
                    container.addView(row, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));

                    row = new LinearLayout(activity);
                    row.setOrientation(LinearLayout.HORIZONTAL);

                    previousLength = item.getMeasuredWidth();
                }else{
                    previousLength = previousLength + item.getMeasuredWidth() + 6;
                }

                row.addView(item, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                ((LinearLayout.LayoutParams)item.getLayoutParams()).setMargins(leftSpace, 0, 3, 5);

                //Adding a row to the container - for incomplete rows
                if(i==items.size()-1){
                    container.addView(row, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                }
            }
        }

        if(addOrEditVisibility && addOrEdit!=null){
            addOrEdit.setText("Add/Edit");
            addOrEdit.setBackgroundResource(R.drawable.background_red_flexbox_item);
            addOrEdit.setTextSize(12);

            container.addView(addOrEdit, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            ((LinearLayout.LayoutParams)addOrEdit.getLayoutParams()).setMargins(leftSpace, 0, 0, 5);
        }
    }

    public void setAddOrEditListener(View.OnClickListener onClickListener){
        addOrEdit.setOnClickListener(onClickListener);
    }
}
