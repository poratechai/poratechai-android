package com.hyptastic.poratechai.adapters.job_feed.tutor;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.activities.PorateChaiActivity;
import com.hyptastic.poratechai.activities.job_feed.JobUtilitiesActivity;
import com.hyptastic.poratechai.activities.profile.ProfileViewActivity;
import com.hyptastic.poratechai.adapters.job_feed.JobFeedAdapter;
import com.hyptastic.poratechai.fragments.job_feed.tutor.dialog.ConfirmDeleteAppliedMatchDialog;
import com.hyptastic.poratechai.fragments.job_feed.tutor.dialog.ConfirmDeleteMatchDialog;
import com.hyptastic.poratechai.fragments.job_feed.tutor.ContractFragment;
import com.hyptastic.poratechai.fragments.job_feed.utilities.UtilitiesDetailsFragment;
import com.hyptastic.poratechai.models.job_feed.JobFeedResponse;
import com.hyptastic.poratechai.models.job_feed.Match;
import com.hyptastic.poratechai.models.users.User;
import com.hyptastic.poratechai.utils.Constants;
import com.hyptastic.poratechai.utils.Functions;

/**
 * Created by ISHRAK on 3/22/2018.
 */

public class AppliedAdapter extends JobFeedAdapter<Match> implements ConfirmDeleteAppliedMatchDialog.ConfirmDeleteAppliedMatchListener, ContractFragment.ContractFragmentCloseListener
{
    public static class AppliedAdapterViewHolder extends JobFeedAdapter.JobFeedAdapterViewHolder<Match> {
        private TextView profileLabel, parentStudentName;
        private ImageView profilePicture;
        private Button applyButton, cancelButton;
        private View inspectionPhase;

        public AppliedAdapterViewHolder(View itemView, boolean isDetailedView, final Context context, final AppliedAdapter currentAppliedAdapterInstance) {
            super(itemView, isDetailedView, context);

            profileLabel        = itemView.findViewById(R.id.profileLabel);
            parentStudentName   = itemView.findViewById(R.id.name);
            profilePicture      = itemView.findViewById(R.id.profilePicture);
            inspectionPhase     = itemView.findViewById(R.id.shortProfile);
            applyButton         = itemView.findViewById(R.id.applyButton);
            cancelButton        = itemView.findViewById(R.id.cancelButton);

            inspectionPhase.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    Match match                     = (Match)view.getTag(R.id.MATCH_TAG);
                    JobFeedResponse jobFeedResponse = (JobFeedResponse)match.getJobData()[0];

                    Functions.loadOthersProfile(context, false, jobFeedResponse.getUserProfile()[0].getFullName(), jobFeedResponse.getUserId(), match.getCurrentStatus(), match.get_id());
                }
            });

            applyButton.setVisibility(View.INVISIBLE);

            cancelButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    ConfirmDeleteAppliedMatchDialog confirmDeleteAppliedMatchDialog = ConfirmDeleteAppliedMatchDialog.createNewInstance(currentAppliedAdapterInstance, currentAppliedAdapterInstance.arrayList.get(getAdapterPosition()).get_id(), getAdapterPosition());
                    confirmDeleteAppliedMatchDialog.show(((Activity)context).getFragmentManager(), "ConfirmDeleteMatchDialog");
                }
            });

            detailsButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    String matchId = (String)view.getTag();
                    openUtilities(context, JobUtilitiesActivity.DETAILS, null, matchId, UtilitiesDetailsFragment.FEED_TYPE.TUTOR.APPLIED);
                }
            });

            calendarButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    String matchId = (String)view.getTag();
                    openUtilities(context, JobUtilitiesActivity.CALENDAR, null, matchId, UtilitiesDetailsFragment.FEED_TYPE.TUTOR.APPLIED);
                }
            });

            chatButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    String matchId = (String)view.getTag();
                    openUtilities(context, JobUtilitiesActivity.CHAT, null, matchId, UtilitiesDetailsFragment.FEED_TYPE.TUTOR.APPLIED);
                }
            });
        }
    }

    public AppliedAdapter(Context context)
    {
        super(context);
    }

    @Override
    public void setNoItemJobFeedTextView() {
        jobFeedTextView.setText("No matched jobs!\n\nPlease set your preferences from your profile to get job matches.");
        jobFeedTextView.setVisibility(View.VISIBLE);
    }

    //View

    @Override
    public AppliedAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_job_tutor_matches, parent, false);

        AppliedAdapterViewHolder rootViewHolder = null;

        if(viewType == JobFeedAdapter.VIEWTYPE_JOB_FEED_DETAILS){
            rootViewHolder = new AppliedAdapterViewHolder(rootView, true, parent.getContext(), this);
        }else if(viewType == JobFeedAdapter.VIEWTYPE_JOB_FEED_NO_DETAILS){
            rootViewHolder = new AppliedAdapterViewHolder(rootView, false, parent.getContext(), this);
        }

        return rootViewHolder;
    }

    @Override
    public void onBindViewHolder(JobFeedAdapterViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);

        AppliedAdapterViewHolder AppliedAdapterViewHolder = (AppliedAdapterViewHolder)holder;

        Match match = arrayList.get(position);
        JobFeedResponse jobFeedResponse = null;
        if(match.getJobData().length>0) {
            jobFeedResponse = match.getJobData()[0];

            AppliedAdapterViewHolder.profileLabel.setText("Parent/\nStudent");
            AppliedAdapterViewHolder.parentStudentName.setText(jobFeedResponse.getUserProfile()[0].getFullName());
            Functions.loadProfilePicture(context, jobFeedResponse.getUserId(), new Constants.PICTURE_PACKET((AppCompatActivity)context, AppliedAdapterViewHolder.profilePicture, null, Constants.PICTURE_PACKET.PROFILE_PICTURE_WIDTH, Constants.PICTURE_PACKET.PROFILE_PICTURE_HEIGHT));

            AppliedAdapterViewHolder.inspectionPhase.setTag(R.id.MATCH_TAG, match);

            AppliedAdapterViewHolder.applyButton.setEnabled(false);
        }
    }

    //JobFeedAdapterInterface

    @Override
    public void refreshOneItemFromNotifications(String id)
    {
        getJobFunction().getSingleAppliedJob(this, id);
    }

    @Override
    public void refreshView()
    {

    }

    @Override
    public void loadData()
    {
        getJobFunction().getAppliedJobs(this);
    }

//    @Override   //Overridden only in AppliedAdapter to remove self matches
//    public void setArrayList(ArrayList arrayList)
//    {
//        try
//        {
//            Log.e("ArrayList", "Set");
//            this.arrayList = new ArrayList<Match>(arrayList);
//
//            for(Match m : this.arrayList){
//                if(m.getJobData()[0].getUserId().equals(Constants.USER_ID_VALUE)){
//                    this.arrayList.remove(m);
//                }
//            }
//
//            notifyDataSetChanged();
//        }
//        catch(Exception e)
//        {
//            e.printStackTrace();
//        }
//    }

    //DialogInterfaceListener

    @Override
    public void onMatchDeletePositive(String matchId, int position) {
        Match match = arrayList.get(position);

        if(match.getCurrentStatus()==Constants.JOB_STATUS.JOB_STATUS_MATCH_POOL)
        {
            getContractFunction().cancelJobinPhase0(this, position, matchId);
        }
        else if(match.getCurrentStatus()==Constants.JOB_STATUS.JOB_STATUS_CONTRACT_REQUEST_SENT)
        {
            getContractFunction().cancelJobinPhase1(this, position, matchId);
        }
        else if(match.getCurrentStatus()==Constants.JOB_STATUS.JOB_STATUS_CONTRACT_REVIEW)
        {
            getContractFunction().cancelJobinPhase2(this, position, matchId);
        }
        else if(match.getCurrentStatus()==Constants.JOB_STATUS.JOB_STATUS_INSPECTION)
        {
            getContractFunction().cancelJobinPhase3(this, position, matchId);
        }
    }

    //ContractFragmentListener

    @Override
    public void handleContractFragmentClose(DialogInterface dialog) {
        loadData();
    }
}
