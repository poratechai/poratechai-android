package com.hyptastic.poratechai.adapters.profile;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.layout.FlexboxLayout;
import com.hyptastic.poratechai.models.review.Review;
import com.hyptastic.poratechai.retrofit.functions.ReviewFunction;
import com.hyptastic.poratechai.retrofit.interfaces.ReviewClient;
import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
import com.hyptastic.poratechai.utils.Constants;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by ISHRAK on 5/20/2018.
 */

public class ReviewsAdapter extends RecyclerView.Adapter {
    ArrayList<Review> arrayList;
    Context context;

    ReviewFunction reviewFunction;

    public static class ProfileReviewAdapterViewHolder extends RecyclerView.ViewHolder{
        TextView reviewerTextView, dateTextView;
        RatingBar ratingBar;
        LinearLayout reviewsSection;
        FlexboxLayout flexboxLayout;

        ProfileReviewAdapterViewHolder(View itemView) {
            super(itemView);

            reviewerTextView    = (TextView)itemView.findViewById(R.id.reviewerNameTextView);
            dateTextView        = (TextView)itemView.findViewById(R.id.dateTextView);

            ratingBar           = (RatingBar)itemView.findViewById(R.id.ratingBar);
            reviewsSection      = (LinearLayout)itemView.findViewById(R.id.reviewsSection);
        }
    }

    public ReviewsAdapter(Context context){
        this.arrayList = new ArrayList();
        this.context = context;

        this.reviewFunction = new ReviewFunction(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, ReviewClient.class), context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = (View) LayoutInflater.from(context).inflate(R.layout.listitem_reviews, parent, false);

        ProfileReviewAdapterViewHolder viewHolder = new ProfileReviewAdapterViewHolder(rootView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ProfileReviewAdapterViewHolder viewHolder = (ProfileReviewAdapterViewHolder)holder;

        try{
            viewHolder.reviewerTextView.setText(arrayList.get(position).getReviewerName());
            viewHolder.dateTextView.setText(Constants.outputDateFormat.format(Constants.dateFormat.parse(arrayList.get(position).getUpdatedAt())));

            viewHolder.ratingBar.setRating((float)(arrayList.get(position).getRating()));

            viewHolder.flexboxLayout = new FlexboxLayout((Activity)context, new ArrayList<String>(Arrays.asList(arrayList.get(position).getReviewTags())), viewHolder.reviewsSection, (int)(0.8*Constants.SCREEN_WIDTH), false);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public void loadReviews(String userId, boolean tutorReview){
        if (tutorReview) {
            reviewFunction.getReviewsForTutor(this, userId);
        }else{
            reviewFunction.getReviewsForParent(this, userId);
        }
    }

    public void setReviewList(ArrayList<Review> arrayList){
        this.arrayList = arrayList;
//        setRating();
        notifyDataSetChanged();

        for(Review review: arrayList){
            Log.e("Review", review.toString());
        }

        if(arrayList.size()==0){
            //No one has reviewed you yet
        }
    }

    public void setRating(){
        double ratingSum = 3;

        for(Review r : this.arrayList){
            ratingSum = ratingSum + r.getRating();
        }

        int rating = (int)Math.round(ratingSum/this.arrayList.size());
    }
}
