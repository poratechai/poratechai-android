package com.hyptastic.poratechai.adapters.job_feed.parent_student;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.activities.profile.ProfileViewActivity;
import com.hyptastic.poratechai.adapters.job_feed.JobFeedAdapter;
import com.hyptastic.poratechai.fragments.job_feed.parent_student.dialog.ConfirmAcceptInspectionTutorDialog;
import com.hyptastic.poratechai.fragments.job_feed.parent_student.dialog.ConfirmDeleteContractDialog;
import com.hyptastic.poratechai.fragments.job_feed.parent_student.dialog.ConfirmDeleteInspectionTutorDialog;
import com.hyptastic.poratechai.layout.FlexboxLayout;
import com.hyptastic.poratechai.models.job_feed.JobFeedResponse;
import com.hyptastic.poratechai.models.job_feed.Match;
import com.hyptastic.poratechai.models.users.User;
import com.hyptastic.poratechai.retrofit.functions.ContractFunction;
import com.hyptastic.poratechai.retrofit.functions.JobFunction;
import com.hyptastic.poratechai.retrofit.interfaces.ContractClient;
import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
import com.hyptastic.poratechai.utils.Constants;
import com.hyptastic.poratechai.utils.Functions;

import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ISHRAK on 3/17/2018.
 */

public class ContractRequestsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Serializable,
        ConfirmDeleteContractDialog.ConfirmDeleteContractListener,
        ConfirmDeleteInspectionTutorDialog.ConfirmDeleteInspectionTutorListener,
        ConfirmAcceptInspectionTutorDialog.ConfirmAcceptInspectionTutorListener {
    private Context context;
    private JobFunction jobFunction;
    private ContractClient contractClient;
    private ContractFunction contractFunction;
    private static JobFeedResponse jobDetails;
    private static List<Match> contractList;

    private Match inspectionMatch;
    private boolean inspectionPhase;

    public static class ViewHolderJobDetails extends RecyclerView.ViewHolder
    {
        private TextView jobTitleTextView, budgetTextView, weeklyTextView, subjectsOrCategoriesTextView, locationTextView;
        private LinearLayout subjectsOrCategoriesFlexBoxLayout;
        private Context context;

        FlexboxLayout flexboxLayout;

        public ViewHolderJobDetails(View itemView, final Context context, final ContractRequestsAdapter currentContractRequestsAdapterInstance) {
            super(itemView);

            jobTitleTextView                        = itemView.findViewById(R.id.jobTitleTextView);
            subjectsOrCategoriesTextView            = itemView.findViewById(R.id.subjectsOrCategoriesTextView);
            subjectsOrCategoriesFlexBoxLayout       = itemView.findViewById(R.id.subjectsOrCategoriesFlexBoxLayout);
            budgetTextView                          = itemView.findViewById(R.id.budgetValueTextView);
            weeklyTextView                          = itemView.findViewById(R.id.weeklyValueTextView);
            locationTextView                        = itemView.findViewById(R.id.locationValueTextView);

            this.context = context;

            flexboxLayout = new FlexboxLayout((Activity)context, 0);
        }
    }

    public static class ViewHolderTutorContracts extends RecyclerView.ViewHolder
    {
        private TextView tutorName, detailsTextView, daysPerWeekTextView, tutorSalaryTextView, tutorMessageTextView;
        private ImageView profilePicture;
        private Button acceptTutorButton, rejectTutorButton;
        private Context context;

        public ViewHolderTutorContracts(View itemView, final Context context, final ContractRequestsAdapter currentContractRequestsAdapterInstance, final ContractFunction contractFunction) {
            super(itemView);

            tutorName               = itemView.findViewById(R.id.tutorName);
            detailsTextView         = itemView.findViewById(R.id.detailsTextView);
            daysPerWeekTextView     = itemView.findViewById(R.id.tutorDaysPerWeekValueTextView);
            tutorSalaryTextView     = itemView.findViewById(R.id.tutorSalaryValueTextView);
            tutorMessageTextView    = itemView.findViewById(R.id.tutorMessageTextView);
            profilePicture          = itemView.findViewById(R.id.profilePicture);
            acceptTutorButton       = itemView.findViewById(R.id.acceptTutorButton);
            rejectTutorButton       = itemView.findViewById(R.id.rejectTutorButton);

            this.context = context;

            tutorName.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    visitTutorProfile(currentContractRequestsAdapterInstance);
                }
            });
            detailsTextView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    visitTutorProfile(currentContractRequestsAdapterInstance);
                }
            });
            profilePicture.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    visitTutorProfile(currentContractRequestsAdapterInstance);
                }
            });
            acceptTutorButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    ConfirmAcceptInspectionTutorDialog confirmAcceptInspectionTutorDialog = ConfirmAcceptInspectionTutorDialog.createNewInstance(currentContractRequestsAdapterInstance, contractList.get(getAdapterPosition()-1).get_id()); //getAdapterPosition()-1, because, the first item is the job details
                    confirmAcceptInspectionTutorDialog.show(((Activity)context).getFragmentManager(), "ConfirmAcceptInspectionTutorDialog");

                    Toast.makeText(context, "Accept Tutor: " + String.valueOf(getAdapterPosition()), Toast.LENGTH_LONG).show();
                }
            });
            rejectTutorButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    if(view.getTag()!=null){
                        String matchId = (String)view.getTag();

                        ConfirmDeleteInspectionTutorDialog confirmDeleteInspectionTutorDialog = ConfirmDeleteInspectionTutorDialog.createNewInstance(currentContractRequestsAdapterInstance, matchId);
                        confirmDeleteInspectionTutorDialog.show(((Activity)context).getFragmentManager(), "ConfirmDeleteInspectionTutorDialog");
                    }else{
                        ConfirmDeleteContractDialog confirmDeleteContractDialog = ConfirmDeleteContractDialog.createNewInstance(currentContractRequestsAdapterInstance, getAdapterPosition()-1, contractList.get(getAdapterPosition()-1).get_id());
                        confirmDeleteContractDialog.show(((Activity)context).getFragmentManager(), "ConfirmDeleteContractDialog");
                    }
                }
            });
        }

        private void visitTutorProfile(ContractRequestsAdapter currentContractRequestsAdapter){
            User tutorProfile = contractList.get(getAdapterPosition()-1).getTutorProfile()[0];
            Match match = contractList.get(getAdapterPosition()-1);

            String matchId = match.get_id();
            int contractStatus = match.getCurrentStatus();

            Functions.loadOthersProfile(context,true, tutorProfile.getFullName(), tutorProfile.get_id(), contractStatus, matchId);

            Toast.makeText(context, "Tutor Profile: " + String.valueOf(getAdapterPosition()), Toast.LENGTH_LONG).show();
        }
    }

    public ContractRequestsAdapter(Context context, JobFunction jobFunction, JobFeedResponse jobDetails)
    {
        contractList = new ArrayList<Match>();
        this.context = context;
        this.jobFunction = jobFunction;
        this.jobDetails = jobDetails;
        this.contractList = jobDetails.getMatchedTutors();

        contractClient = RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, ContractClient.class);
        contractFunction = new ContractFunction(contractClient, context);

        inspectionPhase = false;

        for(Match m : contractList)
        {
            if(m.getCurrentStatus()==3)
            {
                inspectionPhase = true;
                break;
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(position==0)
        {
            return Constants.VIEW_TYPE.JOB_DETAILS;
        }
        else
        {
            return Constants.VIEW_TYPE.TUTOR_CONTRACT;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView;

        switch(viewType)
        {
            case 0:
                rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_job_basic_contract_requests, parent, false);

                ViewHolderJobDetails viewHolderJobDetails = new ViewHolderJobDetails(rootView, context, this);
                return viewHolderJobDetails;
            case 1:
                rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_job_tutor_contract, parent, false);

                ViewHolderTutorContracts viewHolderTutorContracts = new ViewHolderTutorContracts(rootView, context, this, contractFunction);
                return viewHolderTutorContracts;
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(position==0)
        {
            ViewHolderJobDetails viewHolderJobDetails = (ViewHolderJobDetails)holder;

            viewHolderJobDetails.jobTitleTextView.setText(jobDetails.getJobTitle());
            viewHolderJobDetails.budgetTextView.setText(String.valueOf(jobDetails.getSalary()) + " BDT");
            viewHolderJobDetails.weeklyTextView.setText(String.valueOf(jobDetails.getDaysPerWeek()) + " day(s)");

            if(jobDetails.getCategory().equals("Academics")){
                viewHolderJobDetails.subjectsOrCategoriesTextView.setText("Subjects:");
                viewHolderJobDetails.flexboxLayout.refreshFlexbox(viewHolderJobDetails.subjectsOrCategoriesFlexBoxLayout,
                        (jobDetails.getSubjects()==null || jobDetails.getSubjects().length == 0 ? new ArrayList<String>(Arrays.asList(new String[]{"N/A"})) : new ArrayList<String>(Arrays.asList(jobDetails.getSubjects()))),
                        (int)(Constants.SCREEN_WIDTH*0.65), false);
            }else{
                viewHolderJobDetails.subjectsOrCategoriesTextView.setText("Category:");
                viewHolderJobDetails.flexboxLayout.refreshFlexbox(viewHolderJobDetails.subjectsOrCategoriesFlexBoxLayout,
                        (new ArrayList<String>(Arrays.asList(new String[]{jobDetails.getSubcategory()}))), (int)(Constants.SCREEN_WIDTH*0.65), false);
            }

            inspectionMatch = Functions.checkInspectionPhase(jobDetails);
            if(inspectionMatch!=null)
            {
                inspectionPhase = true;
            }

            viewHolderJobDetails.locationTextView.setText(jobDetails.getLocation());
        }
        else
        {
            ViewHolderTutorContracts viewHolderTutorContracts = (ViewHolderTutorContracts)holder;

            User tutorProfile = contractList.get(position-1).getTutorProfile()[0];  //position-1, because the first item is the job details

            Functions.loadProfilePicture(context, tutorProfile.get_id(), new Constants.PICTURE_PACKET((Activity)context, viewHolderTutorContracts.profilePicture, null, Constants.PICTURE_PACKET.PROFILE_PICTURE_WIDTH, Constants.PICTURE_PACKET.PROFILE_PICTURE_HEIGHT));
            viewHolderTutorContracts.tutorName.setText(tutorProfile.getFullName());
            viewHolderTutorContracts.daysPerWeekTextView.setText(String.valueOf(contractList.get(position-1).getDaysPerWeek()) + " day(s)");
            viewHolderTutorContracts.tutorSalaryTextView.setText(String.valueOf(contractList.get(position-1).getSalaryOffer()) + " BDT");
            viewHolderTutorContracts.tutorMessageTextView.setText(String.valueOf(contractList.get(position-1).getTutorMessage()));

            if(contractList.get(position-1).getCurrentStatus()==1)
            {
                contractFunction.acceptContractReviewRequest(contractList.get(position-1).get_id());
            }

            if(inspectionPhase)
            {
                viewHolderTutorContracts.acceptTutorButton.setEnabled(false);
                viewHolderTutorContracts.rejectTutorButton.setTag(inspectionMatch.get_id());
            }
        }
    }

    @Override
    public int getItemCount() {
        return contractList.size()+1;
    }

    @Override
    public void onAcceptInspectionTutorPositive(String matchId) {
        Log.e("Accept Handler", "Match Id : " + matchId);

        contractFunction.acceptTutor(this, matchId);
    }

    @Override
    public void onContractDeletePositive(int position, String matchId) {
        Log.e("Delete Handler", "Position: " + position + " MatchId: " + matchId);

        contractFunction.cancelJobinPhase2(this, position, matchId);
    }

    public void removeItemFromList(int position)
    {
        contractList.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public void onInspectionTutorDeletePositive(String matchId) {
        contractFunction.cancelJobinPhase3(this, matchId);
    }

    //ContractFunction/cancelJobInPhase3 is called from the this/onInspectionTutorDeletePositive method
    //Refresh is called from ContractFunction/cancelJobInPhase3
    public void refresh()
    {
        jobFunction.getSinglePostedJob(this, jobDetails.get_id());
    }

    //JobFunction/getSinglePostedJob is called from this/refresh method, which loads the details for the current job
    //setJobDetails is called from the JobFunction/getSinglePostedJob after loading the details of the given job id
    public void setJobDetails(JobFeedResponse jobDetails)
    {
        this.jobDetails = jobDetails;
        this.contractList = jobDetails.getMatchedTutors();

        notifyDataSetChanged();
    }
}
