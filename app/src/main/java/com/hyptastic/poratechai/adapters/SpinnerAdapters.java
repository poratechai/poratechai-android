package com.hyptastic.poratechai.adapters;

import android.app.Activity;
import android.content.Context;
import android.widget.ArrayAdapter;

import com.hyptastic.poratechai.utils.Constants;

import java.util.Hashtable;

/**
 * Created by root on 2/3/18.
 */

public class SpinnerAdapters extends Activity{
    private Hashtable<String, ArrayAdapter<CharSequence>> subCategoryAdaptersTable;

    Context mContext;

    public SpinnerAdapters(Context mContext){
        this.mContext = mContext;
    }
    public void init(){
        subCategoryAdaptersTable = new Hashtable<String, ArrayAdapter<CharSequence>>();

        for(String key : Constants.SYSTEM_ARRAY.subcategories.keySet()){
            ArrayAdapter subcategoriesAdapter = new ArrayAdapter(mContext, android.R.layout.simple_spinner_item, Constants.SYSTEM_ARRAY.subcategories.get(key));
            subcategoriesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            subCategoryAdaptersTable.put(key, subcategoriesAdapter);
        }
    }

    public ArrayAdapter<CharSequence> getArrayAdapter(String category){
        return subCategoryAdaptersTable.get(category);
    }
}
