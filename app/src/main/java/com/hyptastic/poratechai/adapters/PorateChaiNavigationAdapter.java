package com.hyptastic.poratechai.adapters;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;

import com.hyptastic.poratechai.fragments.PorateChaiNavigationFragment;

import java.util.ArrayList;

/**
 * Created by ISHRAK on 2/13/2019.
 */

public class PorateChaiNavigationAdapter extends FragmentStatePagerAdapter {
    private ArrayList<PorateChaiNavigationFragment> pageItems;
    private String[] titles;

    public PorateChaiNavigationAdapter(FragmentManager fm, ArrayList<PorateChaiNavigationFragment> pageItems, String[] titles) {
        super(fm);
        this.pageItems = pageItems;
        this.titles = titles;
    }

    @Override
    public Fragment getItem(int position) {
        return pageItems.get(position);
    }

    @Override
    public int getItemPosition(Object object) {
        return PagerAdapter.POSITION_NONE;
    }

    @Override
    public int getCount() {
        return pageItems.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public Parcelable saveState() {
        return null;
    }
}
