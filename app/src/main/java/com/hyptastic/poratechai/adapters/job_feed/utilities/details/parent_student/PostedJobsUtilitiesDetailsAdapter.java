package com.hyptastic.poratechai.adapters.job_feed.utilities.details.parent_student;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.adapters.job_feed.JobFeedAdapter;
import com.hyptastic.poratechai.adapters.job_feed.parent_student.PostedJobsAdapter;

/**
 * Created by ISHRAK on 6/4/2019.
 */

public class PostedJobsUtilitiesDetailsAdapter extends PostedJobsAdapter {
    String jobId;

    public PostedJobsUtilitiesDetailsAdapter(Context context, String jobId){
        super(context);

        this.jobId = jobId;
    }

    @Override
    public PostedJobsAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return super.onCreateViewHolder(parent, viewType);
    }

    @Override
    public int getItemViewType(int position) {
        return JobFeedAdapter.VIEWTYPE_JOB_FEED_DETAILS;
    }

    @Override
    public void onBindViewHolder(JobFeedAdapterViewHolder holder, int position) {
        try{
            super.onBindViewHolder(holder, position);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void loadData() {
        Log.e("Details Adapter", jobId);
        getJobFunction().getSinglePostedJob(this, jobId);
    }

    @Override
    public void removeItem(int position) {
        super.removeItem(position);

        ((AppCompatActivity)context).finish();
    }

    @Override
    public int getItemCount() {
        return 1;
    }
}
