package com.hyptastic.poratechai.adapters.job_feed;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.activities.PorateChaiActivity;
import com.hyptastic.poratechai.fragments.job_feed.DialogFragmentReviews;
import com.hyptastic.poratechai.layout.FlexboxLayout;
import com.hyptastic.poratechai.models.job_feed.JobFeedResponse;
import com.hyptastic.poratechai.models.review.Review;
import com.hyptastic.poratechai.models.users.User;
import com.hyptastic.poratechai.utils.Constants;
import com.hyptastic.poratechai.utils.Functions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by ISHRAK on 4/13/2018.
 */

public abstract class JobFeedActiveHistoryAdapter extends JobFeedAdapter<JobFeedResponse>
        implements Serializable, DialogFragmentReviews.DialogFragmentReviewsCloseListener{
    protected Context context;

    public static class JobFeedActiveHistoryAdapterViewHolder extends JobFeedAdapter.JobFeedAdapterViewHolder<JobFeedResponse> {
        private TextView profileLabel, name;
        private ImageView profilePicture;
        private View shortProfile;
        private RatingBar ratingBar;
        private TextView reviewTitleTextView;
        private LinearLayout reviewsFlexBoxLayout;
        private Button addReviewsButton;

        private FlexboxLayout flexboxLayout;

        private float x = 0;

        private JobFeedActiveHistoryAdapter currentJobFeedActiveHistoryAdapterInstance;

        public JobFeedActiveHistoryAdapterViewHolder(View v, boolean areUtilitiesVisible, final Context context, final JobFeedAdapter<JobFeedResponse> currentJobFeedAdapterInstance)
        {
            super(v, areUtilitiesVisible, context);

            profilePicture              = v.findViewById(R.id.profilePicture);
            profileLabel                = v.findViewById(R.id.profileLabel);
            name                        = v.findViewById(R.id.name);
            shortProfile                = v.findViewById(R.id.shortProfile);
            reviewTitleTextView         = v.findViewById(R.id.reviewTitleTextView);
            ratingBar                   = v.findViewById(R.id.ratingBar);
            reviewsFlexBoxLayout        = v.findViewById(R.id.reviewsFlexBoxLayout);
            addReviewsButton            = v.findViewById(R.id.addReviewsButton);

            shortProfile.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    User user = (User)view.getTag();

                    if(Functions.isTutor()){
                        Functions.loadOthersProfile(context, false, user.getFullName(), user.get_id(), Constants.JOB_STATUS.JOB_STATUS_ACTIVE, "");
                    }else{
                        Functions.loadOthersProfile(context, true, user.getFullName(), user.get_id(), Constants.JOB_STATUS.JOB_STATUS_ACTIVE, "");
                    }
                }
            });

            ratingBar.setOnTouchListener(Functions.getNonSwipeableViewTouchListener());

            ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener(){
                @Override
                public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                    Log.e("Rating Bar", "Changed");
                }
            });

            addReviewsButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    try{
                        JobFeedResponse jobFeedResponse = ((JobFeedResponse)view.getTag());

                        String userId = null, reviewId = null, jobId = null;
                        Review review = null;
                        int rating = 0;
                        String[] reviewTags = new String[]{};

                        if(Functions.isTutor()){
                            userId = jobFeedResponse.getUserId();
                            reviewId = jobFeedResponse.getReviewIdParent();

                            if(jobFeedResponse.getReviewParent()!=null && jobFeedResponse.getReviewParent().length>0){
                                review = jobFeedResponse.getReviewParent()[0];

                                rating = (int)review.getRating();
                                reviewTags = review.getReviewTags();
                            }
                        }else{
                            userId = jobFeedResponse.getTutorId();
                            reviewId = jobFeedResponse.getReviewIdTutor();

                            if(jobFeedResponse.getReviewTutor()!=null && jobFeedResponse.getReviewTutor().length>0){
                                review = jobFeedResponse.getReviewTutor()[0];

                                rating = (int)review.getRating();
                                reviewTags = review.getReviewTags();
                            }
                        }

                        jobId = (String)((JobFeedResponse)view.getTag()).get_id();

                        DialogFragmentReviews dialogFragmentReviews = DialogFragmentReviews.createInstance(currentJobFeedActiveHistoryAdapterInstance, userId, reviewId, jobId, rating, reviewTags);
                        dialogFragmentReviews.show(((AppCompatActivity)context).getSupportFragmentManager(), "DialogFragmentReviews");
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }

        void setCurrentJobFeedActiveHistoryAdapterInstance(JobFeedActiveHistoryAdapter currentJobFeedActiveHistoryAdapterInstance){
            this.currentJobFeedActiveHistoryAdapterInstance = currentJobFeedActiveHistoryAdapterInstance;
        }
    }

    public JobFeedActiveHistoryAdapter(Context context)
    {
        super(context);

        this.context = context;
    }

    @Override
    public void onBindViewHolder(JobFeedAdapterViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);

        Log.e("onBindViewHolder", "JobFeedActiveHistoryAdapter");

        JobFeedActiveHistoryAdapter.JobFeedActiveHistoryAdapterViewHolder jobFeedActiveHistoryAdapterViewHolder = (JobFeedActiveHistoryAdapter.JobFeedActiveHistoryAdapterViewHolder)holder;
        jobFeedActiveHistoryAdapterViewHolder.setCurrentJobFeedActiveHistoryAdapterInstance(this);

        JobFeedResponse jobFeedResponse = (JobFeedResponse)arrayList.get(position);
        User profile = null;
        Review review = null;

        if(Functions.isTutor())
        {
            jobFeedActiveHistoryAdapterViewHolder.reviewTitleTextView.setText("Review for parent/student");

            if(jobFeedResponse.getUserProfile()!=null && jobFeedResponse.getUserProfile().length>0)
            {
                profile = jobFeedResponse.getUserProfile()[0];
            }
            if(jobFeedResponse.getReviewParent()!=null && jobFeedResponse.getReviewParent().length>0)
            {
                review = jobFeedResponse.getReviewParent()[0];
            }
        }
        else
        {
            jobFeedActiveHistoryAdapterViewHolder.reviewTitleTextView.setText("Review for tutor");

            if(jobFeedResponse.getTutorProfile()!=null && jobFeedResponse.getTutorProfile().length>0)
            {
                profile = jobFeedResponse.getTutorProfile()[0];
            }
            if(jobFeedResponse.getReviewTutor()!=null && jobFeedResponse.getReviewTutor().length>0)
            {
                review = jobFeedResponse.getReviewTutor()[0];
            }
        }

        if(profile!=null) {
            jobFeedActiveHistoryAdapterViewHolder.shortProfile.setTag(profile);

            if(Functions.isTutor()) {
                jobFeedActiveHistoryAdapterViewHolder.profileLabel.setText("Parent/\nStudent");
            }else{
                jobFeedActiveHistoryAdapterViewHolder.profileLabel.setText("Hired\nTutor");
            }
            Functions.loadProfilePicture(context, profile.get_id(), new Constants.PICTURE_PACKET((Activity)context, jobFeedActiveHistoryAdapterViewHolder.profilePicture, null, Constants.PICTURE_PACKET.PROFILE_PICTURE_WIDTH, Constants.PICTURE_PACKET.PROFILE_PICTURE_HEIGHT));
            jobFeedActiveHistoryAdapterViewHolder.name.setText(profile.getFullName());

            jobFeedActiveHistoryAdapterViewHolder.reviewTitleTextView.setText("Review for " + profile.getFullName());
        }

        ArrayList<String> reviewTags = new ArrayList<String>(1);

        //To be used when addReviewsButton is clicked
        jobFeedActiveHistoryAdapterViewHolder.addReviewsButton.setTag(jobFeedResponse);

        if(review!=null)
        {
            jobFeedActiveHistoryAdapterViewHolder.ratingBar.setRating((float)review.getRating());

            if(review.getReviewTags().length==0){
                reviewTags.add("No Review Yet");
            }else{
                reviewTags = new ArrayList<String>(Arrays.asList(review.getReviewTags()));
            }
        }else{
            reviewTags.add("No Review Yet");
        }

        jobFeedActiveHistoryAdapterViewHolder.flexboxLayout = new FlexboxLayout((Activity)context, reviewTags,
                jobFeedActiveHistoryAdapterViewHolder.reviewsFlexBoxLayout, (int)(0.8*Constants.SCREEN_WIDTH),
                false);
    }

    @Override
    public void dialogClosed() {
        loadData();
    }
}
