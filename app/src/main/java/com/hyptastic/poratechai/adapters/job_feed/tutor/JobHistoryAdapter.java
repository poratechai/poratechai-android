package com.hyptastic.poratechai.adapters.job_feed.tutor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.activities.job_feed.JobUtilitiesActivity;
import com.hyptastic.poratechai.adapters.job_feed.JobFeedActiveHistoryAdapter;
import com.hyptastic.poratechai.adapters.job_feed.JobFeedAdapter;
import com.hyptastic.poratechai.fragments.job_feed.utilities.UtilitiesDetailsFragment;

/**
 * Created by ISHRAK on 4/7/2018.
 */

public class JobHistoryAdapter extends JobFeedActiveHistoryAdapter {
    public static class JobHistoryAdapterViewHolder extends JobFeedActiveHistoryAdapter.JobFeedActiveHistoryAdapterViewHolder {
        private Button archiveButton;

        public JobHistoryAdapterViewHolder(View v, boolean isDetailedView, final Context context, final JobHistoryAdapter currentJobHistoryAdapterInstance) {
            super(v, isDetailedView, context, currentJobHistoryAdapterInstance);

            archiveButton = v.findViewById(R.id.archiveButton);

            detailsButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    String jobId = (String)view.getTag();
                    openUtilities(context, JobUtilitiesActivity.DETAILS, jobId, null, UtilitiesDetailsFragment.FEED_TYPE.TUTOR.JOB_HISTORY);
                }
            });

            calendarButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    String jobId = (String)view.getTag();
                    openUtilities(context, JobUtilitiesActivity.CALENDAR, jobId, null, UtilitiesDetailsFragment.FEED_TYPE.TUTOR.JOB_HISTORY);
                }
            });

            chatButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    String jobId = (String)view.getTag();
                    openUtilities(context, JobUtilitiesActivity.CHAT, jobId, null, UtilitiesDetailsFragment.FEED_TYPE.TUTOR.JOB_HISTORY);
                }
            });
        }
    }

    public JobHistoryAdapter(Context context)
    {
        super(context);
    }

    @Override
    public void setNoItemJobFeedTextView() {
        if(jobFeedTextView != null){
            jobFeedTextView.setText("No Active Job has been archived.");
            jobFeedTextView.setVisibility(View.VISIBLE);
        }
    }

    //JobFeedAdapterInterface

    @Override
    public void refreshOneItemFromNotifications(String id)
    {
        getJobFunction().getSingleJobHistory(this, id);
    }

    @Override
    public void refreshView()
    {

    }

    @Override
    public void loadData()
    {
        getJobFunction().getJobHistory(this);
    }

    //View

    @Override
    public JobHistoryAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = (View) LayoutInflater.from(context).inflate(R.layout.listitem_job_active, parent, false);

        JobHistoryAdapterViewHolder rootViewHolder = null;

        if(viewType == JobFeedAdapter.VIEWTYPE_JOB_FEED_DETAILS){
            rootViewHolder = new JobHistoryAdapterViewHolder(rootView, true, context, this);
        }else if(viewType == JobFeedAdapter.VIEWTYPE_JOB_FEED_NO_DETAILS){
            rootViewHolder = new JobHistoryAdapterViewHolder(rootView, false, context, this);
        }

        return rootViewHolder;
    }

    @Override
    public void onBindViewHolder(JobFeedAdapterViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);

        JobHistoryAdapterViewHolder jobHistoryAdapterViewHolder = (JobHistoryAdapterViewHolder)holder;

        jobHistoryAdapterViewHolder.archiveButton.getLayoutParams().height = 0;
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}
