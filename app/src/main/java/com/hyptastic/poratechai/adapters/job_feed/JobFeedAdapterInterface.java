package com.hyptastic.poratechai.adapters.job_feed;

import java.util.ArrayList;

/**
 * Created by ISHRAK on 4/11/2018.
 */

public interface JobFeedAdapterInterface<T> {
    void removeOneItemFromNotifications(String id);
    void refreshOneItemFromNotifications(String id);
    void refreshOneItemFromServer(T item);

    void refreshView();
    void loadData();

    void setArrayList(ArrayList arrayList);
    void removeItem(int position);
}
