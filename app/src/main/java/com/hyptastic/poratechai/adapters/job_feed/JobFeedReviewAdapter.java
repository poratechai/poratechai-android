//package com.hyptastic.poratechai.adapters.job_feed;
//
//import android.content.Context;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.RatingBar;
//import android.widget.TextView;
//
//import com.hyptastic.poratechai.R;
//import com.hyptastic.poratechai.models.review.Review;
//import com.hyptastic.poratechai.retrofit.functions.ReviewFunction;
//import com.hyptastic.poratechai.retrofit.interfaces.ReviewClient;
//import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
//import com.hyptastic.poratechai.utils.Constants;
//
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.Iterator;
//
///**
// * Created by ISHRAK on 5/20/2018.
// */
//
//public class JobFeedReviewAdapter extends RecyclerView.Adapter {
//
//    ArrayList<Review> arrayList;
//    Context context;
//    RatingBar ratingBar;
//    ReviewFunction reviewFunction;
//
//    public static class JobFeedReviewAdapterViewHolder extends RecyclerView.ViewHolder{
//        TextView reviewTextView, reviewTimestampTextView;
//
//        public JobFeedReviewAdapterViewHolder(View itemView) {
//            super(itemView);
//
//            reviewTextView = itemView.findViewById(R.id.reviewTextView);
//            reviewTimestampTextView = itemView.findViewById(R.id.reviewTimestampTextView);
//        }
//    }
//
//    public JobFeedReviewAdapter(Context context, RatingBar ratingBar){
//        this.arrayList = new ArrayList();
//        this.context = context;
//        this.ratingBar = ratingBar;
//
//        this.reviewFunction = new ReviewFunction(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, ReviewClient.class), context);
//    }
//
//    @Override
//    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View rootView = (View) LayoutInflater.from(context).inflate(R.layout.listitem_job_feed_dialog_profile_review, parent, false);
//
//        JobFeedReviewAdapterViewHolder viewHolder = new JobFeedReviewAdapterViewHolder(rootView);
//
//        return viewHolder;
//    }
//
//    @Override
//    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
//        JobFeedReviewAdapterViewHolder viewHolder = (JobFeedReviewAdapterViewHolder)holder;
//
//        try{
////            viewHolder.reviewTextView.setText(arrayList.get(position).getReview());
//            viewHolder.reviewTimestampTextView.setText(Constants.outputDateFormat.format(Constants.dateFormat.parse(arrayList.get(position).getUpdatedAt())));
//        }catch(Exception e){
//            Log.e("Exception", e.getMessage());
//        }
//    }
//
//    @Override
//    public int getItemCount() {
//        return arrayList.size();
//    }
//
//    public void loadReviews(String userId, boolean tutorReview){
//        if (tutorReview) {
//            reviewFunction.getReviewsForTutor(this, userId);
//        }else{
//            reviewFunction.getReviewsForParent(this, userId);
//        }
//    }
//
//    public void setReviewList(ArrayList arrayList){
//        this.arrayList = arrayList;
//
//        if(arrayList.size()==0){
////            arrayList.add(new Review("", 0.0, "No one has reviewed yet", ""));
//            notifyDataSetChanged();
//        }else{
//            setRating();
//            setTopThreeReviews();
//            notifyDataSetChanged();
//        }
//    }
//
//    public void setRating(){
//        double ratingSum = 0;
//
//        for(Review r : this.arrayList){
//            ratingSum = ratingSum + r.getRating();
//        }
//
//        int rating = (int)Math.round(ratingSum/this.arrayList.size());
//        ratingBar.setProgress(rating);
//    }
//
//    public void setTopThreeReviews(){
//        Collections.sort(this.arrayList, new Comparator<Review>(){
//            @Override
//            public int compare(Review review, Review t1) {
//                try{
//                    return Constants.dateFormat.parse(t1.getUpdatedAt()).compareTo(Constants.dateFormat.parse(review.getUpdatedAt()));
//                }catch(Exception e){
//                    Log.e("Date Parse", e.getMessage());
//                }
//
//                return 0;
//            }
//        });
//
//        int counter = 0;
//        Iterator<Review> reviewListIterator = arrayList.listIterator();
//
//        while(reviewListIterator.hasNext()){
////            if (reviewListIterator.next().getReview().equals("")) {
//                reviewListIterator.remove();
////            }
//        }
//
////        reviewListIterator = arrayList.listIterator();
//
////        while(reviewListIterator.hasNext()){
////            reviewListIterator.next();
////            if(counter>=3){
////                reviewListIterator.remove();
////            }
////            counter++;
////        }
//    }
//}
