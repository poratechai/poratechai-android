package com.hyptastic.poratechai.adapters.job_feed.utilities.details.parent_student;

import android.content.Context;
import android.util.Log;
import android.view.ViewGroup;

import com.hyptastic.poratechai.adapters.job_feed.JobFeedAdapter;
import com.hyptastic.poratechai.adapters.job_feed.parent_student.JobHistoryAdapter;

/**
 * Created by ISHRAK on 6/7/2019.
 */

public class JobHistoryUtilitiesDetailsAdapter extends JobHistoryAdapter {
    String jobId;

    public JobHistoryUtilitiesDetailsAdapter(Context context, String jobId){
        super(context);

        this.jobId = jobId;
    }

    @Override
    public JobHistoryAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return super.onCreateViewHolder(parent, viewType);
    }

    @Override
    public int getItemViewType(int position) {
        return JobFeedAdapter.VIEWTYPE_JOB_FEED_DETAILS;
    }

    @Override
    public void onBindViewHolder(JobFeedAdapterViewHolder holder, int position) {
        try{
            super.onBindViewHolder(holder, position);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void loadData() {
        Log.e("Details Adapter", jobId);
        getJobFunction().getSinglePostedJobHistory(this, jobId);
    }

    @Override
    public int getItemCount() {
        return 1;
    }
}
