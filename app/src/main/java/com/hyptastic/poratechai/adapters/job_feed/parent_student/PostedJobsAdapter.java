package com.hyptastic.poratechai.adapters.job_feed.parent_student;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hyptastic.poratechai.activities.job_feed.ContractRequestsActivity;
import com.hyptastic.poratechai.activities.job_feed.EditJobActivity;
import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.activities.job_feed.JobUtilitiesActivity;
import com.hyptastic.poratechai.adapters.job_feed.JobFeedAdapter;
import com.hyptastic.poratechai.fragments.job_feed.parent_student.dialog.ConfirmDeleteJobDialog;
import com.hyptastic.poratechai.fragments.job_feed.utilities.UtilitiesDetailsFragment;
import com.hyptastic.poratechai.models.job_feed.JobFeedResponse;
import com.hyptastic.poratechai.models.job_feed.Match;
import com.hyptastic.poratechai.utils.Constants;
import com.hyptastic.poratechai.utils.Functions;

/**
 * Created by ISHRAK on 4/12/2018.
 */

public class PostedJobsAdapter extends JobFeedAdapter<JobFeedResponse> implements ConfirmDeleteJobDialog.ConfirmDeleteJobListener {
    public static class PostedJobsAdapterViewHolder extends JobFeedAdapter.JobFeedAdapterViewHolder<JobFeedResponse> {
        private TextView profileLabel, tutorName;
        private Button contractsButton, editButton, deleteButton;
        private ImageView profilePicture;
        private View inspectionPhase;

        public PostedJobsAdapterViewHolder(View v, boolean isDetailedView, final Context context, final PostedJobsAdapter currentPostedJobsAdapterInstance)
        {
            super(v, isDetailedView, context);

            profilePicture              = v.findViewById(R.id.profilePicture);
            profileLabel                = v.findViewById(R.id.profileLabel);
            tutorName                   = v.findViewById(R.id.name);
            inspectionPhase             = v.findViewById(R.id.shortProfile);

            contractsButton             = v.findViewById(R.id.contractsButton);
            editButton                  = v.findViewById(R.id.editButton);
            deleteButton                = v.findViewById(R.id.deleteButton);

            inspectionPhase.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    Match inspectionMatch = (Match)view.getTag();

                    Functions.loadOthersProfile(context, true, inspectionMatch.getTutorProfile()[0].getFullName(), inspectionMatch.getTutorProfile()[0].get_id(), inspectionMatch.getCurrentStatus(), inspectionMatch.get_id());
                }
            });

            contractsButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    try{
                        JobFeedResponse jobFeedResponse = (JobFeedResponse)currentPostedJobsAdapterInstance.arrayList.get(getAdapterPosition());

                        if(jobFeedResponse.getMatchedTutors().size()>0)
                        {
                            Intent intent = new Intent(context, ContractRequestsActivity.class);

                            Bundle bundle = new Bundle();
                            bundle.putParcelable("Job", jobFeedResponse);

                            intent.putExtras(bundle);

                            context.startActivity(intent);
                        }
                        else
                        {
                            Toast.makeText(context, "No contract request for this job", Toast.LENGTH_LONG).show();
                        }

                        Toast.makeText(context, String.valueOf(getAdapterPosition()), Toast.LENGTH_LONG).show();
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
            });

            editButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    JobFeedResponse jobFeedResponse = (JobFeedResponse)currentPostedJobsAdapterInstance.arrayList.get(getAdapterPosition());

                    Intent intent = new Intent(context, EditJobActivity.class);
                    intent.putExtra("Job", jobFeedResponse);

                    context.startActivity(intent);

                    Toast.makeText(context, String.valueOf(getAdapterPosition()), Toast.LENGTH_LONG).show();
                }
            });

            deleteButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    JobFeedResponse jobFeedResponse = (JobFeedResponse)currentPostedJobsAdapterInstance.arrayList.get(getAdapterPosition());

                    ConfirmDeleteJobDialog confirmDeleteJobDialog = ConfirmDeleteJobDialog.createNewInstance(currentPostedJobsAdapterInstance, getAdapterPosition(), jobFeedResponse.get_id());
                    confirmDeleteJobDialog.show(((FragmentActivity)context).getFragmentManager(), "ConfirmDeleteJobDialog");
                }
            });

            detailsButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    String jobId = (String)view.getTag();
                    openUtilities(context, JobUtilitiesActivity.DETAILS, jobId, null, UtilitiesDetailsFragment.FEED_TYPE.PARENT_STUDENT.POSTED_JOBS);
                }
            });

            calendarButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    String jobId = (String)view.getTag();
                    openUtilities(context, JobUtilitiesActivity.CALENDAR, jobId, null, UtilitiesDetailsFragment.FEED_TYPE.PARENT_STUDENT.POSTED_JOBS);
                }
            });

            chatButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    String jobId = (String)view.getTag();
                    openUtilities(context, JobUtilitiesActivity.CHAT, jobId, null, UtilitiesDetailsFragment.FEED_TYPE.PARENT_STUDENT.POSTED_JOBS);
                }
            });
        }
    }

    public PostedJobsAdapter(Context context) {
        super(context);
    }

    @Override
    public void setNoItemJobFeedTextView() {
        if(jobFeedTextView != null){
            jobFeedTextView.setText("No posted jobs.\n\nTo post a job, click the 'plus' button at the bottom.");
            jobFeedTextView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public PostedJobsAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_job, parent, false);

        PostedJobsAdapterViewHolder rootViewHolder = null;

        if(viewType == JobFeedAdapter.VIEWTYPE_JOB_FEED_DETAILS){
            rootViewHolder = new PostedJobsAdapterViewHolder(rootView, true, context, this);
        }else if(viewType == JobFeedAdapter.VIEWTYPE_JOB_FEED_NO_DETAILS){
            rootViewHolder = new PostedJobsAdapterViewHolder(rootView, false, context, this);
        }

        return rootViewHolder;
    }

    @Override
    public void onBindViewHolder(JobFeedAdapterViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);

        PostedJobsAdapterViewHolder postedJobsAdapterViewHolder = (PostedJobsAdapterViewHolder)holder;
        JobFeedResponse jobFeedResponse = (JobFeedResponse)arrayList.get(position);

        Match inspectionMatch = Functions.checkInspectionPhase(jobFeedResponse);
        if(inspectionMatch!=null){
            Log.e("Job: ", jobFeedResponse.getJobTitle());

            postedJobsAdapterViewHolder.inspectionPhase.getLayoutParams().height = LinearLayout.LayoutParams.WRAP_CONTENT;

            postedJobsAdapterViewHolder.inspectionPhase.setTag(inspectionMatch);

            postedJobsAdapterViewHolder.profileLabel.setText("Hired\nTutor");
            Functions.loadProfilePicture(context, inspectionMatch.getTutorProfile()[0].get_id(), new Constants.PICTURE_PACKET((Activity)context, postedJobsAdapterViewHolder.profilePicture, null, Constants.PICTURE_PACKET.PROFILE_PICTURE_WIDTH, Constants.PICTURE_PACKET.PROFILE_PICTURE_HEIGHT));
            postedJobsAdapterViewHolder.tutorName.setText(inspectionMatch.getTutorProfile()[0].getFullName());
        }else{
            LinearLayout.LayoutParams emptyLayoutParams = new LinearLayout.LayoutParams(0, 0);
            emptyLayoutParams.setMargins(0, 0, 0, 0);

            postedJobsAdapterViewHolder.inspectionPhase.setLayoutParams(emptyLayoutParams);
        }

        if(jobFeedResponse.getMatchedTutors().size()==0){
            postedJobsAdapterViewHolder.contractsButton.setText(String.valueOf("Waiting for Tutors"));

            postedJobsAdapterViewHolder.editButton.setEnabled(true);
            postedJobsAdapterViewHolder.deleteButton.setEnabled(true);
        }else{
            postedJobsAdapterViewHolder.contractsButton.setText(String.valueOf(jobFeedResponse.getMatchedTutors().size()) + " Tutor(s)");

            postedJobsAdapterViewHolder.editButton.setEnabled(false);
            postedJobsAdapterViewHolder.deleteButton.setEnabled(false);
        }


    }

    //-----------------------------------------------------------------------------------------------------------------------------//
    //JobFeedAdapterInterface

    @Override
    public void refreshOneItemFromNotifications(String id)
    {
        Log.e("Posted Jobs Adapter", "Single Posted Job - " + id);
        getJobFunction().getSinglePostedJob(this, id);
    }

    @Override
    public void refreshView() {

    }

    @Override
    public void loadData() {
        Log.e("Posted Jobs", "Load Data Called");
        getJobFunction().getPostedJobs(this);
    }

    //-----------------------------------------------------------------------------------------------------------------------------//

    //DialogListenerInterface

    //Event handler to handle the positive button press from ConfirmDeleteJobDialog
    //Called from ConfirmDeleteJobDialog (fragments->job_feed->parent_student->ConfirmDeleteJobDialog)
    @Override
    public void onPostedJobDeletePositive(int position, String jobId) {
        Log.e("Delete Handler", "Position: " + position + " " + "JobID: " + jobId);

        getJobFunction().delete(jobId, position, this);
    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {

        return super.getItemCount();
    }
}