package com.hyptastic.poratechai.adapters.job_feed.utilities.details.tutor;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ViewGroup;

import com.hyptastic.poratechai.adapters.job_feed.JobFeedAdapter;
import com.hyptastic.poratechai.adapters.job_feed.tutor.AppliedAdapter;

/**
 * Created by ISHRAK on 6/7/2019.
 */

public class AppliedUtilitiesDetailsAdapter extends AppliedAdapter {
    String jobId;

    public AppliedUtilitiesDetailsAdapter(Context context, String jobId){
        super(context);

        this.jobId = jobId;
    }

    @Override
    public AppliedAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return super.onCreateViewHolder(parent, viewType);
    }

    @Override
    public int getItemViewType(int position) {
        return JobFeedAdapter.VIEWTYPE_JOB_FEED_DETAILS;
    }

    @Override
    public void onBindViewHolder(JobFeedAdapterViewHolder holder, int position) {
        try{
            super.onBindViewHolder(holder, position);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void loadData() {
        Log.e("Details Adapter", jobId);
        getJobFunction().getSingleAppliedJob(this, jobId);
    }

    @Override
    public void removeItem(int position) {
        super.removeItem(position);

        ((AppCompatActivity)context).finish();
    }

    @Override
    public int getItemCount() {
        return 1;
    }
}
