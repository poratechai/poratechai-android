package com.hyptastic.poratechai.adapters.payment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by ISHRAK on 6/25/2018.
 */

public class NonSwipeablePagerAdapter extends FragmentPagerAdapter {
    private ArrayList<Fragment> buyContractsList;

    public NonSwipeablePagerAdapter(FragmentManager fm) {
        super(fm);

        this.buyContractsList = new ArrayList<Fragment>();
    }

    @Override
    public int getCount() {
        return buyContractsList.size();
    }

    @Override
    public android.support.v4.app.Fragment getItem(int position) {
        return buyContractsList.get(position);
    }

    public void addFragment(Fragment fragment){
        buyContractsList.add(fragment);
    }
}
