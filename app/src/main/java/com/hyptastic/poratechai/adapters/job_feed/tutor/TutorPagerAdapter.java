package com.hyptastic.poratechai.adapters.job_feed.tutor;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;

import com.hyptastic.poratechai.fragments.job_feed.JobFeedFragment;

import java.util.ArrayList;

/**
 * Created by ISHRAK on 2/26/2018.
 */

public class TutorPagerAdapter extends FragmentStatePagerAdapter {
    private ArrayList<JobFeedFragment> pages = null;
    private String pageTitles[] = null;

    public TutorPagerAdapter(FragmentManager fm, ArrayList<JobFeedFragment> pages, String[] pageTitles) {
        super(fm);
        this.pages = pages;
        this.pageTitles = pageTitles;
    }

    @Override
    public Fragment getItem(int position) {
        return pages.get(position);
    }

    @Override
    public int getItemPosition(Object object) {
        return PagerAdapter.POSITION_NONE;
    }

    @Override
    public int getCount() {
        return pages.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return pageTitles[position];
    }

    @Override
    public Parcelable saveState() {
        return null;
    }
}
