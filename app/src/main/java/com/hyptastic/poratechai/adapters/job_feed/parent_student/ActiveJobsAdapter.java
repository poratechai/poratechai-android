package com.hyptastic.poratechai.adapters.job_feed.parent_student;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.activities.job_feed.JobUtilitiesActivity;
import com.hyptastic.poratechai.adapters.job_feed.JobFeedActiveHistoryAdapter;
import com.hyptastic.poratechai.adapters.job_feed.JobFeedAdapter;
import com.hyptastic.poratechai.fragments.job_feed.utilities.UtilitiesDetailsFragment;

/**
 * Created by ISHRAK on 4/7/2018.
 */

public class ActiveJobsAdapter extends JobFeedActiveHistoryAdapter {
    public static class ActiveJobsAdapterViewHolder extends JobFeedActiveHistoryAdapter.JobFeedActiveHistoryAdapterViewHolder {
        private Button archiveButton;

        public ActiveJobsAdapterViewHolder(View v, boolean isDetailedView, final Context context, final ActiveJobsAdapter currentActiveJobsAdapterInstance) {
            super(v, isDetailedView, context, currentActiveJobsAdapterInstance);

            archiveButton = v.findViewById(R.id.archiveButton);

            archiveButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    currentActiveJobsAdapterInstance.getContractFunction().cancelJobinPhase4(currentActiveJobsAdapterInstance, currentActiveJobsAdapterInstance.arrayList.get(getAdapterPosition()).get_id());
                }
            });

            detailsButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    String jobId = (String)view.getTag();
                    openUtilities(context, JobUtilitiesActivity.DETAILS, jobId, null, UtilitiesDetailsFragment.FEED_TYPE.PARENT_STUDENT.ACTIVE_POSTED_JOB);
                }
            });

            calendarButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    String jobId = (String)view.getTag();
                    openUtilities(context, JobUtilitiesActivity.CALENDAR, jobId, null, UtilitiesDetailsFragment.FEED_TYPE.PARENT_STUDENT.ACTIVE_POSTED_JOB);
                }
            });

            chatButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    String jobId = (String)view.getTag();
                    openUtilities(context, JobUtilitiesActivity.CHAT, jobId, null, UtilitiesDetailsFragment.FEED_TYPE.PARENT_STUDENT.ACTIVE_POSTED_JOB);
                }
            });
        }
    }

    public ActiveJobsAdapter(Context context)
    {
        super(context);
    }

    @Override
    public void setNoItemJobFeedTextView() {
        if(jobFeedTextView != null){
            jobFeedTextView.setText("No Active Posted Jobs.\n\n If you have accepted a tutor for a job, that job will appear here after 14 days of inspection");
            jobFeedTextView.setVisibility(View.VISIBLE);
        }
    }

    //JobFeedAdapterInterface

    @Override
    public void refreshOneItemFromNotifications(String id)
    {
        getJobFunction().getSingleActivePostedJob(this, id);
    }

    @Override
    public void refreshView()
    {

    }

    @Override
    public void loadData()
    {
        getJobFunction().getActivePostedJobs(this);
    }

    //View

    @Override
    public ActiveJobsAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = (View) LayoutInflater.from(context).inflate(R.layout.listitem_job_active, parent, false);

        ActiveJobsAdapterViewHolder rootViewHolder = null;

        if(viewType == JobFeedAdapter.VIEWTYPE_JOB_FEED_DETAILS){
            rootViewHolder = new ActiveJobsAdapterViewHolder(rootView, true, context, this);
        }else if(viewType == JobFeedAdapter.VIEWTYPE_JOB_FEED_NO_DETAILS){
            rootViewHolder = new ActiveJobsAdapterViewHolder(rootView, false, context, this);
        }

        return rootViewHolder;
    }

    @Override
    public void onBindViewHolder(JobFeedAdapterViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);

        Log.e("onBindViewHolder", "ActiveJobsAdapter");
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}
