package com.hyptastic.poratechai.adapters.job_feed;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.SpinKitView;
import com.github.ybq.android.spinkit.sprite.SpriteContainer;
import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.activities.job_feed.JobUtilitiesActivity;
import com.hyptastic.poratechai.layout.FlexboxLayout;
import com.hyptastic.poratechai.models.job_feed.JobFeedResponse;
import com.hyptastic.poratechai.models.job_feed.Match;
import com.hyptastic.poratechai.retrofit.functions.ContractFunction;
import com.hyptastic.poratechai.retrofit.functions.JobFunction;
import com.hyptastic.poratechai.retrofit.functions.ReviewFunction;
import com.hyptastic.poratechai.retrofit.interfaces.ContractClient;
import com.hyptastic.poratechai.retrofit.interfaces.JobClient;
import com.hyptastic.poratechai.retrofit.interfaces.ReviewClient;
import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
import com.hyptastic.poratechai.utils.Constants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ListIterator;

/**
 * Created by ISHRAK on 4/11/2018.
 */

public abstract class JobFeedAdapter<T> extends RecyclerView.Adapter<JobFeedAdapter.JobFeedAdapterViewHolder<T>> implements Serializable, JobFeedAdapterInterface<T>{
    protected static final long serialVersionUID = 123L;

    protected static final int VIEWTYPE_JOB_FEED_DETAILS = 10;
    protected static final int VIEWTYPE_JOB_FEED_NO_DETAILS = 11;

    protected Context context;

    protected ArrayList<T> arrayList;

    private JobFunction jobFunction;
    private ContractFunction contractFunction;
    private ReviewFunction reviewFunction;

    private SwipeRefreshLayout swipeRefreshLayout;
    private SpriteContainer spinKitViewStyle;
    private SpinKitView spinKitView;
    protected TextView jobFeedTextView;

    private String jobOrMatchId;

    public static class JobFeedAdapterViewHolder<T> extends RecyclerView.ViewHolder {
        private Context context;

        private TextView jobTitleTextView, budgetTextView, weeklyTextView, subcategoryValueTextView, classValueTextView, subjectsOrCategoriesTextView, locationTextView;
        private LinearLayout subcategoryLayout, classLayout, subjectsOrCategoriesFlexBoxLayout, locationLayout;

        private FlexboxLayout flexboxLayout;

        private View utilitiesSection;
        protected Button detailsButton, calendarButton, chatButton;

        protected JobFeedAdapterViewHolder(View itemView, boolean isDetailedView, final Context context) {
            super(itemView);

            this.context                        = context;

            jobTitleTextView                    = itemView.findViewById(R.id.jobTitleTextView);
            budgetTextView                      = itemView.findViewById(R.id.budgetValueTextView);
            weeklyTextView                      = itemView.findViewById(R.id.weeklyValueTextView);

            subcategoryLayout                   = itemView.findViewById(R.id.subcategoryLayout);
            subcategoryValueTextView            = itemView.findViewById(R.id.subcategoryValueTextView);

            classLayout                         = itemView.findViewById(R.id.classLayout);
            classValueTextView                  = itemView.findViewById(R.id.classValueTextView);

            subjectsOrCategoriesTextView        = itemView.findViewById(R.id.subjectsOrCategoriesTextView);
            subjectsOrCategoriesFlexBoxLayout   = itemView.findViewById(R.id.subjectsOrCategoriesFlexBoxLayout);

            locationLayout                      = itemView.findViewById(R.id.locationLayout);
            locationTextView                    = itemView.findViewById(R.id.locationValueTextView);

            utilitiesSection                    = itemView.findViewById(R.id.listitem_job_utilities);
            detailsButton                       = itemView.findViewById(R.id.detailsButton);
            calendarButton                      = itemView.findViewById(R.id.calendarButton);
            chatButton                          = itemView.findViewById(R.id.chatButton);

            locationTextView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    Location location = (Location)view.getTag();

                    Double lat = location.lat;
                    Double lon = location.lon;
                    String label = location.label;
                    String uriBegin = "geo:"+lat+","+lon;
                    String query = lat + "," + lon + "(" + label + ")";
                    String encodedQuery = Uri.encode(query);
                    String uriString = uriBegin + "?q=" + encodedQuery+"&z=13";
                    Uri uri = Uri.parse(uriString);
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, uri);
                    context.startActivity(mapIntent);

                    Toast.makeText(context, String.valueOf(getAdapterPosition()), Toast.LENGTH_LONG).show();
                }
            });

            flexboxLayout = new FlexboxLayout((Activity)context, 0);

            if(isDetailedView){
                utilitiesSection.getLayoutParams().height = 0;

                subcategoryLayout.getLayoutParams().height = RelativeLayout.LayoutParams.WRAP_CONTENT;
                classLayout.getLayoutParams().height = RelativeLayout.LayoutParams.WRAP_CONTENT;

                ((RelativeLayout.LayoutParams)locationLayout.getLayoutParams()).addRule(RelativeLayout.BELOW, R.id.classLayout);
            }
        }

        protected void openUtilities(Context context, int whichTab, String jobId, String matchId, int utilitiesFeedType){
            Intent intent = new Intent(context, JobUtilitiesActivity.class);

            intent.putExtra("WHICH_TAB", whichTab);
            intent.putExtra("JOB_ID", jobId);
            intent.putExtra("MATCH_ID", matchId);
            intent.putExtra("UTILITIES_FEED_TYPE", utilitiesFeedType);

            context.startActivity(intent);
        }
    }

    private static class Location{
        public Double lat;
        public Double lon;
        public String label;

        public Location(Double lat, Double lon, String label) {
            this.lat = lat;
            this.lon = lon;
            this.label = label;
        }
    }

    public JobFeedAdapter(Context context)
    {
        this.context = context;

        arrayList = new ArrayList<T>();

        jobFunction = new JobFunction(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, JobClient.class), context);
        contractFunction = new ContractFunction(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, ContractClient.class), context);
        reviewFunction = new ReviewFunction(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, ReviewClient.class), context);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        swipeRefreshLayout = (SwipeRefreshLayout)recyclerView.getTag(R.id.SWIPE_REFRESH_LAYOUT);
        spinKitViewStyle = (SpriteContainer)recyclerView.getTag(R.id.SPIN_KIT_VIEW_STYLE);
        spinKitView = (SpinKitView)recyclerView.getTag(R.id.SPIN_KIT_VIEW);
        jobFeedTextView = (TextView)recyclerView.getTag(R.id.JOB_FEED_TEXT_VIEW);
    }

    public abstract void setNoItemJobFeedTextView();

    //-----------------------------------------------------------------------------------------------------------------------------//

    @Override
    public int getItemViewType(int position) {
        return JobFeedAdapter.VIEWTYPE_JOB_FEED_NO_DETAILS;
    }

    //View
    @Override
    public void onBindViewHolder(JobFeedAdapterViewHolder holder, int position) {
        Log.e("onBindViewHolder", "JobFeedAdapter");

        JobFeedResponse jobFeedResponse = null;
        try{
            jobFeedResponse = (JobFeedResponse)arrayList.get(position);

            jobOrMatchId = jobFeedResponse.get_id();
        }catch(ClassCastException e){
            Log.e("JobFeedAdapter", "Cast to Match");

            Match match = (Match)arrayList.get(position);
            jobFeedResponse = match.getJobData()[0];

            jobOrMatchId = match.get_id();
        }catch(Exception e){
            e.printStackTrace();
        }

        holder.jobTitleTextView.setText(jobFeedResponse.getJobTitle());

        if(jobFeedResponse.getCategory().equals("Academics")){
            holder.subjectsOrCategoriesTextView.setText("Subjects ");
            holder.flexboxLayout.refreshFlexbox(holder.subjectsOrCategoriesFlexBoxLayout, new ArrayList<String>(Arrays.asList(jobFeedResponse.getSubjects())), (int)(Constants.SCREEN_WIDTH*0.65), false);

            holder.subcategoryValueTextView.setText(jobFeedResponse.getSubcategory());
            holder.classValueTextView.setText(jobFeedResponse.getClassValue());
        }else{
            holder.subjectsOrCategoriesTextView.setText("Category ");
            holder.flexboxLayout.refreshFlexbox(holder.subjectsOrCategoriesFlexBoxLayout, new ArrayList<String>(Arrays.asList(new String[]{jobFeedResponse.getSubcategory()})), (int)(Constants.SCREEN_WIDTH*0.5), false);

            holder.subcategoryValueTextView.setText("N/A");
            holder.classValueTextView.setText("N/A");
        }

        holder.budgetTextView.setText(String.valueOf(jobFeedResponse.getSalary()) + " BDT");
        holder.weeklyTextView.setText(String.valueOf(jobFeedResponse.getDaysPerWeek()) + " day(s)");

        holder.locationTextView.setText(jobFeedResponse.getLocation() + "\n(View on Maps)");
        holder.locationTextView.setTag(new Location(jobFeedResponse.getLocationLat(), jobFeedResponse.getLocationLong(), jobFeedResponse.getLocation()));

        holder.detailsButton.setTag(jobOrMatchId);
        holder.calendarButton.setTag(jobOrMatchId);
        holder.chatButton.setTag(jobOrMatchId);
    }

    //-----------------------------------------------------------------------------------------------------------------------------//

    //ArrayList
    @Override
    public void setArrayList(ArrayList arrayList) {
        try
        {
            Log.e("ArrayList", "Set");
            this.arrayList = new ArrayList<T>(arrayList);

            //Loading Animation
            swipeRefreshLayout.setRefreshing(false);
            spinKitViewStyle.stop();
            spinKitView.setVisibility(View.INVISIBLE);

            //If the ArrayList is empty, the following textview will be shown
            if(arrayList.size()==0){
                setNoItemJobFeedTextView();
            }

            notifyDataSetChanged();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void removeItem(int position) {
        try
        {
            arrayList.remove(position);

            //After removal, if the ArrayList is empty, the following textview will be shown
            if(arrayList.size()==0){
                setNoItemJobFeedTextView();
            }

            notifyDataSetChanged();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void removeOneItemFromNotifications(String id)
    {
        try
        {
            for(T response: arrayList)
            {
                if(((JobFeedResponse)response).get_id().equals(id))
                {
                    arrayList.remove(response);
                }
            }

            notifyDataSetChanged();
        }
        catch(ClassCastException e)
        {
            for(T response: arrayList)
            {
                if(((Match)response).get_id().equals(id))
                {
                    arrayList.remove(response);
                }
            }

            notifyDataSetChanged();
        }
    }

    @Override
    public void refreshOneItemFromServer(T item)
    {
        Log.e("Refresh One Item", "HERE");

        try {
            boolean itemExists = false;

            ListIterator<T> listIterator = arrayList.listIterator();

            while(listIterator.hasNext()){
                JobFeedResponse jobFeedResponse = (JobFeedResponse)listIterator.next();

                if(jobFeedResponse.get_id().equals(((JobFeedResponse)item).get_id())){
                    itemExists = true;
                    listIterator.set(item);
                }
            }

            if(itemExists==false){
                arrayList.add(item);
            }

            notifyDataSetChanged();
        } catch(ClassCastException e) {
            boolean itemExists = false;

            ListIterator<T> listIterator = arrayList.listIterator();

            while(listIterator.hasNext()) {
                Match match = (Match)listIterator.next();

                if(match.get_id().equals(((Match)item).get_id()))
                {
                    itemExists = true;
                    listIterator.set(item);
                }
            }

            if(itemExists==false){
                arrayList.add(item);
            }

            notifyDataSetChanged();
        } catch(Exception e) {
            Log.e("RefreshOneItem", e.toString());
        }
    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    //----------------------------------------------------------------------------------------------------------------------------//

    //Retrofit functions
    public JobFunction getJobFunction()
    {
        return jobFunction;
    }

    public ContractFunction getContractFunction()
    {
        return contractFunction;
    }

    //----------------------------------------------------------------------------------------------------------------------------//
}
