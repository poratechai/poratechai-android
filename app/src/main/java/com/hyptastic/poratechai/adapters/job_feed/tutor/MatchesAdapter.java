package com.hyptastic.poratechai.adapters.job_feed.tutor;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.activities.PorateChaiActivity;
import com.hyptastic.poratechai.activities.job_feed.JobUtilitiesActivity;
import com.hyptastic.poratechai.activities.profile.ProfileViewActivity;
import com.hyptastic.poratechai.adapters.job_feed.JobFeedAdapter;
import com.hyptastic.poratechai.fragments.job_feed.tutor.dialog.ConfirmDeleteMatchDialog;
import com.hyptastic.poratechai.fragments.job_feed.tutor.ContractFragment;
import com.hyptastic.poratechai.fragments.job_feed.utilities.UtilitiesDetailsFragment;
import com.hyptastic.poratechai.models.job_feed.JobFeedResponse;
import com.hyptastic.poratechai.models.job_feed.Match;
import com.hyptastic.poratechai.models.users.User;
import com.hyptastic.poratechai.utils.Constants;
import com.hyptastic.poratechai.utils.Functions;

/**
 * Created by ISHRAK on 3/22/2018.
 */

public class MatchesAdapter extends JobFeedAdapter<Match> implements ConfirmDeleteMatchDialog.ConfirmDeleteMatchListener, ContractFragment.ContractFragmentCloseListener
{
    public static class MatchesAdapterViewHolder extends JobFeedAdapter.JobFeedAdapterViewHolder<Match> {
        private Button applyButton, cancelButton;
        private View inspectionPhase;

        public MatchesAdapterViewHolder(View itemView, boolean isDetailedView, final Context context, final MatchesAdapter currentMatchesAdapterInstance) {
            super(itemView, isDetailedView, context);

            inspectionPhase = itemView.findViewById(R.id.shortProfile);
            applyButton     = itemView.findViewById(R.id.applyButton);
            cancelButton    = itemView.findViewById(R.id.cancelButton);

            applyButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    ContractFragment contractFragment = new ContractFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("MatchesAdapter", currentMatchesAdapterInstance);
                    bundle.putString("MatchId", currentMatchesAdapterInstance.arrayList.get(getAdapterPosition()).get_id());
                    bundle.putInt("Salary", (int)currentMatchesAdapterInstance.arrayList.get(getAdapterPosition()).getJobData()[0].getSalary());
                    bundle.putInt("DaysPerWeek", currentMatchesAdapterInstance.arrayList.get(getAdapterPosition()).getJobData()[0].getDaysPerWeek());
                    contractFragment.setArguments(bundle);
                    contractFragment.show(((FragmentActivity)context).getSupportFragmentManager(), "ContractFragment");
                }
            });

            cancelButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    ConfirmDeleteMatchDialog confirmDeleteMatchDialog = ConfirmDeleteMatchDialog.createNewInstance(currentMatchesAdapterInstance, currentMatchesAdapterInstance.arrayList.get(getAdapterPosition()).get_id(), getAdapterPosition());
                    confirmDeleteMatchDialog.show(((Activity)context).getFragmentManager(), "ConfirmDeleteMatchDialog");
                }
            });

            detailsButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    String matchId = (String)view.getTag();
                    openUtilities(context, JobUtilitiesActivity.DETAILS, null, matchId, UtilitiesDetailsFragment.FEED_TYPE.TUTOR.MATCHES);
                }
            });

            calendarButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    String matchId = (String)view.getTag();
                    openUtilities(context, JobUtilitiesActivity.CALENDAR, null, matchId, UtilitiesDetailsFragment.FEED_TYPE.TUTOR.MATCHES);
                }
            });

            chatButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    String matchId = (String)view.getTag();
                    openUtilities(context, JobUtilitiesActivity.CHAT, null, matchId, UtilitiesDetailsFragment.FEED_TYPE.TUTOR.MATCHES);
                }
            });
        }
    }

    public MatchesAdapter(Context context)
    {
        super(context);
    }

    @Override
    public void setNoItemJobFeedTextView() {
        if(jobFeedTextView != null){
            jobFeedTextView.setText("No matched jobs!\n\nPlease set your preferences from your profile to get job matches.");
            jobFeedTextView.setVisibility(View.VISIBLE);
        }
    }

    //View

    @Override
    public MatchesAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_job_tutor_matches, parent, false);

        MatchesAdapterViewHolder rootViewHolder = null;

        if(viewType == JobFeedAdapter.VIEWTYPE_JOB_FEED_DETAILS){
            rootViewHolder = new MatchesAdapterViewHolder(rootView, true, parent.getContext(), this);
        }else if(viewType == JobFeedAdapter.VIEWTYPE_JOB_FEED_NO_DETAILS){
            rootViewHolder = new MatchesAdapterViewHolder(rootView, false, parent.getContext(), this);
        }

        return rootViewHolder;
    }

    @Override
    public void onBindViewHolder(JobFeedAdapterViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);

        MatchesAdapterViewHolder MatchesAdapterViewHolder = (MatchesAdapterViewHolder)holder;

        Match match = arrayList.get(position);
        if(match.getJobData().length>0) {
            LinearLayout.LayoutParams emptyLayoutParams = new LinearLayout.LayoutParams(0, 0);
            emptyLayoutParams.setMargins(0, 0, 0, 0);
            MatchesAdapterViewHolder.inspectionPhase.setLayoutParams(emptyLayoutParams);

            MatchesAdapterViewHolder.applyButton.setEnabled(true);
        }
    }

    //JobFeedAdapterInterface

    @Override
    public void refreshOneItemFromNotifications(String id)
    {
        getJobFunction().getSingleMatchedJob(this, id);
    }

    @Override
    public void refreshView()
    {

    }

    @Override
    public void loadData()
    {
        getJobFunction().getMatchedJobs(this);
    }

//    @Override   //Overridden only in MatchesAdapter to remove self matches
//    public void setArrayList(ArrayList arrayList)
//    {
//        try
//        {
//            Log.e("ArrayList", "Set");
//            this.arrayList = new ArrayList<Match>(arrayList);
//
//            for(Match m : this.arrayList){
//                if(m.getJobData()[0].getUserId().equals(Constants.USER_ID_VALUE)){
//                    this.arrayList.remove(m);
//                }
//            }
//
//            notifyDataSetChanged();
//        }
//        catch(Exception e)
//        {
//            e.printStackTrace();
//        }
//    }

    //DialogInterfaceListener

    @Override
    public void onMatchDeletePositive(String matchId, int position) {
        Match match = arrayList.get(position);

        if(match.getCurrentStatus()==Constants.JOB_STATUS.JOB_STATUS_MATCH_POOL)
        {
            getContractFunction().cancelJobinPhase0(this, position, matchId);
        }
        else if(match.getCurrentStatus()==Constants.JOB_STATUS.JOB_STATUS_CONTRACT_REQUEST_SENT)
        {
            getContractFunction().cancelJobinPhase1(this, position, matchId);
        }
        else if(match.getCurrentStatus()==Constants.JOB_STATUS.JOB_STATUS_CONTRACT_REVIEW)
        {
            getContractFunction().cancelJobinPhase2(this, position, matchId);
        }
        else if(match.getCurrentStatus()==Constants.JOB_STATUS.JOB_STATUS_INSPECTION)
        {
            getContractFunction().cancelJobinPhase3(this, position, matchId);
        }
    }

    //ContractFragmentListener

    @Override
    public void handleContractFragmentClose(DialogInterface dialog) {
        loadData();
    }
}
