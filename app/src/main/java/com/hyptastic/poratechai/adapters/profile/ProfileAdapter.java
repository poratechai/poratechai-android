package com.hyptastic.poratechai.adapters.profile;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;

import java.util.ArrayList;

/**
 * Created by ISHRAK on 2/20/2019.
 */

public class ProfileAdapter extends FragmentStatePagerAdapter {
    private ArrayList<Fragment> pageItems;
    private String[] items;

    public ProfileAdapter(FragmentManager fm, ArrayList<Fragment> pageItems, String[] items) {
        super(fm);
        this.pageItems = pageItems;
        this.items = items;
    }

    @Override
    public Fragment getItem(int position) {
        return pageItems.get(position);
    }

    @Override
    public int getItemPosition(Object object) {
        return PagerAdapter.POSITION_NONE;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return items[position];
    }

    @Override
    public int getCount() {
        return pageItems.size();
    }

    @Override
    public Parcelable saveState() {
        return null;
    }
}
