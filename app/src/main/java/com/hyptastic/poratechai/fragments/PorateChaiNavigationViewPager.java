package com.hyptastic.poratechai.fragments;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by ISHRAK on 2/16/2019.
 */

public class PorateChaiNavigationViewPager extends ViewPager {
    public PorateChaiNavigationViewPager(Context context) {
        super(context);
    }

    public PorateChaiNavigationViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return false;
    }
}
