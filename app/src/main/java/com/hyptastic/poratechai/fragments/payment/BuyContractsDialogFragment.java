//package com.hyptastic.poratechai.fragments.payment;
//
//import android.app.Dialog;
//import android.content.DialogInterface;
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.v4.app.DialogFragment;
//import android.view.KeyEvent;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.Window;
//import android.widget.Button;
//
//import com.hyptastic.poratechai.R;
//import com.hyptastic.poratechai.adapters.payment.NonSwipeablePagerAdapter;
//import com.hyptastic.poratechai.fragments.payment.parent_student.InvoiceFragmentV1;
//import com.hyptastic.poratechai.fragments.payment.parent_student.PaymentPortWalletFragmentV1;
//import com.hyptastic.poratechai.models.wallet.InvoiceResponse;
//import com.hyptastic.poratechai.retrofit.functions.WalletFunction;
//import com.hyptastic.poratechai.retrofit.interfaces.WalletClient;
//import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
//import com.hyptastic.poratechai.utils.Constants;
//
///**
// * Created by ISHRAK on 6/25/2018.
// */
//
//public class BuyContractsDialogFragment extends DialogFragment {
//    PaymentInterface paymentInterface;
//
//    NonSwipeableViewPager paymentViewPager;
//    Button proceedBackButton, cancelButton, deleteButton;
//
//    NonSwipeablePagerAdapter nonSwipeablePagerAdapter;
//
//    WalletFunction walletFunction;
//
//    public static interface CloseListener{
//        public void handleDialogClose();
//    }
//
//    private static class State{
//        public boolean invoiceFragment = true;
//    }
//
//    public static BuyContractsDialogFragment createNewInstance(InvoiceResponse invoiceResponse, PaymentInterface paymentInterface){
//        BuyContractsDialogFragment buyContractsDialogFragment = new BuyContractsDialogFragment();
//
//        Bundle args = new Bundle();
//        args.putParcelable("invoice", invoiceResponse);
//        args.putSerializable("paymentInterface", paymentInterface);
//        buyContractsDialogFragment.setArguments(args);
//
//        return buyContractsDialogFragment;
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        return inflater.inflate(R.layout.dialog_buy_contracts, container, false);
//    }
//
//    @NonNull
//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        Dialog dialog = super.onCreateDialog(savedInstanceState);
//        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//
//        dialog.setOnKeyListener(new Dialog.OnKeyListener(){
//            @Override
//            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
//                if(keyEvent.getKeyCode() == KeyEvent.KEYCODE_BACK){
//                    dismissDialog();
//                    return true;
//                }
//                return false;
//            }
//        });
//
//        return dialog;
//    }
//
//    @Override
//    public void onStart() {
//        super.onStart();
//
//        final Dialog dialog = getDialog();
//        if(dialog!=null)
//        {
//            dialog.getWindow().setLayout((int)(0.95*Constants.SCREEN_WIDTH), (int)(0.88*Constants.SCREEN_HEIGHT));
//            dialog.getWindow().setBackgroundDrawableResource(R.drawable.background_listitem_job_tutor_profile);
//
//            initLayout(this, dialog);
//        }
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//
//        proceedBackButton.setText("Proceed");   //Resetting ProceedBackButton text because the viewpager is reset to the first page
//    }
//
//    private void initLayout(final BuyContractsDialogFragment currentBuyContractsDialogFragment, Dialog dialog){
//        final State state = new State();
//
//        walletFunction = new WalletFunction(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, WalletClient.class), getActivity());
//
//        paymentInterface = (PaymentInterface)getArguments().getSerializable("paymentInterface");
//
//        paymentViewPager = dialog.findViewById(R.id.paymentViewPager);
//        proceedBackButton = dialog.findViewById(R.id.proceedBackButton);
//        cancelButton = dialog.findViewById(R.id.cancelButton);
//        deleteButton = dialog.findViewById(R.id.deleteButton);
//
//        nonSwipeablePagerAdapter = new NonSwipeablePagerAdapter(getChildFragmentManager());
//        nonSwipeablePagerAdapter.addFragment(InvoiceFragmentV1.createNewInstance((InvoiceResponse)getArguments().getParcelable("invoice")));
//        nonSwipeablePagerAdapter.addFragment(PaymentPortWalletFragmentV1.createNewInstance((InvoiceResponse)getArguments().getParcelable("invoice")));
//
//        paymentViewPager.setAdapter(nonSwipeablePagerAdapter);
//
//        proceedBackButton.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View view) {
//                if(state.invoiceFragment){
//                    paymentViewPager.setCurrentItem(1);
//                    proceedBackButton.setText("Back");
//                }else{
//                    paymentViewPager.setCurrentItem(0);
//                    proceedBackButton.setText("Proceed");
//                }
//                state.invoiceFragment = !state.invoiceFragment;
//            }
//        });
//
//        cancelButton.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View view) {
//                dismissDialog();
//            }
//        });
//
//        deleteButton.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View view) {
//                walletFunction.deleteInvoice(currentBuyContractsDialogFragment, ((InvoiceResponse)getArguments().getParcelable("invoice")).getData().getInvoiceId());
//            }
//        });
//    }
//
//    public void dismissDialog(){
//        ((CloseListener)paymentInterface).handleDialogClose();
//
//        getDialog().dismiss();
//    }
//}
