//package com.hyptastic.poratechai.fragments.payment.parent_student;
//
//import android.support.v7.app.AppCompatActivity;
//import android.graphics.Color;
//import android.util.Log;
//import android.view.View;
//import android.widget.Button;
//import android.widget.ImageButton;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.github.ybq.android.spinkit.SpinKitView;
//import com.github.ybq.android.spinkit.sprite.Sprite;
//import com.github.ybq.android.spinkit.style.CubeGrid;
//import com.hyptastic.poratechai.R;
//import com.hyptastic.poratechai.fragments.payment.BuyContractsDialogFragment;
//import com.hyptastic.poratechai.fragments.payment.PaymentInterface;
//import com.hyptastic.poratechai.models.wallet.InvoiceRequest;
//import com.hyptastic.poratechai.models.wallet.InvoiceResponse;
//import com.hyptastic.poratechai.models.wallet.UpdateResponse;
//import com.hyptastic.poratechai.models.wallet.WalletConstants;
//import com.hyptastic.poratechai.models.wallet.v1.UpdateResponseV1;
//import com.hyptastic.poratechai.models.wallet.v1.WalletConstantsV1;
//import com.hyptastic.poratechai.retrofit.functions.SystemFunction;
//import com.hyptastic.poratechai.retrofit.functions.WalletFunction;
//import com.hyptastic.poratechai.retrofit.interfaces.WalletClient;
//import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
//import com.hyptastic.poratechai.utils.Constants;
//
///**
// * Created by ISHRAK on 8/15/2018.
// */
//
//public class PaymentTokenSystem implements PaymentInterface, BuyContractsDialogFragment.CloseListener {
//    private RelativeLayout paymentSection;
//    private LinearLayout contractsUpDown;
//    private TextView contractBalanceTextView, paymentAmountTextView, contractsValueTextView;
//    private ImageButton syncButton, upButton, downButton;
//    private Button refreshButton, buyButton;
//    private SpinKitView contractBalanceSpinKitView;
//    private SpinKitView buyContractsSpinKitView;
//
//    private Sprite contractBalanceSpinKitSprite;
//    private Sprite buyContractsSpinKitSprite;
//
//    private int contractsNumber = 0;
//    private double paymentAmount = 0;
//
//    private WalletFunction walletFunction = null;
//    private SystemFunction systemFunction = null;
//
//    private InvoiceResponse invoiceResponse;
//
//    private WalletConstantsV1 walletConstants;
//
//    private boolean pending = false;
//
//    AppCompatActivity profileActivity;
//
//    public PaymentTokenSystem(AppCompatActivity profileActivity, SystemFunction systemFunction){
//        this.profileActivity = profileActivity;
//        this.systemFunction = systemFunction;
//
//        loadPaymentSection();
//    }
//
//    public void loadPaymentSection()
//    {
//        walletFunction = new WalletFunction(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, WalletClient.class), profileActivity.getApplicationContext());
//
//        contractBalanceSpinKitView = profileActivity.findViewById(R.id.contractBalanceSpinKitView);
//        contractBalanceSpinKitSprite = new CubeGrid();
//        contractBalanceSpinKitView.setIndeterminateDrawable(contractBalanceSpinKitSprite);
//
//        buyContractsSpinKitView = profileActivity.findViewById(R.id.buyContractsSpinKitView);
//        buyContractsSpinKitSprite = new CubeGrid();
//        buyContractsSpinKitView.setIndeterminateDrawable(buyContractsSpinKitSprite);
//
//        contractBalanceSpinKitSprite.stop();
//        contractBalanceSpinKitView.setVisibility(View.INVISIBLE);
//
//        buyContractsSpinKitSprite.stop();
//        buyContractsSpinKitView.setVisibility(View.INVISIBLE);
//
//        contractBalanceTextView = profileActivity.findViewById(R.id.balanceContractsTextView);
//        paymentAmountTextView = profileActivity.findViewById(R.id.payValueTextView);
//        contractsValueTextView = profileActivity.findViewById(R.id.contractsValueTextView);
//
//        syncButton = profileActivity.findViewById(R.id.syncButton);
//        upButton = profileActivity.findViewById(R.id.upButton);
//        downButton = profileActivity.findViewById(R.id.downButton);
//
//        refreshButton = profileActivity.findViewById(R.id.refreshButton);
//        buyButton = profileActivity.findViewById(R.id.buyButton);
//
//        contractsUpDown = profileActivity.findViewById(R.id.contractsUpDown);
//
//        upButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                contractsNumber++;
//                paymentAmount = contractsNumber*walletConstants.getUNIT_PRICE();
//                contractsValueTextView.setText(String.valueOf(contractsNumber));
//                paymentAmountTextView.setText(String.valueOf(paymentAmount));
//            }
//        });
//
//        downButton.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View view) {
//                if(contractsNumber>0){
//                    contractsNumber--;
//                    paymentAmount = contractsNumber*walletConstants.getUNIT_PRICE();
//                    contractsValueTextView.setText(String.valueOf(contractsNumber));
//                    paymentAmountTextView.setText(String.valueOf(paymentAmount));
//                }
//            }
//        });
//
//        syncButton.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View view) {
//                updateWallet();
//            }
//        });
//
//        refreshButton.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View view) {
//                updateWallet();
//            }
//        });
//
//        buyButton.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View view) {
//                createInvoice(paymentAmount, contractsNumber);
//            }
//        });
//
//        updateWallet();
//        getWalletConstants();
//    }
//
//    public void createInvoice(double paymentAmount, double tokenAmount){
//        if(paymentAmount>0 || pending){
//            Toast.makeText(profileActivity.getApplicationContext(), "Loading Invoice", Toast.LENGTH_LONG).show();
//
//            InvoiceRequest invoiceRequest = new InvoiceRequest(String.valueOf(paymentAmount), "Contract Tokens", String.valueOf(tokenAmount)+" x Contract Tokens",
//                    Constants.USER_NAME_VALUE, Constants.EMAIL_VALUE, "phone", "address", "city", "state", "zipcode", "BD", Constants.USER_ID_VALUE, 0, 0);
//
//            walletFunction.createInvoice(this, invoiceRequest);
//        }else{
//            Toast.makeText(profileActivity, "Please select tokens", Toast.LENGTH_LONG).show();
//        }
//    }
//
//    public void updateWallet()
//    {
//        contractBalanceSpinKitSprite.start();
//        contractBalanceSpinKitView.setVisibility(View.VISIBLE);
//
//        syncButton.setVisibility(View.INVISIBLE);
//
//        walletFunction.updateWalletV1(this);
//    }
//
//    public void getInvoice(String invoiceNumber){
//        walletFunction.getInvoice(this, invoiceNumber);
//    }
//
//    //loadInvoice is called after response from getInvoice
//    public void loadInvoice(InvoiceResponse invoiceResponse){
//        this.invoiceResponse = invoiceResponse;
//
//        BuyContractsDialogFragment buyContractsDialogFragment = BuyContractsDialogFragment.createNewInstance(invoiceResponse, this);
//
//        buyContractsDialogFragment.show(profileActivity.getSupportFragmentManager(), "BuyContractsDialogFragment");
//    }
//
//    //refreshBalance is called after response from updateWallet
//    public void refreshBalance(UpdateResponse updateResponseV1)
//    {
//        UpdateResponseV1 updateResponse = (UpdateResponseV1)updateResponseV1;
//
//        try{
//            contractBalanceSpinKitSprite.stop();
//            contractBalanceSpinKitView.setVisibility(View.INVISIBLE);
//
//            syncButton.setVisibility(View.VISIBLE);
//
//            contractBalanceTextView.setText(String.format("%.2f", updateResponse.getBalance()));
//
//            Log.e("WalletV1 Balance", String.format("%.2f", updateResponse.getBalance()));
//
//            if(updateResponse.getPendingInvoice().getData()==null){
//                buyButton.setText("Buy");
//                buyButton.setTextColor(Color.BLACK);
//
//                pending = false;
//
//                invoiceResponse = null;
//            }else{
//                buyButton.setText("Pending");
//                buyButton.setTextColor(Color.RED);
//
//                pending = true;
//
//                invoiceResponse = updateResponse.getPendingInvoice();
//            }
//        }catch(Exception e){
//            Log.e("Balance Refresh", e.toString());
//        }
//    }
//
//    @Override
//    public void handleDialogClose() {
//        updateWallet();
//    }
//
//    public  void getWalletConstants(){
//        buyContractsSpinKitSprite.start();
//        buyContractsSpinKitView.setVisibility(View.VISIBLE);
//
//        contractsUpDown.setVisibility(View.INVISIBLE);
//
//        systemFunction.getSystemWalletConstantsV1(this);
//    }
//
//    public void walletConstantsGotten(WalletConstants walletConstantsV1){
//        this.walletConstants = (WalletConstantsV1)walletConstantsV1;
//
//        Constants.TOKEN_UNIT_PRICE = walletConstants.getUNIT_PRICE();
//        Constants.SALARY_STEP_UNIT = walletConstants.getUNIT_SALARY_STEP();
//
//        buyContractsSpinKitSprite.stop();
//        buyContractsSpinKitView.setVisibility(View.INVISIBLE);
//
//        contractsUpDown.setVisibility(View.VISIBLE);
//    }
//}
