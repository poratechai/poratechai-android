package com.hyptastic.poratechai.fragments.job_feed.parent_student.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.adapters.job_feed.parent_student.ContractRequestsAdapter;

/**
 * Created by ISHRAK on 3/19/2018.
 */

public class ConfirmDeleteContractDialog extends DialogFragment {
    public interface ConfirmDeleteContractListener {
        public void onContractDeletePositive(int position, String matchId);
    }

    private ConfirmDeleteContractListener mListener;

    public static ConfirmDeleteContractDialog createNewInstance(ContractRequestsAdapter contractRequestsAdapter, int position, String matchId)
    {
        ConfirmDeleteContractDialog confirmDeleteContractDialog = new ConfirmDeleteContractDialog();

        Bundle args = new Bundle();
        args.putSerializable("adapter", contractRequestsAdapter);
        args.putInt("position", position);
        args.putString("matchId", matchId);
        confirmDeleteContractDialog.setArguments(args);

        return confirmDeleteContractDialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        final ContractRequestsAdapter contractRequestsAdapter = (ContractRequestsAdapter)getArguments().getSerializable("adapter");
        final int position = getArguments().getInt("position");
        final String matchId = getArguments().getString("matchId");

        try{
            mListener = (ConfirmDeleteContractListener)contractRequestsAdapter;
        }catch(ClassCastException e){
            throw new ClassCastException(contractRequestsAdapter.toString() + " must implement ConfirmDeleteContractListener");
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.contract_deletion_confirmation)
                .setTitle(R.string.contract_deletion_title)
                .setPositiveButton(R.string.contract_deletion_positive, new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mListener.onContractDeletePositive(position, matchId);
                    }
                })
                .setNegativeButton(R.string.contract_deletion_negative, new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

        return builder.create();
    }
}
