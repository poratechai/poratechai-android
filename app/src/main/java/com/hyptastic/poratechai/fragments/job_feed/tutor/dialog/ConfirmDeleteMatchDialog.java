package com.hyptastic.poratechai.fragments.job_feed.tutor.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.adapters.job_feed.tutor.MatchesAdapter;

/**
 * Created by ISHRAK on 3/23/2018.
 */

public class ConfirmDeleteMatchDialog extends DialogFragment {
    public interface ConfirmDeleteMatchListener {
        public void onMatchDeletePositive(String matchId, int position);
    }

    private ConfirmDeleteMatchListener mListener;

    public static ConfirmDeleteMatchDialog createNewInstance(MatchesAdapter adapter, String matchId, int position)
    {
        ConfirmDeleteMatchDialog confirmDeleteMatchDialog = new ConfirmDeleteMatchDialog();

        Bundle bundle = new Bundle();
        bundle.putSerializable("adapter", adapter);
        bundle.putString("matchId", matchId);
        bundle.putInt("position", position);

        confirmDeleteMatchDialog.setArguments(bundle);

        return confirmDeleteMatchDialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final MatchesAdapter matchesAdapter = (MatchesAdapter)getArguments().getSerializable("adapter");
        final String matchId = getArguments().getString("matchId");
        final int position = getArguments().getInt("position");

        try{
            mListener = (ConfirmDeleteMatchListener)matchesAdapter;
        }catch(ClassCastException e){
            throw new ClassCastException(matchesAdapter.toString() + " must implement ConfirmDeleteMatchDialog");
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.match_deletion_confirmation)
                .setTitle(R.string.match_deletion_title)
                .setPositiveButton(R.string.match_deletion_positive, new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mListener.onMatchDeletePositive(matchId, position);
                    }
                })
                .setNegativeButton(R.string.match_deletion_negative, new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

        return builder.create();
    }
}
