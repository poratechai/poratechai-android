package com.hyptastic.poratechai.fragments.profile.tutor.stats;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hyptastic.poratechai.R;

/**
 * Created by ISHRAK on 2/19/2019.
 */

public class StatsFragment extends Fragment {
    TextView statsTextView;

    public static StatsFragment createInstance(String userId){
        StatsFragment statsFragment = new StatsFragment();

        Bundle bundle = new Bundle();
        bundle.putString("USER_ID", userId);

        statsFragment.setArguments(bundle);

        return statsFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_stats_tutor, null);

        statsTextView = (TextView)view.findViewById(R.id.statsTextView);

        if(getArguments()!=null && getArguments().containsKey("USER_ID")){
            statsTextView.setText("No current stats\navailable for this user");
        }else{
            statsTextView.setText("Keep using to \nget your stats");
        }

        return view;
    }
}
