//package com.hyptastic.poratechai.fragments.job_feed;
//
//import android.app.Dialog;
//import android.app.DialogFragment;
//import android.content.DialogInterface;
//import android.graphics.Color;
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//import android.view.GestureDetector;
//import android.view.View;
//import android.view.Window;
//import android.widget.Button;
//import android.widget.ImageButton;
//import android.widget.ImageView;
//import android.widget.RatingBar;
//import android.widget.TextView;
//
//import com.hyptastic.poratechai.R;
//import com.hyptastic.poratechai.adapters.job_feed.JobFeedReviewAdapter;
//import com.hyptastic.poratechai.models.users.User;
//import com.hyptastic.poratechai.utils.Constants;
//import com.hyptastic.poratechai.utils.Functions;
//
//import org.apache.commons.lang.StringUtils;
//
///**
// * Created by ISHRAK on 5/18/2018.
// */
//
//public class DialogFragmentProfile extends DialogFragment {
//    ImageView profilePicture;
//    TextView nameTextView, institutionTextView, emailTextView, contactNumberTextView;
//
//    RatingBar ratingBar;
//    RecyclerView recyclerView;
//    LinearLayoutManager reviewListLayoutManager;
//    JobFeedReviewAdapter jobFeedReviewAdapter;
//
//    Button backButton;
//    ImageButton leftButton, rightButton;
//
//    static int recyclerViewCounter;
//
//    GestureDetector gestureDetector;
//
//    @NonNull
//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        Dialog dialog = super.onCreateDialog(savedInstanceState);
//        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//
//        return dialog;
//    }
//
//    @Override
//    public void onStart() {
//        super.onStart();
//
//        final Dialog dialog = getDialog();
//        if(dialog!=null)
//        {
//            dialog.setContentView(R.layout.dialog_profile);
//            dialog.getWindow().setLayout((int)(0.88* Constants.SCREEN_WIDTH), (int)(0.88*Constants.SCREEN_HEIGHT));
//            dialog.getWindow().setBackgroundDrawableResource(R.drawable.background_listitem_job_tutor_profile);
//
//            initLayout(this, dialog);
//        }
//    }
//
//    private void initLayout(final DialogFragmentProfile currentDialogFragmentProfile, final Dialog dialog)
//    {
//        User user = (User)(getArguments().getParcelable("User"));
//        boolean tutorReview = getArguments().getBoolean("TutorReview");
//        int status = getArguments().getInt("Status");
//
//        profilePicture = dialog.findViewById(R.id.dialog_profile_picture);
//        nameTextView = dialog.findViewById(R.id.dialog_profile_name_textView);
//        institutionTextView = dialog.findViewById(R.id.dialog_profile_institution_textView);
//        emailTextView = dialog.findViewById(R.id.dialog_profile_email_textView);
//        contactNumberTextView = dialog.findViewById(R.id.dialog_profile_contact_number_textView);
//        ratingBar = dialog.findViewById(R.id.dialog_profile_review_ratingBar);
//        recyclerView = dialog.findViewById(R.id.dialog_profile_review_recyclerView);
//        backButton = dialog.findViewById(R.id.backButton);
//        leftButton = dialog.findViewById(R.id.reviewLeftButton);
//        rightButton = dialog.findViewById(R.id.reviewRightButton);
//
//        jobFeedReviewAdapter = new JobFeedReviewAdapter(getActivity(), ratingBar);
//        reviewListLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
//
//        Functions.loadProfilePicture(getActivity(), user.get_id(), new Constants.PICTURE_PACKET(getActivity(), profilePicture, null, 180, 180));
//        nameTextView.setText(user.getFullName());
//        institutionTextView.setText(StringUtils.join(user.getInstitution(), ", "));
//        if(status<Constants.JOB_STATUS.JOB_STATUS_INSPECTION){
//            emailTextView.setText("Available after Accepting");
//            emailTextView.setTextColor(Color.RED);
//
//            contactNumberTextView.setText("Available after Accepting");
//            contactNumberTextView.setTextColor(Color.RED);
//        }else{
//            emailTextView.setText(user.getEmail());
//            emailTextView.setTextColor(Color.BLACK);
//
//            contactNumberTextView.setText("0" + user.getContactNumber());
//            contactNumberTextView.setTextColor(Color.BLACK);
//        }
//
//        recyclerView.setOnTouchListener(Functions.getNonSwipeableViewTouchListener());
//
//        recyclerView.setLayoutManager(reviewListLayoutManager);
//        recyclerView.setAdapter(jobFeedReviewAdapter);
//        recyclerViewCounter= 0;
//
//        Log.e("User ID", user.get_id());
//        jobFeedReviewAdapter.loadReviews(user.get_id(), tutorReview);
//
//        backButton.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View view) {
//                dismissDialog();
//            }
//        });
//
//        leftButton.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View view) {
//                movePrevious();
//            }
//        });
//
//        rightButton.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View view) {
//                moveNext();
//            }
//        });
//    }
//
//    //DismissDialog function is called by the sendContractRequest function in ContractFunction, after successful
//    //posting of Contract Request.
//    public void dismissDialog()
//    {
//        getDialog().dismiss();
//    }
//
//    @Override
//    public void onDismiss(DialogInterface dialog) {
//        super.onDismiss(dialog);
//    }
//
//    public void moveNext(){
//        if(recyclerViewCounter<jobFeedReviewAdapter.getItemCount()-1){
//            recyclerViewCounter++;
//            recyclerView.smoothScrollToPosition(recyclerViewCounter);
//        }
//    }
//
//    public void movePrevious(){
//        if(recyclerViewCounter>0){
//            recyclerViewCounter--;
//            recyclerView.smoothScrollToPosition(recyclerViewCounter);
//        }
//    }
//}
