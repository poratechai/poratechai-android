package com.hyptastic.poratechai.fragments.login_registration;

/**
 * Created by User on 31-Jan-18.
 */

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.models.users.User;
import com.hyptastic.poratechai.retrofit.functions.UserFunctions;
import com.hyptastic.poratechai.retrofit.interfaces.UserClient;
import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;


import static com.hyptastic.poratechai.utils.Validation.validateEmail;
import static com.hyptastic.poratechai.utils.Validation.validateFields;

public class ResetPasswordDialog extends DialogFragment {
    public interface Listener {
        void onPasswordReset(String message);
    }

    public static final String TAG = ResetPasswordDialog.class.getSimpleName();

    private EditText mEtContactNumber;
    private Button mBtResetPassword;
    private ProgressBar mProgressBar;


    private String mContactNumber;

    private Listener mListner;

    UserClient userClient = null;
    UserFunctions userFunctions = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_reset_password,container,false);
        initViews(view);
        return view;
    }

    private void initViews(View v) {

        mEtContactNumber = (EditText) v.findViewById(R.id.et_contact_number);
        mBtResetPassword = (Button) v.findViewById(R.id.btn_reset_password);
        mProgressBar = (ProgressBar) v.findViewById(R.id.progress);

        mBtResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mProgressBar.setVisibility(View.VISIBLE);
                resetPasswordInit();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListner = (Listener)getActivity();
    }

    public void setToken(String token) {

    }

    public void closeDialog(){
        mProgressBar.setVisibility(View.INVISIBLE);
        dismiss();
    }

    private void resetPasswordInit() {
        mContactNumber = mEtContactNumber.getText().toString();

        int err = 0;

        if (!validateFields(mContactNumber)) {

            err++;
        }

        if (err == 0) {
            User user = new User();
            user.setContactNumber(Integer.parseInt(mEtContactNumber.getText().toString()));

            userClient = RetrofitServiceGenerator.getRetrofit();
            userFunctions = new UserFunctions(userClient,getActivity().getApplicationContext());
            userFunctions.resetPassword(this,user);
        }
        else{
            mProgressBar.setVisibility(View.GONE);
            Toast.makeText(getActivity(), "Contact number cannot be empty", Toast.LENGTH_LONG).show();
        }
    }

    public void resetPasswordError() {
        mProgressBar.setVisibility(View.GONE);
        Toast.makeText(getActivity(), "An account with this contact number does not exist !", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
