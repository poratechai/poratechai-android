package com.hyptastic.poratechai.fragments.job_feed.utilities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hyptastic.poratechai.R;

/**
 * Created by ISHRAK on 5/31/2019.
 */

public class UtilitiesChatFragment extends Fragment {
    public static UtilitiesChatFragment createInstance(){
        UtilitiesChatFragment utilitiesChatFragment = new UtilitiesChatFragment();

        return utilitiesChatFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_utilities_chat, null);

        return view;
    }
}
