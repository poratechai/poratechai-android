package com.hyptastic.poratechai.fragments.profile.tutor.profile;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.activities.profile.TutorMapActivity;
import com.hyptastic.poratechai.fragments.profile.NavigationFragment;
import com.hyptastic.poratechai.layout.FlexboxLayout;
import com.hyptastic.poratechai.models.users.User;
import com.hyptastic.poratechai.utils.Constants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

import static android.app.Activity.RESULT_OK;

/**
 * Created by ISHRAK on 2/19/2019.
 */

public class ProfileFragment extends com.hyptastic.poratechai.fragments.profile.ProfileFragment implements Serializable {
    TextView schoolTextView, collegeTextView, universityTextView, locationTextView, daysPerWeekTextView, salaryTextView;
    LinearLayout skillsSection, classesSection, subjectsSection;
    ImageButton institutionsEditButton, skillsEditButton, classesEditButton, subjectsEditButton,
                locationEditButton, daysPerWeekEditButton, salaryEditButton;

    com.hyptastic.poratechai.fragments.profile.tutor.profile.ProfileFragment currentProfileFragment;
    NavigationFragment.LoadListener loadListener = null;

    static final int PICK_RADIUS_REQUEST = 1;

    ArrayList<String> skillsArrayList, classesArrayList, subjectsArrayList;

    FlexboxLayout skillsLayout, classesLayout, subjectsLayout;
    final int flexboxWidth = (int)(0.6*Constants.SCREEN_WIDTH);

    public static ProfileFragment createInstance(NavigationFragment.LoadListener loadListener, String userId, int contractStatus){
        ProfileFragment profileFragment = new ProfileFragment();

        Bundle bundle = new Bundle();
        bundle.putParcelable("LOAD_LISTENER", loadListener);
        bundle.putString("USER_ID", userId);
        bundle.putInt("CONTRACT_STATUS", contractStatus);

        profileFragment.setArguments(bundle);

        return profileFragment;
    }

    public static ProfileFragment createInstance(NavigationFragment.LoadListener loadListener) {
        ProfileFragment profileFragment = new ProfileFragment();

        Bundle bundle = new Bundle();
        bundle.putParcelable("LOAD_LISTENER", loadListener);

        profileFragment.setArguments(bundle);

        return profileFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Bundle bundle = (getArguments()==null?new Bundle():getArguments());
        bundle.putInt("LayoutResourceId", R.layout.fragment_profile_tutor);
        setArguments(bundle);

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        schoolTextView              = view.findViewById(R.id.schoolTextView);
        collegeTextView             = view.findViewById(R.id.collegeTextView);
        universityTextView          = view.findViewById(R.id.universityTextView);
        locationTextView            = view.findViewById(R.id.locationTextView);
        daysPerWeekTextView         = view.findViewById(R.id.daysPerWeekTextView);
        salaryTextView              = view.findViewById(R.id.salaryTextView);

        skillsSection               = view.findViewById(R.id.skillsSection);
        classesSection              = view.findViewById(R.id.classesSection);
        subjectsSection             = view.findViewById(R.id.subjectsSection);

        institutionsEditButton      = view.findViewById(R.id.institutionsEditButton);
        skillsEditButton            = view.findViewById(R.id.skillsEditButton);
        classesEditButton           = view.findViewById(R.id.classesEditButton);
        subjectsEditButton          = view.findViewById(R.id.subjectsEditButton);
        locationEditButton          = view.findViewById(R.id.locationEditButton);
        daysPerWeekEditButton       = view.findViewById(R.id.daysPerWeekEditButton);
        salaryEditButton            = view.findViewById(R.id.salaryEditButton);

        currentProfileFragment = this;

        if(getArguments().containsKey("USER_ID")){
            removeButtons();            //others viewing
        }else{
            setButtonListeners();       //self viewing
        }

        if(getArguments().containsKey("LOAD_LISTENER")){
            loadListener = (NavigationFragment.LoadListener)getArguments().getParcelable("LOAD_LISTENER");
        }

        skillsArrayList = new ArrayList<String>();
        classesArrayList = new ArrayList<String>();
        subjectsArrayList = new ArrayList<String>();

        systemFunction.getSystemArrayConstants(currentProfileFragment);

        if(loadListener != null){
            loadListener.loadFragment(true);
        }
    }

    private void setButtonListeners(){
        try{
            institutionsEditButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    try{
                        DialogFragmentInstitutions.DialogFragmentInstitutionsListener dialogFragmentInstitutionsListener
                                = new DialogFragmentInstitutions.DialogFragmentInstitutionsListener() {
                            @Override
                            public void dialogFragmentInstitutionsClosed(String school, String college, String university) {
                                schoolTextView.setText(school);
                                collegeTextView.setText(college);
                                universityTextView.setText(university);
                            }

                            @Override
                            public int describeContents() {
                                return 0;
                            }

                            @Override
                            public void writeToParcel(Parcel parcel, int i) { }
                        };

                        DialogFragmentInstitutions dialogFragmentInstitutions = DialogFragmentInstitutions.createInstance(
                                dialogFragmentInstitutionsListener, schoolTextView.getText().toString(), collegeTextView.getText().toString(),
                                universityTextView.getText().toString());

                        dialogFragmentInstitutions.show(getActivity().getSupportFragmentManager(), "DialogFragmentInstitutions");
                        getActivity().getSupportFragmentManager().executePendingTransactions();
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
            });

            skillsEditButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    DialogFragmentSkills.DialogFragmentSkillsCloseListener dialogFragmentSkillsCloseListener
                            = new DialogFragmentSkills.DialogFragmentSkillsCloseListener() {
                        @Override
                        public void dialogFragmentSkillsClosed() {
                            skillsLayout.refreshFlexbox(skillsSection, skillsArrayList, flexboxWidth, false);
                        }

                        @Override
                        public int describeContents() { return 0; }

                        @Override
                        public void writeToParcel(Parcel parcel, int i) { }
                    };

                    DialogFragmentSkills dialogFragmentSkills = DialogFragmentSkills.createInstance(dialogFragmentSkillsCloseListener, Constants.SYSTEM_ARRAY.subcategories, skillsArrayList);
                    dialogFragmentSkills.show(getActivity().getSupportFragmentManager(), "Skills");
                }
            });

            classesEditButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    DialogFragmentClasses.DialogFragmentClassesCloseListener dialogFragmentClassesCloseListener =
                            new DialogFragmentClasses.DialogFragmentClassesCloseListener() {
                                @Override
                                public void dialogFragmentClassesClosed() {
                                    classesLayout.refreshFlexbox(classesSection, classesArrayList, flexboxWidth, false);
                                }

                                @Override
                                public int describeContents() { return 0; }

                                @Override
                                public void writeToParcel(Parcel parcel, int i) { }
                            };

                    DialogFragmentClasses dialogFragmentClasses = DialogFragmentClasses.createInstance(dialogFragmentClassesCloseListener, Constants.SYSTEM_ARRAY.classesToTeach, classesArrayList);
                    dialogFragmentClasses.show(getActivity().getSupportFragmentManager(), "Classes to Teach");
                }
            });

            subjectsEditButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    DialogFragmentSubjects.DialogFragmentSubjectsCloseListener dialogFragmentSubjectsCloseListener =
                            new DialogFragmentSubjects.DialogFragmentSubjectsCloseListener() {
                                @Override
                                public void dialogFragmentSubjectsClosed() {
                                    subjectsLayout.refreshFlexbox(subjectsSection, subjectsArrayList, flexboxWidth, false);
                                }

                                @Override
                                public int describeContents() { return 0; }

                                @Override
                                public void writeToParcel(Parcel parcel, int i) { }
                            };

                    DialogFragmentSubjects dialogFragmentSubjects = DialogFragmentSubjects.createInstance(dialogFragmentSubjectsCloseListener, Constants.SYSTEM_ARRAY.subjects, subjectsArrayList);
                    dialogFragmentSubjects.show(getActivity().getSupportFragmentManager(), "Subjects");
                }
            });

            locationEditButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity().getApplicationContext(), TutorMapActivity.class);
                    startActivityForResult(intent,PICK_RADIUS_REQUEST);
                }
            });

            daysPerWeekEditButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    int daysPerWeek = 0;

                    if(!daysPerWeekTextView.getText().toString().equals("")){
                        daysPerWeek = Integer.parseInt(daysPerWeekTextView.getText().toString());
                    }

                    DialogFragmentDaysPerWeek.DialogFragmentDaysPerWeekListener dialogFragmentDaysPerWeekListener =
                            new DialogFragmentDaysPerWeek.DialogFragmentDaysPerWeekListener() {
                                @Override
                                public void dialogFragmentDaysPerWeekClosed(String daysPerWeek) {
                                    daysPerWeekTextView.setText(daysPerWeek);
                                }

                                @Override
                                public int describeContents() { return 0; }

                                @Override
                                public void writeToParcel(Parcel parcel, int i) { }
                            };

                    DialogFragmentDaysPerWeek dialogFragmentDaysPerWeek = DialogFragmentDaysPerWeek.createInstance(dialogFragmentDaysPerWeekListener, daysPerWeek);
                    dialogFragmentDaysPerWeek.show(getActivity().getSupportFragmentManager(), "Days Per Week");
                }
            });

            salaryEditButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    int salary = 0;

                    if(!salaryTextView.getText().toString().equals("")){
                        salary = Integer.parseInt(salaryTextView.getText().toString());
                    }

                    DialogFragmentSalary.DialogFragmentSalaryListener dialogFragmentSalaryListener =
                            new DialogFragmentSalary.DialogFragmentSalaryListener() {
                                @Override
                                public void dialogFragmentSalaryClosed(String salary) {
                                    salaryTextView.setText(salary);
                                }

                                @Override
                                public int describeContents() { return 0; }

                                @Override
                                public void writeToParcel(Parcel parcel, int i) { }
                            };

                    DialogFragmentSalary dialogFragmentSalary = DialogFragmentSalary.createInstance(dialogFragmentSalaryListener, salary);
                    dialogFragmentSalary.show(getActivity().getSupportFragmentManager(), "Salary");
                }
            });
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void removeButtons(){
        verificationImagesButton.setVisibility(View.INVISIBLE);
        institutionsEditButton.setVisibility(View.INVISIBLE);
        skillsEditButton.setVisibility(View.INVISIBLE);
        classesEditButton.setVisibility(View.INVISIBLE);
        subjectsEditButton.setVisibility(View.INVISIBLE);
        locationEditButton.setVisibility(View.INVISIBLE);
        daysPerWeekEditButton.setVisibility(View.INVISIBLE);
        salaryEditButton.setVisibility(View.INVISIBLE);
        changePasswordButton.setVisibility(View.INVISIBLE);
        logoutButton.setVisibility(View.INVISIBLE);
    }

    //--------------------------------------------------------------------------------------------------------------------------------
    //Server callback - start
    public void systemArrayConstantsGotten(){
        String userId = getArguments().getString("USER_ID");
        if(userId==null){
            userFunctions.getTutorProfileForViewSelf(currentProfileFragment);
        }else{
            userFunctions.getTutorProfileForViewOthers(currentProfileFragment, userId);
        }
    }

    public void tutorProfileGotten(User user){
        try{
            if(getArguments().containsKey("CONTRACT_STATUS")){
                setContactData(user.getEmail(), user.getContactNumber().toString(), getArguments().getInt("CONTRACT_STATUS"));
            }else{
                setContactData(user.getEmail(), user.getContactNumber().toString());
            }

            if(user.getInstitution() != null && user.getInstitution().length > 0){
                schoolTextView.setText(user.getInstitution()[0]);
                collegeTextView.setText(user.getInstitution()[1]);
                universityTextView.setText(user.getInstitution()[2]);
            }

            setVerificationImagesGallery(Arrays.asList(user.getVerificationImages()));

            skillsArrayList = new ArrayList<String>(Arrays.asList(user.getPreferredSubcategory()));
            classesArrayList = new ArrayList<String>(Arrays.asList(user.getPreferredClassToTeach()));
            subjectsArrayList = new ArrayList<String>(Arrays.asList(user.getPreferredSubjects()));

            skillsLayout = new FlexboxLayout(getActivity(),skillsArrayList, skillsSection, flexboxWidth, false);
            classesLayout = new FlexboxLayout(getActivity(), classesArrayList, classesSection, flexboxWidth, false);
            subjectsLayout = new FlexboxLayout(getActivity(), subjectsArrayList, subjectsSection, flexboxWidth, false);

            locationTextView.setText(user.getLocation());
            daysPerWeekTextView.setText(String.valueOf(user.getPreferredDaysPerWeek()));
            salaryTextView.setText(String.valueOf(user.getMinPreferredSalary()));

            if(loadListener != null){
                loadListener.loadFragment(false);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    //Server callback - end
    //--------------------------------------------------------------------------------------------------------------------------------

    //--------------------------------------------------------------------------------------------------------------------------------
    //Location - start
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_RADIUS_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                String address = data.getStringExtra("address");

                locationTextView.setText(address);
            }
        }
    }
    //Location - end
    //--------------------------------------------------------------------------------------------------------------------------------
}
