package com.hyptastic.poratechai.fragments.login_registration;

import android.app.Fragment;

/**
 * Created by User on 31-Jan-18.
 */

import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.activities.HireOrTeachActivity;
import com.hyptastic.poratechai.activities.PorateChaiActivity;
import com.hyptastic.poratechai.models.users.Response;
import com.hyptastic.poratechai.retrofit.functions.UserFunctions;
import com.hyptastic.poratechai.retrofit.interfaces.UserClient;
import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
import com.hyptastic.poratechai.utils.Functions;


import java.io.Serializable;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.hyptastic.poratechai.utils.Validation.validateEmail;
import static com.hyptastic.poratechai.utils.Validation.validateFields;
import static com.hyptastic.poratechai.utils.Validation.validatePasswordLength;

public class RegisterFragment extends Fragment implements Serializable, TermsAndConditionsDialog.Listener{
    UserClient userClient = null;
    UserFunctions userFunctions = null;

    private String userId;

    public static final String TAG = RegisterFragment.class.getSimpleName();
    private static RegisterFragment instance = null;

    TermsAndConditionsDialog fragment;

    private EditText mEtName;
    private EditText mEtEmail;
    private EditText mEtPassword;
    private EditText mEtConfirmPassword;
    private EditText mEtContactNumber;
    private Button   mBtRegister;
    private TextView mTvLogin;
    private RadioGroup mRgGender;
    private RadioButton mRbGender;
    private int selectedId;
    private ProgressBar mProgressbar;

    private CheckBox mCbTermsAndConditions;
    private TextView mTvTermsAndConditions;
    private Boolean termsAndConditionsChecked = false;

    private int passwordError = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        instance = this;
        View view = inflater.inflate(R.layout.fragment_register,container,false);
        initViews(view,this);
        return view;
    }

    private void initViews(View v, final RegisterFragment registerFragment) {

        mEtName = (EditText) v.findViewById(R.id.et_name);
//        mEtEmail = (EditText) v.findViewById(R.id.et_email);
//        mEtPassword = (EditText) v.findViewById(R.id.et_password);
//        mEtConfirmPassword = (EditText) v.findViewById(R.id.et_confirm_password);
        mEtContactNumber = (EditText)v.findViewById(R.id.et_contact_number);
        mBtRegister = (Button) v.findViewById(R.id.btn_register);
        mTvLogin = (TextView) v.findViewById(R.id.tv_login);
        mRgGender = (RadioGroup) v.findViewById(R.id.radioGender);
        mProgressbar = (ProgressBar) v.findViewById(R.id.progress);
        mCbTermsAndConditions = (CheckBox) v.findViewById(R.id.terms_and_conditions_checkbox);
        mTvTermsAndConditions = (TextView) v.findViewById(R.id.terms_and_conditions_textview);

        mBtRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                register(registerFragment, v);
            }
        });

        mTvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToLogin();
            }
        });

        mCbTermsAndConditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean checked = ((CheckBox)view).isChecked();
                if(checked){
                    termsAndConditionsChecked = true;
                }
                else{
                    termsAndConditionsChecked = false;
                }
            }
        });

        mTvTermsAndConditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTermsAndConditionsDiaglog();
            }
        });

//        mEtConfirmPassword.addTextChangedListener(new TextWatcher() {
//            public void afterTextChanged(Editable s) {
//                String strPass1 = mEtPassword.getText().toString();
//                String strPass2 = mEtConfirmPassword.getText().toString();
//                if (strPass1.equals(strPass2)) {
//                    passwordError = 0;
//                } else {
//                    mEtConfirmPassword.setError("Passwords don't Match");
//                    passwordError++;
//                }
//            }
//
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
//            public void onTextChanged(CharSequence s, int start, int before, int count) {}
//        });

    }

    private void register(final RegisterFragment registerFragment, View view) {

        setError();

        int err = 0;

        selectedId = mRgGender.getCheckedRadioButtonId();
        mRbGender = (RadioButton)view.findViewById(selectedId);

        int contactNumber=0;
        String name = mEtName.getText().toString();
//        String email = mEtEmail.getText().toString();
//        String password = mEtPassword.getText().toString();
        String gender;

        if(termsAndConditionsChecked == false){
            err++;
            Toast.makeText(registerFragment.getActivity(), "Check Terms and Conditions", Toast.LENGTH_LONG).show();
        }

        if(mRbGender.getText().toString().equals("Male")){
            gender = "m";
        }
        else{
            gender = "f";
        }

        String profilePicture = "http://www.essetinoconnexions.com/wp-content/uploads/2015/08/default.jpg";

        try {
            contactNumber = Integer.parseInt(mEtContactNumber.getText().toString());
        }
        catch (NumberFormatException e) {
            // s is not a valid integer
            err++;
            mEtContactNumber.setError("Number Should not be empty");
        }


        if (!validateFields(name)) {
            err++;
            mEtName.setError("Name should not be empty !");
        }

        if (!validateFields(gender)) {
            err++;
            mRbGender.setError("Gender should not be empty !");

        }

//        if (!validateEmail(email)) {
//            err++;
//            mEtEmail.setError("Email should be valid !");
//        }

//        if (!validateFields(password) || !validatePasswordLength(password.length())) {
//            err++;
//            System.out.println(password.length());
//            mEtPassword.setError("Password should not be empty or less than _ characters!");
//        }


        if (err == 0) {
//            userClient = RetrofitServiceGenerator.getRetrofit();
//            userFunctions = new UserFunctions(userClient,getActivity().getApplicationContext());
//            userFunctions.registerUser(registerFragment, name,contactNumber,gender,profilePicture);
            showConfirmCodeDialog(name, contactNumber, gender, profilePicture);
        }
        else if(passwordError != 0){
            showSnackBarMessage("Passwords don't match");
        }
        else {
            showSnackBarMessage("Enter Valid DetailsInterface !");
        }
    }

    private void setError() {

        mEtName.setError(null);
//        mEtEmail.setError(null);
//        mEtPassword.setError(null);
        mEtContactNumber.setError(null);
    }

    public void goToLogin(){

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        LoginFragment fragment = new LoginFragment();
        ft.replace(R.id.fragmentFrame, fragment).addToBackStack(LoginFragment.TAG);
        ft.commit();
    }

    public void login(int contactNumber){
        userClient = RetrofitServiceGenerator.getRetrofitForAuthentication(contactNumber);
        userClient.login()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        CompositeDisposable compositeDisposable =new CompositeDisposable();
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(Response response) {
                        try{
                            Log.v("Response from LOG", response.getInstitution());
                            Functions.storeLoginDataToSharedPreferences(response.getMessage(), response.getToken(), response.getUserId(), response.getProfilePicturePath(), response.getFullName(), response.getInstitution(), response.getIsTutor(), response.getIsVerified(), response.getTutorUpdateFlag());
                            if(response.getIsTutor().equals("true") || response.getIsTutor().equals("false")){
                                Intent intent = new Intent(getActivity(), PorateChaiActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().finish();
                            }
                            else {
                                Intent intent = new Intent(getActivity(), HireOrTeachActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().finish();
                            }
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        try
                        {
                            Log.e("Login Failed ", e.getMessage());
                            Toast.makeText(getActivity().getApplicationContext(),"Login Failed",Toast.LENGTH_LONG).show();
                        }
                        catch(Exception exception)
                        {
                            Log.e("User Creation EXCEPTION", exception.getMessage());
                        }
                    }

                    @Override
                    public void onComplete() {
                        Log.v("Login", "Successful");
                        String id = "";
                    }
                });
    }

    public void showDialog(){
        ConfirmCodeDialog fragment = new ConfirmCodeDialog();

        fragment.show(getFragmentManager(), ConfirmCodeDialog.TAG);
    }

    public void showConfirmCodeDialog(String name,int contactNumber, String gender, String profilePicture){
        ConfirmCodeDialog fragment = new ConfirmCodeDialog();
        Bundle args = new Bundle();
        args.putInt("contactNumber", contactNumber);
        args.putInt("login", 0);
        args.putString("name", name);
        args.putString("gender", gender);
        args.putString("profilePicture", profilePicture);
        fragment.setArguments(args);

        fragment.show(getFragmentManager(), ConfirmCodeDialog.TAG);
    }

    public void showTermsAndConditionsDiaglog(){
        fragment = TermsAndConditionsDialog.createTermsAndConditionsDialog(this);
        fragment.show(getFragmentManager(), TermsAndConditionsDialog.TAG);
    }

    @Override
    public void accepted() {
        mCbTermsAndConditions.setChecked(true);
        termsAndConditionsChecked = true;
        fragment.dismiss();
    }

    @Override
    public void declined() {
        mCbTermsAndConditions.setChecked(false);
        termsAndConditionsChecked = false;
        fragment.dismiss();
    }

    @Override
    public void close(){
        fragment.dismiss();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
     }

    private void showSnackBarMessage(String message) {
        if (getView() != null) {
            Snackbar.make(getView(),message,Snackbar.LENGTH_SHORT).show();
        }
    }

    public static RegisterFragment getInstance() {
        return instance;
    }
}
