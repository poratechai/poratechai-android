package com.hyptastic.poratechai.fragments.inbox;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.fragments.PorateChaiNavigationFragment;

public class NavigationFragment extends PorateChaiNavigationFragment {
	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_navigation_inbox, null);

		return view;
	}

	@Override
	public void loadFragment() {

	}
}
