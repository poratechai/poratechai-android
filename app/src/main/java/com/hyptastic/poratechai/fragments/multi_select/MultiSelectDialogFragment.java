package com.hyptastic.poratechai.fragments.multi_select;

import android.app.Dialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.models.multi_select.MultiSelectPrimary;
import com.hyptastic.poratechai.models.multi_select.MultiSelectSecondary;
import com.hyptastic.poratechai.retrofit.functions.SystemFunction;
import com.hyptastic.poratechai.retrofit.functions.UserFunctions;
import com.hyptastic.poratechai.retrofit.interfaces.SystemClient;
import com.hyptastic.poratechai.retrofit.interfaces.UserClient;
import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
import com.hyptastic.poratechai.utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ISHRAK on 1/15/2019.
 */

public class MultiSelectDialogFragment extends DialogFragment {
    MultiSelectDialogFragment currentFragment;

    protected ArrayList<String> selectedItems;

    protected TextView multiSelectTitle;
    protected ScrollView multiSelectPrimaryScrollView;
    protected LinearLayout multiSelectPrimaryList;
    protected Button confirmButton, cancelButton;

    protected UserFunctions userFunctions;

    public static void createInstance(MultiSelectDialogFragment subClassFragment, HashMap<String, String[]> dictionary, ArrayList<String> selectedItems){
        Bundle args = new Bundle();
        args.putSerializable("Dictionary", dictionary);
        args.putSerializable("Selected", selectedItems);

        subClassFragment.setArguments(args);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        currentFragment = this;

        userFunctions = new UserFunctions(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, UserClient.class), getActivity());

        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();

        final Dialog dialog = getDialog();

        if(dialog!=null){
            dialog.setContentView(R.layout.dialog_multiselect);

            double multiSelectDialogHeightMultiplier = 0.92;

            HashMap<String, String[]> dictionary = (HashMap<String, String[]>)getArguments().getSerializable("Dictionary");
            selectedItems = (ArrayList<String>)getArguments().getSerializable("Selected");

            String[] firstValue = dictionary.get(dictionary.keySet().toArray()[0]);

            if(dictionary.keySet().size()>=3){
                multiSelectDialogHeightMultiplier = 0.92;
            }else if(firstValue.length>=3){
                multiSelectDialogHeightMultiplier = 0.92;
            }else{
                multiSelectDialogHeightMultiplier = 0.5;
            }

            dialog.getWindow().setLayout((int)(0.92*Constants.SCREEN_WIDTH), (int)(multiSelectDialogHeightMultiplier*Constants.SCREEN_HEIGHT));
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.background_listitem_job_tutor_profile);

            multiSelectTitle                = (TextView)dialog.findViewById(R.id.multiSelectTitle);
            multiSelectPrimaryScrollView    = (ScrollView)dialog.findViewById(R.id.multiSelectPrimaryScrollView);
            multiSelectPrimaryList          = (LinearLayout)dialog.findViewById(R.id.multiSelectPrimaryList);
            cancelButton                    = (Button)dialog.findViewById(R.id.cancelButton);
            confirmButton                   = (Button)dialog.findViewById(R.id.confirmButton);

            multiSelectPrimaryScrollView.getLayoutParams().height = (int)(0.75*multiSelectDialogHeightMultiplier*Constants.SCREEN_HEIGHT);

            multiSelectTitle.setText(getTag());
            initView(convertDictionaryToMultiSelectModel(dictionary));

            cancelButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }
    }

    public ArrayList<MultiSelectPrimary> convertDictionaryToMultiSelectModel(HashMap<String, String[]> dictionary) {
        ArrayList<MultiSelectPrimary> items = new ArrayList<MultiSelectPrimary>();

        for(String key: dictionary.keySet()){
            ArrayList<MultiSelectSecondary> multiSelectSecondary = new ArrayList<MultiSelectSecondary>();

            for(String value: dictionary.get(key)){
                multiSelectSecondary.add(new MultiSelectSecondary((selectedItems!=null && selectedItems.contains(value)), value));
            }

            MultiSelectPrimary item = new MultiSelectPrimary(key, multiSelectSecondary);

            items.add(item);
        }
        return items;
    }

    public void initView(ArrayList<MultiSelectPrimary> items){
        LinearLayout.LayoutParams multiSelectItemLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        multiSelectItemLayoutParams.setMargins(25, 5, 0, 0);

        for(MultiSelectPrimary multiSelectPrimaryItem: items){
            TextView multiSelectPrimaryTitle = new TextView(getActivity());
            multiSelectPrimaryTitle.setText(multiSelectPrimaryItem.getTitle());
            multiSelectPrimaryTitle.setTypeface(null, Typeface.BOLD);
            multiSelectPrimaryTitle.setTextSize(18);

            multiSelectPrimaryList.addView(multiSelectPrimaryTitle);

            for(MultiSelectSecondary multiSelectSecondaryItem: multiSelectPrimaryItem.getMultiSelectSecondaryItem()){
                LinearLayout newItem = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.listitem_multiselect_secondary, null);

                CheckBox newItemCheckBox = newItem.findViewById(R.id.multiSelectCheckBox);
                TextView newItemTextView = newItem.findViewById(R.id.multiSelectTextView);

                newItemCheckBox.setChecked(multiSelectSecondaryItem.isChecked());
                newItemTextView.setText(multiSelectSecondaryItem.getItem());

                newItemCheckBox.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener(){
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if(b){
                            if(selectedItems != null && selectedItems.contains(multiSelectSecondaryItem.getItem())){

                            }else{
                                selectedItems.add(multiSelectSecondaryItem.getItem());
                            }
                        }else{
                            selectedItems.remove(multiSelectSecondaryItem.getItem());
                        }
                    }
                });

                multiSelectPrimaryList.addView(newItem, multiSelectItemLayoutParams);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) { }
}
