package com.hyptastic.poratechai.fragments.profile.tutor.reviews;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hyptastic.poratechai.R;

/**
 * Created by ISHRAK on 2/19/2019.
 */

public class ReviewsFragment extends com.hyptastic.poratechai.fragments.profile.ReviewsFragment {
    public static ReviewsFragment createInstance(String userId){
        ReviewsFragment reviewsFragment = new ReviewsFragment();

        Bundle bundle = new Bundle();
        bundle.putString("USER_ID", userId);

        reviewsFragment.setArguments(bundle);

        return reviewsFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Bundle bundle = (getArguments()==null?new Bundle():getArguments());
        bundle.putInt("LayoutId", R.layout.fragment_reviews_tutor);
        setArguments(bundle);

        View view = super.onCreateView(inflater, container, savedInstanceState);

        return view;
    }
}
