package com.hyptastic.poratechai.fragments.job_feed.parent_student;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.hyptastic.poratechai.activities.job_feed.PostJobActivity;
import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.adapters.job_feed.parent_student.PostedJobsAdapter;
import com.hyptastic.poratechai.fragments.job_feed.JobFeedFragment;

/**
 * Created by ISHRAK on 2/26/2018.
 */

public class PostedJobsFragment extends JobFeedFragment {
    private Button addJobButton = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.setJobFeedAdapter(new PostedJobsAdapter(getActivity()));

        final View rootView = super.onCreateView(inflater, container, savedInstanceState);

        addJobButton = (Button)rootView.findViewById(R.id.addJobButton);
        addJobButton.getLayoutParams().height = RelativeLayout.LayoutParams.WRAP_CONTENT;

        addJobButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(rootView.getContext(), PostJobActivity.class);
                startActivity(intent);
            }
        });

        registerBroadcastReceiver();

        return rootView;
    }

    //Broadcast Receiver - Overriding super class's method

    @Override
    public void registerBroadcastReceiver()
    {
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(feedUpdateNotificationReceiver, new IntentFilter("update_feed_posted_job"));
    }
}