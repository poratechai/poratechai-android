package com.hyptastic.poratechai.fragments.payment.tutor;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.models.wallet.InvoiceResponse;
import com.hyptastic.poratechai.retrofit.functions.WalletFunction;
import com.hyptastic.poratechai.retrofit.interfaces.WalletClient;
import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
import com.hyptastic.poratechai.utils.Constants;

/**
 * Created by ISHRAK on 6/25/2018.
 */

public class InvoiceFragment extends Fragment {
    InvoiceResponse invoiceResponse;

    TextView invoiceNumber, nameTextView, addressTextView, quantityTextView, productTextView, priceTextView, totalTextView;
    ImageButton backButton, cancelButton;
    FloatingActionButton proceedButton;

    WalletFunction walletFunction;

    public static InvoiceFragment createNewInstance(InvoiceResponse invoiceResponse){
        InvoiceFragment invoiceFragment = new InvoiceFragment();

        Bundle args = new Bundle();
        args.putParcelable("invoiceResponse", invoiceResponse);
        invoiceFragment.setArguments(args);

        return invoiceFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;

        try{
            view = inflater.inflate(R.layout.fragment_payment_invoice, container, false);

            walletFunction = new WalletFunction(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, WalletClient.class), getActivity());

            invoiceResponse = (InvoiceResponse)getArguments().getParcelable("invoiceResponse");

            invoiceNumber = view.findViewById(R.id.invoiceId);
            nameTextView = view.findViewById(R.id.nameTextView);
            addressTextView = view.findViewById(R.id.addressTextView);
            quantityTextView = view.findViewById(R.id.quantityTextView);
            productTextView = view.findViewById(R.id.productTextView);
            priceTextView = view.findViewById(R.id.priceTextView);
            totalTextView = view.findViewById(R.id.totalTextView);
            backButton = view.findViewById(R.id.backButton);
            cancelButton = view.findViewById(R.id.cancelButton);
            proceedButton = view.findViewById(R.id.proceedButton);

            invoiceNumber.setText("#" + invoiceResponse.getData().getInvoiceId());
            nameTextView.setText(invoiceResponse.getData().getBuyerName());
            addressTextView.setText(invoiceResponse.getData().getBuyerAddress());

            quantityTextView.setText(String.valueOf(invoiceResponse.getData().getProductDescription().split("\\.")[0]) + " month(s)");
            productTextView.setText(invoiceResponse.getData().getProductName());
            priceTextView.setText(invoiceResponse.getData().getAmount() + " Tk.");
            totalTextView.setText(invoiceResponse.getData().getAmount() + " Tk.");

            backButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    getActivity().onBackPressed();
                }
            });

            cancelButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    deleteInvoice();
                }
            });

            proceedButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    //TODO Check
//                    Intent intent = new Intent(getActivity(), PaymentProcessingActivity.class);
//                    intent.putExtra("invoiceResponse", invoiceResponse);
//                    startActivity(intent);
                }
            });
        }catch(Exception e){
            e.printStackTrace();
        }

        return view;
    }

    private void deleteInvoice(){
        walletFunction.deleteInvoice(this, invoiceResponse.getData().getInvoiceId());
    }

    public void invoiceDeleted(){
        getActivity().onBackPressed();
    }
}
