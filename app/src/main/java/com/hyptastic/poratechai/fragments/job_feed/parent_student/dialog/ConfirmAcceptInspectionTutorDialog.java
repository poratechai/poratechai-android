package com.hyptastic.poratechai.fragments.job_feed.parent_student.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.adapters.job_feed.parent_student.ContractRequestsAdapter;

import java.io.Serializable;

/**
 * Created by ISHRAK on 3/20/2018.
 */

public class ConfirmAcceptInspectionTutorDialog extends DialogFragment {
    public interface ConfirmAcceptInspectionTutorListener extends Serializable {
        public void onAcceptInspectionTutorPositive(String matchId);
    }

    private ConfirmAcceptInspectionTutorListener mListener;

    public static ConfirmAcceptInspectionTutorDialog createNewInstance(ConfirmAcceptInspectionTutorListener acceptListener, String matchId)
    {
        ConfirmAcceptInspectionTutorDialog confirmAcceptInspectionTutorDialog = new ConfirmAcceptInspectionTutorDialog();

        Bundle bundle = new Bundle();
        bundle.putSerializable("listener", acceptListener);
        bundle.putString("match_id", matchId);

        confirmAcceptInspectionTutorDialog.setArguments(bundle);

        return confirmAcceptInspectionTutorDialog;

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final ConfirmAcceptInspectionTutorListener mListener = (ConfirmAcceptInspectionTutorListener) getArguments().getSerializable("listener");
        final String matchId = getArguments().getString("match_id");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.accept_inspection_tutor_confirmation)
                .setTitle(R.string.accept_inspection_tutor_title)
                .setPositiveButton(R.string.accept_inspection_tutor_positive, new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mListener.onAcceptInspectionTutorPositive(matchId);
                    }
                })
                .setNegativeButton(R.string.accept_inspection_tutor_negative, new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

        return builder.create();
    }
}
