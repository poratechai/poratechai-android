package com.hyptastic.poratechai.fragments.login_registration;

import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.hyptastic.poratechai.activities.HireOrTeachActivity;
import com.hyptastic.poratechai.activities.PorateChaiActivity;
import com.hyptastic.poratechai.activities.authentication.AuthenticationActivity;
import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.fragments.profile.ProfileFragment;
import com.hyptastic.poratechai.models.users.Response;
import com.hyptastic.poratechai.retrofit.functions.UserFunctions;
import com.hyptastic.poratechai.retrofit.interfaces.UserClient;
import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
import com.hyptastic.poratechai.utils.Constants;
import com.hyptastic.poratechai.utils.Functions;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by User on 21-Apr-18.
 */

public class ConfirmCodeDialog extends DialogFragment {
    public interface Listener {
        void onConfirmCode(String message);
    }

    UserClient userClient = null;
    UserFunctions userFunctions = null;

    public static final String TAG = ConfirmCodeDialog.class.getSimpleName();

    private FirebaseAuth mAuth;

    private EditText mEtCode;
    private Button mBtConfirm;
    private TextView mTvCode;
    private TextView mTvResendCode;
    private TextView mTvCountdownTimer;
    public int counter;

    private String mVerificationId;
//    private String mResendToken;
    PhoneAuthProvider.ForceResendingToken mResendToken;
    private String code;
    private Integer login;
    private String name;
    private String gender;
    private String profilePicture;
    private Integer contactNumber;

    private Listener mListner;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_confirm_code,container,false);
        this.getDialog().setCanceledOnTouchOutside(false);
        setCancelable(false);
        initViews(view);
        return view;
    }

    private void initViews(View v) {
        contactNumber = getArguments().getInt("contactNumber");
        login = getArguments().getInt("login");
        if(login == 0){
            name = getArguments().getString("name");
            gender = getArguments().getString("gender");
            profilePicture = getArguments().getString("profilePicture");
        }
        mAuth = FirebaseAuth.getInstance();
        mTvResendCode = (TextView) v.findViewById(R.id.resend_code_text_view);
        mTvCountdownTimer = (TextView)v.findViewById(R.id.countdown_timer_text_view);
//        startTimer();

        mEtCode = (EditText) v.findViewById(R.id.et_code);
        mBtConfirm = (Button) v.findViewById(R.id.btn_confirm);


        mBtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String code = mEtCode.getText().toString().trim();
                if (code.isEmpty() || code.length() < 6) {
                    mEtCode.setError("Enter valid code");
                    mEtCode.requestFocus();
                    return;
                }
                verifyVerificationCode(code);
            }
        });

        mTvResendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTvResendCode.setVisibility(View.INVISIBLE);
                resendVerificationCode(String.valueOf(contactNumber), mResendToken);
            }
        });

        sendVerificationCode(String.valueOf(contactNumber));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof AuthenticationActivity) mListner = (AuthenticationActivity)context;
        if(context instanceof PorateChaiActivity) mListner = (PorateChaiActivity)context;
    }

    private void verifyCode(){
        code = mEtCode.getText().toString();
        System.out.println(Constants.EMAIL);
        System.out.println(Constants.EMAIL_VALUE);

        userClient = RetrofitServiceGenerator.getRetrofit();
        userFunctions = new UserFunctions(userClient,getActivity().getApplicationContext());
        userFunctions.verifyCode(this,code);
    }

    public void verificationSuccess(boolean isSuccessful){
        if(isSuccessful) {
            Functions.storeSmsVerificationStatus("true");

            if(Constants.EMAIL_VALUE.equals("") || Constants.EMAIL_VALUE == null){
                dismiss();
                goToLogin();
            }
            else {
                dismiss();
            }
        }else{
            Functions.storeSmsVerificationStatus("false");
        }
    }

    private void goToLogin(){
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        LoginFragment fragment = new LoginFragment();
        ft.replace(R.id.fragmentFrame, fragment).addToBackStack(null);
        ft.commit();
    }
    
    private void resendCode(){
        startTimer();
        userClient = RetrofitServiceGenerator.getRetrofit();
        userFunctions = new UserFunctions(userClient,getActivity().getApplicationContext());
        userFunctions.resendCode(this);
    }

    private void startTimer(){
        mTvCountdownTimer.setVisibility(View.VISIBLE);
        new CountDownTimer(15000, 1000){
            public void onTick(long millisUntilFinished){
                mTvCountdownTimer.setText("Wait to resend: " + millisUntilFinished / 1000 + " secs");
                counter++;
            }
            public  void onFinish(){
                mTvCountdownTimer.setVisibility(View.INVISIBLE);
                mTvResendCode.setVisibility(View.VISIBLE);
            }
        }.start();
    }

    private void sendVerificationCode(String contactNumber) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                "+880" + contactNumber,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallbacks);
    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

            //Getting the code sent by SMS
            String code = phoneAuthCredential.getSmsCode();

            //sometime the code is not detected automatically
            //in this case the code will be null
            //so user has to manually enter the code
            if (code != null) {
                mEtCode.setText(code);
                //verifying the code
                verifyVerificationCode(code);
            }
            else {
                signInWithPhoneAuthCredential(phoneAuthCredential);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
            Log.d("FB", e.getMessage());
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);

            //storing the verification id that is sent to the user
            mVerificationId = s;
            mResendToken = forceResendingToken;
            startTimer();
        }
    };

    private void resendVerificationCode(String contactNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                "+880" + contactNumber,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallbacks,
                token);
    }

    private void verifyVerificationCode(String code) {
        //creating the credential
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);

        //signing the user
        signInWithPhoneAuthCredential(credential);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            //verification successful we will start the profile activity
                            if(login == 1){
                                userClient = RetrofitServiceGenerator.getRetrofitForAuthentication(contactNumber);
                                userClient.login()
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new Observer<Response>() {
                                            @Override
                                            public void onSubscribe(Disposable d) {
                                                CompositeDisposable compositeDisposable =new CompositeDisposable();
                                                compositeDisposable.add(d);
                                            }

                                            @Override
                                            public void onNext(Response response) {
                                                try{
                                                    Log.v("Response from LOG", response.getInstitution());
                                                    Functions.storeLoginDataToSharedPreferences(response.getMessage(), response.getToken(), response.getUserId(), response.getProfilePicturePath(), response.getFullName(), response.getInstitution(), response.getIsTutor(), response.getIsVerified(), response.getTutorUpdateFlag());
                                                    if(response.getIsTutor().equals("true") || response.getIsTutor().equals("false")){
                                                        Intent intent = new Intent(getActivity(), PorateChaiActivity.class);
                                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                        startActivity(intent);
                                                        getActivity().finish();
                                                    }
                                                    else {
                                                        Intent intent = new Intent(getActivity(), HireOrTeachActivity.class);
                                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                        startActivity(intent);
                                                        getActivity().finish();
                                                    }
                                                }catch(Exception e){
                                                    e.printStackTrace();
                                                }
                                            }

                                            @Override
                                            public void onError(Throwable e) {
                                                try
                                                {
                                                    Log.e("Login Failed ", e.getMessage());
                                                    Toast.makeText(getActivity().getApplicationContext(),"Login Failed",Toast.LENGTH_LONG).show();
                                                }
                                                catch(Exception exception)
                                                {
                                                    Log.e("User Creation EXCEPTION", exception.getMessage());
                                                }
                                            }

                                            @Override
                                            public void onComplete() {
                                                Log.v("Login", "Successful");
                                                String id = "";
                                            }
                                        });
                            }
                            else {
                                dismiss();
                                RegisterFragment registerFragment = new RegisterFragment();
                                userClient = RetrofitServiceGenerator.getRetrofit();
                                userFunctions = new UserFunctions(userClient,getActivity().getApplicationContext());
                                userFunctions.registerUser(registerFragment.getInstance(), name,contactNumber,gender,profilePicture);
                            }

                        } else {

                            //verification unsuccessful.. display an error message

                            String message = "Somthing is wrong, we will fix it soon...";

                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                message = "Invalid code entered ";
                            }

                            Log.d("FB", message);
                            Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
