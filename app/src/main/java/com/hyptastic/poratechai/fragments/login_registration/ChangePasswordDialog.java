package com.hyptastic.poratechai.fragments.login_registration;


import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.fragments.profile.DialogFragmentVerificationImages;
import com.hyptastic.poratechai.models.users.ChangePassword;
import com.hyptastic.poratechai.retrofit.functions.UserFunctions;
import com.hyptastic.poratechai.retrofit.interfaces.UserClient;
import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
import com.hyptastic.poratechai.utils.Constants;

import static com.hyptastic.poratechai.utils.Validation.validatePasswordLength;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChangePasswordDialog extends DialogFragment {
    UserClient userClient = null;
    UserFunctions userFunctions = null;

    private EditText mEtOldPassword;
    private EditText mEtNewPassword;
    private Button mBtConfirm;
    private Button mBtCancel;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        userFunctions = new UserFunctions(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, UserClient.class), getActivity());

        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();

        final Dialog dialog = getDialog();

        if(dialog != null){
            dialog.setContentView(R.layout.dialog_change_password);

            dialog.getWindow().setLayout((int)(0.92*Constants.SCREEN_WIDTH), (int)(0.5*Constants.SCREEN_HEIGHT));
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.background_listitem_job_tutor_profile);
        }

        initViews();
    }

    private void initViews() {
        mEtOldPassword = (EditText) getDialog().findViewById(R.id.et_old_password);
        mEtNewPassword = (EditText) getDialog().findViewById(R.id.et_new_password);
        mBtConfirm = (Button) getDialog().findViewById(R.id.btn_confirm);
        mBtCancel = (Button) getDialog().findViewById(R.id.btn_cancel);

        mBtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initiateChangePassword();
            }
        });

        mBtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeDialog();
            }
        });
    }

    private void initiateChangePassword(){
        ChangePassword changePassword = new ChangePassword();

        changePassword.oldPassword = mEtOldPassword.getText().toString();
        changePassword.newPassword = mEtNewPassword.getText().toString();

        if(!changePassword.oldPassword.isEmpty() && !changePassword.newPassword.isEmpty()){
            if(!validatePasswordLength(changePassword.newPassword.length())){
                Toast.makeText(getActivity().getApplicationContext(), "Password should be greater than 3 characters", Toast.LENGTH_SHORT).show();
            }
            else{
                userClient = RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, UserClient.class);
                userFunctions = new UserFunctions(userClient,getActivity().getApplicationContext());
                userFunctions.changePassword(this, changePassword);
            }
        }
    }

    public void closeDialog(){
        dismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
