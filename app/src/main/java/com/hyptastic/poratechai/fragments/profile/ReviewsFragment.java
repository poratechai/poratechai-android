package com.hyptastic.poratechai.fragments.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.adapters.profile.ReviewsAdapter;
import com.hyptastic.poratechai.utils.Constants;

/**
 * Created by ISHRAK on 3/11/2019.
 */

public class ReviewsFragment extends Fragment {
    int layoutResourceId = -1;
    private RecyclerView reviewRecyclerView;
    private ReviewsAdapter reviewsAdapter;
    private RecyclerView.LayoutManager reviewLayoutManager;

    boolean tutorReview = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layoutResourceId = getArguments().getInt("LayoutId");
        tutorReview = (layoutResourceId == R.layout.fragment_reviews_tutor);

        View view = inflater.from(getActivity()).inflate(layoutResourceId, null);

        reviewRecyclerView = (RecyclerView)view.findViewById(R.id.reviewsRecyclerView);

        reviewLayoutManager = new LinearLayoutManager(getActivity());
        reviewsAdapter = new ReviewsAdapter(getActivity());

        if(getArguments().containsKey("USER_ID")){  //others review
            reviewsAdapter.loadReviews(getArguments().getString("USER_ID"), tutorReview);
        }else{
            reviewsAdapter.loadReviews(Constants.USER_ID_VALUE, tutorReview);
        }

        reviewRecyclerView.setLayoutManager(reviewLayoutManager);
        reviewRecyclerView.setAdapter(reviewsAdapter);

        return view;
    }
}
