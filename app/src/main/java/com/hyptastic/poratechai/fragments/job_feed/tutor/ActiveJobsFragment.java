package com.hyptastic.poratechai.fragments.job_feed.tutor;

import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hyptastic.poratechai.adapters.job_feed.tutor.ActiveJobsAdapter;
import com.hyptastic.poratechai.fragments.job_feed.JobFeedFragment;

/**
 * Created by ISHRAK on 2/26/2018.
 */

public class ActiveJobsFragment extends JobFeedFragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.setJobFeedAdapter(new ActiveJobsAdapter(getActivity()));

        final View rootView = super.onCreateView(inflater, container, savedInstanceState);

        registerBroadcastReceiver();

        return rootView;
    }

    //Broadcast Receiver

    @Override
    public void registerBroadcastReceiver()
    {
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(feedUpdateNotificationReceiver, new IntentFilter("update_feed_active_job"));
    }
}
