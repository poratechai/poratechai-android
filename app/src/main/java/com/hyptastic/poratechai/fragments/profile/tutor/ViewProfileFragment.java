//package com.hyptastic.poratechai.fragments.profile.tutor;
//
//
//import android.app.FragmentTransaction;
//import android.os.Bundle;
//import android.app.Fragment;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.ProgressBar;
//import android.widget.TextView;
//
//import com.hyptastic.poratechai.R;
//import com.hyptastic.poratechai.fragments.login_registration.ChangePasswordDialog;
//import com.hyptastic.poratechai.models.users.User;
//import com.hyptastic.poratechai.retrofit.functions.UserFunctions;
//import com.hyptastic.poratechai.retrofit.interfaces.UserClient;
//import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
//import com.hyptastic.poratechai.utils.Constants;
//import com.hyptastic.poratechai.utils.Functions;
//
//import org.apache.commons.lang.StringUtils;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//
///**
// * A simple {@link Fragment} subclass.
// */
//public class ViewProfileFragment extends Fragment {
//    public static final String TAG = ViewProfileFragment.class.getSimpleName();
//
//    UserClient userClient = null;
//    UserFunctions userFunctions = null;
//
//    private TextView mTvSkills;
//    private TextView mTvSubjects;
//    private TextView mTvDaysPerWeek;
//    private TextView mTvMinimumSalary;
//    private TextView mTvContactNumber;
//    private TextView mTvClassToTeach;
//
//    private String _id;
//    private String fullName;
//    private String contactNumber;
//    private String skills;
//    private String subjects;
//    private String classToTeach;
//    private String daysPerWeek;
//    private String minimumSalary;
//
//    private String [] institutionArray;
//    private String [] skillsArray;
//    private String [] subjectsArray;
//    private String [] classToTeachArray;
//
//    private static Double radius;
//    private static Double lat;
//    private static Double lon;
//
//    private Button editTutor;
//    private Button changePassword;
//
//    private ProgressBar mProgressbar;
//
//    public ViewProfileFragment() {
//        // Required empty public constructor
//    }
//
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        // Inflate the layout for this fragment
//        View view = inflater.inflate(R.layout.fragment_tutor_view_profile, container, false);
//
//        initViews(view);
//
//        getProfile();
//
//        return view;
//    }
//
//    private void initViews(View v) {
//        mTvContactNumber = (TextView) v.findViewById(R.id.tv_contact_number);
//        mTvSkills = (TextView) v.findViewById(R.id.tv_skills);
//        mTvSubjects = (TextView) v.findViewById(R.id.tv_subjects);
//        mTvClassToTeach = (TextView) v.findViewById(R.id.tv_class_to_teach);
//        mTvDaysPerWeek = (TextView) v.findViewById(R.id.tv_days_per_week);
//        mTvMinimumSalary = (TextView) v.findViewById(R.id.tv_minimum_salary);
//        mProgressbar = (ProgressBar) v.findViewById(R.id.progress);
//
//        editTutor = (Button) v.findViewById(R.id.btn_edit_tutor_profile);
//        changePassword = (Button) v.findViewById(R.id.btn_change_password);
//
//        editTutor.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                goToEditProfile();
//            }
//        });
//
//        changePassword.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View view) {
//                ChangePasswordDialog changePasswordDialog = new ChangePasswordDialog();
////                changePasswordDialog.show(getFragmentManager(), "ChangePassword");
//            }
//        });
//    }
//
//    private void getProfile(){
//        userClient = RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE,Constants.TOKEN_VALUE,UserClient.class);
//        userFunctions = new UserFunctions(userClient,getActivity().getApplicationContext());
//        userFunctions.getTutorProfileForView(this);
//    }
//
//    public void setTutorProfileView(User user){
//        _id = user.get_id();
//        fullName = user.getFullName();
//        contactNumber = user.getContactNumber().toString();
//        minimumSalary = user.getMinPreferredSalary().toString();
//        daysPerWeek = user.getPreferredDaysPerWeek().toString();
//        institutionArray = user.getInstitution();
//
//        skillsArray = user.getPreferredSubcategory();
//        subjectsArray = user.getPreferredSubjects();
//        classToTeachArray = user.getPreferredClassToTeach();
//
//        lat = user.getLocationLat();
//        lon = user.getLocationLong();
//        radius = user.getRadius();
//
//        //Converting string array to string
//
//        skills = StringUtils.join(skillsArray,", ");
//        subjects = StringUtils.join(subjectsArray,", ");
//        classToTeach = StringUtils.join(classToTeachArray,", ");
//
//
////        System.out.println(skills);
////        System.out.println(subjects);
////        System.out.println(classToTeach);
////        System.out.println(minimumSalary);
////        System.out.println(daysPerWeek);
//
//        mProgressbar.setVisibility(View.GONE);
//        mTvContactNumber.setText("0" + contactNumber);
//
//        if(skills.equals("")){
//            mTvSkills.setText("N/A");
//        }
//        else{
//            mTvSkills.setText(skills);
//        }
//
//        if(subjects.equals("")){
//            mTvSubjects.setText("N/A");
//        }
//        else{
//            mTvSubjects.setText(subjects);
//        }
//
//        if(classToTeach.equals("")){
//            mTvClassToTeach.setText("N/A");
//        }
//        else{
//            mTvClassToTeach.setText(classToTeach);
//        }
//
//        if(daysPerWeek.equals("0")){
//            mTvDaysPerWeek.setText("N/A");
//        }
//        else{
//            mTvDaysPerWeek.setText(daysPerWeek);
//        }
//
//        if(minimumSalary.equals("0")){
//            mTvMinimumSalary.setText("N/A");
//        }
//        else{
//            mTvMinimumSalary.setText(minimumSalary);
//        }
//
//        Functions.storeFullNameToSharedPreferences(fullName);
//
//        if(institutionArray.length>2 && !institutionArray[2].isEmpty()){
//            Functions.storeInstitutionToSharedPreferences(institutionArray.length>0?institutionArray[2]:"");
//        }
//        else if (institutionArray.length>1 && !institutionArray[1].isEmpty()){
//            Functions.storeInstitutionToSharedPreferences(institutionArray.length>0?institutionArray[1]:"");
//        }
//        else {
//            Functions.storeInstitutionToSharedPreferences(institutionArray.length>0?institutionArray[0]:"");
//        }
//    }
//
//    private void goToEditProfile(){
//        Bundle data = new Bundle();
//        data.putString("_id", _id);
//        data.putString("fullName",fullName);
//        data.putString("contactNumber", contactNumber);
//        data.putString("minimumSalary", minimumSalary);
//        data.putString("daysPerWeek",daysPerWeek);
//        data.putString("school", (institutionArray.length>0 && !institutionArray[0].equals(""))?institutionArray[0]:"");
//        data.putString("college", (institutionArray.length>1 && !institutionArray[1].equals(""))?institutionArray[1]:"");
//        data.putString("university", (institutionArray.length>2 && !institutionArray[2].equals(""))?institutionArray[2]:"");
//        data.putDouble("latitude", lat);
//        data.putDouble("longitude", lon);
//        data.putDouble("radius", radius);
//
//        if(subjects != null && !subjects.trim().isEmpty()){
//            ArrayList<String> subjectsArrayList = new ArrayList<String>(Arrays.asList(subjects.split(", ")));
//            data.putStringArrayList("subjects", subjectsArrayList);
//        }
//
//        if(classToTeach != null && !classToTeach.trim().isEmpty()){
//            ArrayList<String> classToTeachArrayList = new ArrayList<String>(Arrays.asList(classToTeach.split(", ")));
//            data.putStringArrayList("classToTeach", classToTeachArrayList);
//        }
//
//        if(skills != null && !skills.trim().isEmpty()){
//            ArrayList<String> skillsArrayList = new ArrayList<String>(Arrays.asList(skills.split(", ")));
//            data.putStringArrayList("skills", skillsArrayList);
//        }
//
//        FragmentTransaction ft = getFragmentManager().beginTransaction();
//        EditProfileFragment fragment = new EditProfileFragment();
//        fragment.setArguments(data);
//        ft.replace(R.id.fragmentFrame,fragment,EditProfileFragment.TAG);
//        ft.commit();
//        getFragmentManager().executePendingTransactions();
//    }
//}
