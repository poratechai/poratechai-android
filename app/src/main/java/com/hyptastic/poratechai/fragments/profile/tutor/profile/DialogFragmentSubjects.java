package com.hyptastic.poratechai.fragments.profile.tutor.profile;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.hyptastic.poratechai.fragments.multi_select.MultiSelectDialogFragment;
import com.hyptastic.poratechai.utils.Functions;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ISHRAK on 2/28/2019.
 */

public class DialogFragmentSubjects extends MultiSelectDialogFragment {
    DialogFragmentSubjects currentDialogFragmentSubjects;

    DialogFragmentSubjectsCloseListener dialogFragmentSubjectsCloseListener = null;

    public static interface DialogFragmentSubjectsCloseListener extends Parcelable {
        public void dialogFragmentSubjectsClosed();

        public static final Parcelable.Creator CREATOR = null;
    }

    public static DialogFragmentSubjects createInstance(DialogFragmentSubjectsCloseListener dialogFragmentSubjectsCloseListener, HashMap<String, String[]> dictionary, ArrayList<String> selectedItems){
        DialogFragmentSubjects dialogFragmentSubjects = new DialogFragmentSubjects();

        createInstance(dialogFragmentSubjects, dictionary, selectedItems);
        dialogFragmentSubjects.getArguments().putParcelable("CloseListener", dialogFragmentSubjectsCloseListener);

        return dialogFragmentSubjects;
    }

    @Override
    public void onStart() {
        super.onStart();

        currentDialogFragmentSubjects = this;

        dialogFragmentSubjectsCloseListener = (DialogFragmentSubjectsCloseListener) getArguments().getParcelable("CloseListener");

        confirmButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                userFunctions.updateSubjects(currentDialogFragmentSubjects, selectedItems);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        dismiss();
    }

    public void subjectsUpdated(boolean isUpdateSuccessful){
        dialogFragmentSubjectsCloseListener.dialogFragmentSubjectsClosed();

        getDialog().dismiss();
    }
}
