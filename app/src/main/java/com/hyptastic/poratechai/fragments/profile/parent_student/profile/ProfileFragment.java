package com.hyptastic.poratechai.fragments.profile.parent_student.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.fragments.profile.NavigationFragment;
import com.hyptastic.poratechai.models.users.User;

import java.util.Arrays;

/**
 * Created by ISHRAK on 2/19/2019.
 */

public class ProfileFragment extends com.hyptastic.poratechai.fragments.profile.ProfileFragment {
    public static int PARENT_STUDENT_PROFILE_HASH_CODE;

    com.hyptastic.poratechai.fragments.profile.parent_student.profile.ProfileFragment currentProfileFragment;
    NavigationFragment.LoadListener loadListener;

    public static ProfileFragment createInstance(NavigationFragment.LoadListener loadListener, String userId, int contractStatus){
        ProfileFragment profileFragment = new ProfileFragment();

        Bundle bundle = new Bundle();
        bundle.putParcelable("LOAD_LISTENER", loadListener);
        bundle.putString("USER_ID", userId);
        bundle.putInt("CONTRACT_STATUS", contractStatus);

        profileFragment.setArguments(bundle);

        return profileFragment;
    }

    public static ProfileFragment createInstance(NavigationFragment.LoadListener loadListener){
        ProfileFragment profileFragment = new ProfileFragment();

        Bundle bundle = new Bundle();
        bundle.putParcelable("LOAD_LISTENER", loadListener);

        profileFragment.setArguments(bundle);

        return profileFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Bundle bundle = (getArguments()==null?new Bundle():getArguments());
        bundle.putInt("LayoutResourceId", R.layout.fragment_profile_parent);
        setArguments(bundle);

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        currentProfileFragment = this;

        if(getArguments().containsKey("USER_ID")){
            removeButtons();
        }

        if(getArguments().containsKey("LOAD_LISTENER")){
            loadListener = (NavigationFragment.LoadListener)getArguments().getParcelable("LOAD_LISTENER");
        }

        systemFunction.getSystemArrayConstants(currentProfileFragment);

        if(loadListener != null){
            loadListener.loadFragment(true);
        }
    }

    public void removeButtons(){
        verificationImagesButton.setVisibility(View.INVISIBLE);
        changePasswordButton.setVisibility(View.INVISIBLE);
        logoutButton.setVisibility(View.INVISIBLE);
    }

    //--------------------------------------------------------------------------------------------------------------------------------
    //Server callback - start
    public void systemArrayConstantsGotten(){
        String userId = getArguments().getString("USER_ID");
        if(userId==null){
            userFunctions.getParentStudentProfileForViewSelf(currentProfileFragment);
        }else{
            userFunctions.getParentStudentProfileForViewOthers(currentProfileFragment, userId);
        }
    }

    public void parentStudentProfileGotten(User user){
        if(getArguments().containsKey("CONTRACT_STATUS")){
            setContactData(user.getEmail(), user.getContactNumber().toString(), getArguments().getInt("CONTRACT_STATUS"));
        }else{
            setContactData(user.getEmail(), user.getContactNumber().toString());
        }

        setVerificationImagesGallery(Arrays.asList(user.getVerificationImages()));

        if(loadListener != null){
            loadListener.loadFragment(false);
        }
    }
    //Server callback - end
    //--------------------------------------------------------------------------------------------------------------------------------
}
