package com.hyptastic.poratechai.fragments.profile.tutor.profile;

import android.app.Dialog;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.retrofit.functions.UserFunctions;
import com.hyptastic.poratechai.retrofit.interfaces.UserClient;
import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
import com.hyptastic.poratechai.utils.Constants;
import com.hyptastic.poratechai.utils.Functions;

/**
 * Created by ISHRAK on 3/3/2019.
 */

public class DialogFragmentDaysPerWeek extends DialogFragment {
    TextView daysPerWeekTextView;
    ImageButton upButton, downButton;
    Button cancelButton, confirmButton;

    int daysPerWeek = 0;
    DialogFragmentDaysPerWeek currentDialogFragmentDaysPerWeek;

    UserFunctions userFunctions;

    DialogFragmentDaysPerWeekListener dialogFragmentDaysPerWeekListener = null;

    public static interface DialogFragmentDaysPerWeekListener extends Parcelable {
        public void dialogFragmentDaysPerWeekClosed(String daysPerWeek);

        public static final Parcelable.Creator CREATOR = null;
    }

    public static DialogFragmentDaysPerWeek createInstance(DialogFragmentDaysPerWeekListener dialogFragmentDaysPerWeekListener, int daysPerWeek){
        DialogFragmentDaysPerWeek dialogFragmentDaysPerWeek = new DialogFragmentDaysPerWeek();

        Bundle args = new Bundle();
        args.putParcelable("CloseListener", dialogFragmentDaysPerWeekListener);
        args.putInt("DaysPerWeek", daysPerWeek);

        dialogFragmentDaysPerWeek.setArguments(args);

        return dialogFragmentDaysPerWeek;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialogFragmentDaysPerWeekListener = (DialogFragmentDaysPerWeekListener) getArguments().getParcelable("CloseListener");

        userFunctions = new UserFunctions(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, UserClient.class), getActivity());

        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();

        final Dialog dialog = getDialog();

        if(dialog != null){
            dialog.setContentView(R.layout.dialog_days_per_week);

            dialog.getWindow().setLayout((int)(0.92*Constants.SCREEN_WIDTH), (int)(0.4*Constants.SCREEN_HEIGHT));
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.background_listitem_job_tutor_profile);
        }

        currentDialogFragmentDaysPerWeek = this;
        initLayout();
    }

    private void initLayout(){
        try{
            daysPerWeek = (int)getArguments().getInt("DaysPerWeek");

            daysPerWeekTextView = getDialog().findViewById(R.id.daysPerWeekTextView);
            upButton            = getDialog().findViewById(R.id.upButton);
            downButton          = getDialog().findViewById(R.id.downButton);
            cancelButton        = getDialog().findViewById(R.id.cancelButton);
            confirmButton       = getDialog().findViewById(R.id.confirmButton);

            daysPerWeekTextView.setText(String.valueOf(daysPerWeek));

            upButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    int daysPerWeekTemp = Integer.parseInt(daysPerWeekTextView.getText().toString());

                    if(daysPerWeekTemp<=6){
                        daysPerWeekTemp++;
                        daysPerWeekTextView.setText(String.valueOf(daysPerWeekTemp));
                    }
                }
            });

            downButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    int daysPerWeekTemp = Integer.parseInt(daysPerWeekTextView.getText().toString());

                    if(daysPerWeekTemp>=2){
                        daysPerWeekTemp--;
                        daysPerWeekTextView.setText(String.valueOf(daysPerWeekTemp));
                    }
                }
            });

            cancelButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    getDialog().dismiss();
                }
            });
            confirmButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    userFunctions.updateDaysPerWeek(currentDialogFragmentDaysPerWeek, Integer.parseInt(daysPerWeekTextView.getText().toString()));
                }
            });
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void daysPerWeekUpdatedOverNetwork(boolean isUpdatedSuccessfully){
        if(isUpdatedSuccessfully){
            dialogFragmentDaysPerWeekListener.dialogFragmentDaysPerWeekClosed(daysPerWeekTextView.getText().toString());
        }else{
            dialogFragmentDaysPerWeekListener.dialogFragmentDaysPerWeekClosed(String.valueOf(daysPerWeek));
        }

        getDialog().dismiss();
    }
}
