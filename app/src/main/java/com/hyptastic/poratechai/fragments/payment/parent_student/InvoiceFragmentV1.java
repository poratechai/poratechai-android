package com.hyptastic.poratechai.fragments.payment.parent_student;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.models.wallet.InvoiceResponse;
import com.hyptastic.poratechai.utils.Constants;

/**
 * Created by ISHRAK on 6/25/2018.
 */

public class InvoiceFragmentV1 extends Fragment {
    TextView invoiceNumber, nameTextView, addressTextView, quantityTextView, productTextView, priceTextView, totalTextView;

    public static InvoiceFragmentV1 createNewInstance(InvoiceResponse invoiceResponse){
        InvoiceFragmentV1 invoiceFragment = new InvoiceFragmentV1();

        Bundle args = new Bundle();
        args.putParcelable("invoice", invoiceResponse);
        invoiceFragment.setArguments(args);

        return invoiceFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        InvoiceResponse invoiceResponse = (InvoiceResponse)getArguments().getParcelable("invoice");

        Log.e("Invoice", invoiceResponse.toString());

        View view = inflater.inflate(R.layout.fragment_invoice, container, false);

        invoiceNumber = view.findViewById(R.id.invoiceId);
        nameTextView = view.findViewById(R.id.nameTextView);
        addressTextView = view.findViewById(R.id.addressTextView);
        quantityTextView = view.findViewById(R.id.quantityTextView);
        productTextView = view.findViewById(R.id.productTextView);
        priceTextView = view.findViewById(R.id.priceTextView);
        totalTextView = view.findViewById(R.id.totalTextView);

        invoiceNumber.setText("#" + invoiceResponse.getData().getInvoiceId());
        nameTextView.setText(invoiceResponse.getData().getBuyerName());
        addressTextView.setText(invoiceResponse.getData().getBuyerAddress());

        double numberOfTokens = Double.parseDouble(invoiceResponse.getData().getAmount())/ Constants.TOKEN_UNIT_PRICE;

        quantityTextView.setText(String.valueOf(numberOfTokens));
        productTextView.setText(invoiceResponse.getData().getProductName());
        priceTextView.setText(String.valueOf(numberOfTokens) + " x " + Double.valueOf(Constants.TOKEN_UNIT_PRICE) + " Tk.");
        totalTextView.setText(invoiceResponse.getData().getAmount() + "Tk.");

        return view;
    }
}
