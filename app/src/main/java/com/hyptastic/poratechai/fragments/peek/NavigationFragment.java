package com.hyptastic.poratechai.fragments.peek;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.CardView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.support.v7.widget.AppCompatSpinner;
import android.widget.TextView;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.fragments.PorateChaiNavigationFragment;
import com.hyptastic.poratechai.retrofit.functions.SystemFunction;
import com.hyptastic.poratechai.retrofit.interfaces.SystemClient;
import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
import com.hyptastic.poratechai.utils.Constants;
import com.hyptastic.poratechai.utils.Functions;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by ISHRAK on 2/13/2019.
 */

public class NavigationFragment extends PorateChaiNavigationFragment {
    boolean allJobsSelected = false;

    CardView peekIntoClassesCardView, peekIntoSubcategoriesCardView, peekIntoSubjectsCardView;
    Button allTutorsButton, allJobsButton;
    LinearLayout classesLayout, categoriesLayout, subjectsLayout;
    AppCompatSpinner categoriesSpinner;

    int lastCategorySelected = 0;

    HashMap<String, HashMap<String, Integer>> classesToTeachDictionary;
    HashMap<String, HashMap<String, HashMap<String, Integer>>> subcategoriesDictionary;
    HashMap<String, HashMap<String, Integer>> subjectsDictionary;

    SystemFunction systemFunction;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_navigation_peek, null);

        systemFunction = new SystemFunction(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, SystemClient.class), getActivity());

        peekIntoClassesCardView = (CardView)view.findViewById(R.id.peekIntoClassesCardView);
        peekIntoSubcategoriesCardView = (CardView)view.findViewById(R.id.peekIntoCategoriesCardView);
        peekIntoSubjectsCardView = (CardView)view.findViewById(R.id.peekIntoSubjectsCardView);

        classesLayout = (LinearLayout)view.findViewById(R.id.peekIntoClassesLayout);
        categoriesLayout = (LinearLayout)view.findViewById(R.id.peekIntoCategoriesLayout);
        subjectsLayout = (LinearLayout)view.findViewById(R.id.peekIntoSubjectsLayout);

        categoriesSpinner = (AppCompatSpinner)view.findViewById(R.id.peekIntoCategoriesSpinner);

        allTutorsButton = (Button)view.findViewById(R.id.allTutorsButton);
        allJobsButton = (Button)view.findViewById(R.id.allJobsButton);

        classesToTeachDictionary = new HashMap<>(3);
        subcategoriesDictionary = new HashMap<>(3);
        subjectsDictionary = new HashMap<>(3);

        if(Functions.isTutor()){
            selectButton(false);
        }else{
            selectButton(true);
        }

        allTutorsButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                selectButton(true);
            }
        });

        allJobsButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                selectButton(false);
            }
        });

        systemFunction.getPeekResults(this);

        return view;
    }

    @Override
    public void loadFragment() {
        systemFunction.getPeekResults(this);
    }

    private void selectButton(boolean allTutorsSelected){
        if(allTutorsSelected){
            allTutorsButton.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.colorAccent, null));
            allTutorsButton.setTextColor(Color.WHITE);

            allJobsButton.setBackgroundResource(R.drawable.background_grey_flexbox_item);
            allJobsButton.setTextColor(ResourcesCompat.getColor(getResources(), R.color.colorAccent, null));

            allJobsSelected = false;
        }else{
            allTutorsButton.setBackgroundResource(R.drawable.background_grey_flexbox_item);
            allTutorsButton.setTextColor(ResourcesCompat.getColor(getResources(), R.color.colorAccent, null));

            allJobsButton.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.colorAccent, null));
            allJobsButton.setTextColor(Color.WHITE);

            allJobsSelected = true;
        }

        setupLayout(classesToTeachDictionary, classesLayout);
        setupSubcategories();
        setupLayout(subjectsDictionary, subjectsLayout);
    }

    public void parsePeekResults(String peekResults){
        try{
            JSONObject peekObject = new JSONObject(peekResults);

            JSONObject classesToTeachObject = (JSONObject)peekObject.get("classesToTeach");
            JSONObject subcategoriesObject = (JSONObject)peekObject.get("subcategories");
            JSONObject subjectsObject = (JSONObject)peekObject.get("subjects");

            Iterator<String> classesToTeachIterator = classesToTeachObject.keys();
            Iterator<String> subcategoriesIterator = subcategoriesObject.keys();
            Iterator<String> subjectsIterator = subjectsObject.keys();

            while(classesToTeachIterator.hasNext()){
                String key = classesToTeachIterator.next();
                JSONObject value = (JSONObject)classesToTeachObject.get(key);

                HashMap<String, Integer> classesToTeachValuesDictionary = new HashMap<>(2);
                classesToTeachValuesDictionary.put("tutors", (Integer)value.get("tutors"));
                classesToTeachValuesDictionary.put("jobs", (Integer)value.get("jobs"));

                classesToTeachDictionary.put(key, classesToTeachValuesDictionary);
            }

            while(subcategoriesIterator.hasNext()){
                String key = subcategoriesIterator.next();
                JSONObject subcategoryObject = (JSONObject)subcategoriesObject.get(key);

                HashMap<String, HashMap<String, Integer>> subcategoryHashMap = new HashMap<>(3);

                Iterator<String> subcategoryKeys = subcategoryObject.keys();

                while(subcategoryKeys.hasNext()){
                    String subcategoryKey = subcategoryKeys.next();
                    JSONObject value = (JSONObject)subcategoryObject.get(subcategoryKey);

                    HashMap<String, Integer> subcategoriesValuesDictionary = new HashMap<>(2);
                    subcategoriesValuesDictionary.put("tutors", (Integer)value.get("tutors"));
                    subcategoriesValuesDictionary.put("jobs", (Integer)value.get("jobs"));

                    subcategoryHashMap.put(subcategoryKey, subcategoriesValuesDictionary);
                }

                subcategoriesDictionary.put(key, subcategoryHashMap);
            }

            while(subjectsIterator.hasNext()){
                String key = subjectsIterator.next();
                JSONObject value = (JSONObject)subjectsObject.get(key);

                HashMap<String, Integer> subjectsValuesDictionary = new HashMap<>(2);
                subjectsValuesDictionary.put("tutors", (Integer)value.get("tutors"));
                subjectsValuesDictionary.put("jobs", (Integer)value.get("jobs"));

                subjectsDictionary.put(key, subjectsValuesDictionary);
            }

            setupLayout(classesToTeachDictionary, classesLayout);
            setupSubcategories();
            setupLayout(subjectsDictionary, subjectsLayout);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void setupLayout(HashMap<String, HashMap<String, Integer>> peekDictionary, LinearLayout layout){
        layout.removeAllViews();

        for(String key: peekDictionary.keySet()){
            LinearLayout row = new LinearLayout(getActivity());
            LinearLayout.LayoutParams rowParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            row.setLayoutParams(rowParams);

            LinearLayout.LayoutParams nameLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            nameLayoutParams.weight = 0.78f;
            nameLayoutParams.setMargins(10, 5, 0, 5);
            LinearLayout.LayoutParams totalLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            totalLayoutParams.weight = 0.22f;
            totalLayoutParams.setMargins(0, 5, 10, 5);

            TextView name = new TextView(getActivity());
            name.setLayoutParams(nameLayoutParams);
            TextView total = new TextView(getActivity());
            total.setLayoutParams(totalLayoutParams);
            total.setGravity(Gravity.END);

            if(allJobsSelected){
                name.setText(key);
                total.setText(String.valueOf(peekDictionary.get(key).get("jobs")));
            }else{
                name.setText(key);
                total.setText(String.valueOf(peekDictionary.get(key).get("tutors")));
            }

            row.addView(name);
            row.addView(total);

            layout.addView(row);
        }
    }

    public void setupSubcategories(){
        categoriesSpinner.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item,
                subcategoriesDictionary.keySet().toArray(new String[subcategoriesDictionary.keySet().size()])));
        categoriesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                setupLayout(subcategoriesDictionary.get(categoriesSpinner.getAdapter().getItem(i).toString()), categoriesLayout);

                if(categoriesSpinner.getAdapter().getItem(i).toString().equals("Academics")){
                    peekIntoSubjectsCardView.setVisibility(View.VISIBLE);
                }else{
                    peekIntoSubjectsCardView.setVisibility(View.INVISIBLE);
                }

                lastCategorySelected = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        categoriesSpinner.setSelection(lastCategorySelected);
    }
}
