package com.hyptastic.poratechai.fragments.profile;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.ybq.android.spinkit.SpinKitView;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.CubeGrid;
import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.activities.PorateChaiActivity;
import com.hyptastic.poratechai.retrofit.functions.UserFunctions;
import com.hyptastic.poratechai.retrofit.interfaces.UserClient;
import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
import com.hyptastic.poratechai.utils.Constants;
import com.hyptastic.poratechai.utils.Functions;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

/**
 * Created by ISHRAK on 2/26/2019.
 */

public class DialogFragmentVerificationImages extends DialogFragment {
    TextView verificationImagesExplanationTextView;
    LinearLayout verificationGallery;

    ImageView verificationImageView;
    ImageButton leftButton, rightButton, deleteButton, addButton;
    Button closeButton;

    UserFunctions userFunctions;

    DialogFragmentVerificationImages currentDialogFragmentVerificationImages;

    ArrayList<String> verificationImages;
    int currentImageIndex;

    private static final int INTENT_REQUEST_CODE_GALLERY = 1037;

    private boolean imageIsBeingUploaded = false;

    SpinKitView loadingSpinKitView;
    Sprite loadingSprite;

    DialogFragmentVerificationImagesListener dialogFragmentVerificationImagesListener = null;

    public static interface DialogFragmentVerificationImagesListener extends Parcelable {
        public void dialogFragmentVerificationImagesClosed();

        public static final Parcelable.Creator CREATOR = null;
    }

    public static DialogFragmentVerificationImages createInstance(DialogFragmentVerificationImagesListener dialogFragmentVerificationImagesListener,
                                                                  List<String> verificationImages)
    {
        DialogFragmentVerificationImages dialogFragmentVerificationImages = new DialogFragmentVerificationImages();

        Bundle args = new Bundle();

        args.putParcelable("CloseListener", dialogFragmentVerificationImagesListener);
        if(verificationImages!=null){
            args.putStringArrayList("VerificationImages", new ArrayList<String>(verificationImages));
        }

        dialogFragmentVerificationImages.setArguments(args);

        return dialogFragmentVerificationImages;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialogFragmentVerificationImagesListener = (DialogFragmentVerificationImagesListener)getArguments().getParcelable("CloseListener");

        userFunctions = new UserFunctions(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, UserClient.class), getActivity());

        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();

        final Dialog dialog = getDialog();

        if(dialog != null){
            dialog.setContentView(R.layout.dialog_verification_images);

            dialog.getWindow().setLayout((int)(0.92*Constants.SCREEN_WIDTH), (int)(0.92*Constants.SCREEN_HEIGHT));
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.background_listitem_job_tutor_profile);
        }

        currentDialogFragmentVerificationImages = this;
        initLayout();
    }

    @Override
    public void onResume() {
        super.onResume();

        if(imageIsBeingUploaded){
            showLoadingSpinKit(true);
        }else{
            showLoadingSpinKit(false);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    private void initLayout(){
        try{
            verificationImages      = (ArrayList<String>)getArguments().getSerializable("VerificationImages");

            verificationImagesExplanationTextView = getDialog().findViewById(R.id.verificationImagesExplanationTextView);
            verificationImageView                 = getDialog().findViewById(R.id.displayImageView);
            loadingSpinKitView                    = getDialog().findViewById(R.id.loadingSpinKit);
            leftButton                            = getDialog().findViewById(R.id.leftButton);
            rightButton                           = getDialog().findViewById(R.id.rightButton);
            deleteButton                          = getDialog().findViewById(R.id.deleteButton);
            addButton                             = getDialog().findViewById(R.id.addButton);
            verificationGallery                   = getDialog().findViewById(R.id.verificationGallery);
            closeButton                           = getDialog().findViewById(R.id.closeButton);

            String styledPrompt = "Upload the following photos: <font color='red'><br/>MANDATORY</font>  (NID/Passport and Academic ID) " +
                    ",OPTIONAL</font>  Academic results.<br/>Our moderation team will verify. " +
                    "<font color='red'>(Max: 5 photos)</font></p>";

            verificationImagesExplanationTextView.setText(Html.fromHtml(styledPrompt));

            loadingSprite = new CubeGrid();
            loadingSpinKitView.setIndeterminateDrawable(loadingSprite);

            currentImageIndex = 0;

            if(verificationImages.size()>0){
                Glide.with(getActivity())
                        .load(verificationImages.get(0))
                        .into(verificationImageView);

                verificationGallery.removeAllViews();

                for(String url: verificationImages){
                    addImageViewToGallery(url);
                }
            }

            leftButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    if(verificationImages != null){
                        if(currentImageIndex>0){
                            currentImageIndex--;
                        }

                        Glide.with(getActivity())
                                .load(verificationImages.get(currentImageIndex))
                                .into(verificationImageView);
                    }
                }
            });

            rightButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    if(verificationImages != null){
                        if(currentImageIndex<verificationImages.size()-1){
                            currentImageIndex++;
                        }

                        Glide.with(getActivity())
                                .load(verificationImages.get(currentImageIndex))
                                .into(verificationImageView);
                    }
                }
            });

            deleteButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    if(verificationImages != null){
                        String styledMessage = "Deleting a verification image will cause your status to become " +
                                "<font color='red'>'NOT VERIFIED'</font> until our moderator approves the changes. " +
                                "<p>Are you sure you want to delete this verification image?</p>";

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setTitle("Confirm Deletion")
                                .setMessage(Html.fromHtml(styledMessage))
                                .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        deleteImage(verificationImages.get(currentImageIndex));
                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                    }
                                });
                        alertDialogBuilder.show();
                    }
                }
            });

            addButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    if(verificationImages == null){
                        verificationImages = new ArrayList<String>(1);
                    }

                    if(verificationImages.size() <= 4){
                        Intent intent = new Intent(Intent.ACTION_PICK);
                        intent.setType("image/*");

                        try {
                            startActivityForResult(intent, INTENT_REQUEST_CODE_GALLERY);

                        } catch (ActivityNotFoundException e) {

                            e.printStackTrace();
                        }
                    }else{
                        //Handled in server with status 400

                        Toast.makeText(getActivity(), "Sorry, you can upload no more than 5 verification images", Toast.LENGTH_LONG).show();
                    }
                }
            });

            closeButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    dialogFragmentVerificationImagesListener.dialogFragmentVerificationImagesClosed();

                    getDialog().dismiss();
                }
            });
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void addImageViewToGallery(String url){
        try{
            CardView card = new CardView(getActivity());

            LinearLayout.LayoutParams cardLayoutParams = new LinearLayout.LayoutParams(128, 128);
            cardLayoutParams.setMargins(5, 5, 5, 5);
            card.setLayoutParams(cardLayoutParams);

            card.setRadius(5);
            card.setCardElevation(5);

            ImageView imageView = new ImageView(getActivity());
            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            imageView.setLayoutParams(layoutParams);
            imageView.setTag(R.id.VERIFICATION_IMAGE_URL, url);

            imageView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    currentImageIndex = verificationImages.indexOf(url);

                    Glide.with(getActivity())
                            .load(verificationImages.get(currentImageIndex))
                            .into(verificationImageView);
                }
            });

            Glide.with(getActivity())
                    .load(url)
                    .into(imageView);

            card.addView(imageView);
            verificationGallery.addView(card);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        int dataSize = 0;

        try{
            if(requestCode == INTENT_REQUEST_CODE_GALLERY && resultCode == RESULT_OK && data != null){
                try {
                    Uri uri  = data.getData();
                    String scheme = uri.getScheme();
                    InputStream fileInputStream = getActivity().getContentResolver().openInputStream(uri);

                    if(scheme.equals(ContentResolver.SCHEME_CONTENT)){
                        try {
                            dataSize = fileInputStream.available()/1024;
                            if(dataSize > 4096){
//                                System.out.println("File size in bytes: "+ dataSize);
                                Toast.makeText(getActivity(),"Image size cannot exceed 4MB",Toast.LENGTH_LONG).show();
                            }
                            else {
//                                System.out.println("ELSE File size in bytes: "+ dataSize);
                                uploadImage(Functions.getBytes(fileInputStream));

                                Toast.makeText(getActivity(), "Uploading Image. Please wait.", Toast.LENGTH_LONG).show();
                                imageIsBeingUploaded = true;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    else if(scheme.equals(ContentResolver.SCHEME_FILE)){
                        String path = uri.getPath();
                        File f= null;
                        try {
                            f = new File(path);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void uploadImage(byte[] imageBytes){
        userFunctions.uploadCredentialPicture(currentDialogFragmentVerificationImages, imageBytes);
    }

    public void imageUploaded(String url, boolean isUpdatedSuccessfully){
        if(isUpdatedSuccessfully){
            verificationImages.add(url);
            currentImageIndex = verificationImages.size()-1;

            addImageViewToGallery(url);

            Glide.with(getActivity())
                    .load(url)
                    .into(verificationImageView);

            imageIsBeingUploaded = false;
            showLoadingSpinKit(false);
        }
    }

    public void deleteImage(String url){
        userFunctions.deleteCredentialPicture(currentDialogFragmentVerificationImages, url);
    }

    public void imageDeletedSuccessfully(String url){
        for(int i=0;i<verificationGallery.getChildCount();i++){
            CardView cardView = (CardView)verificationGallery.getChildAt(i);
            ImageView imageView = (ImageView)(cardView).getChildAt(0);

            try{
                if(imageView.getTag(R.id.VERIFICATION_IMAGE_URL).equals(url)){
                    verificationGallery.removeView(cardView);
                    verificationImages.remove(currentImageIndex);

                    if(currentImageIndex > verificationImages.size()-1){
                        currentImageIndex--;
                    }

                    if(verificationImages.size() > 0){
                        Glide.with(getActivity())
                                .load(verificationImages.get(currentImageIndex))
                                .into(verificationImageView);
                    }else{
                        verificationImageView.setImageResource(android.R.color.transparent);
                    }

                    break;
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    private void showLoadingSpinKit(boolean willShow){
        if(willShow){
            verificationImageView.setVisibility(View.INVISIBLE);

            loadingSpinKitView.setVisibility(View.VISIBLE);
            loadingSprite.start();
        }else{
            verificationImageView.setVisibility(View.VISIBLE);

            loadingSpinKitView.setVisibility(View.INVISIBLE);
            loadingSprite.stop();
        }
    }
}
