package com.hyptastic.poratechai.fragments.payment.tutor;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.fragments.payment.PaymentInterface;
import com.hyptastic.poratechai.models.wallet.InvoiceRequest;
import com.hyptastic.poratechai.models.wallet.InvoiceResponse;
import com.hyptastic.poratechai.models.wallet.UpdateResponse;
import com.hyptastic.poratechai.models.wallet.WalletConstants;
import com.hyptastic.poratechai.models.wallet.v2.UpdateResponseV2;
import com.hyptastic.poratechai.models.wallet.v2.WalletConstantsV2;
import com.hyptastic.poratechai.retrofit.functions.SystemFunction;
import com.hyptastic.poratechai.retrofit.functions.WalletFunction;
import com.hyptastic.poratechai.retrofit.interfaces.SystemClient;
import com.hyptastic.poratechai.retrofit.interfaces.WalletClient;
import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
import com.hyptastic.poratechai.utils.Constants;

/**
 * Created by ISHRAK on 6/25/2018.
 */

public class PaymentChoiceFragment extends Fragment implements PaymentInterface {
    TextView currentMembershipValueTextView, daysLeftValueTextView, chooseSubscriptionTitle, pendingTextView;
    CardView subscriptionCardOne, subscriptionCardThree;
    View onePendingView, threePendingView;
    Button subscriptionButtonOne, subscriptionButtonThree;
    FloatingActionButton proceedButton;

    InvoiceResponse invoiceResponse;
    WalletConstantsV2 walletConstants;

    WalletFunction walletFunction;
    SystemFunction systemFunction;

    boolean oneMonthSelected = true;
    boolean pending;
    boolean walletConstantsGotten = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment_choice, container, false);

        walletFunction = new WalletFunction(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, WalletClient.class), getActivity());
        systemFunction = new SystemFunction(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, SystemClient.class), getActivity());

        currentMembershipValueTextView  = (TextView)view.findViewById(R.id.currentMembershipValueTextView);
        daysLeftValueTextView           = (TextView)view.findViewById(R.id.daysLeftValueTextView);
        chooseSubscriptionTitle         = (TextView)view.findViewById(R.id.chooseSubscriptionTitle);
        pendingTextView                 = (TextView)view.findViewById(R.id.pendingTextView);
        subscriptionCardOne             = (CardView)view.findViewById(R.id.subscribeCardOne);
        subscriptionCardThree           = (CardView)view.findViewById(R.id.subscribeCardThree);
        onePendingView                  = (View)view.findViewById(R.id.onePendingView);
        threePendingView                = (View)view.findViewById(R.id.threePendingView);
        subscriptionButtonOne           = (Button)view.findViewById(R.id.subscribeButtonOne);
        subscriptionButtonThree         = (Button)view.findViewById(R.id.subscribeButtonThree);
        proceedButton                   = (FloatingActionButton)view.findViewById(R.id.proceedButton);

        subscriptionCardOne.setOnClickListener(oneClicked);
        subscriptionCardThree.setOnClickListener(threeClicked);
        subscriptionButtonOne.setOnClickListener(oneClicked);
        subscriptionButtonThree.setOnClickListener(threeClicked);

        walletConstantsGotten = false;

        proceedButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                try{
                    AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                            .setTitle("Free Subscription")
                            .setMessage("We are giving away free subscriptions! All the best!")
                            .setIcon(R.drawable.ic_fireworks)
                            .setPositiveButton("Okay, got it!", new DialogInterface.OnClickListener(){
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            }).create();

                    alertDialog.show();

//                    if(!walletConstantsGotten){
//                        Toast.makeText(getActivity(), "Please try again", Toast.LENGTH_LONG).show();
//                    }else if(walletConstants.getCOST_ONE_MONTH() == 0.0 && walletConstants.getCOST_THREE_MONTHS() == 0.0){
//                        AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
//                                .setTitle("Free Subscription")
//                                .setMessage("We are giving away free subscriptions! All the best!")
//                                .setIcon(R.drawable.ic_fireworks)
//                                .setPositiveButton("Okay, got it!", new DialogInterface.OnClickListener(){
//                                    @Override
//                                    public void onClick(DialogInterface dialogInterface, int i) {
//                                        dialogInterface.dismiss();
//                                    }
//                                }).create();
//
//                        alertDialog.show();
//                    }else{
//                        if(oneMonthSelected){
//                            createInvoice(walletConstants.getCOST_ONE_MONTH(), walletConstants.getDURATION_ONE_MONTH());
//                        }else{
//                            createInvoice(walletConstants.getCOST_THREE_MONTHS(), walletConstants.getDURATION_THREE_MONTHS());
//                        }
//                    }
                }catch(Exception e){
                    Log.e("Invoice creation", e.getMessage());
                }
            }
        });

        getPrice();   //updateWallet is called in walletConstantsGotten

        return view;
    }

    View.OnClickListener oneClicked = new View.OnClickListener(){
        @Override
        public void onClick(View view) {
            oneMonthSelected = true;

            subscriptionButtonOne.setTextColor(Color.WHITE);
            subscriptionButtonOne.setBackgroundColor(Color.parseColor("#f0c267"));

            subscriptionButtonThree.setTextColor(Color.BLACK);
            subscriptionButtonThree.setBackgroundColor(Color.WHITE);
        }
    };

    View.OnClickListener threeClicked = new View.OnClickListener(){
        @Override
        public void onClick(View view) {
            oneMonthSelected = false;

            subscriptionButtonOne.setTextColor(Color.BLACK);
            subscriptionButtonOne.setBackgroundColor(Color.WHITE);

            subscriptionButtonThree.setTextColor(Color.WHITE);
            subscriptionButtonThree.setBackgroundColor(Color.parseColor("#f0c267"));
        }
    };

    public void createInvoice(double paymentAmount, double subscriptionMonths){
        Toast.makeText(getActivity(), "Loading Invoice", Toast.LENGTH_LONG).show();

        InvoiceRequest invoiceRequest = new InvoiceRequest(String.valueOf(paymentAmount), "Subscription", String.valueOf(subscriptionMonths)+" x month(s)",
                Constants.USER_NAME_VALUE, Constants.EMAIL_VALUE, "phone", "address", "city", "state", "zipcode", "BD", Constants.USER_ID_VALUE, walletConstants.getCOST_ONE_MONTH(), walletConstants.getCOST_THREE_MONTHS());

        walletFunction.createInvoice(this, invoiceRequest);
    }
    //Done

    public void updateWallet()
    {
        //SpinKitView

        walletFunction.updateWalletV2(this);
    }
    //Done

    public void getInvoice(String invoiceNumber){
        walletFunction.getInvoice(this, invoiceNumber);
    }
    //Done

    //loadInvoice is called after response from getInvoice
    public void loadInvoice(InvoiceResponse invoiceResponse){
        this.invoiceResponse = invoiceResponse;

        //TODO Check
//        Intent intent = new Intent(getActivity(), PaymentInvoiceActivity.class);
//        intent.putExtra("invoiceResponse", invoiceResponse);
//        startActivity(intent);
    }
    //Done

    //refreshBalance is called after response from updateWallet
    public void refreshBalance(UpdateResponse updateResponseV2)
    {
        UpdateResponseV2 updateResponse = (UpdateResponseV2)updateResponseV2;
        try{
            Log.e("Validity(days):", String.valueOf(updateResponse.getValidityInDays()));

            String subscriptionType = updateResponse.getSubscriptionType() + " month(s)";
            currentMembershipValueTextView.setText(subscriptionType);
            daysLeftValueTextView.setText(String.valueOf(updateResponse.getValidityInDays()));

            if(updateResponse.getPendingInvoice().getData()==null){
                invoiceResponse = null;

                setPending(invoiceResponse);
            }else{
                invoiceResponse = updateResponse.getPendingInvoice();   //this pending invoiceResponse is not used for loading invoice
                                                                        //because the backend will send the pending invoiceResponse
                                                                        //in createInvoice call
                setPending(invoiceResponse);
            }
        }catch(Exception e){
            Log.e("Balance Refresh", e.toString());
        }
    }
    //Done

    //setPending is called after response from checkPending
    public void setPending(InvoiceResponse invoiceResponse){
        if(invoiceResponse!=null){
            //Pending
            //Product description will be in the format "1.0 x month(s)" - So, splitting is required
            if(Integer.parseInt(invoiceResponse.getData().getProductDescription().split("\\.")[0])==walletConstants.getDURATION_ONE_MONTH()){
                pendingTextView.setVisibility(View.VISIBLE);

                onePendingView.setBackgroundResource(R.color.pink);
                threePendingView.setBackgroundResource(R.color.white);

                subscriptionCardThree.setEnabled(false);
                subscriptionButtonThree.setBackgroundColor(Color.WHITE);
                subscriptionButtonThree.setTextColor(Color.BLACK);
                subscriptionButtonThree.setEnabled(false);

                subscriptionCardOne.setEnabled(false);
                subscriptionButtonOne.setBackgroundResource(R.color.yellow);
                subscriptionButtonOne.setTextColor(Color.WHITE);
                subscriptionButtonOne.setEnabled(false);
            }else if(Integer.parseInt(invoiceResponse.getData().getProductDescription().split("\\.")[0])==walletConstants.getDURATION_THREE_MONTHS()){
                pendingTextView.setVisibility(View.VISIBLE);

                onePendingView.setBackgroundResource(R.color.white);
                threePendingView.setBackgroundResource(R.color.pink);

                subscriptionCardOne.setEnabled(false);
                subscriptionButtonOne.setBackgroundColor(Color.WHITE);
                subscriptionButtonOne.setTextColor(Color.BLACK);
                subscriptionButtonOne.setEnabled(false);

                subscriptionCardThree.setEnabled(false);
                subscriptionButtonThree.setBackgroundResource(R.color.yellow);
                subscriptionButtonThree.setTextColor(Color.WHITE);
                subscriptionButtonThree.setEnabled(false);
            }

            pending = true;
        }else{
            //Not Pending
            pendingTextView.setVisibility(View.INVISIBLE);

            onePendingView.setBackgroundResource(R.color.white);
            threePendingView.setBackgroundResource(R.color.white);

            subscriptionCardOne.setEnabled(true);
            subscriptionButtonOne.setBackgroundResource(R.color.yellow);    //default selection
            subscriptionButtonOne.setTextColor(Color.WHITE);
            subscriptionButtonOne.setEnabled(true);

            subscriptionCardThree.setEnabled(true);
            subscriptionButtonThree.setBackgroundColor(Color.WHITE);
            subscriptionButtonThree.setTextColor(Color.BLACK);
            subscriptionButtonThree.setEnabled(true);

            oneMonthSelected = true;                                        //default selection

            pending = false;
        }
    }
    //Done

    public void getWalletConstants(){
        //SpinKitView - start

        systemFunction.getSystemWalletConstantsV2(this);
    }
    //Done

    public void getPrice(){
        systemFunction.getPrice(this);
    }

    public void walletConstantsGotten(WalletConstants walletConstants){
        //SpinKitView - end

        this.walletConstants = (WalletConstantsV2)walletConstants;

        subscriptionButtonOne.setText(String.valueOf(((WalletConstantsV2) walletConstants).getCOST_ONE_MONTH()) + " Tk.");
        subscriptionButtonThree.setText(String.valueOf(((WalletConstantsV2)walletConstants).getCOST_THREE_MONTHS()) + " Tk.");

        walletConstantsGotten = true;

        updateWallet();
    }
    //Done
}
