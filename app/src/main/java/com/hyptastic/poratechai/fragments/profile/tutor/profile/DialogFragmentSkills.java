package com.hyptastic.poratechai.fragments.profile.tutor.profile;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.hyptastic.poratechai.fragments.multi_select.MultiSelectDialogFragment;
import com.hyptastic.poratechai.utils.Functions;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ISHRAK on 2/28/2019.
 */

public class DialogFragmentSkills extends MultiSelectDialogFragment {
    DialogFragmentSkills currentDialogFragmentSkills;

    DialogFragmentSkillsCloseListener dialogFragmentSkillsCloseListener = null;

    public static interface DialogFragmentSkillsCloseListener extends Parcelable {
        public void dialogFragmentSkillsClosed();

        public static final Parcelable.Creator CREATOR = null;
    }

    public static DialogFragmentSkills createInstance(DialogFragmentSkillsCloseListener dialogFragmentSkillsCloseListener, HashMap<String, String[]> dictionary, ArrayList<String> selectedItems){
        DialogFragmentSkills dialogFragmentSkills = new DialogFragmentSkills();

        createInstance(dialogFragmentSkills, dictionary, selectedItems);
        dialogFragmentSkills.getArguments().putParcelable("CloseListener", dialogFragmentSkillsCloseListener);

        return dialogFragmentSkills;
    }

    @Override
    public void onStart() {
        super.onStart();

        dialogFragmentSkillsCloseListener = (DialogFragmentSkillsCloseListener)getArguments().getParcelable("CloseListener");
        currentDialogFragmentSkills = this;

        confirmButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                userFunctions.updateSkills(currentDialogFragmentSkills, selectedItems);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        dismiss();
    }

    public void skillsUpdated(boolean isUpdateSuccessful){
        dialogFragmentSkillsCloseListener.dialogFragmentSkillsClosed();

        getDialog().dismiss();
    }
}
