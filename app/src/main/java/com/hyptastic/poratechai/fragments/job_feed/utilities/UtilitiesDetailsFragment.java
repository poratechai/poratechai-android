package com.hyptastic.poratechai.fragments.job_feed.utilities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.adapters.job_feed.utilities.details.tutor.AppliedUtilitiesDetailsAdapter;
import com.hyptastic.poratechai.adapters.job_feed.utilities.details.tutor.MatchesUtilitiesDetailsAdapter;
import com.hyptastic.poratechai.adapters.job_feed.utilities.details.parent_student.PostedJobsUtilitiesDetailsAdapter;

/**
 * Created by ISHRAK on 5/31/2019.
 */

public class UtilitiesDetailsFragment extends Fragment {
    public static class FEED_TYPE{
        public static class PARENT_STUDENT{
            public static final int POSTED_JOBS = 11;
            public static final int ACTIVE_POSTED_JOB = 12;
            public static final int POSTED_JOB_HISTORY = 13;
        }
        public static class TUTOR{
            public static final int  MATCHES = 14;
            public static final int APPLIED = 15;
            public static final int ACTIVE_JOB = 16;
            public static final int JOB_HISTORY = 17;
        }
    }

    RecyclerView utilitiesDetailsRecyclerView;
    RecyclerView.LayoutManager utilitiesLayoutManager;

    public static UtilitiesDetailsFragment createInstance(String jobId, String matchId, int utilitiesFeedType){
        UtilitiesDetailsFragment utilitiesDetailsFragment = new UtilitiesDetailsFragment();

        Bundle bundle = new Bundle();
        bundle.putString("JOB_ID", jobId);
        bundle.putString("MATCH_ID", matchId);
        bundle.putInt("UTILITIES_FEED_TYPE", utilitiesFeedType);

        utilitiesDetailsFragment.setArguments(bundle);

        return utilitiesDetailsFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;

        try{
            super.onCreateView(inflater, container, savedInstanceState);

            view = inflater.inflate(R.layout.fragment_utilities_details, null);

            String jobId = getArguments().getString("JOB_ID");
            String matchId = getArguments().getString("MATCH_ID");
            int utilitiesFeedType = getArguments().getInt("UTILITIES_FEED_TYPE");

            utilitiesDetailsRecyclerView = (RecyclerView)view.findViewById(R.id.recycler_view);

            utilitiesLayoutManager = new LinearLayoutManager(getActivity());
            utilitiesDetailsRecyclerView.setLayoutManager(utilitiesLayoutManager);

            switch(utilitiesFeedType){
                case FEED_TYPE.PARENT_STUDENT.POSTED_JOBS:
                    PostedJobsUtilitiesDetailsAdapter postedJobsUtilitiesDetailsAdapter = new PostedJobsUtilitiesDetailsAdapter(getActivity(), jobId);
                    utilitiesDetailsRecyclerView.setAdapter(postedJobsUtilitiesDetailsAdapter);

                    postedJobsUtilitiesDetailsAdapter.loadData();
                    break;
                case FEED_TYPE.PARENT_STUDENT.ACTIVE_POSTED_JOB:
                    com.hyptastic.poratechai.adapters.job_feed.utilities.details.parent_student.ActiveJobsUtilitiesDetailsAdapter activePostedJobsUtilitiesDetailsAdapter
                            = new com.hyptastic.poratechai.adapters.job_feed.utilities.details.parent_student.ActiveJobsUtilitiesDetailsAdapter(getActivity(), jobId);
                    utilitiesDetailsRecyclerView.setAdapter(activePostedJobsUtilitiesDetailsAdapter);

                    activePostedJobsUtilitiesDetailsAdapter.loadData();
                    break;
                case FEED_TYPE.PARENT_STUDENT.POSTED_JOB_HISTORY:
                    com.hyptastic.poratechai.adapters.job_feed.utilities.details.parent_student.JobHistoryUtilitiesDetailsAdapter postedJobHistoryUtilitiesDetailsAdapter
                            = new com.hyptastic.poratechai.adapters.job_feed.utilities.details.parent_student.JobHistoryUtilitiesDetailsAdapter(getActivity(), jobId);
                    utilitiesDetailsRecyclerView.setAdapter(postedJobHistoryUtilitiesDetailsAdapter);

                    postedJobHistoryUtilitiesDetailsAdapter.loadData();
                    break;
                case FEED_TYPE.TUTOR.MATCHES:
                    MatchesUtilitiesDetailsAdapter matchesUtilitiesDetailsAdapter = new MatchesUtilitiesDetailsAdapter(getActivity(), matchId);
                    utilitiesDetailsRecyclerView.setAdapter(matchesUtilitiesDetailsAdapter);

                    matchesUtilitiesDetailsAdapter.loadData();
                    break;
                case FEED_TYPE.TUTOR.APPLIED:
                    AppliedUtilitiesDetailsAdapter appliedUtilitiesDetailsAdapter = new AppliedUtilitiesDetailsAdapter(getActivity(), matchId);
                    utilitiesDetailsRecyclerView.setAdapter(appliedUtilitiesDetailsAdapter);

                    appliedUtilitiesDetailsAdapter.loadData();
                    break;
                case FEED_TYPE.TUTOR.ACTIVE_JOB:
                    com.hyptastic.poratechai.adapters.job_feed.utilities.details.tutor.ActiveJobsUtilitiesDetailsAdapter activeJobsUtilitiesDetailsAdapter
                            = new com.hyptastic.poratechai.adapters.job_feed.utilities.details.tutor.ActiveJobsUtilitiesDetailsAdapter(getActivity(), jobId);
                    utilitiesDetailsRecyclerView.setAdapter(activeJobsUtilitiesDetailsAdapter);

                    activeJobsUtilitiesDetailsAdapter.loadData();
                    break;
                case FEED_TYPE.TUTOR.JOB_HISTORY:
                    com.hyptastic.poratechai.adapters.job_feed.utilities.details.tutor.JobHistoryUtilitiesDetailsAdapter jobHistoryUtilitiesDetailsAdapter
                            = new com.hyptastic.poratechai.adapters.job_feed.utilities.details.tutor.JobHistoryUtilitiesDetailsAdapter(getActivity(), jobId);
                    utilitiesDetailsRecyclerView.setAdapter(jobHistoryUtilitiesDetailsAdapter);

                    jobHistoryUtilitiesDetailsAdapter.loadData();
                    break;
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        return view;
    }
}