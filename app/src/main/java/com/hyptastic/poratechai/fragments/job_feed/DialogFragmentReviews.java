package com.hyptastic.poratechai.fragments.job_feed;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RatingBar;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.adapters.job_feed.JobFeedActiveHistoryAdapter;
import com.hyptastic.poratechai.models.review.Review;
import com.hyptastic.poratechai.retrofit.functions.ReviewFunction;
import com.hyptastic.poratechai.retrofit.interfaces.ReviewClient;
import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
import com.hyptastic.poratechai.utils.Constants;
import com.hyptastic.poratechai.utils.Functions;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ISHRAK on 3/25/2019.
 */

public class DialogFragmentReviews extends DialogFragment {
    private RatingBar ratingBar;
    private CheckBox tagOne, tagTwo, tagThree;
    private Button cancelButton, confirmButton;

    DialogFragmentReviews currentDialogFragmentReviews = null;
    ReviewFunction reviewFunction = null;

    public static interface DialogFragmentReviewsCloseListener {
        public void dialogClosed();
    }

    public static DialogFragmentReviews createInstance(JobFeedActiveHistoryAdapter jobFeedActivityHistoryAdapter,
                                                       String userId, String reviewId, String jobId, double rating,
                                                       String[] reviewTags)
    {
        DialogFragmentReviews dialogFragmentReviews = new DialogFragmentReviews();

        Bundle bundle = new Bundle();
        bundle.putSerializable("Adapter", jobFeedActivityHistoryAdapter);
        bundle.putString("UserId", userId);
        bundle.putString("ReviewId", reviewId);
        bundle.putString("JobId", jobId);
        bundle.putDouble("Rating", rating);
        bundle.putStringArray("ReviewTags", reviewTags);

        dialogFragmentReviews.setArguments(bundle);

        return dialogFragmentReviews;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        try{
            Dialog dialog = super.onCreateDialog(savedInstanceState);

            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_review);
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.background_listitem_job_tutor_profile);
            dialog.getWindow().setLayout((int)(0.95*Constants.SCREEN_WIDTH), (int)(0.5*Constants.SCREEN_HEIGHT));

            currentDialogFragmentReviews = this;

            reviewFunction = new ReviewFunction(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, ReviewClient.class), getActivity());

            return dialog;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onStart() {
        try{
            super.onStart();

            String userId = getArguments().getString("UserId");
            String reviewId = getArguments().getString("ReviewId");
            String jobId = getArguments().getString("JobId");
            double rating = getArguments().getDouble("Rating");
            String[] reviewTags = getArguments().getStringArray("ReviewTags");

            Dialog dialog = getDialog();

            ratingBar = dialog.findViewById(R.id.reviewRatingBar);
            tagOne = dialog.findViewById(R.id.tagOne);
            tagTwo = dialog.findViewById(R.id.tagTwo);
            tagThree = dialog.findViewById(R.id.tagThree);
            cancelButton = dialog.findViewById(R.id.cancelButton);
            confirmButton = dialog.findViewById(R.id.confirmButton);

            if(Functions.isTutor()){
                tagOne.setText("Attentive");
                tagTwo.setText("Well-behaved");
                tagThree.setText("Punctual");
            }else{
                tagOne.setText("Punctual");
                tagTwo.setText("Communicates Well");
                tagThree.setText("Good Problem Solver");
            }

            ArrayList<String> selectedItems = new ArrayList<String>();

            //Initialize to saved values - start
            ratingBar.setRating((float)rating);
            for(String review: reviewTags){
                if(review.equals(tagOne.getText().toString())){
                    tagOne.setChecked(true);
                    selectedItems.add(tagOne.getText().toString());
                }
                if(review.equals(tagTwo.getText().toString())){
                    tagTwo.setChecked(true);
                    selectedItems.add(tagTwo.getText().toString());
                }
                if(review.equals(tagThree.getText().toString())){
                    tagThree.setChecked(true);
                    selectedItems.add(tagThree.getText().toString());
                }
            }
            //Initialize to saved values - end

            tagOne.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b){
                        selectedItems.add(tagOne.getText().toString());
                    }else{
                        selectedItems.remove(tagOne.getText().toString());
                    }
                }
            });

            tagTwo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b){
                        selectedItems.add(tagTwo.getText().toString());
                    }else{
                        selectedItems.remove(tagTwo.getText().toString());
                    }
                }
            });

            tagThree.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b){
                        selectedItems.add(tagThree.getText().toString());
                    }else{
                        selectedItems.remove(tagThree.getText().toString());
                    }
                }
            });

            cancelButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            confirmButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    String[] selectedReviewTags = new String[selectedItems.size()];
                    selectedItems.toArray(selectedReviewTags);

                    if(Functions.isTutor()){
                        if(reviewId == null){
                            reviewFunction.reviewByTutor(currentDialogFragmentReviews, jobId, new Review(userId, Constants.USER_NAME_VALUE, ratingBar.getRating(), selectedReviewTags));
                        }else{
                            reviewFunction.updateReviewByTutor(currentDialogFragmentReviews, reviewId, new Review(userId, Constants.USER_NAME_VALUE, ratingBar.getRating(), selectedReviewTags));
                        }
                    }else{
                        if(reviewId == null){
                            reviewFunction.reviewByParent(currentDialogFragmentReviews, jobId, new Review(userId, Constants.USER_NAME_VALUE, ratingBar.getRating(), selectedReviewTags));
                        }else{
                            reviewFunction.updateReviewByParent(currentDialogFragmentReviews, reviewId, new Review(userId, Constants.USER_NAME_VALUE, ratingBar.getRating(), selectedReviewTags));
                        }
                    }
                }
            });
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void reviewSubmittedSuccessfullyOverNetwork(){
        try{
            ((JobFeedActiveHistoryAdapter)getArguments().getSerializable("Adapter")).dialogClosed();
            getDialog().dismiss();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
