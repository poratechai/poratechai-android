//package com.hyptastic.poratechai.fragments.profile.parent_student;
//
//import android.app.FragmentTransaction;
//import android.os.Bundle;
//import android.app.Fragment;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.ProgressBar;
//import android.widget.TextView;
//
//import com.hyptastic.poratechai.R;
//import com.hyptastic.poratechai.fragments.login_registration.ChangePasswordDialog;
//import com.hyptastic.poratechai.models.users.User;
//import com.hyptastic.poratechai.retrofit.functions.UserFunctions;
//import com.hyptastic.poratechai.retrofit.interfaces.UserClient;
//import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
//import com.hyptastic.poratechai.utils.Constants;
//import com.hyptastic.poratechai.utils.Functions;
//
///**
// * A simple {@link Fragment} subclass.
// */
//public class ViewProfileFragment extends Fragment {
//    public static final String TAG = ViewProfileFragment.class.getSimpleName();
//
//    UserClient userClient = null;
//    UserFunctions userFunctions = null;
//
//    private TextView mTvEmail;
//    private TextView mTvContactNumber;
//
//    private String _id;
//    private String fullName;
//    private String contactNumber;
//    private String email;
//
//    private Button mBtEdit;
//    private Button mBtChangePassword;
//
//    private ProgressBar mProgressbar;
//
//    public ViewProfileFragment() {
//        // Required empty public constructor
//    }
//
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        // Inflate the layout for this fragment
//        View view = inflater.inflate(R.layout.fragment_parent_student_view_profile, container, false);
//
//        initViews(view);
//        getProfile();
//
//        return view;
//    }
//
//    private void initViews(View v) {
//        mTvEmail = (TextView) v.findViewById(R.id.tv_email);
//        mTvContactNumber = (TextView) v.findViewById(R.id.tv_contact_number);
//        mBtEdit = (Button) v.findViewById(R.id.btn_edit_parent_student_profile);
//        mBtChangePassword = (Button) v.findViewById(R.id.btn_change_password);
//        mProgressbar = (ProgressBar) v.findViewById(R.id.progress);
//
//        mBtChangePassword.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View view) {
//                ChangePasswordDialog changePasswordDialog = new ChangePasswordDialog();
////                changePasswordDialog.show(getFragmentManager(), "ChangePassword");
//            }
//        });
//
//        mBtEdit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                goToEditProfile();
//            }
//        });
//
//    }
//
//    private void getProfile(){
//        userClient = RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE,Constants.TOKEN_VALUE,UserClient.class);
//        userFunctions = new UserFunctions(userClient,getActivity().getApplicationContext());
//        userFunctions.getParentStudentProfileForView(this);
//    }
//
//    public void setParentStudentProfileView(User user){
//        _id = user.get_id();
//        fullName = user.getFullName();
//        contactNumber = user.getContactNumber().toString();
//        email = user.getEmail();
//
//        mProgressbar.setVisibility(View.GONE);
//        mTvEmail.setText(email);
//        mTvContactNumber.setText("0"+contactNumber);
//
//        Functions.storeFullNameToSharedPreferences(fullName);
//    }
//
//    private void goToEditProfile(){
//        Bundle data = new Bundle();
//        data.putString("_id", _id);
//        data.putString("fullName",fullName);
//        data.putString("contactNumber", contactNumber);
//        data.putString("email",email);
//
//        FragmentTransaction ft = getFragmentManager().beginTransaction();
//        EditProfileFragment fragment = new EditProfileFragment();
//        fragment.setArguments(data);
//        ft.replace(R.id.fragmentFrame,fragment,EditProfileFragment.TAG);
//        ft.commit();
//    }
//
//
//}