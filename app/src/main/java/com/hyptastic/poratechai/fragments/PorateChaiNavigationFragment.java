package com.hyptastic.poratechai.fragments;

import android.support.v4.app.Fragment;

/**
 * Created by ISHRAK on 2/13/2019.
 */

public abstract class PorateChaiNavigationFragment extends Fragment {
    public abstract void loadFragment();
}
