package com.hyptastic.poratechai.fragments.profile;

import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.SpinKitView;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.CubeGrid;
import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.activities.PorateChaiActivity;
import com.hyptastic.poratechai.adapters.profile.ProfileAdapter;
import com.hyptastic.poratechai.fragments.PorateChaiNavigationFragment;
import com.hyptastic.poratechai.fragments.profile.tutor.reviews.ReviewsFragment;
import com.hyptastic.poratechai.fragments.profile.tutor.stats.StatsFragment;
import com.hyptastic.poratechai.retrofit.functions.SystemFunction;
import com.hyptastic.poratechai.retrofit.functions.UserFunctions;
import com.hyptastic.poratechai.retrofit.interfaces.SystemClient;
import com.hyptastic.poratechai.retrofit.interfaces.UserClient;
import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
import com.hyptastic.poratechai.utils.Constants;
import com.hyptastic.poratechai.utils.Functions;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

import static android.app.Activity.RESULT_OK;

/**
 * Created by ISHRAK on 2/13/2019.
 */

public class NavigationFragment extends PorateChaiNavigationFragment {
    ImageView profileImageView, verifiedTickImageView;
    TextView profileNameTextView, verifiedTextView;
    TabLayout profileTabs;
    ViewPager profilePager;

    SpinKitView loadingSpinner = null;
    Sprite loadingSprite = null;

    NavigationFragment currentNavigationFragment;

    ArrayList<Fragment> tutorProfileFragments, parentProfileFragments;
    String[] tutorProfileTitles = new String[]{"Profile", "Stats", "Reviews"};
    String[] parentProfileTitles = new String[]{"Profile", "Stats", "Reviews"};
    ProfileAdapter tutorProfileAdapter, parentProfileAdapter;

    private UserFunctions userFunctions;

    private static final int INTENT_REQUEST_CODE = 100;

    private final Object lock = new Object();
    boolean isActivityAttached = false;

    public static interface LoadListener extends Parcelable {
        public abstract void loadFragment(boolean isStart);

        public static final Parcelable.Creator CREATOR = null;
    }

    public static NavigationFragment createInstance(int feedType){
        NavigationFragment navigationFragment = new NavigationFragment();

        Bundle bundle = new Bundle();
        bundle.putInt("feedType", feedType);

        navigationFragment.setArguments(bundle);

        return navigationFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_navigation_profile, null);

        currentNavigationFragment = this;

        profileImageView        = (ImageView)view.findViewById(R.id.profileImageView);
        profileNameTextView     = (TextView)view.findViewById(R.id.profileNameTextView);
        verifiedTextView        = (TextView)view.findViewById(R.id.verifiedTextView);
        verifiedTickImageView   = (ImageView)view.findViewById(R.id.verifiedTickImageView);

        profileTabs = (TabLayout)view.findViewById(R.id.profileTabs);
        profilePager = (ViewPager)view.findViewById(R.id.profilePager);

        loadingSpinner = (SpinKitView)view.findViewById(R.id.profileLoadingSpinKit);
        loadingSprite = new CubeGrid();
        loadingSpinner.setIndeterminateDrawable(loadingSprite);

        profilePager.setOffscreenPageLimit(3);

        LoadListener loadListener = new LoadListener(){
            @Override
            public void loadFragment(boolean isStart) {
                if(isStart){
                    profilePager.setVisibility(View.INVISIBLE);

                    loadingSpinner.setVisibility(View.VISIBLE);
                    loadingSprite.start();
                }else{
                    profilePager.setVisibility(View.VISIBLE);

                    loadingSpinner.setVisibility(View.INVISIBLE);
                    loadingSprite.stop();
                }
            }

            @Override
            public int describeContents() { return 0; }

            @Override
            public void writeToParcel(Parcel parcel, int i) { }
        };

        tutorProfileFragments = new ArrayList<>(Arrays.asList(
                com.hyptastic.poratechai.fragments.profile.tutor.profile.ProfileFragment.createInstance(loadListener),
                new StatsFragment(),
                new ReviewsFragment()));
        tutorProfileAdapter = new ProfileAdapter(getActivity().getSupportFragmentManager(), tutorProfileFragments, tutorProfileTitles);
        parentProfileFragments = new ArrayList<>(Arrays.asList(
                com.hyptastic.poratechai.fragments.profile.parent_student.profile.ProfileFragment.createInstance(loadListener),
                new com.hyptastic.poratechai.fragments.profile.parent_student.stats.StatsFragment(),
                new com.hyptastic.poratechai.fragments.profile.parent_student.reviews.ReviewsFragment()));
        parentProfileAdapter = new ProfileAdapter(getActivity().getSupportFragmentManager(), parentProfileFragments, parentProfileTitles);

        userFunctions = new UserFunctions(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE,Constants.TOKEN_VALUE,UserClient.class), getActivity());

        loadFragment();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        isActivityAttached = true;
    }

    @Override
    public void loadFragment(){
        profileNameTextView.setText(Constants.USER_NAME_VALUE);

        Functions.loadProfilePicture(getActivity(), Constants.USER_ID_VALUE, new Constants.PICTURE_PACKET(getActivity(), profileImageView, null, Constants.PICTURE_PACKET.PROFILE_PICTURE_WIDTH, Constants.PICTURE_PACKET.PROFILE_PICTURE_HEIGHT));
        getVerificationStatus();
        profileImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/jpeg");

                try {
                    startActivityForResult(intent, INTENT_REQUEST_CODE);

                } catch (ActivityNotFoundException e) {

                    e.printStackTrace();
                }
            }
        });

        if(Functions.isTutor()){
            profilePager.setAdapter(tutorProfileAdapter);
        }else{
            profilePager.setAdapter(parentProfileAdapter);
        }

        if(getArguments()!=null && getArguments().containsKey("feedType")){
            profilePager.setCurrentItem(getArguments().getInt("feedType"));
        }else{
            profilePager.setCurrentItem(0);
        }
        profileTabs.setupWithViewPager(profilePager);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("ProfilePicture", "Activity Result");

        int dataSize = 0;

        //For profile picture
        if (requestCode == INTENT_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                try {
                    Uri uri  = data.getData();
                    String scheme = uri.getScheme();
                    InputStream fileInputStream = getActivity().getContentResolver().openInputStream(uri);

//                    InputStream is = getContentResolver().openInputStream(data.getData());

                    if(scheme.equals(ContentResolver.SCHEME_CONTENT)){
                        try {
                            dataSize = fileInputStream.available()/1024;
                            if(dataSize > 4096){
//                                System.out.println("File size in bytes: "+ dataSize);
                                Toast.makeText(getActivity(),"Image size cannot exceed 4MB",Toast.LENGTH_LONG).show();
                            }
                            else {
//                                System.out.println("ELSE File size in bytes: "+ dataSize);
                                uploadImage(Functions.getBytes(fileInputStream));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    else if(scheme.equals(ContentResolver.SCHEME_FILE)){
                        String path = uri.getPath();
                        File f= null;
                        try {
                            f = new File(path);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

//---------------------------------------------------------------------------------------------------------------------
    //Profile picture - start
    private void uploadImage(byte[] imageBytes){
        userFunctions.uploadProfilePicture(this, imageBytes);
    }

    public void profilePictureUploaded(String url){
        Functions.loadProfilePicture(getActivity(), Constants.USER_ID_VALUE, new Constants.PICTURE_PACKET(getActivity(), profileImageView, url, Constants.PICTURE_PACKET.PROFILE_PICTURE_WIDTH, Constants.PICTURE_PACKET.PROFILE_PICTURE_HEIGHT));
    }
    //Profile picture - end
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
    //Verification Status - start
    private void getVerificationStatus(){
        verifiedTextView.setText("CHECKING ...");
        verifiedTickImageView.setImageResource(android.R.color.transparent);

        userFunctions.getVerificationStatus(this, Constants.USER_ID_VALUE);
    }

    public void verificationStatusGotten(boolean isGetSuccessful, boolean isVerifiedByVerificationImages){
        if(isGetSuccessful){
            if(isVerifiedByVerificationImages){
                verifiedTextView.setText("VERIFIED");
                verifiedTickImageView.setImageResource(R.drawable.ic_verified);
            }else{
                verifiedTextView.setText("NOT VERIFIED");
                verifiedTickImageView.setImageResource(R.drawable.ic_not_verified);
            }
        }else{
            verifiedTextView.setText("CHECKING ...");
            verifiedTickImageView.setImageResource(android.R.color.transparent);
        }
    }
    //Verification Status - end
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
}
