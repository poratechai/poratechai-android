package com.hyptastic.poratechai.fragments.job_feed.utilities;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.CompoundButtonCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.models.job_feed.calendar.Calendar;
import com.hyptastic.poratechai.models.job_feed.calendar.Dates;
import com.hyptastic.poratechai.retrofit.functions.CalendarFunction;
import com.hyptastic.poratechai.retrofit.interfaces.CalendarClient;
import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
import com.hyptastic.poratechai.utils.Constants;
import com.hyptastic.poratechai.utils.Functions;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;
import com.prolificinteractive.materialcalendarview.spans.DotSpan;

import org.threeten.bp.LocalDate;
import org.threeten.bp.DayOfWeek;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by ISHRAK on 5/31/2019.
 */

public class UtilitiesCalendarFragment extends Fragment {
    private SwipeRefreshLayout calendarSwipeRefreshLayout;
    private MaterialCalendarView calendarView;
    private AppCompatCheckBox tutorSign, studentSign;
    private EditText notes;
    private TextView savingTextView;

    private String jobId;

    private Calendar currentJobMonthCalendar;
    private SparseArray<Dates> currentMonthDetails;

    private CalendarFunction calendarFunction;
    private UtilitiesCalendarFragment currentUtilitiesCalendarFragment = null;

    private int year, month;

    public static UtilitiesCalendarFragment createInstance(String jobId){
        UtilitiesCalendarFragment utilitiesCalendarFragment = new UtilitiesCalendarFragment();

        Bundle bundle = new Bundle();
        bundle.putString("JOB_ID", jobId);

        utilitiesCalendarFragment.setArguments(bundle);

        return utilitiesCalendarFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        jobId = (String)getArguments().getString("JOB_ID");

        calendarFunction = new CalendarFunction(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, CalendarClient.class), getActivity());

        currentUtilitiesCalendarFragment = this;

        View view = inflater.inflate(R.layout.fragment_utilities_calendar, null);

        calendarSwipeRefreshLayout  = view.findViewById(R.id.calendarSwipeRefreshLayout);
        calendarView                = view.findViewById(R.id.calendarView);
        tutorSign                   = view.findViewById(R.id.tutorSign);
        studentSign                 = view.findViewById(R.id.studentSign);
        notes                       = view.findViewById(R.id.notesEditText);
        savingTextView              = view.findViewById(R.id.savingTextView);

        calendarSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                calendarFunction.getJobCalendarMonth(currentUtilitiesCalendarFragment, jobId, year, month);
            }
        });

        savingTextViewHandler = new SavingTextViewHandler(savingTextView);

        setCheckBoxColor();
        setCalendarListener();

        studentSign.setOnCheckedChangeListener(studentSignOnCheckedListener);
        tutorSign.setOnCheckedChangeListener(tutorSignOnCheckedListener);

        notes.addTextChangedListener(textWatcher);

        return view;
    }

    private void setCalendarListener(){
        calendarView.setSelectedDate(CalendarDay.today());
        getJobCalendarMonth(CalendarDay.today().getYear(), CalendarDay.today().getMonth());

        calendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView materialCalendarView, @NonNull CalendarDay calendarDay, boolean b) {
                if(currentMonthDetails != null){
                    setStudentSign(currentMonthDetails.get(calendarDay.getDay()).isParentStudentSignature());
                    setTutorSign(currentMonthDetails.get(calendarDay.getDay()).isTutorSignature());

                    if(Functions.isTutor()){
                        setNotes(currentMonthDetails.get(calendarDay.getDay()).getTutorNote());
                    }else{
                        setNotes(currentMonthDetails.get(calendarDay.getDay()).getParentStudentNote());
                    }

                    if(currentMonthDetails.get(calendarDay.getDay()).isParentStudentSignature() && currentMonthDetails.get(calendarDay.getDay()).isTutorSignature()){
                        studentSign.setEnabled(false);
                        tutorSign.setEnabled(false);
                    }
                }
            }
        });

        calendarView.setOnMonthChangedListener(new OnMonthChangedListener() {
            @Override
            public void onMonthChanged(MaterialCalendarView materialCalendarView, CalendarDay calendarDay) {
                setStudentSign(false);
                setTutorSign(false);
                setNotes("");

                getJobCalendarMonth(calendarDay.getYear(), calendarDay.getMonth());

                year = calendarDay.getYear();
                month = calendarDay.getMonth();
            }
        });
    }

    private class StudentDecorator implements DayViewDecorator {
        private final HashSet<CalendarDay> dates;

        StudentDecorator(Collection<CalendarDay> dates) {
            this.dates = new HashSet<CalendarDay>(dates);
        }

        @Override
        public boolean shouldDecorate(CalendarDay calendarDay) {
            return dates.contains(calendarDay);
        }

        @Override
        public void decorate(DayViewFacade dayViewFacade) {
            dayViewFacade.addSpan(new DotSpan(5, Color.parseColor("#0044ff")));
        }
    }

    private class TutorDecorator implements DayViewDecorator {
        private final HashSet<CalendarDay> dates;

        TutorDecorator(Collection<CalendarDay> dates) {
            this.dates = new HashSet<CalendarDay>(dates);
        }

        @Override
        public boolean shouldDecorate(CalendarDay calendarDay) {
            return dates.contains(calendarDay);
        }

        @Override
        public void decorate(DayViewFacade dayViewFacade) {
            dayViewFacade.addSpan(new DotSpan(5, Color.parseColor("#ee2e2e")));
        }
    }

    private class BothDecorator implements DayViewDecorator {
        private final HashSet<CalendarDay> dates;

        BothDecorator(Collection<CalendarDay> dates) {
            this.dates = new HashSet<CalendarDay>(dates);
        }

        @Override
        public boolean shouldDecorate(CalendarDay calendarDay) {
            return dates.contains(calendarDay);
        }

        @Override
        public void decorate(DayViewFacade dayViewFacade) {
            dayViewFacade.addSpan(new DotSpan(5, Color.parseColor("#11b5a3")));
        }
    }

    private class NotesDecorator implements DayViewDecorator {
        private final HashSet<CalendarDay> dates;

        public NotesDecorator(Collection<CalendarDay> dates) {
            this.dates = new HashSet<CalendarDay>(dates);
        }

        @Override
        public boolean shouldDecorate(CalendarDay calendarDay) {
            return dates.contains(calendarDay);
        }

        @Override
        public void decorate(DayViewFacade dayViewFacade) {
            dayViewFacade.setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.background_calendar_notes));
        }
    }

    private class WeekendDecorator implements DayViewDecorator {
        private HashSet<CalendarDay> dates;

        WeekendDecorator(Collection<CalendarDay> dates){
            this.dates = new HashSet<CalendarDay>(dates);
        }

        @Override
        public boolean shouldDecorate(CalendarDay calendarDay) {
			LocalDate date = calendarDay.getDate();

			if(date.getDayOfWeek() == DayOfWeek.FRIDAY || date.getDayOfWeek() == DayOfWeek.SATURDAY){
			    return true;
            }

            return false;
        }

        @Override
        public void decorate(DayViewFacade dayViewFacade) {
            dayViewFacade.addSpan(new ForegroundColorSpan(ResourcesCompat.getColor(getResources(), R.color.pink, null)));
        }
    }

//Checkbox Listeners - start
    private boolean tempStudentSign;
    private CompoundButton.OnCheckedChangeListener studentSignOnCheckedListener = new CompoundButton.OnCheckedChangeListener(){
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            if(compoundButton.isFocusable()){
                tempStudentSign = b;

                CalendarDay selectedDay = calendarView.getSelectedDate();
                calendarFunction.parentStudentSignature(currentUtilitiesCalendarFragment, jobId,
                        selectedDay.getYear(), selectedDay.getMonth(), selectedDay.getDay(), b);
            }else{
                Toast.makeText(getActivity(), "Attendance is locked for this day", Toast.LENGTH_LONG).show();
            }
        }
    };

    private boolean tempTutorSign;
    private CompoundButton.OnCheckedChangeListener tutorSignOnCheckedListener = new CompoundButton.OnCheckedChangeListener(){
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            if(compoundButton.isFocusable()){
                tempTutorSign = b;

                CalendarDay selectedDay = calendarView.getSelectedDate();
                calendarFunction.tutorSignature(currentUtilitiesCalendarFragment, jobId,
                        selectedDay.getYear(), selectedDay.getMonth(), selectedDay.getDay(), b);
            }else{
                Toast.makeText(getActivity(), "Attendance is locked for this day", Toast.LENGTH_LONG).show();
            }
        }
    };

    private void setStudentSign(boolean b){
        studentSign.setOnCheckedChangeListener(null);
        studentSign.setChecked(b);
        studentSign.setOnCheckedChangeListener(studentSignOnCheckedListener);
    }

    private void setTutorSign(boolean b){
        tutorSign.setOnCheckedChangeListener(null);
        tutorSign.setChecked(b);
        tutorSign.setOnCheckedChangeListener(tutorSignOnCheckedListener);
    }

    private void setCheckBoxColor(){
        ColorStateList checkBoxColorStateList = new ColorStateList(
                new int[][]{
                        new int[] { -android.R.attr.state_checked },
                        new int[] { android.R.attr.state_checked }
                },
                new int[]{
                        ResourcesCompat.getColor(getResources(), R.color.colorLightGrey, null),
                        ResourcesCompat.getColor(getResources(), R.color.colorLightGrey, null)
                }
        );

        if(Functions.isTutor()){
            studentSign.setEnabled(false);
            CompoundButtonCompat.setButtonTintList(studentSign, checkBoxColorStateList);
        }else{
            tutorSign.setEnabled(false);
            CompoundButtonCompat.setButtonTintList(tutorSign, checkBoxColorStateList);
        }
    }
//Checkbox Listeners - end

//Notes EditText Listener - start
    private Timer timer = new Timer();
    private final long DELAY = 1000;

    private static class SavingTextViewHandler extends Handler {
        private WeakReference<TextView> savingTextViewWeakReference;

        SavingTextViewHandler(TextView savingTextView){
            this.savingTextViewWeakReference = new WeakReference<TextView>(savingTextView);
        }

        @Override
        public void handleMessage(Message msg) {
            Log.e("Handler", "HERE");

            Bundle bundle = msg.getData();
            String status = bundle.getString("STATUS");

            savingTextViewWeakReference.get().setText(status);
        }
    }

    private SavingTextViewHandler savingTextViewHandler;

    private TextWatcher textWatcher = new TextWatcher(){
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

        @Override
        public void afterTextChanged(Editable editable) {
            timer.cancel();
            timer = new Timer();
            timer.schedule(new TimerTask(){
                @Override
                public void run() {
                    Message message = savingTextViewHandler.obtainMessage();

                    Bundle bundle = new Bundle();
                    bundle.putString("STATUS", "Saving...");

                    message.setData(bundle);
                    message.sendToTarget();

                    if(Functions.isTutor()){
                        calendarFunction.tutorNote(currentUtilitiesCalendarFragment, jobId, calendarView.getSelectedDate().getYear(),
                                calendarView.getSelectedDate().getMonth(), calendarView.getSelectedDate().getDay(), notes.getText().toString());
                    }else{
                        calendarFunction.parentStudentNote(currentUtilitiesCalendarFragment, jobId, calendarView.getSelectedDate().getYear(),
                                calendarView.getSelectedDate().getMonth(), calendarView.getSelectedDate().getDay(), notes.getText().toString());
                    }
                }
            }, DELAY);
        }
    };

    private void setNotes(String text){
        notes.removeTextChangedListener(textWatcher);
        notes.setText(text);
        notes.addTextChangedListener(textWatcher);
    }
//Notes EditText Listener - end

    private void getJobCalendarMonth(int year, int month){
        calendarFunction.getJobCalendarMonth(currentUtilitiesCalendarFragment, jobId, year, month);

        //block ui
    }

//--------------------------------------------------------------------------------------------------------------------------------
//Responses - start
    public void jobCalendarMonthGotten(Calendar jobCalendarMonth){
        Log.e("Job Calendar Month", jobCalendarMonth.toString());

        calendarView.removeDecorators();

        currentJobMonthCalendar = jobCalendarMonth;

        Collection<CalendarDay> studentAttendance = new ArrayList<CalendarDay>();
        Collection<CalendarDay> tutorAttendance = new ArrayList<CalendarDay>();
        Collection<CalendarDay> bothAttendance = new ArrayList<CalendarDay>();
        Collection<CalendarDay> notesDays = new ArrayList<CalendarDay>();

        if(currentJobMonthCalendar != null){
            currentMonthDetails = new SparseArray<Dates>(31);

            int year = (currentJobMonthCalendar.getDates() != null && currentJobMonthCalendar.getDates().length > 0 ? currentJobMonthCalendar.getDates()[0].getYear() : -1);
            int month = (currentJobMonthCalendar.getDates() != null && currentJobMonthCalendar.getDates().length > 0 ? currentJobMonthCalendar.getDates()[0].getMonth() : -1);

            for(int i = 0; i < 31; i++) {
                Dates date = new Dates(false, false, "", "", year, month, i+1);
                currentMonthDetails.append(i+1, date);
            }

            Dates date;
            CalendarDay day;
            for(int i =0; i < currentJobMonthCalendar.getDates().length; i++){
                date = currentJobMonthCalendar.getDates()[i];
                day = CalendarDay.from(date.getYear(), date.getMonth(), date.getDay());

                currentMonthDetails.append(date.getDay(), date);

                if(date.isParentStudentSignature() && !date.isTutorSignature()) studentAttendance.add(day);
                if(!date.isParentStudentSignature() && date.isTutorSignature()) tutorAttendance.add(day);
                if(date.isParentStudentSignature() && date.isTutorSignature()) bothAttendance.add(day);
                if(Functions.isTutor() && date.getTutorNote().length()>0) notesDays.add(day);
                if(!Functions.isTutor() && date.getParentStudentNote().length()>0) notesDays.add(day);
            }
        }

        calendarView.setSelectedDate(calendarView.getSelectedDate());

        //Get Weekends - start
        Collection<CalendarDay> monthDates = new ArrayList<CalendarDay>(31);
        int year = CalendarDay.today().getYear();
        int month = CalendarDay.today().getMonth();
        for(int i = 0; i < 31; i++){
            try{
                monthDates.add(CalendarDay.from(year, month, i+1));
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        //Get Weekends - end

        calendarView.addDecorator(new WeekendDecorator(monthDates));
        calendarView.addDecorator(new StudentDecorator(studentAttendance));
        calendarView.addDecorator(new TutorDecorator(tutorAttendance));
        calendarView.addDecorator(new BothDecorator(bothAttendance));
        calendarView.addDecorator(new NotesDecorator(notesDays));

        calendarSwipeRefreshLayout.setRefreshing(false);
    }

    public void studentSignatureCompleted(boolean isSuccessful, Dates d){
        if(!isSuccessful){
            setStudentSign(tempStudentSign);
        }else{
            currentMonthDetails.append(d.getDay(), d);
        }

        if(d.isParentStudentSignature() && d.isTutorSignature()){
            studentSign.setEnabled(false);
            tutorSign.setEnabled(false);
        }
    }

    public void tutorSignatureCompleted(boolean isSuccessful, Dates d){
        if(!isSuccessful){
            setTutorSign(tempTutorSign);
        }else{
            currentMonthDetails.append(d.getDay(), d);
        }

        if(d.isParentStudentSignature() && d.isTutorSignature()){
            studentSign.setEnabled(false);
            tutorSign.setEnabled(false);
        }
    }

    public void noteCompleted(boolean isSuccessful, Dates d){
        if(isSuccessful){
            savingTextView.setTextColor(ResourcesCompat.getColor(getResources(), R.color.colorMediumAccent, null));
            savingTextView.setText("Saved");
            savingTextView.setTextColor(ResourcesCompat.getColor(getResources(), R.color.colorDarkGrey, null));

            currentMonthDetails.append(d.getDay(), d);
        }else{
            savingTextView.setTextColor(ResourcesCompat.getColor(getResources(), R.color.red, null));
            savingTextView.setText("Save failed");
            savingTextView.setTextColor(ResourcesCompat.getColor(getResources(), R.color.colorDarkGrey, null));
        }

        Timer timer = new Timer();
        timer.schedule(new TimerTask(){
            @Override
            public void run() {
                Message message = savingTextViewHandler.obtainMessage();

                Bundle bundle = new Bundle();
                bundle.putString("STATUS", "");

                message.setData(bundle);
                message.sendToTarget();
            }
        }, 1000);
    }
//Responses - end
//------------------------------------------------------------------------------------------------------------------------------

    @Override
    public void onStop() {
        super.onStop();
    }
}
