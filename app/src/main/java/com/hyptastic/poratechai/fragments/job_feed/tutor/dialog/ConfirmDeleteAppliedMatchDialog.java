package com.hyptastic.poratechai.fragments.job_feed.tutor.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.adapters.job_feed.tutor.AppliedAdapter;
import com.hyptastic.poratechai.adapters.job_feed.tutor.MatchesAdapter;

/**
 * Created by ISHRAK on 3/23/2018.
 */

public class ConfirmDeleteAppliedMatchDialog extends DialogFragment {
    public interface ConfirmDeleteAppliedMatchListener {
        public void onMatchDeletePositive(String matchId, int position);
    }

    private ConfirmDeleteAppliedMatchListener mListener;

    public static ConfirmDeleteAppliedMatchDialog createNewInstance(AppliedAdapter adapter, String matchId, int position)
    {
        ConfirmDeleteAppliedMatchDialog confirmDeleteAppliedMatchDialog = new ConfirmDeleteAppliedMatchDialog();

        Bundle bundle = new Bundle();
        bundle.putSerializable("adapter", adapter);
        bundle.putString("matchId", matchId);
        bundle.putInt("position", position);

        confirmDeleteAppliedMatchDialog.setArguments(bundle);

        return confirmDeleteAppliedMatchDialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AppliedAdapter appliedAdapter = (AppliedAdapter) getArguments().getSerializable("adapter");
        final String matchId = getArguments().getString("matchId");
        final int position = getArguments().getInt("position");

        try{
            mListener = (ConfirmDeleteAppliedMatchListener)appliedAdapter;
        }catch(ClassCastException e){
            throw new ClassCastException(appliedAdapter.toString() + " must implement ConfirmDeleteMatchDialog");
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.match_deletion_confirmation)
                .setTitle(R.string.match_deletion_title)
                .setPositiveButton(R.string.match_deletion_positive, new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mListener.onMatchDeletePositive(matchId, position);
                    }
                })
                .setNegativeButton(R.string.match_deletion_negative, new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

        return builder.create();
    }
}
