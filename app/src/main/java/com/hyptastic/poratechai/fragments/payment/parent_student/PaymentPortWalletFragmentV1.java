package com.hyptastic.poratechai.fragments.payment.parent_student;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.fragments.payment.PortwalletWebViewClient;
import com.hyptastic.poratechai.fragments.payment.PortwalletWebViewJavascriptInterface;
import com.hyptastic.poratechai.models.wallet.InvoiceResponse;

/**
 * Created by ISHRAK on 6/25/2018.
 */

public class PaymentPortWalletFragmentV1 extends Fragment {
    WebView paymentPortwalletWebView;
    InvoiceResponse invoiceResponse;

    public static PaymentPortWalletFragmentV1 createNewInstance(InvoiceResponse invoiceResponse){
        PaymentPortWalletFragmentV1 paymentPortWalletFragment = new PaymentPortWalletFragmentV1();

        Bundle args = new Bundle();
        args.putParcelable("invoice", invoiceResponse);

        paymentPortWalletFragment.setArguments(args);

        return paymentPortWalletFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment_processing, container, false);

        try{
            paymentPortwalletWebView = view.findViewById(R.id.paymentPortwalletWebView);

            PortwalletWebViewClient portWalletWebViewClient = new PortwalletWebViewClient(getActivity());
            paymentPortwalletWebView.setWebViewClient(portWalletWebViewClient);
            paymentPortwalletWebView.getSettings().setJavaScriptEnabled(true);
            paymentPortwalletWebView.addJavascriptInterface(new PortwalletWebViewJavascriptInterface((Context)getActivity()), "HtmlViewer");

            invoiceResponse = (InvoiceResponse)getArguments().getParcelable("invoice");
            String invoiceId = invoiceResponse.getData().getInvoiceId();

            paymentPortwalletWebView.loadUrl("https://payment-sandbox.portwallet.com/payment/?invoice=" + invoiceId);
        }catch(Exception e){
            Log.e("PaymentFragment", e.getMessage());
        }

        return view;
    }
}
