package com.hyptastic.poratechai.fragments.dashboard;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.Window;
import android.widget.Toast;

import com.hyptastic.poratechai.R;

import com.hyptastic.poratechai.utils.Functions;

/**
 * Created by ISHRAK on 8/13/2018.
 */

public class BlockingFragment extends DialogFragment {
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();

        final Dialog dialog = getDialog();
        if(dialog!=null)
        {
            dialog.setContentView(R.layout.switch_blocking_fragment);
            dialog.setCancelable(false);
        }
    }

    public void dismissDialog(){
        getDialog().dismiss();
    }
}
