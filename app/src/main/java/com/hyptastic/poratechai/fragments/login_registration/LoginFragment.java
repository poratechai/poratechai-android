package com.hyptastic.poratechai.fragments.login_registration;

/**
 * Created by User on 31-Jan-18.
 */

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hyptastic.poratechai.activities.PorateChaiActivity;
import com.hyptastic.poratechai.activities.HireOrTeachActivity;
import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.models.users.Response;
import com.hyptastic.poratechai.retrofit.functions.UserFunctions;
import com.hyptastic.poratechai.retrofit.interfaces.UserClient;
import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
import com.hyptastic.poratechai.utils.Functions;


import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


import static com.hyptastic.poratechai.utils.Validation.validateEmail;
import static com.hyptastic.poratechai.utils.Validation.validateFields;


public class LoginFragment extends Fragment {
    UserClient userClient = null;
    UserFunctions userFunctions = null;
    public static final String TAG = LoginFragment.class.getSimpleName();

    private ImageView imgLogo;
    private EditText mEtEmail;
    private EditText mEtPassword;
    private EditText mEtContactNumber;
    private ImageButton showPasswordButton;
    private Button mBtLogin;
    private TextView mTvRegister;
    private TextView mTvForgotPassword;
    private ProgressBar mProgressBar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login,container,false);

        /*if(mSharedPreferences.getBoolean(Constants.STATUS,false)){
            Intent intent = new Intent(getActivity(), ProfileActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            getActivity().finish();
        }*/

        initViews(view);

        return view;
    }

    private void initViews(View v) {

        imgLogo = (ImageView)v.findViewById(R.id.img_logo);
//        mEtEmail = (EditText) v.findViewById(R.id.et_email);
        mEtContactNumber = (EditText) v.findViewById(R.id.et_contact_number);
//        mEtPassword = (EditText) v.findViewById(R.id.et_password);
        mBtLogin = (Button) v.findViewById(R.id.btn_login);
//        showPasswordButton = (ImageButton)v.findViewById(R.id.show_password_button);
        mProgressBar = (ProgressBar) v.findViewById(R.id.progress);
        mTvRegister = (TextView) v.findViewById(R.id.tv_register);
        mTvForgotPassword = (TextView) v.findViewById(R.id.tv_forgot_password);

        imgLogo.setImageBitmap(Functions.decodeSampledBitmapFromResource(getResources(), R.drawable.pc_login, 500, 700));

//        showPasswordButton.setTag(false);
//        showPasswordButton.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View view) {
//                boolean isVisible = (boolean)view.getTag();
//
//                if(isVisible){
//                    mEtPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
//                    mEtPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
//
//                    view.setTag(false);
//
//                    showPasswordButton.setImageResource(R.drawable.ic_visibility_off);
//                }else{
//                    mEtPassword.setInputType(InputType.TYPE_CLASS_TEXT);
//
//                    view.setTag(true);
//
//                    showPasswordButton.setImageResource(R.drawable.ic_visibility);
//                }
//            }
//        });

        mBtLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

        mTvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToRegister();
            }
        });

        mTvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });
    }

    private void login() {
//        String email = mEtEmail.getText().toString();
//        String password = mEtPassword.getText().toString();
        int contactNumber = Integer.parseInt(mEtContactNumber.getText().toString());

        if (!validateFields(contactNumber)) {
            Toast.makeText(getActivity(), "Please provide valid Contact Number", Toast.LENGTH_LONG).show();
            return;
        }

//        if (!validateFields(password)) {
//            Toast.makeText(getActivity(), "Please provide valid Password", Toast.LENGTH_LONG).show();
//            return;
//        }

        loginUser(getActivity(), contactNumber);
        mProgressBar.setVisibility(View.VISIBLE);
    }

    public void stopLoader(String message){
        mProgressBar.setVisibility(View.GONE);
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

//    private void loginUser(Context context, final String email, String password){
//        userClient = RetrofitServiceGenerator.getRetrofitForAuthentication(email,password);
//        userClient.login()
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Observer<Response>() {
//                    @Override
//                    public void onSubscribe(Disposable d) {
//                        CompositeDisposable compositeDisposable =new CompositeDisposable();
//                        compositeDisposable.add(d);
//                    }
//
//                    @Override
//                    public void onNext(Response response) {
//                        try{
//                            Log.v("Response from LOG", response.getInstitution());
//                            Functions.storeLoginDataToSharedPreferences(response.getMessage(), response.getToken(), response.getUserId(), response.getProfilePicturePath(), response.getFullName(), response.getInstitution(), response.getIsTutor(), response.getIsVerified(), response.getTutorUpdateFlag());
//                            if(response.getIsTutor().equals("true") || response.getIsTutor().equals("false")){
//                                Intent intent = new Intent(context, PorateChaiActivity.class);
//                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                startActivity(intent);
//                                getActivity().finish();
//                            }
//                            else {
//                                Intent intent = new Intent(context, HireOrTeachActivity.class);
//                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                startActivity(intent);
//                                getActivity().finish();
//                            }
//                        }catch(Exception e){
//                            e.printStackTrace();
//                        }
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        try
//                        {
//                            mProgressBar.setVisibility(View.GONE);
//                            Log.e("Login Failed ", e.getMessage());
//                            Toast.makeText(getActivity().getApplicationContext(),"Login Failed",Toast.LENGTH_LONG).show();
//                        }
//                        catch(Exception exception)
//                        {
//                            Log.e("User Creation EXCEPTION", exception.getMessage());
//                        }
//                    }
//
//                    @Override
//                    public void onComplete() {
//                        Log.v("Login", "Successful");
//                        mProgressBar.setVisibility(View.GONE);
//                        String id = "";
//
//                        mEtEmail.setText(null);
//                        mEtPassword.setText(null);
//
////                        Toast.makeText(getActivity().getApplicationContext(),Constants.USER_ID_VALUE,Toast.LENGTH_LONG).show();
//                    }
//                });
//    }

    private void loginUser(Context context, int contactNumber){
        userClient = RetrofitServiceGenerator.getRetrofit();
        userFunctions = new UserFunctions(userClient,getActivity().getApplicationContext());
        userFunctions.userExists(this, contactNumber);
    }

    private void goToRegister(){
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        RegisterFragment fragment = new RegisterFragment();
        ft.replace(R.id.fragmentFrame,fragment).addToBackStack(RegisterFragment.TAG);
        ft.commit();
    }

    private void showDialog(){
        ResetPasswordDialog fragment = new ResetPasswordDialog();

        fragment.show(getFragmentManager(), ResetPasswordDialog.TAG);
    }

    public void showConfirmCodeDialog(int contactNumber){
        ConfirmCodeDialog fragment = new ConfirmCodeDialog();
        Bundle args = new Bundle();
        args.putInt("contactNumber", contactNumber);
        args.putInt("login", 1);
        fragment.setArguments(args);

        fragment.show(getFragmentManager(), ConfirmCodeDialog.TAG);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
     }

    private void showSnackBarMessage(String message) {
        if (getView() != null) {
            Snackbar.make(getView(),message,Snackbar.LENGTH_SHORT).show();
        }
    }
}
