package com.hyptastic.poratechai.fragments.job_feed.parent_student.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.adapters.job_feed.parent_student.PostedJobsAdapter;

/**
 * Created by ISHRAK on 3/16/2018.
 */

public class ConfirmDeleteJobDialog extends DialogFragment {

    public interface ConfirmDeleteJobListener{
        public void onPostedJobDeletePositive(int position, String jobId);
    }

    private ConfirmDeleteJobListener mListener;

    public static ConfirmDeleteJobDialog createNewInstance(PostedJobsAdapter postedJobsAdapter, int position, String jobId)
    {
        ConfirmDeleteJobDialog ConfirmDeleteJobDialog = new ConfirmDeleteJobDialog();

        Bundle args = new Bundle();
        args.putSerializable("adapter", postedJobsAdapter);
        args.putInt("position", position);
        args.putString("jobId", jobId);
        ConfirmDeleteJobDialog.setArguments(args);

        return ConfirmDeleteJobDialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        final PostedJobsAdapter postedJobsAdapter = (PostedJobsAdapter)getArguments().getSerializable("adapter");
        final int position = getArguments().getInt("position");
        final String jobId = getArguments().getString("jobId");

        try{
            mListener = (ConfirmDeleteJobListener)postedJobsAdapter;
        }catch(ClassCastException e){
            throw new ClassCastException(postedJobsAdapter.toString() + " must implement ConfirmDeleteJobListener");
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.job_deletion_confirmation)
                .setTitle(R.string.job_deletion_title)
                .setPositiveButton(R.string.job_deletion_positive, new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mListener.onPostedJobDeletePositive(position, jobId);
                    }
                })
                .setNegativeButton(R.string.job_deletion_negative, new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

        return builder.create();
    }
}

