package com.hyptastic.poratechai.fragments.job_feed.tutor;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.SpinKitView;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.CubeGrid;
import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.activities.PorateChaiActivity;
import com.hyptastic.poratechai.adapters.job_feed.tutor.MatchesAdapter;
import com.hyptastic.poratechai.models.job_feed.Contract;
import com.hyptastic.poratechai.models.wallet.v2.UpdateResponseV2;
import com.hyptastic.poratechai.models.wallet.v2.WalletConstantsV2;
import com.hyptastic.poratechai.retrofit.functions.ContractFunction;
import com.hyptastic.poratechai.retrofit.functions.SystemFunction;
import com.hyptastic.poratechai.retrofit.functions.WalletFunction;
import com.hyptastic.poratechai.retrofit.interfaces.ContractClient;
import com.hyptastic.poratechai.retrofit.interfaces.SystemClient;
import com.hyptastic.poratechai.retrofit.interfaces.WalletClient;
import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
import com.hyptastic.poratechai.utils.Constants;

/**
 * Created by ISHRAK on 3/25/2018.
 */

public class ContractFragment extends DialogFragment {
    private SeekBar salarySeekBar, daysPerWeekSeekBar;
    private TextView salaryValueTextView, daysPerWeekValueTextView;
    private EditText messageEditText;
    private TextView validityValueTextView;
    private ImageButton walletButton;
    private ImageButton cancelButton, sendContractButton;

    private SpinKitView validitySpinKitView;
    private Sprite validitySpinKitSprite;

    private WalletFunction walletFunction;
    private ContractFunction contractFunction;
    private SystemFunction systemFunction;

    private State state = null;

    private MatchesAdapter currentMatchesAdapterInstance = null;

    private int validityLeft = -1;

    public interface ContractFragmentCloseListener {
        public void handleContractFragmentClose(DialogInterface dialog);
    }

    private class State
    {
        public String matchId = "";
        public int postedSalary = 0;
        public int daysPerWeek = 0;

        public int interval = 500;
        public int currentSalarySeekBarValueScaled = 0;
        public int currentSalarySeekBarValue = 0;

        public int currentDaysPerWeekSeekBarValue = 0;

        public int scale(int currentPosition)
        {
            return (postedSalary-1000) + currentPosition;
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        contractFunction = new ContractFunction(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, ContractClient.class), getActivity());
        walletFunction = new WalletFunction(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, WalletClient.class), getActivity());
        systemFunction = new SystemFunction(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, SystemClient.class), getActivity());

        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();

        final Dialog dialog = getDialog();
        if(dialog!=null)
        {
            dialog.setContentView(R.layout.dialog_contract);
            dialog.getWindow().setLayout((int)(0.92*Constants.SCREEN_WIDTH), (int)(0.92*Constants.SCREEN_HEIGHT));
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.background_listitem_job_tutor_profile);

            initLayout(this, dialog);
        }
    }

    public void initLayout(final ContractFragment currentContractFragment, final Dialog dialog)
    {
        state = new State();

        //Setting state object values
        currentMatchesAdapterInstance = (MatchesAdapter)getArguments().getSerializable("MatchesAdapter");
        state.matchId = getArguments().getString("MatchId");
        state.postedSalary = getArguments().getInt("Salary");
        state.daysPerWeek = getArguments().getInt("DaysPerWeek");
        state.currentSalarySeekBarValue = 1000;
        state.currentSalarySeekBarValueScaled = state.postedSalary;
        state.currentDaysPerWeekSeekBarValue = state.daysPerWeek;

//        Toast.makeText(getContext(), state.matchId, Toast.LENGTH_LONG).show();

        salarySeekBar = dialog.findViewById(R.id.salarySeekBar);
        salaryValueTextView = dialog.findViewById(R.id.salaryValueTextView);
        daysPerWeekSeekBar = dialog.findViewById(R.id.daysPerWeekSeekBar);
        daysPerWeekValueTextView = dialog.findViewById(R.id.daysPerWeekValueTextView);
        messageEditText = dialog.findViewById(R.id.messageEditText);
        validityValueTextView = dialog.findViewById(R.id.validityValueTextView);
        walletButton = dialog.findViewById(R.id.walletButton);
        cancelButton = dialog.findViewById(R.id.cancelContractButton);
        sendContractButton = dialog.findViewById(R.id.sendContractButton);

        //SpinKitViews
        validitySpinKitView = dialog.findViewById(R.id.validityValueSpinKitView);
        validitySpinKitSprite = new CubeGrid();
        validitySpinKitView.setIndeterminateDrawable(validitySpinKitSprite);

        //Setting textviews initially
        salaryValueTextView.setText(String.valueOf(state.postedSalary) + " Tk.");
        daysPerWeekValueTextView.setText(String.valueOf(state.daysPerWeek) + " days");

        //Salary SeekBar
        salarySeekBar.setMax(2000);
        salarySeekBar.setProgress(state.currentSalarySeekBarValue);
        salarySeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                state.currentSalarySeekBarValue = i;
                state.currentSalarySeekBarValueScaled = state.scale(i);

                salaryValueTextView.setText(String.valueOf(state.currentSalarySeekBarValueScaled) + " Tk.");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int snapValue = Math.round(((float)state.currentSalarySeekBarValue /state.interval))*state.interval;
                salarySeekBar.setProgress(snapValue);

                state.currentSalarySeekBarValueScaled = state.scale(snapValue);
                salaryValueTextView.setText(String.valueOf(state.currentSalarySeekBarValueScaled) + " Tk.");
            }
        });

        //DaysPerWeek SeekBar
        daysPerWeekSeekBar.setMax(6);
        daysPerWeekSeekBar.setProgress(state.currentDaysPerWeekSeekBarValue-1); //-1 because, seekBar progress is 0 based
        daysPerWeekSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
        {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                daysPerWeekValueTextView.setText(String.valueOf(i+1) + " days");
                state.currentDaysPerWeekSeekBarValue = i+1; //+1 because, seekBar progress is 0 based
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        //DialogBalance Section

        systemFunction.getSystemWalletConstantsV2(this);
        walletFunction.updateWalletV2(this);

        walletButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                dismissAllowingStateLoss();

                ((PorateChaiActivity)getActivity()).switchPager(3);
            }
        });

        sendContractButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if(validityLeft==-1){
                    Toast.makeText(getActivity(), "Could not connect. Please try again.", Toast.LENGTH_LONG).show();
                }else if(validityLeft==0){
                    Toast.makeText(getActivity(), "Please subscribe for sending contract offer", Toast.LENGTH_LONG).show();
                }else{
                    Contract newContract = new Contract(state.currentDaysPerWeekSeekBarValue, state.currentSalarySeekBarValueScaled, messageEditText.getText().toString());
                    contractFunction.sendContractReviewRequest(currentContractFragment, state.matchId, newContract);
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    //Called in response to getSystemWalletConstants
    public void walletConstantsGotten(WalletConstantsV2 walletConstants){

    }

    public void balanceUpdated(UpdateResponseV2 updateResponse){
        validityLeft = updateResponse.getValidityInDays();

        if(updateResponse.getValidityInDays()==0){
            validityValueTextView.setTextColor(ContextCompat.getColor(getActivity(), R.color.pink));
        }else{
            validityValueTextView.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
        }

        validitySpinKitSprite.stop();
        validitySpinKitView.setVisibility(View.INVISIBLE);

        validityValueTextView.setText(String.valueOf(updateResponse.getValidityInDays()));
    }

    //DismissDialog function is called by the sendContractRequest function in ContractFunction, after successful
    //posting of Contract Request.
    public void dismissDialog()
    {
        currentMatchesAdapterInstance.handleContractFragmentClose(getDialog());

        dismiss();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

        RetrofitServiceGenerator.cancelRequests();

        currentMatchesAdapterInstance.handleContractFragmentClose(dialog);
    }

    @Override
    public void onPause() {
        super.onPause();
        dismiss();
    }
}
