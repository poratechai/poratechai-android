//package com.hyptastic.poratechai.fragments.profile.parent_student;
//
//
//import android.app.FragmentTransaction;
//import android.os.Bundle;
//import android.app.Fragment;
//import android.support.design.widget.TextInputLayout;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.EditText;
//
//import com.hyptastic.poratechai.activities.profile.ProfileActivity;
//import com.hyptastic.poratechai.R;
//import com.hyptastic.poratechai.models.users.User;
//import com.hyptastic.poratechai.retrofit.functions.UserFunctions;
//import com.hyptastic.poratechai.retrofit.interfaces.UserClient;
//import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
//import com.hyptastic.poratechai.utils.Constants;
//
///**
// * A simple {@link Fragment} subclass.
// */
//public class EditProfileFragment extends Fragment {
//    public static final String TAG = EditProfileFragment.class.getSimpleName();
//
//    UserClient userClient = null;
//    UserFunctions userFunctions = null;
//
//    private EditText mEtFullName;
//    private TextInputLayout mTiFullName;
//    private Button saveBtn;
//
//    private String _id = "";
//    private String fullName = "";
//    private String profilePicture = "";
//
//    private int contactNumber;
//
//
//    public EditProfileFragment() {
//        // Required empty public constructor
//    }
//
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        // Inflate the layout for this fragment
//        View view = inflater.inflate(R.layout.fragment_parent_student_edit_profile, container, false);
//
//        initViews(view);
//
//        getProfile();
//
//        return view;
//    }
//
//    private void initViews(View v){
//        mEtFullName = (EditText) v.findViewById(R.id.et_full_name);
//        mTiFullName = (TextInputLayout)v.findViewById(R.id.ti_full_name);
//        saveBtn = (Button) v.findViewById(R.id.btn_save_parent_student_profile);
//
//        saveBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                editStudentTutorProfile();
//            }
//        });
//    }
//
//    private void getProfile(){
////        userClient = RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE,Constants.TOKEN_VALUE,UserClient.class);
////        userFunctions = new UserFunctions(userClient,getActivity().getApplicationContext());
////        userFunctions.getParentStudentProfileForEdit(this);
//        Bundle data = this.getArguments();
//        if(data != null){
//            _id = data.getString("_id");
//            fullName = data.getString("fullName");
//        }
//        setParentStudentProfileView();
//    }
//
//    public void setParentStudentProfileView(){
//        mEtFullName.setText(fullName);
//    }
//
//    public void editStudentTutorProfile(){
//        userClient = RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE,Constants.TOKEN_VALUE,UserClient.class);
//        userFunctions = new UserFunctions(userClient,getActivity().getApplicationContext());
//        fullName = mEtFullName.getText().toString();
//        User user = new User(fullName);
//        userFunctions.editParentStudentProfile(this,user);
//    }
//
//    public void goToViewProfile(){
////        ((ProfileActivity)getActivity()).setFullNameTextView(fullName);
//        FragmentTransaction ft = getFragmentManager().beginTransaction();
//        ViewProfileFragment fragment = new ViewProfileFragment();
//        ft.replace(R.id.fragmentFrame,fragment,ViewProfileFragment.TAG);
//        ft.commit();
//    }
//
//}
