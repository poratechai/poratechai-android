package com.hyptastic.poratechai.fragments.payment;

import com.hyptastic.poratechai.models.wallet.InvoiceResponse;
import com.hyptastic.poratechai.models.wallet.UpdateResponse;
import com.hyptastic.poratechai.models.wallet.WalletConstants;

import java.io.Serializable;

/**
 * Created by ISHRAK on 8/15/2018.
 */

public interface PaymentInterface extends Serializable {
    void createInvoice(double paymentAmount, double productAmount);

    void updateWallet();

    void getInvoice(String invoiceNumber);

    //loadInvoice is called after response from getInvoice
    void loadInvoice(InvoiceResponse invoiceResponse);

    //refreshBalance is called after response from updateWallet
    void refreshBalance(UpdateResponse updateResponse);

    void getWalletConstants();

    void walletConstantsGotten(WalletConstants walletConstants);
}
