package com.hyptastic.poratechai.fragments.login_registration;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.JavascriptInterface;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

import com.github.ybq.android.spinkit.SpinKitView;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.CubeGrid;
import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.retrofit.functions.SystemFunction;
import com.hyptastic.poratechai.retrofit.interfaces.SystemClient;
import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
import com.hyptastic.poratechai.utils.Constants;

public class TermsAndConditionsDialog extends DialogFragment {
    public interface Listener {
        public void accepted();
        public void declined();
        public void close();
    }

    public static final String TAG = TermsAndConditionsDialog.class.getSimpleName();

    Button acceptButton, declineButton;

    SpinKitView termsAndConditionsSpinKitView;
    Sprite termsAndConditionsSprite;

    WebView webView;

    public static TermsAndConditionsDialog createTermsAndConditionsDialog(RegisterFragment registerFragment){
        TermsAndConditionsDialog termsAndConditionsDialog = new TermsAndConditionsDialog();

        Bundle extras = new Bundle();
        extras.putSerializable("RegisterFragment", registerFragment);

        termsAndConditionsDialog.setArguments(extras);

        return termsAndConditionsDialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);

        View view = inflater.inflate(R.layout.dialog_terms_and_conditions,container,false);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        final Dialog dialog = getDialog();

        if(dialog!=null)
        {
            dialog.getWindow().setLayout((int)(0.95* Constants.SCREEN_WIDTH), (int)(0.88*Constants.SCREEN_HEIGHT));
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.background_listitem_job_tutor_profile);

            initLayout(this, dialog);
        }
    }

    private void initLayout(TermsAndConditionsDialog currentTermsAndConditionsDialog, Dialog dialog) {
        webView = dialog.findViewById(R.id.privacyPolicyWebView);
        acceptButton = dialog.findViewById(R.id.acceptButton);
        declineButton = dialog.findViewById(R.id.declineButton);

        termsAndConditionsSpinKitView = dialog.findViewById(R.id.termsAndConditionsSpinKitView);
        termsAndConditionsSprite = new CubeGrid();
        termsAndConditionsSpinKitView.setIndeterminateDrawable(termsAndConditionsSprite);

        webView.setVisibility(View.INVISIBLE);

        acceptButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                ((Listener)getArguments().getSerializable("RegisterFragment")).accepted();
            }
        });

        declineButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                ((Listener)getArguments().getSerializable("RegisterFragment")).declined();
            }
        });

        showTermsAndConditions();
    }

    public void showTermsAndConditions(){
        String whiteList = "https://www.poratechai.com/terms_app";

        WebViewClient webViewClient = new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                if(!view.getUrl().equals(whiteList)){
                    return true;
                }
                return false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                webView.setVisibility(View.VISIBLE);

                termsAndConditionsSpinKitView.setVisibility(View.INVISIBLE);
                termsAndConditionsSprite.stop();
            }
        };

        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(webViewClient);
        webView.loadUrl(whiteList);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onPause() {
        super.onPause();
        ((Listener)getArguments().getSerializable("RegisterFragment")).close();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
