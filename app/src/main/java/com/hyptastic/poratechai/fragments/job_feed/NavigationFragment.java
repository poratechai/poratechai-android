package com.hyptastic.poratechai.fragments.job_feed;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.adapters.job_feed.parent_student.ParentStudentPagerAdapter;
import com.hyptastic.poratechai.adapters.job_feed.tutor.TutorPagerAdapter;
import com.hyptastic.poratechai.fragments.PorateChaiNavigationFragment;
import com.hyptastic.poratechai.utils.Constants;
import com.hyptastic.poratechai.utils.Functions;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by ISHRAK on 2/13/2019.
 */

public class NavigationFragment extends PorateChaiNavigationFragment {
    ViewPager jobFeedViewPager = null;
    TabLayout jobFeedTabLayout = null;

    ParentStudentPagerAdapter parentStudentPagerAdapter = null;
    TutorPagerAdapter tutorPagerAdapter = null;

    ArrayList<JobFeedFragment> parentStudentFragments = null;
    String parentStudentPageTitles[] = null;
    ArrayList<JobFeedFragment> tutorFragments = null;
    String tutorPageTitles[] = null;

    NavigationFragment currentNavigationFragment;

    public static NavigationFragment createInstance(int feedType){
        NavigationFragment navigationFragment = new NavigationFragment();

        Bundle bundle = new Bundle();
        bundle.putInt("feedType", feedType);

        navigationFragment.setArguments(bundle);

        return navigationFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_navigation_job_feed, null);

        currentNavigationFragment = this;

        jobFeedViewPager = (ViewPager)view.findViewById(R.id.job_feed_pager);
        jobFeedTabLayout = (TabLayout)view.findViewById(R.id.job_feed_tabs);

        tutorFragments = new ArrayList<>(
                Arrays.asList(
                        new com.hyptastic.poratechai.fragments.job_feed.tutor.MatchesFragment(),
                        new com.hyptastic.poratechai.fragments.job_feed.tutor.AppliedFragment(),
                        new com.hyptastic.poratechai.fragments.job_feed.tutor.ActiveJobsFragment(),
                        new com.hyptastic.poratechai.fragments.job_feed.tutor.JobHistoryFragment()));
        tutorPageTitles = new String[]{"Matches", "Applied", "Active", "History"};

        parentStudentFragments = new ArrayList<>(
                Arrays.asList(
                        new com.hyptastic.poratechai.fragments.job_feed.parent_student.PostedJobsFragment(),
                        new com.hyptastic.poratechai.fragments.job_feed.parent_student.ActiveJobsFragment(),
                        new com.hyptastic.poratechai.fragments.job_feed.parent_student.JobHistoryFragment()));
        parentStudentPageTitles = new String[]{"Jobs", "Active", "History"};

        tutorPagerAdapter = new TutorPagerAdapter(getActivity().getSupportFragmentManager(), tutorFragments, tutorPageTitles);
        parentStudentPagerAdapter = new ParentStudentPagerAdapter(getActivity().getSupportFragmentManager(), parentStudentFragments, parentStudentPageTitles);

        loadFragment();

        return view;
    }

    private class ViewPagerListener implements ViewPager.OnPageChangeListener
    {
        NavigationFragment navigationFragment;

        public ViewPagerListener(NavigationFragment navigationFragment) {
            this.navigationFragment = navigationFragment;
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }

    private int getFragmentNumberFromNotification(){
        try{
            int feedType = (int)(getArguments().getInt("feedType"));
            int fragment = 0;

            switch(feedType){
                case Constants.NOTIFICATION_DATA_FEED_TYPE.POSTED_JOB:
                    fragment = 0;
                    break;
                case Constants.NOTIFICATION_DATA_FEED_TYPE.ACTIVE_POSTED_JOB:
                    fragment = 1;
                    break;
                case Constants.NOTIFICATION_DATA_FEED_TYPE.POSTED_JOB_HISTORY:
                    fragment = 2;
                    break;
                case Constants.NOTIFICATION_DATA_FEED_TYPE.MATCH:
                    fragment = 0;
                    break;
                case Constants.NOTIFICATION_DATA_FEED_TYPE.ACTIVE_JOB:
                    fragment = 1;
                    break;
                case Constants.NOTIFICATION_DATA_FEED_TYPE.JOB_HISTORY:
                    fragment = 2;
                    break;
            }
            return fragment;
        }catch(NullPointerException e){
            Log.e("JobFeedException", "No extra in job feed intent");
        }
        return 0;
    }

    @Override
    public void loadFragment(){
        jobFeedViewPager.setAdapter(null);

        if(Functions.isTutor()) {
            jobFeedViewPager.setAdapter(tutorPagerAdapter);
        } else {
            jobFeedViewPager.setAdapter(parentStudentPagerAdapter);
        }

        jobFeedViewPager.addOnPageChangeListener(new ViewPagerListener(this));

        //From notifications dispatch in MyFirebaseMessagingService
        jobFeedViewPager.setCurrentItem(getFragmentNumberFromNotification());

        jobFeedTabLayout.setupWithViewPager(jobFeedViewPager);
    }
}
