package com.hyptastic.poratechai.fragments.job_feed.parent_student;

import android.view.View;

import com.hyptastic.poratechai.fragments.multi_select.MultiSelectDialogFragment;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ISHRAK on 3/12/2019.
 */

public class DialogFragmentSubjects extends MultiSelectDialogFragment {
    public static interface DialogFragmentSubjectsCloseListener{
        public void dialogFragmentSubjectsClosed();
    }

    public static DialogFragmentSubjects createInstance(HashMap<String, String[]> dictionary, ArrayList<String> selectedItems){
        DialogFragmentSubjects dialogFragmentSubjects = new DialogFragmentSubjects();

        createInstance(dialogFragmentSubjects, dictionary, selectedItems);

        return dialogFragmentSubjects;
    }

    @Override
    public void onStart() {
        super.onStart();

        confirmButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                ((DialogFragmentSubjectsCloseListener)getActivity()).dialogFragmentSubjectsClosed();
                dismiss();
            }
        });
    }
}
