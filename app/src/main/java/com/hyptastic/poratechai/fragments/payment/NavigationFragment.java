package com.hyptastic.poratechai.fragments.payment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.fragments.PorateChaiNavigationFragment;
import com.hyptastic.poratechai.fragments.payment.parent_student.ParentStudentPaymentFragment;
import com.hyptastic.poratechai.fragments.payment.tutor.InvoiceFragment;
import com.hyptastic.poratechai.fragments.payment.tutor.PaymentChoiceFragment;
import com.hyptastic.poratechai.fragments.payment.tutor.PaymentProcessingFragment;
import com.hyptastic.poratechai.utils.Functions;

/**
 * Created by ISHRAK on 6/25/2018.
 */

public class NavigationFragment extends PorateChaiNavigationFragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_navigation_payment, container, false);

        loadFragment();

        return view;
    }

    @Override
    public void loadFragment() {
        try{
            if(Functions.isTutor()){
                PaymentChoiceFragment tutorFragment = new PaymentChoiceFragment();

                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.paymentFragment, tutorFragment);

                fragmentTransaction.commit();
            }else{
                ParentStudentPaymentFragment parentStudentFragment = new ParentStudentPaymentFragment();

                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.paymentFragment, parentStudentFragment);

                fragmentTransaction.commit();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
