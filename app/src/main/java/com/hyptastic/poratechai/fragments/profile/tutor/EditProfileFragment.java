//package com.hyptastic.poratechai.fragments.profile.tutor;
//
//
//import android.app.FragmentTransaction;
//import android.content.Intent;
//import android.os.Bundle;
//import android.app.Fragment;
//import android.app.FragmentManager;
//import android.text.Editable;
//import android.text.TextWatcher;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.AdapterView;
//import android.widget.ArrayAdapter;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.Spinner;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.hyptastic.poratechai.activities.profile.ProfileActivity;
//import com.hyptastic.poratechai.R;
//import com.hyptastic.poratechai.activities.profile.TutorMapActivity;
//import com.hyptastic.poratechai.fragments.multi_select.MultiSelectDialogFragment;
//import com.hyptastic.poratechai.models.users.User;
//import com.hyptastic.poratechai.retrofit.functions.UserFunctions;
//import com.hyptastic.poratechai.retrofit.interfaces.UserClient;
//import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
//import com.hyptastic.poratechai.utils.Constants;
//import com.hyptastic.poratechai.utils.Functions;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Map;
//import java.util.Set;
//
//import io.apptik.widget.multiselectspinner.MultiSelectSpinner;
//
//import static android.app.Activity.RESULT_OK;
//
///**
// * A simple {@link Fragment} subclass.
// */
//public class EditProfileFragment extends Fragment {
//    public static final String TAG = "EditProfileFragment";
//
//    UserClient userClient = null;
//    UserFunctions userFunctions = null;
//
//    private static Double radius;
//    private static Double lat;
//    private static Double lon;
//
//    private EditText mEtFullName;
//    private EditText mEtSalary;
//    private EditText mEtSchool;
//    private EditText mEtCollege;
//    private EditText mEtUniversity;
//    private TextView mTvLocation;
//    private TextView skillsSpinnerTextView;
//    private Button enterLocationBtn;
//    private Button saveBtn;
//
//    static final int PICK_RADIUS_REQUEST = 1;
//
//    private String _id = "";
//    private String fullName = "";
//    private String minimumPreferredSalaryString;
//    private String address;
//
//    private int minimumPreferredSalary;
//    private int daysPerWeek=0;
//
//    private String[] institution = new String[3];
//
//    Spinner daysPerWeekSpinner;
//
//    private ArrayList<String> skillsList = new ArrayList<>();
//    private ArrayList<String> subjectsList = new ArrayList<>();
//    private ArrayList<String> classToTeachList = new ArrayList<>();
//
//    public EditProfileFragment() {
//        // Required empty public constructor
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        // Inflate the layout for this fragment
//        View view = inflater.inflate(R.layout.fragment_tutor_edit_profile, container, false);
//
//        if(getArguments() == null){
//            System.out.println("NULL");
//        }
//
//        initViews(view);
//
//        return view;
//    }
//
//    private void initViews(View v){
//        mEtFullName = (EditText) v.findViewById(R.id.et_full_name);
//        mEtSalary = (EditText) v.findViewById(R.id.et_salary);
//        mEtSchool = (EditText) v.findViewById(R.id.et_school);
//        mEtCollege = (EditText) v.findViewById(R.id.et_college);
//        mEtUniversity = (EditText) v.findViewById(R.id.et_university);
//        mTvLocation = (TextView) v.findViewById(R.id.tv_location);
//
//        skillsSpinnerTextView = (TextView) v.findViewById(R.id.skillsSpinnerTextView);
//        skillsSpinnerTextView.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View view) {
////                MultiSelectDialogFragment multiSelectDialogFragment = MultiSelectDialogFragment.createInstance(Constants.SYSTEM_ARRAY.subcategories, skillsList);
////                multiSelectDialogFragment.show(getChildFragmentManager(), "Select Skills");
//            }
//        });
//
//        daysPerWeekSpinner = (Spinner) v.findViewById(R.id.daysPerWeekSpinner);
//        enterLocationBtn = (Button) v.findViewById(R.id.enterLocationButton);
//        saveBtn = (Button) v.findViewById(R.id.btn_save_tutor_profile);
//
//        mEtFullName.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                if(mEtFullName.getText().toString().equals("")){
//                    mEtFullName.setError("Enter Fullname");
//                }
//                else{
//                    mEtFullName.setError(null);
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });
//
//        mEtSalary.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                if (!mEtSalary.getText().toString().matches("[0-9]+")) {
//                    mEtSalary.setError("Please enter a valid number");
//                }
//                else{
//                    mEtSalary.setError(null);
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });
//
//        getProfile();
//
//        ArrayAdapter<CharSequence> daysPerWeekSpinnerAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.days_per_week_array, android.R.layout.simple_spinner_item);
//        daysPerWeekSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        daysPerWeekSpinner.setAdapter(daysPerWeekSpinnerAdapter);
//
//        if (daysPerWeek != 0) {
//            daysPerWeekSpinner.setSelection(daysPerWeek-1);
//        }
//
//        daysPerWeekSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                daysPerWeek = i+1;
//                daysPerWeekSpinner.setSelection(i);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });
//
//        enterLocationBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(getActivity().getApplicationContext(), TutorMapActivity.class);
//                startActivityForResult(intent,PICK_RADIUS_REQUEST);
//            }
//        });
//
//        saveBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                int err = 0;
//
//                if(mEtFullName.getText().toString().equals("")){
//                    err++;
//                    mEtFullName.setError("Enter a title");
//                    Toast.makeText(getActivity().getApplicationContext(), "Fill out the empty fields", Toast.LENGTH_SHORT).show();
//                }
//                else{
//                    fullName = mEtFullName.getText().toString();
//                }
//
//                if(mEtSalary.getText().toString().equals("")){
//                    err++;
//                    mEtSalary.setError("Enter Salary");
//                    Toast.makeText(getActivity().getApplicationContext(), "Fill out the empty fields", Toast.LENGTH_SHORT).show();
//                }
//                else{
//                    minimumPreferredSalary = Integer.parseInt(mEtSalary.getText().toString());
//                }
//
//                if(mEtSchool.getText().toString().equals("") && mEtCollege.getText().toString().equals("") && mEtUniversity.getText().toString().equals("")){
//                    err++;
//                    Toast.makeText(getActivity().getApplicationContext(), "Enter atleast one institution", Toast.LENGTH_SHORT).show();
//                }
//                else{
//                    if(!mEtSchool.getText().toString().isEmpty()){
//                        institution[0] = mEtSchool.getText().toString();
//                    }
//                    else{
//                        institution[0] = "";
//                    }
//
//                    if(!mEtCollege.getText().toString().isEmpty()){
//                        institution[1] = mEtCollege.getText().toString();
//                    }
//                    else{
//                        institution[1] = "";
//                    }
//
//                    if(!mEtUniversity.getText().toString().isEmpty()){
//                        institution[2] = mEtUniversity.getText().toString();
//                    }
//                    else{
//                        institution[2] = "";
//                    }
//
//                    if(!institution[2].isEmpty()){
//                        Functions.storeInstitutionToSharedPreferences(institution[2]);
//                    }
//                    else if(!institution[1].isEmpty()){
//                        Functions.storeInstitutionToSharedPreferences(institution[1]);
//                    }
//                    else {
//                        Functions.storeInstitutionToSharedPreferences(institution[0]);
//                    }
//                }
//
//                if(daysPerWeek == 0){
//                    err++;
//                    Toast.makeText(getActivity().getApplicationContext(), "Fill out the empty fields", Toast.LENGTH_SHORT).show();
//                }
//
////                fullName = mEtFullName.getText().toString();
////                contactNumber = Integer.parseInt(mEtContactNumber.getText().toString());
////                minimumPreferredSalary = Integer.parseInt(mEtSalary.getText().toString());
//
//                if(err == 0){
//                    editTutorProfile();
//                }
//
//            }
//        });
//    }
//
//    private void getProfile(){
//        Bundle data = this.getArguments();
//        if(data != null){
//            _id = data.getString("_id");
//            fullName = data.getString("fullName");
//            minimumPreferredSalaryString = data.getString("minimumSalary");
//            daysPerWeek = Integer.parseInt(data.getString("daysPerWeek"));
//
//            if(data.getString("school") != null){
//                institution[0] = data.getString("school");
//            }
//            if(data.getString("college") != null){
//                institution[1] = data.getString("college");
//            }
//            if(data.getString("university") != null){
//                institution[2] = data.getString("university");
//            }
//            if(data.getStringArrayList("subjects") != null){
//                subjectsList = data.getStringArrayList("subjects");
//            }
//            if(data.getStringArrayList("skills") != null){
//                skillsList = data.getStringArrayList("skills");
//            }
//            if(data.getStringArrayList("classToTeach") != null){
//                classToTeachList = data.getStringArrayList("classToTeach");
//            }
//
//            if(data.getDouble("latitude") != 0){
//                lat = data.getDouble("latitude");
//            }
//            if(data.getDouble("longitude") != 0){
//                lon = data.getDouble("longitude");
//            }
//            if(data.getDouble("radius") != 0){
//                radius = data.getDouble("radius");
//            }
//        }
//        setTutorProfileView();
//    }
//
//    public void setTutorProfileView(){
//        mEtFullName.setText(fullName);
//        mEtSalary.setText(minimumPreferredSalaryString);
//        mEtSchool.setText(institution[0]);
//        mEtCollege.setText(institution[1]);
//        mEtUniversity.setText(institution[2]);
//    }
//
//    public void editTutorProfile(){
//        userClient = RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE,Constants.TOKEN_VALUE,UserClient.class);
//        userFunctions = new UserFunctions(userClient,getActivity().getApplicationContext());
//
//        String[] skills = new String[skillsList.size()];
//        String[] subjects = new String[subjectsList.size()];
//        String[] classToTeach = new String[classToTeachList.size()];
//
//        skillsList.toArray(skills);
//        subjectsList.toArray(subjects);
//        classToTeachList.toArray(classToTeach);
//
////        User user = new User(_id, fullName,"true", minimumPreferredSalary, institution, classToTeach, skills, subjects, daysPerWeek, lat, lon, radius, "");
////        userFunctions.editTutorProfile(this,user);
//    }
//
////    public void goToViewProfile(){
////        ((ProfileActivity)getActivity()).setFullNameTextView(fullName);
////        if(!institution[2].isEmpty()){
////            ((ProfileActivity)getActivity()).setInstitutionTextView(institution[2]);
////        }
////        else if(!institution[1].isEmpty()){
////            ((ProfileActivity)getActivity()).setInstitutionTextView(institution[1]);
////        }
////        else {
////            ((ProfileActivity)getActivity()).setInstitutionTextView(institution[0]);
////        }
////        FragmentTransaction ft = getFragmentManager().beginTransaction();
////        ViewProfileFragment fragment = new ViewProfileFragment();
////        ft.replace(R.id.fragmentFrame,fragment,ViewProfileFragment.TAG);
////        ft.commit();
////    }
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == PICK_RADIUS_REQUEST) {
//            // Make sure the request was successful
//            if (resultCode == RESULT_OK) {
//                 radius = data.getDoubleExtra("radius",0);
//                 lat = data.getDoubleExtra("latitude",0);
//                 lon = data.getDoubleExtra("longitude",0);
//                 address = data.getStringExtra("address");
//
//                 mTvLocation.setText(address);
//                 Toast.makeText(getActivity().getApplicationContext(), "Point Chosen: " + radius.toString() + "Lat: " + lat.toString() + "Lon: " + lon.toString(), Toast.LENGTH_LONG).show();
//            }
//        }
//    }
//
//    public void tutorProfileEdited(){
//        Functions.setTutorUpdated("true");
//    }
//}
