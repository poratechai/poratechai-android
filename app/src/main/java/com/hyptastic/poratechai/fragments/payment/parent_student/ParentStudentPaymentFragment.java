package com.hyptastic.poratechai.fragments.payment.parent_student;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hyptastic.poratechai.R;

/**
 * Created by ISHRAK on 4/2/2019.
 */

public class ParentStudentPaymentFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment_parent_student, container, false);

        return view;
    }
}
