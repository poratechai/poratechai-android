//package com.hyptastic.projectxandroid.fragments.payment.payment_ui_for_profile;
//
//import android.support.v7.app.AppCompatActivity;
//import android.util.Log;
//import android.view.View;
//import android.widget.Button;
//import android.widget.RelativeLayout;
//import android.widget.Toast;
//
//import com.hyptastic.poratechai.R;
//import com.hyptastic.poratechai.fragments.payment.BuyContractsDialogFragment;
//import com.hyptastic.poratechai.fragments.payment.PaymentInterface;
//import com.hyptastic.poratechai.models.wallet.InvoiceRequest;
//import com.hyptastic.poratechai.models.wallet.InvoiceResponse;
//import com.hyptastic.poratechai.models.wallet.UpdateResponse;
//import com.hyptastic.poratechai.models.wallet.WalletConstants;
//import com.hyptastic.poratechai.models.wallet.v2.UpdateResponseV2;
//import com.hyptastic.poratechai.models.wallet.v2.WalletConstantsV2;
//import com.hyptastic.poratechai.retrofit.functions.SystemFunction;
//import com.hyptastic.poratechai.retrofit.functions.WalletFunction;
//import com.hyptastic.poratechai.retrofit.interfaces.WalletClient;
//import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
//import com.hyptastic.poratechai.utils.Constants;
//import com.hyptastic.poratechai.utils.Functions;
//
///**
// * Created by ISHRAK on 8/15/2018.
// */
//
//public class PaymentSubscriptionSystem implements PaymentInterface, BuyContractsDialogFragment.CloseListener {
//    private RelativeLayout paymentSection;
//    private Button subscriptionButtonOne, subscriptionButtonThree;
//
//    private int subscriptionMonths = 0;
//    private double paymentAmount = 0;
//
//    private WalletFunction walletFunction = null;
//    private SystemFunction systemFunction = null;
//
//    private InvoiceResponse invoiceResponse;
//
//    private WalletConstantsV2 walletConstants;
//
//    private boolean pending = false;
//
//    AppCompatActivity profileActivity;
//
//    public PaymentSubscriptionSystem(AppCompatActivity profileActivity, SystemFunction systemFunction){
//        this.profileActivity = profileActivity;
//        this.systemFunction = systemFunction;
//    }
//
//    public void loadPaymentSection()
//    {
//        walletFunction = new WalletFunction(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, WalletClient.class), profileActivity.getApplicationContext());
//
//        if(Functions.isTutor())
//        {
//            subscriptionButtonOne = (Button)profileActivity.findViewById(R.id.subscribeButtonOne);
//            subscriptionButtonThree = (Button)profileActivity.findViewById(R.id.subscribeButtonThree);
//
//            subscriptionButtonOne.setOnClickListener(new View.OnClickListener(){
//                @Override
//                public void onClick(View view) {
//                    createInvoice(walletConstants.getCOST_ONE_MONTH(), walletConstants.getCOST_ONE_MONTH());
//                }
//            });
//
//            subscriptionButtonThree.setOnClickListener(new View.OnClickListener(){
//                @Override
//                public void onClick(View view) {
//                    createInvoice(walletConstants.getCOST_THREE_MONTHS(), walletConstants.getDURATION_THREE_MONTHS());
//                }
//            });
//
//            updateWallet();
//            getWalletConstants();
//        }
//        else
//        {
//            paymentSection = profileActivity.findViewById(R.id.paymentSection);
//            paymentSection.getLayoutParams().height = 0;
//            paymentSection.setVisibility(View.INVISIBLE);
//        }
//    }
//    //bind button event calls to methods
//
//    public void createInvoice(double paymentAmount, double subscriptionMonths){
//        Toast.makeText(profileActivity.getApplicationContext(), "Loading Invoice", Toast.LENGTH_LONG).show();
//
//        InvoiceRequest invoiceRequest = new InvoiceRequest(String.valueOf(paymentAmount), "Subscription", String.valueOf(subscriptionMonths)+" x month(s)",
//                Constants.USER_NAME_VALUE, Constants.EMAIL_VALUE, "phone", "address", "city", "state", "zipcode", "BD", Constants.USER_ID_VALUE);
//
//        walletFunction.createInvoice(this, invoiceRequest);
//    }
//    //Done
//
//    public void updateWallet()
//    {
//        //SpinKitView
//
//        walletFunction.updateWalletV2(this, Constants.USER_ID_VALUE);
//    }
//    //Done
//
//    public void getInvoice(String invoiceNumber){
//        walletFunction.getInvoice(this, invoiceNumber);
//    }
//    //Done
//
//    //loadInvoice is called after response from getInvoice
//    public void loadInvoice(InvoiceResponse invoiceResponse){
//        this.invoiceResponse = invoiceResponse;
//
//        BuyContractsDialogFragment buyContractsDialogFragment = BuyContractsDialogFragment.createNewInstance(invoiceResponse, this);
//
//        buyContractsDialogFragment.show(profileActivity.getSupportFragmentManager(), "BuyContractsDialogFragment");
//    }
//    //Done
//
//    //refreshBalance is called after response from updateWallet
//    public void refreshBalance(UpdateResponse updateResponseV2)
//    {
//        UpdateResponseV2 updateResponse = (UpdateResponseV2)updateResponseV2;
//        try{
//            Log.e("Validity(days):", String.valueOf(updateResponse.getvalidityInDays()));
//            //update UI validity in days
//
//            if(updateResponse.getPendingInvoice().getData()==null){
//                //Update UI for not pending
//
//                pending = false;
//
//                invoiceResponse = null;
//            }else{
//                //Update UI for pending
//
//                pending = true;
//
//                invoiceResponse = updateResponse.getPendingInvoice();   //this pending invoiceResponse is not used for loading invoice
//                                                                        //because the backend will send the pending invoiceResponse
//                                                                        //in createInvoice call
//            }
//        }catch(Exception e){
//            Log.e("Balance Refresh", e.toString());
//        }
//    }
//    //Update UI
//
//    @Override
//    public void handleDialogClose() {
//        updateWallet();
//    }
//    //Done
//
//    public  void getWalletConstants(){
//        //SpinKitView - start
//
//        systemFunction.getSystemWalletConstantsV2(this);
//    }
//    //Done
//
//    public void walletConstantsGotten(WalletConstants walletConstants){
//        //SpinKitView - end
//
//        this.walletConstants = (WalletConstantsV2)walletConstants;
//    }
//    //Done
//}
