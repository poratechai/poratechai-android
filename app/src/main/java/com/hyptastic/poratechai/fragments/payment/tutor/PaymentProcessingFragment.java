package com.hyptastic.poratechai.fragments.payment.tutor;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageButton;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.fragments.payment.PortwalletWebViewClient;
import com.hyptastic.poratechai.models.wallet.InvoiceResponse;
import com.hyptastic.poratechai.retrofit.functions.WalletFunction;
import com.hyptastic.poratechai.retrofit.interfaces.WalletClient;
import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
import com.hyptastic.poratechai.utils.Constants;

/**
 * Created by ISHRAK on 6/25/2018.
 */

public class PaymentProcessingFragment extends Fragment {
    ImageButton browserBackButton;

    WebView paymentPortwalletWebView;
    InvoiceResponse invoiceResponse;

    WalletFunction walletFunction;

    public static PaymentProcessingFragment createNewInstance(InvoiceResponse invoiceResponse){
        PaymentProcessingFragment paymentProcessingFragment = new PaymentProcessingFragment();

        Bundle args = new Bundle();
        args.putParcelable("invoiceResponse", invoiceResponse);
        paymentProcessingFragment.setArguments(args);

        return paymentProcessingFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment_processing, container, false);

        try{
            walletFunction = new WalletFunction(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, WalletClient.class), getActivity());

            invoiceResponse = (InvoiceResponse)getArguments().getParcelable("invoiceResponse");

            paymentPortwalletWebView = view.findViewById(R.id.paymentPortwalletWebView);
            browserBackButton = view.findViewById(R.id.browserBackButton);

            PortwalletWebViewClient portWalletWebViewClient = new PortwalletWebViewClient(getActivity());
            paymentPortwalletWebView.setWebViewClient(portWalletWebViewClient);
            paymentPortwalletWebView.getSettings().setJavaScriptEnabled(true);

            paymentPortwalletWebView.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorLightAccent));
            //security-threat
            //paymentPortwalletWebView.addJavascriptInterface(new PortwalletWebViewJavascriptInterface((Context)getActivity()), "HtmlViewer");

            browserBackButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    getActivity().onBackPressed();
                }
            });

            String invoiceId = invoiceResponse.getData().getInvoiceId();
            paymentPortwalletWebView.loadUrl("https://payment-sandbox.portwallet.com/payment/?invoice=" + invoiceId);
        }catch(Exception e){
            Log.e("PaymentFragment", e.getMessage());
        }

        return view;
    }

    private void deleteInvoice(){
        walletFunction.deleteInvoice(this, invoiceResponse.getData().getInvoiceId());
    }

    public void invoiceDeleted(){
        //TODO Check
//        Intent intent = new Intent(getActivity(), PaymentActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(intent);
//        getActivity().finish();
    }

    public boolean canWebBrowserGoBack(){
        return paymentPortwalletWebView.canGoBack();
    }

    public void browserBack(){
        paymentPortwalletWebView.goBack();
    }
}
