package com.hyptastic.poratechai.fragments.profile;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcel;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.activities.authentication.AuthenticationActivity;
import com.hyptastic.poratechai.fragments.login_registration.ChangePasswordDialog;
import com.hyptastic.poratechai.retrofit.functions.SystemFunction;
import com.hyptastic.poratechai.retrofit.functions.UserFunctions;
import com.hyptastic.poratechai.retrofit.interfaces.SystemClient;
import com.hyptastic.poratechai.retrofit.interfaces.UserClient;
import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
import com.hyptastic.poratechai.utils.Constants;
import com.hyptastic.poratechai.utils.Functions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ISHRAK on 3/11/2019.
 */

public class ProfileFragment extends Fragment implements Serializable {
    ProfileFragment currentProfileFragment;

    int layoutResourceId = -1;
    protected TextView emailTextView, mobileNumberTextView;
    protected ImageButton verificationImagesButton;
    protected Button changePasswordButton, logoutButton;

    LinearLayout verificationImagesSection;

    protected SystemFunction systemFunction;
    protected UserFunctions userFunctions;

    List<String> verificationImages;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layoutResourceId = getArguments().getInt("LayoutResourceId");

        View view = LayoutInflater.from(getActivity()).inflate(layoutResourceId, null);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        currentProfileFragment = this;

        verificationImages = new ArrayList<String>();

        emailTextView               = view.findViewById(R.id.emailTextView);
        mobileNumberTextView        = view.findViewById(R.id.mobileNumberTextView);

        verificationImagesSection   = view.findViewById(R.id.verificationImagesSection);
        verificationImagesButton    = view.findViewById(R.id.verificationImagesButton);

        changePasswordButton        = view.findViewById(R.id.changePasswordButton);
        logoutButton                = view.findViewById(R.id.logoutButton);

        DialogFragmentVerificationImages.DialogFragmentVerificationImagesListener dialogFragmentVerificationImagesListener =
                new DialogFragmentVerificationImages.DialogFragmentVerificationImagesListener() {
                    @Override
                    public void dialogFragmentVerificationImagesClosed() {
                        getVerificationImages();
                    }

                    @Override
                    public int describeContents() { return 0; }

                    @Override
                    public void writeToParcel(Parcel parcel, int i) { }
                };

        verificationImagesButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                DialogFragmentVerificationImages dialogFragmentVerificationImages = DialogFragmentVerificationImages.createInstance(
                        dialogFragmentVerificationImagesListener, verificationImages);
                dialogFragmentVerificationImages.show(getActivity().getSupportFragmentManager(), "VerificationImages");
            }
        });

        changePasswordButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                ChangePasswordDialog changePasswordDialog = new ChangePasswordDialog();
                changePasswordDialog.show(getActivity().getSupportFragmentManager(), "ChangePassword");
            }
        });

        logoutButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                initiateLogout();
            }
        });

        systemFunction = new SystemFunction(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, SystemClient.class), getActivity());
        userFunctions = new UserFunctions(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, UserClient.class), getActivity());
    }

    public void setContactData(String email, String mobileNumber){
        emailTextView.setText(email);
        mobileNumberTextView.setText("0" + mobileNumber);
    }

    public void setContactData(String email, String mobileNumber, int status){
        if(status<Constants.JOB_STATUS.JOB_STATUS_INSPECTION){
            emailTextView.setText("Available after Accepting");
            emailTextView.setTextColor(Color.RED);

            mobileNumberTextView.setText("Available after Accepting");
            mobileNumberTextView.setTextColor(Color.RED);
        }else{
            emailTextView.setText(email);
            emailTextView.setTextColor(Color.BLACK);

            mobileNumberTextView.setText("0" + mobileNumber);
            mobileNumberTextView.setTextColor(Color.BLACK);
        }
    }

    //--------------------------------------------------------------------------------------------------------------------------------
    //Verification Images - start
    public void getVerificationImages(){
        userFunctions.getCredentialPictures(this);
    }

    public void setVerificationImagesGallery(List<String> credentialPictures){
        try{
            verificationImages = credentialPictures;

            verificationImagesSection.removeAllViews();

            for(String url: credentialPictures){
                CardView card = new CardView(getActivity());

                LinearLayout.LayoutParams cardLayoutParams = new LinearLayout.LayoutParams(128, 128);
                cardLayoutParams.setMargins(5, 5, 5, 5);
                card.setLayoutParams(cardLayoutParams);

                card.setRadius(5);
                card.setCardElevation(5);

                ImageView imageView = new ImageView(getActivity());
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(128, 128);
                imageView.setLayoutParams(layoutParams);

                Glide.with(getActivity()).load(url).into(imageView);

                card.addView(imageView);
                verificationImagesSection.addView(card);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    //Verification Images - end
    //--------------------------------------------------------------------------------------------------------------------------------


    //--------------------------------------------------------------------------------------------------------------------------------
    //Logout - start
    private void initiateLogout() {
        userFunctions.logoutUser(this);
    }

    public void logout(){
        Functions.clearLoginDataFromSharedPreferences();
        FirebaseAuth.getInstance().signOut();

        Intent intent = new Intent(getActivity(), AuthenticationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

        Toast.makeText(getActivity(), "Logged Out Successfully", Toast.LENGTH_LONG).show();
        getActivity().finish();
    }
    //Logout - end
    //--------------------------------------------------------------------------------------------------------------------------------
}
