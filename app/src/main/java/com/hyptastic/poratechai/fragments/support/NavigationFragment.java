package com.hyptastic.poratechai.fragments.support;

import android.Manifest;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.activities.PorateChaiActivity;
import com.hyptastic.poratechai.activities.support.AboutActivity;
import com.hyptastic.poratechai.fragments.PorateChaiNavigationFragment;
import com.hyptastic.poratechai.retrofit.functions.SystemFunction;
import com.hyptastic.poratechai.retrofit.interfaces.SystemClient;
import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
import com.hyptastic.poratechai.utils.Constants;

/**
 * Created by ISHRAK on 2/13/2019.
 */

public class NavigationFragment extends PorateChaiNavigationFragment {
    LinearLayout phoneNumber, email, howItWorks, faq, termsAndConditions, privacyPolicy, attribution;
    TextView phoneNumberTextView;
    ImageView supportLogo;

    String supportNumber = "";

    SystemFunction systemFunction = null;

    final int CALL_PHONE_PERMISSION = 1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_navigation_support,  null);

        phoneNumber = (LinearLayout)view.findViewById(R.id.phoneNumber);
        email = (LinearLayout)view.findViewById(R.id.email);
        howItWorks = (LinearLayout)view.findViewById(R.id.howItWorks);
        faq = (LinearLayout)view.findViewById(R.id.faq);
        termsAndConditions = (LinearLayout)view.findViewById(R.id.termsAndConditions);
        privacyPolicy = (LinearLayout)view.findViewById(R.id.privacyPolicy);
        attribution = (LinearLayout)view.findViewById(R.id.attribution);

        phoneNumberTextView = (TextView)view.findViewById(R.id.phoneNumberTextView);
        supportLogo = (ImageView)view.findViewById(R.id.supportLogo);

        systemFunction = new SystemFunction(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, SystemClient.class), getActivity());

        phoneNumber.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if(!supportNumber.equals("")){
                    if(ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED){
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, CALL_PHONE_PERMISSION);
                    }else{
                        Intent phoneIntent = new Intent(Intent.ACTION_CALL);
                        phoneIntent.setData(Uri.parse("tel:" + supportNumber));

                        startActivity(phoneIntent);
                    }

                    ClipboardManager clipboard = (ClipboardManager)getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clipData = ClipData.newPlainText("SupportNumber", supportNumber);
                    clipboard.setPrimaryClip(clipData);

                    Toast.makeText(getActivity(), "Number copied to clipboard", Toast.LENGTH_LONG).show();
                }
            }
        });

        email.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("message/rfc822");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"support@poratechai.com"});

                try{
                    startActivity(Intent.createChooser(intent, "Send email ... "));
                }catch(Exception e){
                    Toast.makeText(getActivity(), "There are no email clients installed", Toast.LENGTH_SHORT);
                }
            }
        });

        howItWorks.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.poratechai.com/how"));
                startActivity(intent);
            }
        });

        faq.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.poratechai.com/faq"));
                startActivity(intent);
            }
        });

        termsAndConditions.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.poratechai.com/terms"));
                startActivity(intent);
            }
        });

        privacyPolicy.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.poratechai.com/privacy"));
                startActivity(intent);
            }
        });

        attribution.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AboutActivity.class);
                startActivity(intent);
            }
        });

        supportLogo.getLayoutParams().height = (int)(0.15*Constants.SCREEN_HEIGHT);
        supportLogo.getLayoutParams().width  = (int)(0.15*Constants.SCREEN_HEIGHT);

        systemFunction.getSupportNumber(this);

        return view;
    }

    @Override
    public void loadFragment() {
        systemFunction.getSupportNumber(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && requestCode == CALL_PHONE_PERMISSION){
            Intent phoneIntent = new Intent(Intent.ACTION_CALL);
            phoneIntent.setData(Uri.parse("tel:" + supportNumber));

            startActivity(phoneIntent);
        }
    }

    public void supportNumberGotten(String supportNumber){
        this.supportNumber = supportNumber;

        phoneNumberTextView.setText(supportNumber);
    }
}
