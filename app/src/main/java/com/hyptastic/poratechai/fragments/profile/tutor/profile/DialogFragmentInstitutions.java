package com.hyptastic.poratechai.fragments.profile.tutor.profile;

import android.app.Dialog;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.retrofit.functions.UserFunctions;
import com.hyptastic.poratechai.retrofit.interfaces.UserClient;
import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
import com.hyptastic.poratechai.utils.Constants;
import com.hyptastic.poratechai.utils.Functions;

import java.io.IOException;
import java.io.Serializable;

/**
 * Created by ISHRAK on 2/26/2019.
 */

public class DialogFragmentInstitutions extends DialogFragment {
    EditText schoolEditText, collegeEditText, universityEditText;
    Button cancelButton, confirmButton;
    UserFunctions userFunctions;

    DialogFragmentInstitutions currentDialogFragmentInstitutions;

    DialogFragmentInstitutionsListener dialogFragmentInstitutionsListener = null;

    public static interface DialogFragmentInstitutionsListener extends Parcelable {
        public void dialogFragmentInstitutionsClosed(String school, String college, String university);

        public static final Parcelable.Creator CREATOR = null;
    }

    public static DialogFragmentInstitutions createInstance(DialogFragmentInstitutionsListener dialogFragmentInstitutionsListener, String school, String college, String university){
        DialogFragmentInstitutions dialogFragmentInstitutions = new DialogFragmentInstitutions();

        Bundle args = new Bundle();
        args.putParcelable("CloseListener", dialogFragmentInstitutionsListener);
        args.putString("School", school);
        args.putString("College", college);
        args.putString("University", university);

        dialogFragmentInstitutions.setArguments(args);

        return dialogFragmentInstitutions;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialogFragmentInstitutionsListener = (DialogFragmentInstitutionsListener)getArguments().getParcelable("CloseListener");

        userFunctions = new UserFunctions(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, UserClient.class), getActivity());

        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();

        final Dialog dialog = getDialog();

        if(dialog != null){
            dialog.setContentView(R.layout.dialog_institutions);

            dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.background_poratechai_dialog_fragment_small);
        }

        currentDialogFragmentInstitutions = this;
        initLayout();
    }

    private void initLayout(){
        String school       = getArguments().getString("School");
        String college      = getArguments().getString("College");
        String university   = getArguments().getString("University");

        schoolEditText      = getDialog().findViewById(R.id.schoolEditText);
        collegeEditText     = getDialog().findViewById(R.id.collegeEditText);
        universityEditText  = getDialog().findViewById(R.id.universityEditText);
        cancelButton        = getDialog().findViewById(R.id.cancelButton);
        confirmButton       = getDialog().findViewById(R.id.confirmButton);

        schoolEditText.setText(school);
        collegeEditText.setText(college);
        universityEditText.setText(university);

        cancelButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
            }
        });
        confirmButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                userFunctions.updateInstitutions(currentDialogFragmentInstitutions, schoolEditText.getText().toString(),
                        collegeEditText.getText().toString(), universityEditText.getText().toString());
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    public void institutionsUpdatedOverNetwork(){
        dialogFragmentInstitutionsListener.dialogFragmentInstitutionsClosed(schoolEditText.getText().toString(), collegeEditText.getText().toString(), universityEditText.getText().toString());
        getDialog().dismiss();
    }
}
