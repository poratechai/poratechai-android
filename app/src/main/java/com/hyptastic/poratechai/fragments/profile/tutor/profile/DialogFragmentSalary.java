package com.hyptastic.poratechai.fragments.profile.tutor.profile;

import android.app.Dialog;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.retrofit.functions.UserFunctions;
import com.hyptastic.poratechai.retrofit.interfaces.UserClient;
import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
import com.hyptastic.poratechai.utils.Constants;
import com.hyptastic.poratechai.utils.Functions;

/**
 * Created by ISHRAK on 3/3/2019.
 */

public class DialogFragmentSalary extends DialogFragment {
    TextView salaryTextView;
    ImageButton upButton, downButton;
    Button cancelButton, confirmButton;

    com.hyptastic.poratechai.fragments.profile.tutor.profile.ProfileFragment profileFragment;
    int salary = 0;
    DialogFragmentSalary currentDialogFragmentSalary;

    UserFunctions userFunctions;

    DialogFragmentSalaryListener dialogFragmentSalaryListener = null;

    public static interface DialogFragmentSalaryListener extends Parcelable {
        public void dialogFragmentSalaryClosed(String salary);

        public static final Parcelable.Creator CREATOR = null;
    }

    public static DialogFragmentSalary createInstance(DialogFragmentSalaryListener dialogFragmentSalaryListener, int salary){
        DialogFragmentSalary dialogFragmentSalary = new DialogFragmentSalary();

        Bundle args = new Bundle();
        args.putParcelable("CloseListener", dialogFragmentSalaryListener);
        args.putInt("salary", salary);

        dialogFragmentSalary.setArguments(args);

        return dialogFragmentSalary;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialogFragmentSalaryListener = (DialogFragmentSalaryListener) getArguments().getParcelable("CloseListener");

        userFunctions = new UserFunctions(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, UserClient.class), getActivity());

        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();

        final Dialog dialog = getDialog();

        if(dialog != null){
            dialog.setContentView(R.layout.dialog_salary);

            dialog.getWindow().setLayout((int)(0.92*Constants.SCREEN_WIDTH), (int)(0.4*Constants.SCREEN_HEIGHT));
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.background_listitem_job_tutor_profile);
        }

        currentDialogFragmentSalary = this;
        initLayout();
    }

    private void initLayout(){
        try{
            profileFragment     = (com.hyptastic.poratechai.fragments.profile.tutor.profile.ProfileFragment)getArguments().getSerializable("ProfileFragment");

            salary = (int)getArguments().getInt("salary");

            salaryTextView      = getDialog().findViewById(R.id.salaryTextView);
            upButton            = getDialog().findViewById(R.id.upButton);
            downButton          = getDialog().findViewById(R.id.downButton);
            cancelButton        = getDialog().findViewById(R.id.cancelButton);
            confirmButton       = getDialog().findViewById(R.id.confirmButton);

            salaryTextView.setText(String.valueOf(salary));

            upButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    int salaryTemp = Integer.parseInt(salaryTextView.getText().toString());

                    if(salaryTemp<=99500){
                        salaryTemp+=500;
                        salaryTextView.setText(String.valueOf(salaryTemp));
                    }
                }
            });

            downButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    int salaryTemp = Integer.parseInt(salaryTextView.getText().toString());

                    if(salaryTemp>=500){
                        salaryTemp-=500;
                        salaryTextView.setText(String.valueOf(salaryTemp));
                    }
                }
            });

            cancelButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    getDialog().dismiss();
                }
            });
            confirmButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    userFunctions.updateSalary(currentDialogFragmentSalary, Integer.parseInt(salaryTextView.getText().toString()));
                }
            });
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void salaryUpdatedOverNetwork(boolean isUpdatedSuccessfully){
        if(isUpdatedSuccessfully){
            dialogFragmentSalaryListener.dialogFragmentSalaryClosed(salaryTextView.getText().toString());
        }else{
            dialogFragmentSalaryListener.dialogFragmentSalaryClosed(String.valueOf(salary));
        }

        getDialog().dismiss();
    }
}
