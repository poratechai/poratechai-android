package com.hyptastic.poratechai.fragments.job_feed;

import android.support.v4.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.ybq.android.spinkit.SpinKitView;
import com.github.ybq.android.spinkit.sprite.SpriteContainer;
import com.github.ybq.android.spinkit.style.CubeGrid;
import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.adapters.job_feed.JobFeedAdapter;
import com.hyptastic.poratechai.utils.Constants;

/**
 * Created by ISHRAK on 6/15/2018.
 */

public abstract class JobFeedFragment extends Fragment {
    private SwipeRefreshLayout swipeRefreshLayout = null;
    private RecyclerView recyclerView = null;
    private JobFeedAdapter jobFeedAdapter = null;
    private RecyclerView.LayoutManager layoutManager = null;

    private SpriteContainer spinKitViewStyle;
    private SpinKitView spinKitView;
    private TextView jobFeedTextView;

    protected void setJobFeedAdapter(JobFeedAdapter jobFeedAdapter){
        this.jobFeedAdapter = jobFeedAdapter;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_job_feed, container, false);

        swipeRefreshLayout  = rootView.findViewById(R.id.swipe_refresh_job_feed);
        recyclerView        = rootView.findViewById(R.id.recycler_view);
        spinKitView         = rootView.findViewById(R.id.loading_spin_kit);
        jobFeedTextView     = rootView.findViewById(R.id.job_feed_no_item_text_view);
        spinKitViewStyle    = new CubeGrid();

        recyclerView.setTag(R.id.SWIPE_REFRESH_LAYOUT, swipeRefreshLayout);
        recyclerView.setTag(R.id.SPIN_KIT_VIEW_STYLE, spinKitViewStyle);
        recyclerView.setTag(R.id.SPIN_KIT_VIEW, spinKitView);
        recyclerView.setTag(R.id.JOB_FEED_TEXT_VIEW, jobFeedTextView);

//        swipeRefreshLayout.setColorSchemeColors(R.color.colorAccent);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(jobFeedAdapter!=null){
                    jobFeedAdapter.loadData();
                }
            }
        });

        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(rootView.getContext());
        recyclerView.setLayoutManager(layoutManager);

        //jobFeedAdapter is set using the setJobFeedAdapter method from the sub-class
        recyclerView.setAdapter(jobFeedAdapter);
        recyclerView.getRecycledViewPool().setMaxRecycledViews(0, 50);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.e("JobFeedFragment", "Resumed");

        jobFeedAdapter.loadData();

        registerBroadcastReceiver();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(feedUpdateNotificationReceiver);
    }

    //Broadcast Receiver

    protected abstract void registerBroadcastReceiver(); //To be implemented by the sub-class

    protected BroadcastReceiver feedUpdateNotificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int operationType = intent.getIntExtra("operationType", 0);
            String id = intent.getStringExtra("Id");

            switch(operationType)
            {
                case Constants.NOTIFICATION_DATA_OPERATION_TYPE.ADD:
                    Log.e("Update Posted Jobs", "Add");
                    jobFeedAdapter.refreshOneItemFromNotifications(id);
                    break;
                case Constants.NOTIFICATION_DATA_OPERATION_TYPE.REMOVE:
                    Log.e("Update Posted Jobs", "Remove");
                    jobFeedAdapter.removeOneItemFromNotifications(id);
                    break;
                case Constants.NOTIFICATION_DATA_OPERATION_TYPE.UPDATE:
                    Log.e("Update Posted Jobs", "Update");
                    jobFeedAdapter.refreshOneItemFromNotifications(id);
                    break;
            }
        }
    };
}
