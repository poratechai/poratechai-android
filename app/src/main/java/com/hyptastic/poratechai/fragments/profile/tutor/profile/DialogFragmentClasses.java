package com.hyptastic.poratechai.fragments.profile.tutor.profile;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.hyptastic.poratechai.fragments.multi_select.MultiSelectDialogFragment;
import com.hyptastic.poratechai.utils.Functions;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ISHRAK on 2/28/2019.
 */

public class DialogFragmentClasses extends MultiSelectDialogFragment {
    DialogFragmentClasses currentDialogFragmentClasses;

    DialogFragmentClassesCloseListener dialogFragmentClassesCloseListener = null;

    public static interface DialogFragmentClassesCloseListener extends Parcelable {
        public void dialogFragmentClassesClosed();

        public static final Parcelable.Creator CREATOR = null;
    }

    public static DialogFragmentClasses createInstance(DialogFragmentClassesCloseListener dialogFragmentClassesCloseListener, HashMap<String, String[]> dictionary, ArrayList<String> selectedItems)
    {
        DialogFragmentClasses dialogFragmentClasses = new DialogFragmentClasses();

        createInstance(dialogFragmentClasses, dictionary, selectedItems);
        dialogFragmentClasses.getArguments().putParcelable("CloseListener", dialogFragmentClassesCloseListener);

        return dialogFragmentClasses;
    }

    @Override
    public void onStart() {
        super.onStart();

        currentDialogFragmentClasses = this;
        dialogFragmentClassesCloseListener = (DialogFragmentClassesCloseListener) getArguments().getParcelable("CloseListener");

        confirmButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                userFunctions.updateClasses(currentDialogFragmentClasses, selectedItems);
            }
        });
    }

    public void classesUpdated(boolean isUpdateSuccessful){
        dialogFragmentClassesCloseListener.dialogFragmentClassesClosed();

        getDialog().dismiss();
    }
}
