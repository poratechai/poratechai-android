package com.hyptastic.poratechai.fragments.job_feed.parent_student.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.adapters.job_feed.parent_student.ContractRequestsAdapter;

/**
 * Created by ISHRAK on 3/20/2018.
 */

public class ConfirmDeleteInspectionTutorDialog extends DialogFragment {
    public interface ConfirmDeleteInspectionTutorListener{
        public void onInspectionTutorDeletePositive(String matchId);
    }

    public ConfirmDeleteInspectionTutorListener mListener;

    public static ConfirmDeleteInspectionTutorDialog createNewInstance(ContractRequestsAdapter contractRequestsAdapter, String matchId)
    {
        ConfirmDeleteInspectionTutorDialog confirmDeleteInspectionTutorDialog = new ConfirmDeleteInspectionTutorDialog();

        Bundle bundle = new Bundle();
        bundle.putSerializable("adapter", contractRequestsAdapter);
        bundle.putString("matchId", matchId);

        confirmDeleteInspectionTutorDialog.setArguments(bundle);

        return confirmDeleteInspectionTutorDialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final ContractRequestsAdapter contractRequestsAdapter = (ContractRequestsAdapter)getArguments().getSerializable("adapter");
        final String matchId = getArguments().getString("matchId");

        try{
            mListener = (ConfirmDeleteInspectionTutorListener)contractRequestsAdapter;
        }catch(ClassCastException e){
            throw new ClassCastException(contractRequestsAdapter.toString() + " must implement ConfirmDeleteInspectionTutorListener");
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.inspection_tutor_deletion_confirmation)
                .setTitle(R.string.inspection_tutor_deletion_title)
                .setPositiveButton(R.string.inspection_tutor_deletion_positive, new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mListener.onInspectionTutorDeletePositive(matchId);
                    }
                })
                .setNegativeButton(R.string.inspection_tutor_deletion_negative, new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

        return builder.create();
    }
}
