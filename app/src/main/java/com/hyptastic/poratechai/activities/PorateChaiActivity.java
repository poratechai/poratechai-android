package com.hyptastic.poratechai.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v4.widget.ImageViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.activities.authentication.AuthenticationActivity;
import com.hyptastic.poratechai.adapters.PorateChaiNavigationAdapter;
import com.hyptastic.poratechai.fragments.PorateChaiNavigationFragment;
import com.hyptastic.poratechai.fragments.PorateChaiNavigationViewPager;
import com.hyptastic.poratechai.fragments.login_registration.ConfirmCodeDialog;
import com.hyptastic.poratechai.utils.Constants;
import com.hyptastic.poratechai.utils.Functions;

import java.io.Serializable;
import java.util.ArrayList;

public class PorateChaiActivity extends ActionBarActivity implements Serializable, ConfirmCodeDialog.Listener {
    TabLayout porateChaiNavigationTabLayout;
    PorateChaiNavigationViewPager porateChaiNavigationViewPager;
//    String[] titles = new String[]{"Profile", "Peek", "Feed", "Subscribe", "Support"};
    String[] titles = new String[]{"Home", "Profile", "Inbox", "More"};
    PorateChaiNavigationAdapter porateChaiNavigationAdapter;

    PorateChaiActivity currentActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            Crashlytics.setString("EMAIL_ADDRESS", Constants.EMAIL_VALUE);

            Functions.initSharedPreferences(getApplicationContext());
            Functions.loadLoginDataFromSharedPreferences();
            Functions.getScreenDimensions(getWindowManager());

//            if(Constants.IS_VERIFIED_VALUE.equals("false")){
//                showConfirmCodeDialog();
//            }

            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_porate_chai);

            currentActivity = this;

            //------------------------------------------------------------------------------------------------------------------------------------------

            //Updating SharedPreference and checking Logged In status
            loadFeedFromNotification();

            //------------------------------------------------------------------------------------------------------------------------------------------

            //Navigation TabLayout and ViewPager - start
            porateChaiNavigationTabLayout = (TabLayout)findViewById(R.id.porateChaiNavigationTabLayout);
            porateChaiNavigationViewPager = (PorateChaiNavigationViewPager) findViewById(R.id.porateChaiNavigationViewPager);

            setupViewPager(porateChaiNavigationViewPager);

            porateChaiNavigationTabLayout.setupWithViewPager(porateChaiNavigationViewPager);
            setupTabLayout(porateChaiNavigationTabLayout);

            //Navigation TabLayout and ViewPager - end

            //------------------------------------------------------------------------------------------------------------------------------------------

            //ActionBar Refresh Button - start
            refreshButtonClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    refreshPorateChai();
                }
            };
            getRefreshButton().setOnClickListener(refreshButtonClickListener);
            //ActionBar HireOrTeach Switch - start

            //------------------------------------------------------------------------------------------------------------------------------------------
            //Setting ActionBarActivity class's attribute switchTutorOrHireListener
            switchTutorOrHireListener = new Switch.OnCheckedChangeListener(){
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    setSwitch(b);

                    Functions.setIsTutorOnNetwork(b, currentActivity);
                }
            };
            getSwitchTutorOrHire().setOnCheckedChangeListener(switchTutorOrHireListener); //getSwitchTutorOrHire() is inherited from ActionBarActivity
            //ActionBar HireOrTeach Switch - end
            //------------------------------------------------------------------------------------------------------------------------------------------
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void setupTabLayout(TabLayout tabLayout){
        View headerView = LayoutInflater.from(this).inflate(R.layout.layout_poratechai_navigation_tablayout, null);

//        tabLayout.getTabAt(0).setCustomView((LinearLayout)headerView.findViewById(R.id.porateChaiNavigationTabProfile));
//        tabLayout.getTabAt(1).setCustomView((LinearLayout)headerView.findViewById(R.id.porateChaiNavigationTabPeek));
//        tabLayout.getTabAt(2).setCustomView((LinearLayout)headerView.findViewById(R.id.porateChaiNavigationTabFeed));
//        tabLayout.getTabAt(3).setCustomView((LinearLayout)headerView.findViewById(R.id.porateChaiNavigationTabSubscription));
//        tabLayout.getTabAt(4).setCustomView((LinearLayout)headerView.findViewById(R.id.porateChaiNavigationTabSupport));

        tabLayout.getTabAt(0).setCustomView((LinearLayout)headerView.findViewById(R.id.porateChaiNavigationTabHome));
        tabLayout.getTabAt(1).setCustomView((LinearLayout)headerView.findViewById(R.id.porateChaiNavigationTabProfile));
        tabLayout.getTabAt(2).setCustomView((LinearLayout)headerView.findViewById(R.id.porateChaiNavigationTabInbox));
        tabLayout.getTabAt(3).setCustomView((LinearLayout)headerView.findViewById(R.id.porateChaiNavigationTabMore));
    }

    private void setupViewPager(ViewPager viewPager){
        ArrayList<PorateChaiNavigationFragment> porateChaiNavigationFragmentArrayList = new ArrayList(5);

        int feedType = 0;
        if(getIntent().getExtras()!=null && getIntent().getExtras().containsKey("feedType")){
            feedType = getIntent().getExtras().getInt("feedType");
        }

        porateChaiNavigationFragmentArrayList.add(com.hyptastic.poratechai.fragments.job_feed.NavigationFragment.createInstance(feedType));
        porateChaiNavigationFragmentArrayList.add(com.hyptastic.poratechai.fragments.profile.NavigationFragment.createInstance(feedType));
        porateChaiNavigationFragmentArrayList.add(new com.hyptastic.poratechai.fragments.inbox.NavigationFragment());
        porateChaiNavigationFragmentArrayList.add(new com.hyptastic.poratechai.fragments.support.NavigationFragment());

        porateChaiNavigationAdapter = new PorateChaiNavigationAdapter(getSupportFragmentManager(), porateChaiNavigationFragmentArrayList, titles);

        viewPager.setAdapter(porateChaiNavigationAdapter);
        viewPager.setOffscreenPageLimit(5);
    }

    private void loadFeedFromNotification(){
        if(Functions.getMSharedPreferences()==null){
            Functions.initSharedPreferences(getApplicationContext());
        }

        if(Functions.isLoggedIn()){     //Stay in job feed if user is already logged in
            Functions.loadLoginDataFromSharedPreferences();
            refreshPorateChai();
        }else{                          //Authenticate user if user is not already logged in
            Intent intent = new Intent(this, AuthenticationActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }

        Functions.getScreenDimensions(getWindowManager());
    }

    @Override
    protected void onResume() {
        super.onResume();

        Crashlytics.setString("EMAIL_ADDRESS", Constants.EMAIL_VALUE);

        loadFeedFromNotification();
    }

    @Override
    protected void onStart() {
        super.onStart();

        Crashlytics.setString("EMAIL_ADDRESS", Constants.EMAIL_VALUE);

        loadFeedFromNotification();

        setupViewPager(porateChaiNavigationViewPager);
        porateChaiNavigationTabLayout.setupWithViewPager(porateChaiNavigationViewPager);
        setupTabLayout(porateChaiNavigationTabLayout);

        porateChaiNavigationViewPager.setCurrentItem(0);

        refreshPorateChai();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void refreshPorateChai(){
        try{
            for(int i=0;i<=3;i++){
                ((PorateChaiNavigationFragment)porateChaiNavigationAdapter.getItem(i)).loadFragment();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void switchPager(int page){
        porateChaiNavigationViewPager.setCurrentItem(page);
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------------
    //Overriding from ActionBarActivity
    @Override
    public void onTutorParentSwitchedOverNetwork(boolean isTutor, boolean isSuccessful){
        super.onTutorParentSwitchedOverNetwork(isTutor, isSuccessful);

        promptTutorUpdate();

        refreshPorateChai();
    }
    //Overriding from ActionBarActivity
    //---------------------------------------------------------------------------------------------------------------------------------------------------

    //---------------------------------------------------------------------------------------------------------------------------------------------------
    //Showing Confirm code dialog - start
    public void showConfirmCodeDialog(){
        ConfirmCodeDialog fragment = new ConfirmCodeDialog();

        fragment.show(getFragmentManager(), ConfirmCodeDialog.TAG);
    }
    //Showing Confirm code dialog - end
    //---------------------------------------------------------------------------------------------------------------------------------------------------

    //---------------------------------------------------------------------------------------------------------------------------------------------------
    //Showing Confirm code dialog - start
    @Override
    public void onConfirmCode(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
    //Showing Confirm code dialog - end
    //---------------------------------------------------------------------------------------------------------------------------------------------------

    //---------------------------------------------------------------------------------------------------------------------------------------------------
    //Prompt tutor update dialog - start
    public void promptTutorUpdate(){
        if(Functions.isTutor() && Functions.isTutorUpdated().equals("false")){
            AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppTheme_Dialog)
                    .setTitle(R.string.update_tutor_profile_title)
                    .setMessage(R.string.update_tutor_profile_message)
                    .setPositiveButton(R.string.update_tutor_profile_message_positive, new DialogInterface.OnClickListener(){
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            porateChaiNavigationViewPager.setCurrentItem(1);    //Go to profile
                        }
                    })
                    .setCancelable(false);

            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }
    //---------------------------------------------------------------------------------------------------------------------------------------------------
    //Prompt tutor update dialog - end

//-------------------------------------------------------------------------------------------------------------------------------------------------------
}
