package com.hyptastic.poratechai.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.hyptastic.poratechai.R;

/**
 * Created by ISHRAK on 5/15/2018.
 */

public class ActionBarBackActivity extends AppCompatActivity {
    private ImageButton backButton;
    private TextView titleTextView;

    private ActionBarBackActivity currentActivity;

    ActionBar actionBar = null;
    View actionBarView = null;

    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try{
            currentActivity = this;

            actionBar = getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);

            actionBarView = LayoutInflater.from(this).inflate(R.layout.actionbar_back_basic, null);
            actionBarView.setLayoutParams(new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT));
            actionBar.setCustomView(actionBarView);

            toolbar = (Toolbar)actionBarView.getParent();
            toolbar.setPadding(0, 0, 0, 0);
            toolbar.setContentInsetsAbsolute(0, 0);

            backButton = actionBarView.findViewById(R.id.backButton);
            titleTextView = actionBarView.findViewById(R.id.titleTextView);

            backButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
//        finish();

        backButton.performClick();
    }

    protected void setBackButtonListener(View.OnClickListener onClickListener){
        backButton.setOnClickListener(onClickListener);
    }

    protected void setTitle(String title){
        titleTextView.setText(title);
    }
}
