package com.hyptastic.poratechai.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.MotionEvent;
import android.view.View;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.activities.PorateChaiActivity;
import com.hyptastic.poratechai.retrofit.functions.UserFunctions;
import com.hyptastic.poratechai.retrofit.interfaces.UserClient;
import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
import com.hyptastic.poratechai.utils.Constants;
import com.hyptastic.poratechai.utils.Functions;

public class HireOrTeachActivity extends AppCompatActivity {
    UserClient userClient = null;
    UserFunctions userFunctions = null;

    CardView hireCardView, teachCardView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //initViews();
        setContentView(R.layout.activity_hire_or_teach);
        initViews();
    }

    private void initViews() {

        hireCardView = findViewById(R.id.hireCardView);
        teachCardView = findViewById(R.id.teachCardView);

        hireCardView.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction()==MotionEvent.ACTION_UP){
                    hireOrTeach("false");
                }
                return true;
            }
        });
        teachCardView.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction()==MotionEvent.ACTION_UP){
                    hireOrTeach("true");
                }
                return true;
            }
        });
    }

    public void hire(){
        Functions.setIsTutor(false);
        Intent intent = new Intent(this, PorateChaiActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    public void teach(){
        Functions.setIsTutor(true);
        Intent intent = new Intent(this, PorateChaiActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private void hireOrTeach(String flagValue){
        userClient = RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE,Constants.TOKEN_VALUE,UserClient.class);
        userFunctions = new UserFunctions(userClient, this);
        userFunctions.setTutorOrParent(this, flagValue);
    }
}
