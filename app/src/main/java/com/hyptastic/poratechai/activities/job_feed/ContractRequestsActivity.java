package com.hyptastic.poratechai.activities.job_feed;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.activities.ActionBarBackActivity;
import com.hyptastic.poratechai.adapters.job_feed.parent_student.ContractRequestsAdapter;
import com.hyptastic.poratechai.models.job_feed.JobFeedResponse;
import com.hyptastic.poratechai.retrofit.functions.JobFunction;
import com.hyptastic.poratechai.retrofit.interfaces.JobClient;
import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
import com.hyptastic.poratechai.utils.Constants;

import java.io.Serializable;

public class ContractRequestsActivity extends ActionBarBackActivity implements Serializable {
    private RecyclerView contractRequestsRecyclerView;
    private ContractRequestsAdapter contractRequestsAdapter;
    private RecyclerView.LayoutManager contractRequestsLayoutManager;
    private JobFunction jobFunction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contract_requests);

        super.setTitle("Contract Requests");

        JobFeedResponse jobFeedResponse = (JobFeedResponse)(getIntent().getExtras().getParcelable("Job"));

        jobFunction = new JobFunction(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, JobClient.class), this);

        contractRequestsRecyclerView = (RecyclerView)findViewById(R.id.parent_student_posted_jobs_contract_requests_recycler_view);

        contractRequestsRecyclerView.setHasFixedSize(true);

        contractRequestsLayoutManager = new LinearLayoutManager(getApplicationContext());
        contractRequestsRecyclerView.setLayoutManager(contractRequestsLayoutManager);

        contractRequestsAdapter = new ContractRequestsAdapter(this, jobFunction, jobFeedResponse);
        contractRequestsRecyclerView.setAdapter(contractRequestsAdapter);
    }
}
