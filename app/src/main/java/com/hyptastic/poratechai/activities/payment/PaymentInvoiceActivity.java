//package com.hyptastic.poratechai.activities.payment;
//
//import android.support.v7.app.AppCompatActivity;
//import android.os.Bundle;
//
//import com.hyptastic.poratechai.R;
//import com.hyptastic.poratechai.fragments.payment.tutor.InvoiceFragment;
//
//public class PaymentInvoiceActivity extends AppCompatActivity {
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_payment_invoice);
//
//        InvoiceFragment invoiceFragment = InvoiceFragment.createNewInstance(getIntent().getExtras().getParcelable("invoiceResponse"));
//        getSupportFragmentManager().beginTransaction().replace(R.id.paymentInvoiceLayout, invoiceFragment).commit();
//    }
//}
