package com.hyptastic.poratechai.activities.support;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.activities.ActionBarActivity;
import com.hyptastic.poratechai.activities.ActionBarBackActivity;

public class AboutActivity extends ActionBarBackActivity {
    TextView freePikAttributionTextView, goodwareAttributionTextView, flatIconAttributionTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try{
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_about);

            setTitle("Attributions");

            freePikAttributionTextView = findViewById(R.id.freePikAttribution);
            goodwareAttributionTextView = findViewById(R.id.goodwareAttribution);
            flatIconAttributionTextView = findViewById(R.id.flatIconAttribution);

            freePikAttributionTextView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    {
                        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("http://freepik.com"));
                        startActivity(intent);
                    }
                }
            });

            goodwareAttributionTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://www.facebook.com/goodware.std"));
                    startActivity(intent);
                }
            });

            flatIconAttributionTextView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://flaticon.com"));
                    startActivity(intent);
                }
            });
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
