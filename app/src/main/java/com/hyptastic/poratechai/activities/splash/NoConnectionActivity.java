package com.hyptastic.poratechai.activities.splash;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.retrofit.functions.SystemFunction;
import com.hyptastic.poratechai.retrofit.interfaces.SystemClient;
import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
import com.hyptastic.poratechai.utils.Constants;

public class NoConnectionActivity extends AppCompatActivity {
    ImageView noConnectionImageView;
    TextView noConnectionTextView;
    Button retryButton;

    SystemFunction systemFunction = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_connection);

        NoConnectionActivity currentNoConnectionActivity = this;

        noConnectionImageView = findViewById(R.id.noConnectionImageView);
        noConnectionTextView = findViewById(R.id.noConnectionTextView);
        retryButton = findViewById(R.id.noConnectionButton);

        systemFunction = new SystemFunction(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, SystemClient.class), this);

        retryButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                systemFunction.getConnectionStatus(currentNoConnectionActivity);
            }
        });
    }

    public void connectedSuccessfully(){
        RetrofitServiceGenerator.cancelRequests();

        Intent intent = new Intent(this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
