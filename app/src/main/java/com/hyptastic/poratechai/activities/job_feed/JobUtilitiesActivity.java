package com.hyptastic.poratechai.activities.job_feed;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.activities.ActionBarBackActivity;
import com.hyptastic.poratechai.adapters.job_feed.utilities.JobUtilitiesPagerAdapter;
import com.hyptastic.poratechai.fragments.job_feed.utilities.UtilitiesCalendarFragment;
import com.hyptastic.poratechai.fragments.job_feed.utilities.UtilitiesChatFragment;
import com.hyptastic.poratechai.fragments.job_feed.utilities.UtilitiesDetailsFragment;
import com.hyptastic.poratechai.models.job_feed.JobFeedResponse;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by ISHRAK on 5/31/2019.
 */

public class JobUtilitiesActivity extends ActionBarBackActivity {
    public static final int DETAILS = 0;
    public static final int CALENDAR = 1;
    public static final int CHAT = 2;

    private int whichTab = 0;
    private String jobId = "";
    private String matchId = "";
    private int utilitiesFeedType = -1;

    String[] jobUtilitiesTitle;
    ArrayList<Fragment> jobUtilitiesFragmentList;

    JobUtilitiesPagerAdapter jobUtilitiesPagerAdapter;

    TabLayout detailsCalendarChatTabLayout;
    ViewPager detailsCalendarChatViewPager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_utilities);

        loadView();
    }

    @Override
    protected void onResume() {
        super.onResume();

        loadView();
    }

    private void loadView(){
        whichTab = (int)(getIntent().getIntExtra("WHICH_TAB", 0));
        jobId = getIntent().getStringExtra("JOB_ID");
        matchId = getIntent().getStringExtra("MATCH_ID");
        utilitiesFeedType = getIntent().getIntExtra("UTILITIES_FEED_TYPE", -1);

        detailsCalendarChatTabLayout = (TabLayout)findViewById(R.id.utilitiesTabLayout);
        detailsCalendarChatViewPager = (ViewPager)findViewById(R.id.utilitiesViewPager);

        jobUtilitiesTitle = new String[]{"Details", "Calendar", "Chat"};
        jobUtilitiesFragmentList = new ArrayList<Fragment>(Arrays.asList(
                new Fragment[]{UtilitiesDetailsFragment.createInstance(jobId, matchId, utilitiesFeedType),
                        UtilitiesCalendarFragment.createInstance(jobId),
                        UtilitiesChatFragment.createInstance()}
        ));

        jobUtilitiesPagerAdapter = new JobUtilitiesPagerAdapter(getSupportFragmentManager(), jobUtilitiesFragmentList, jobUtilitiesTitle);

        detailsCalendarChatViewPager.setAdapter(jobUtilitiesPagerAdapter);
        detailsCalendarChatTabLayout.setupWithViewPager(detailsCalendarChatViewPager);

        detailsCalendarChatViewPager.setCurrentItem(whichTab);
    }
}
