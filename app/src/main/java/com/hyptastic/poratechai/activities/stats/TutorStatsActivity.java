//package com.hyptastic.poratechai.activities.stats;
//
//import android.content.Context;
//import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
//import android.widget.TextView;
//
//import com.hyptastic.poratechai.R;
//import com.hyptastic.poratechai.models.users.TutorStat;
//import com.hyptastic.poratechai.retrofit.functions.StatsFunction;
//import com.hyptastic.poratechai.retrofit.interfaces.StatClient;
//import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
//import com.hyptastic.poratechai.utils.Constants;
//
//public class TutorStatsActivity extends AppCompatActivity {
//
//    private Context context;
//    private TextView numberOfActiveContracts;
//    private TextView currentIncome;
//    private TextView rating;
//    private TextView matches;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_stats_tutor);
//
//        context = getApplicationContext();
//        numberOfActiveContracts = findViewById(R.id.numberOfContracts);
//        currentIncome = findViewById(R.id.incomeNumber);
//        rating = findViewById(R.id.rating);
//        matches = findViewById(R.id.numberOfMatches);
//
//        getStats();
//    }
//
//    private void getStats(){
//        StatClient statClient = RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, StatClient.class);
//        StatsFunction statFunction = new StatsFunction(statClient, context);
//        statFunction.getTutorStats(this);
//    }
//
//    public void setStats(TutorStat stats){
//        System.out.println("stats are: " + stats.toString());
//        numberOfActiveContracts.setText(String.valueOf(stats.getActivecontracts()));
//        Float income = Float.valueOf(stats.getCurrentIncome()/1000);
//        String incomeText = String.valueOf(income) + "k";
//        currentIncome.setText(incomeText);
//        rating.setText(String.valueOf(stats.getRating()));
//        matches.setText(String.valueOf(stats.getMatches()));
//    }
//}
