//package com.hyptastic.poratechai.activities.payment;
//
//import android.support.v7.app.AppCompatActivity;
//import android.os.Bundle;
//
//import com.hyptastic.poratechai.R;
//import com.hyptastic.poratechai.fragments.payment.tutor.PaymentChoiceFragment;
//
//public class PaymentActivity extends AppCompatActivity {
//    PaymentChoiceFragment paymentChoiceFragment;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_payment);
//
//        paymentChoiceFragment = new PaymentChoiceFragment();
//        getSupportFragmentManager().beginTransaction().replace(R.id.paymentChoiceLayout, paymentChoiceFragment).commit();
//    }
//
//    @Override
//    protected void onResume(){
//        super.onResume();
//
//        //paymentChoiceFragment.getWalletConstants(); //to refresh the fragment data
//        paymentChoiceFragment.getPrice();
//    }
//}
