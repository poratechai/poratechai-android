//package com.hyptastic.poratechai.activities.profile;
//
//import com.hyptastic.poratechai.activities.ActionBarActivity;
//
////import android.content.ActivityNotFoundException;
////import android.content.ContentResolver;
////import android.content.Intent;
////import android.net.Uri;
////import android.os.Bundle;
////import android.support.v7.widget.CardView;
////import android.support.v7.widget.LinearLayoutManager;
////import android.support.v7.widget.RecyclerView;
////import android.util.Log;
////import android.view.MotionEvent;
////import android.view.View;
////import android.widget.Button;
////import android.widget.ImageButton;
////import android.widget.ImageView;
////import android.widget.RatingBar;
////import android.widget.TextView;
////import android.widget.Toast;
////
////import com.bumptech.glide.Glide;
////import com.bumptech.glide.request.RequestOptions;
////import com.github.ybq.android.spinkit.SpinKitView;
////import com.github.ybq.android.spinkit.sprite.Sprite;
////import com.github.ybq.android.spinkit.style.CubeGrid;
////import com.hyptastic.poratechai.activities.ActionBarActivity;
////import com.hyptastic.poratechai.activities.authentication.AuthenticationActivity;
////import com.hyptastic.poratechai.activities.payment.PaymentActivity;
////import com.hyptastic.poratechai.R;
////import com.hyptastic.poratechai.adapters.profile.ProfileReviewAdapter;
////import com.hyptastic.poratechai.fragments.profile.parent_student.ViewProfileFragment;
////
////import com.hyptastic.poratechai.functions.binders.ImageTypeSmallList;
////import com.hyptastic.poratechai.retrofit.functions.SystemFunction;
////import com.hyptastic.poratechai.retrofit.functions.UserFunctions;
////import com.hyptastic.poratechai.retrofit.interfaces.SystemClient;
////import com.hyptastic.poratechai.retrofit.interfaces.UserClient;
////import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
////import com.hyptastic.poratechai.utils.Constants;
////import com.hyptastic.poratechai.utils.Functions;
////import com.mindorks.placeholderview.PlaceHolderView;
////
////import java.io.ByteArrayOutputStream;
////import java.io.File;
////import java.io.IOException;
////import java.io.InputStream;
////import java.util.List;
////
//public class ProfileActivity extends ActionBarActivity {
////    public static final String TAG = ProfileActivity.class.getSimpleName();
////
////    UserClient userClient = null;
////    UserFunctions userFunctions = null;
////
////    private ViewProfileFragment parentStudentFragment;
////    private com.hyptastic.poratechai.fragments.profile.tutor.ViewProfileFragment tutorFragment;
////
////    private TextView fullNameTextView;
////    private TextView institutionTextView;
////    private ImageView profilePictureImageView;
////
////    private SystemFunction systemFunction;
////
////    //Credential Pictures Gallery - Start
////    private Button addCredentialPictureButton;
////    private PlaceHolderView mGalleryView;
////    //Credential Pictures Gallery - End
////
////    //Buy Contracts - Start
//////    private RelativeLayout paymentSection;
//////    private LinearLayout contractsUpDown;
//////    private TextView contractBalanceTextView, paymentAmountTextView, contractsValueTextView;
//////    private ImageButton syncButton, upButton, downButton;
//////    private Button refreshButton, buyButton;
//////    private SpinKitView contractBalanceSpinKitView;
//////    private SpinKitView buyContractsSpinKitView;
//////
//////    private Sprite contractBalanceSpinKitSprite;
//////    private Sprite buyContractsSpinKitSprite;
//////
//////    private int contractsNumber = 0;
//////    private double paymentAmount = 0;
//////
//////    private WalletFunction walletFunction = null;
//////    private SystemFunction systemFunction = null;
//////
//////    private InvoiceResponse invoiceResponse;
//////
//////    private boolean pending = false;
////    //Buy Contracts - End
////
////    //Review - start
////    private TextView reviewTitleTextView;
////    private RecyclerView reviewRecyclerView;
////    private RatingBar reviewRatingBar;
////    private ProfileReviewAdapter profileReviewAdapter;
////    private RecyclerView.LayoutManager reviewLayoutManager;
////    private SpinKitView reviewSpinKitView;
////    private SpinKitView profilePictureSpinKitView;
////    private SpinKitView credentialPicturesSpinKitView;
////
////    private Sprite reviewSpinKitSprite;
////    private Sprite profilePictureSpinKitSprite;
////    private Sprite credentialPicturesSpinKitSprite;
////
////    ImageButton leftButton, rightButton;
////
////    static int recyclerViewCounter;
////    //Review - end
////
////    //Logout - start
////    private Button logoutButton;
////    //Logout - end
////
////    CardView subscriptionCard;
////    TextView subscriptionButton;
////    TextView subscriptionDescription;
////    ImageView subscriptionImageView;
////
////    private static final int INTENT_REQUEST_CODE = 100;
////    private static final int INTENT_REQUEST_CODE_GALLERY = 200;
////
////    @Override
////    protected void onCreate(Bundle savedInstanceState) {
////        super.onCreate(savedInstanceState);
////        setContentView(R.layout.activity_profile);
////
////        setActionBarTitle("Profile");
////        disableActionBarPicture();
////
////        systemFunction = new SystemFunction(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, SystemClient.class), getApplicationContext());
////
////        loadImageView();
////        loadPayment(this);
////        loadCredentialPicturesGallery();
////        loadReviewsSection();
////
////        logoutButton = findViewById(R.id.logout_button);
////
////        logoutButton.setOnClickListener(new View.OnClickListener(){
////            @Override
////            public void onClick(View view) {
////                initiateLogout();
////            }
////        });
////
////        systemFunction.getSystemArrayConstants(this);
////    }
////
////    private void loadPayment(ProfileActivity profileActivity){
////        subscriptionCard = findViewById(R.id.subscriptionCard);
////        subscriptionButton = findViewById(R.id.subscriptionButton);
////        subscriptionDescription = findViewById(R.id.subscriptionDescription);
////        subscriptionImageView = findViewById(R.id.subscriptionImageView);
////
////        if(Functions.isTutor()){
////            subscriptionCard.setOnTouchListener(new View.OnTouchListener(){
////                @Override
////                public boolean onTouch(View view, MotionEvent motionEvent) {
////                    if(motionEvent.getAction()==MotionEvent.ACTION_UP){
////                        Intent intent = new Intent(profileActivity, PaymentActivity.class);
////                        startActivity(intent);
////
////                        return true;
////                    }
////                    return true;
////                }
////            });
////        }else{
////            subscriptionCard.getLayoutParams().height=0;
////            subscriptionCard.setVisibility(View.INVISIBLE);
////        }
////    }
////
////    private void loadCredentialPicturesGallery(){
////        mGalleryView = (PlaceHolderView)findViewById(R.id.galleryView);
////        credentialPicturesSpinKitView = findViewById(R.id.credentialPicturesSpinKitView);
////        credentialPicturesSpinKitSprite = new CubeGrid();
////        credentialPicturesSpinKitSprite.stop();
////        credentialPicturesSpinKitView.setVisibility(View.INVISIBLE);
////
////        addCredentialPictureButton = (Button)findViewById(R.id.btnAddCredentialPicture);
////        addCredentialPictureButton.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View view) {
////                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
////                intent.setType("image/jpeg");
////
////                try {
////                    startActivityForResult(intent, INTENT_REQUEST_CODE_GALLERY);
////
////                } catch (ActivityNotFoundException e) {
////
////                    e.printStackTrace();
////                }
////            }
////        });
////
////        getCredentialPictures();
////    }
////
////    public void getCredentialPictures(){
////        userClient = RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE,Constants.TOKEN_VALUE,UserClient.class);
////        userFunctions = new UserFunctions(userClient,this);
////        userFunctions.getCredentialPictures(this, Constants.USER_ID_VALUE);
////    }
////
////    public void setGallery(List<String> credentialPictures){
////        credentialPicturesSpinKitSprite.stop();
////        credentialPicturesSpinKitView.setVisibility(View.INVISIBLE);
////
////        mGalleryView.removeAllViews();
////        mGalleryView.addView(new ImageTypeSmallList(this.getApplicationContext(),credentialPictures));
////    }
////
////    public void deleteCredentialPicture(String credentialPictureUrl){
////        userClient = RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE,Constants.TOKEN_VALUE,UserClient.class);
////        userFunctions = new UserFunctions(userClient, this);
////        userFunctions.deleteCredentialPicture(this, Constants.USER_ID_VALUE, credentialPictureUrl);
////    }
////
////    public void loadFragment(){
////        Log.e("LoadFragment", "HERE");
////
////        if(!Functions.isTutor()){
////            if(parentStudentFragment == null){
////                parentStudentFragment = new com.hyptastic.poratechai.fragments.profile.parent_student.ViewProfileFragment();
////            }
////            if(!isFinishing()){
////                getFragmentManager().beginTransaction().replace(R.id.fragmentFrame,
////                        parentStudentFragment, com.hyptastic.poratechai.fragments.profile.parent_student.ViewProfileFragment.TAG).commit();
////            }
////        }
////        else{
////            if(tutorFragment == null){
////                tutorFragment = new com.hyptastic.poratechai.fragments.profile.tutor.ViewProfileFragment();
////            }
////            if(!isFinishing()){
////                getFragmentManager().beginTransaction().replace(R.id.fragmentFrame,
////                        tutorFragment, com.hyptastic.poratechai.fragments.profile.tutor.ViewProfileFragment.TAG).commit();
////            }
////        }
////    }
////
////    //Profile Picture - start ----------------------------------------------------------------------------------------------------
////    private void loadImageView(){
////
////        fullNameTextView = findViewById(R.id.full_name_text_view);
////        institutionTextView = findViewById(R.id.institution_text_view);
////        profilePictureImageView = findViewById(R.id.profile_picture_image_view);
////
////        profilePictureSpinKitView = findViewById(R.id.profilePictureSpinKitView);
////        profilePictureSpinKitSprite = new CubeGrid();
////        profilePictureSpinKitSprite.stop();
////        profilePictureSpinKitView.setVisibility(View.INVISIBLE);
////
////        Functions.loadPicture(getApplicationContext(), Constants.USER_ID_VALUE, new Constants.PICTURE_PACKET(this, profilePictureImageView, null, 256, 256));
////
////        profilePictureImageView.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View view) {
////                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
////                intent.setType("image/jpeg");
////
////                try {
////                    startActivityForResult(intent, INTENT_REQUEST_CODE);
////
////                } catch (ActivityNotFoundException e) {
////
////                    e.printStackTrace();
////                }
////            }
////        });
////
////        fullNameTextView.setText(Constants.USER_NAME_VALUE);
////
////        if(!Functions.isTutor()){
////            institutionTextView.setText(null);
////        }
////        else{
////            institutionTextView.setText(Constants.INSTITUTION_VALUE);
////        }
////    }
////
////    @Override
////    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
////        int dataSize=0;
////
////        if (requestCode == INTENT_REQUEST_CODE) {
////
////            if (resultCode == RESULT_OK) {
////
////                try {
////                    Uri uri  = data.getData();
////                    String scheme = uri.getScheme();
////                    InputStream fileInputStream=getContentResolver().openInputStream(uri);
////
////                    if(scheme.equals(ContentResolver.SCHEME_CONTENT))
////                    {
////                        try {
////                            dataSize = fileInputStream.available()/1024;
////                            if(dataSize > 4096){
////                                System.out.println("File size in bytes: "+ dataSize);
////                                Toast.makeText(this,"Image size cannot exceed 4MB",Toast.LENGTH_LONG).show();
////                            }
////                            else {
////                                System.out.println("ELSE File size in bytes: "+ dataSize);
////                                uploadImage(getBytes(fileInputStream), 1);
////                            }
////                        } catch (Exception e) {
////                            e.printStackTrace();
////                        }
////                    }
////                    else if(scheme.equals(ContentResolver.SCHEME_FILE))
////                    {
////                        String path = uri.getPath();
////                        File f= null;
////                        try {
////                            f = new File(path);
////                        } catch (Exception e) {
////                            e.printStackTrace();
////                        }
////                        System.out.println("File size in bytes"+f.length());
////                    }
////
////                } catch (IOException e) {
////                    e.printStackTrace();
////                }
////            }
////        }
////
////        if (requestCode == INTENT_REQUEST_CODE_GALLERY) {
////
////            if (resultCode == RESULT_OK) {
////
////                try {
////
////                    Uri uri  = data.getData();
////                    String scheme = uri.getScheme();
////                    InputStream fileInputStream=getContentResolver().openInputStream(uri);
////
//////                    InputStream is = getContentResolver().openInputStream(data.getData());
////
////                    if(scheme.equals(ContentResolver.SCHEME_CONTENT)){
////                        try {
////                            dataSize = fileInputStream.available()/1024;
////                            if(dataSize > 4096){
////                                System.out.println("File size in bytes: "+ dataSize);
////                                Toast.makeText(this,"Image size cannot exceed 4MB",Toast.LENGTH_LONG).show();
////                            }
////                            else {
////                                System.out.println("ELSE File size in bytes: "+ dataSize);
////                                uploadImage(getBytes(fileInputStream), 0);
////                            }
////                        } catch (Exception e) {
////                            e.printStackTrace();
////                        }
////                    }
////                    else if(scheme.equals(ContentResolver.SCHEME_FILE)){
////                        String path = uri.getPath();
////                        File f= null;
////                        try {
////                            f = new File(path);
////                        } catch (Exception e) {
////                            e.printStackTrace();
////                        }
////                    }
////                } catch (IOException e) {
////                    e.printStackTrace();
////                }
////            }
////        }
////    }
////
////    public byte[] getBytes(InputStream is) throws IOException {
////        ByteArrayOutputStream byteBuff = new ByteArrayOutputStream();
////
////        int buffSize = 1024;
////        byte[] buff = new byte[buffSize];
////
////        int len = 0;
////        while ((len = is.read(buff)) != -1) {
////            byteBuff.write(buff, 0, len);
////        }
////
////        return byteBuff.toByteArray();
////    }
////
////    private void uploadImage(byte[] imageBytes, int flag){
////        userClient = RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE,Constants.TOKEN_VALUE,UserClient.class);
////        userFunctions = new UserFunctions(userClient,this);
////
////        if(flag == 1){
////            profilePictureSpinKitSprite = new CubeGrid();
////            profilePictureSpinKitView.setIndeterminateDrawable(profilePictureSpinKitSprite);
////            profilePictureSpinKitView.setVisibility(View.VISIBLE);
////            userFunctions.uploadProfilePicture(this, imageBytes);
////        }
////        else if(flag == 0){
////            credentialPicturesSpinKitSprite = new CubeGrid();
////            credentialPicturesSpinKitView.setIndeterminateDrawable(credentialPicturesSpinKitSprite);
////            credentialPicturesSpinKitView.setVisibility(View.VISIBLE);
////            userFunctions.uploadCredentialPicture(this, imageBytes);
////         }
////    }
////
////    public void getProfilePicture(String profilePicturePath){
////        profilePictureSpinKitSprite.stop();
////        profilePictureSpinKitView.setVisibility(View.INVISIBLE);
////
////        String url = profilePicturePath;
////        Functions.storeProfilePictureToSharedPreferences(url);
////
////        Glide.with(this)
////                .load(url)
////                .apply(new RequestOptions()
////                        .placeholder(R.drawable.ic_blank_profile_picture)
////                        .override(128, 128)
////                        .centerCrop())
////                .apply(RequestOptions.circleCropTransform())
////                .into(profilePictureImageView);
////
////        reloadActionBarPicture();
////    }
////
////    public void setFullNameTextView(String fullName){
////        fullNameTextView = findViewById(R.id.full_name_text_view);
////        fullNameTextView.setText(fullName);
////    }
////
////    public void setInstitutionTextView(String institution){
////        institutionTextView = findViewById(R.id.institution_text_view);
////        institutionTextView.setText(institution);
////    }
////    //Profile Picture - end -----------------------------------------------------------------------------------------------------
////
////    //Review - start ------------------------------------------------------------------------------------------------------------
////    private void loadReviewsSection(){
////        reviewSpinKitView = findViewById(R.id.reviewSpinKitView);
////        reviewSpinKitSprite = new CubeGrid();
////        reviewSpinKitView.setIndeterminateDrawable(reviewSpinKitSprite);
////
////        reviewTitleTextView = findViewById(R.id.profile_review_title_textview);
////        reviewRatingBar = findViewById(R.id.profile_review_ratingbar);
////        reviewRecyclerView = findViewById(R.id.profile_review_recyclerView);
////        leftButton = findViewById(R.id.reviewLeftButton);
////        rightButton = findViewById(R.id.reviewRightButton);
////
////        profileReviewAdapter = new ProfileReviewAdapter(this, reviewRatingBar);
////        reviewLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
////
////        reviewRecyclerView.setOnTouchListener(Functions.getNonSwipeableViewTouchListener());
////
////        reviewRecyclerView.setLayoutManager(reviewLayoutManager);
////        reviewRecyclerView.setAdapter(profileReviewAdapter);
////        recyclerViewCounter= 0;
////
////        if(Functions.isTutor()){
////            profileReviewAdapter.loadReviews(this, Constants.USER_ID_VALUE, true);
////        }else{
////            profileReviewAdapter.loadReviews(this, Constants.USER_ID_VALUE, false);
////        }
////
////        leftButton.setOnClickListener(new View.OnClickListener(){
////            @Override
////            public void onClick(View view) {
////                movePrevious();
////            }
////        });
////
////        rightButton.setOnClickListener(new View.OnClickListener(){
////            @Override
////            public void onClick(View view) {
////                moveNext();
////            }
////        });
////    }
////
////    public void reviewsLoaded(){
////        reviewSpinKitSprite.stop();
////        reviewSpinKitView.setVisibility(View.INVISIBLE);
////    }
////
////    public void moveNext(){
////        if(recyclerViewCounter<profileReviewAdapter.getItemCount()-1){
////            recyclerViewCounter++;
////            reviewRecyclerView.smoothScrollToPosition(recyclerViewCounter);
////        }
////    }
////
////    public void movePrevious(){
////        if(recyclerViewCounter>0){
////            recyclerViewCounter--;
////            reviewRecyclerView.smoothScrollToPosition(recyclerViewCounter);
////        }
////    }
////    //Review - end --------------------------------------------------------------------------------------------------------------
////
////    //Logout - start ------------------------------------------------------------------------------------------------------------
////    private void initiateLogout() {
////        userClient = RetrofitServiceGenerator.getRetrofit();
////        userFunctions = new UserFunctions(userClient, getApplicationContext());
////        userFunctions.logoutUser(this);
////    }
////
////    public void logout(){
////        Functions.clearLoginDataFromSharedPreferences();
////
////        Intent intent = new Intent(this, AuthenticationActivity.class);
////        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
////        startActivity(intent);
////
////        Toast.makeText(this, "Logged Out Successfully", Toast.LENGTH_LONG).show();
////        finish();
////    }
////    //Logout - end --------------------------------------------------------------------------------------------------------------
//}
