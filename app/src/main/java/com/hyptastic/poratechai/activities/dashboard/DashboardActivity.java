//package com.hyptastic.poratechai.activities.dashboard;
//
//import android.content.Intent;
//import android.support.design.widget.Snackbar;
//import android.support.v7.app.AppCompatActivity;
//import android.os.Bundle;
//import android.view.View;
//import android.widget.Button;
//import android.widget.CompoundButton;
//import android.widget.ImageView;
//import android.widget.Switch;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.hyptastic.poratechai.activities.PorateChaiActivity;
//import com.hyptastic.poratechai.activities.job_feed.JobFeedActivity;
//import com.hyptastic.poratechai.activities.stats.ParentStatsActivity;
//import com.hyptastic.poratechai.activities.profile.ProfileActivity;
//import com.hyptastic.poratechai.R;
//import com.hyptastic.poratechai.activities.stats.TutorStatsActivity;
//import com.hyptastic.poratechai.fragments.login_registration.ConfirmCodeDialog;
//import com.hyptastic.poratechai.utils.Constants;
//import com.hyptastic.poratechai.utils.Functions;
//
//public class DashboardActivity extends AppCompatActivity implements ConfirmCodeDialog.Listener {
//    private ImageView dashboardProfilePicture;
//    private TextView dashboardProfileName, dashboardProfileParentTutor;
//    private Button mBtJobFeed;
//    private Button mBtProfile;
//    private Button mBtStatistics;
//    private Button mBtSettings;
//    private Button mBtSupport;
//    private Switch switchTutorOrHire;
//
//    private CompoundButton.OnCheckedChangeListener switchTutorOrHireListener = null;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//        Functions.initSharedPreferences(getApplicationContext());
//        Functions.loadLoginDataFromSharedPreferences();
//        Functions.getScreenDimensions(getWindowManager());
//
//        if(Constants.IS_VERIFIED_VALUE.equals("false")){
//            showConfirmCodeDialog();
//        }
//
//        setContentView(R.layout.activity_dashboard);
//        initViews(this);
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//
//        Functions.loadProfilePicture(this, Constants.USER_ID_VALUE, new Constants.PICTURE_PACKET(this, dashboardProfilePicture, null, 256, 256));
//        dashboardProfileName.setText(Constants.USER_NAME_VALUE);
//    }
//
//    private void initViews(final DashboardActivity currentDashboardActivity) {
//        dashboardProfilePicture = findViewById(R.id.dashboard_profile_picture);
//        dashboardProfileName = findViewById(R.id.dashboard_profile_name);
//        dashboardProfileParentTutor = findViewById(R.id.dashboard_profile_parent_tutor);
//        switchTutorOrHire = (Switch)findViewById(R.id.switch_tutor_or_hire);
//
//        setSwitch(Functions.isTutor());
//
//        switchTutorOrHireListener = new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
//                setSwitch(isChecked);
//
//                Functions.setIsTutorOnNetwork(isChecked, getApplicationContext(), currentDashboardActivity);
//            }
//        };
//        switchTutorOrHire.setOnCheckedChangeListener(switchTutorOrHireListener);
//
//        dashboardProfileName.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View view) {
//                profile();
//            }
//        });
//
//        dashboardProfilePicture.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View view) {
//                profile();
//            }
//        });
//
//        mBtJobFeed = (Button)findViewById(R.id.button_job_feed);
//        mBtProfile = (Button)findViewById(R.id.button_profile);
//        mBtStatistics = (Button)findViewById(R.id.button_statistics);
//        mBtSupport = (Button)findViewById(R.id.button_support);
//
//        mBtJobFeed.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                jobFeed();
//            }
//        });
//        mBtProfile.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                profile();
//            }
//        });
//        mBtStatistics.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                statistics();
//            }
//        });
//        mBtSupport.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                support();
//            }
//        });
//
//        Functions.loadProfilePicture(this, Constants.USER_ID_VALUE, new Constants.PICTURE_PACKET(this, dashboardProfilePicture, null, 256, 256));
//        dashboardProfileName.setText(Constants.USER_NAME_VALUE);
//    }
//
//    private void jobFeed(){
//        Intent intent = new Intent(this, JobFeedActivity.class);
//        startActivity(intent);
//    }
//
//    private void profile(){
//        Intent intent = new Intent(this, ProfileActivity.class);
//        startActivity(intent);
//    }
//
//    private void statistics(){
//        Toast.makeText(this,"Statistics Called",Toast.LENGTH_LONG).show();
//        if(Functions.isTutor()){
//            Intent intent = new Intent(this, TutorStatsActivity.class);
//            startActivity(intent);
//        } else {
//            Intent intent = new Intent(this, ParentStatsActivity.class);
//            startActivity(intent);
//        }
//    }
//
//    private void support(){
//        Intent intent = new Intent(this, PorateChaiActivity.class);
//        startActivity(intent);
//    }
//
//    //Tutor and Parent/Student Switch - Start ---------------------------------------------------------------------
//
//    public void switchTutorParentResponse(boolean isTutor, boolean isSuccessful){
//        if(isSuccessful){
//            Functions.setIsTutor(isTutor);
//        }else{
//            switchTutorOrHire.setOnCheckedChangeListener(null);
//            setSwitch(!isTutor);
//            switchTutorOrHire.setOnCheckedChangeListener(switchTutorOrHireListener);
//        }
//    }
//
//    public void setSwitch(boolean isTutor){
//        if(isTutor==true){
//            switchTutorOrHire.setChecked(true);
//            switchTutorOrHire.setTextOn("Tutor");
//            switchTutorOrHire.setText("Tutor");
//
//            dashboardProfileParentTutor.setText("Tutor");
//        }else{
//            switchTutorOrHire.setChecked(false);
//            switchTutorOrHire.setTextOff("Parent");
//            switchTutorOrHire.setText("Parent");
//
//            dashboardProfileParentTutor.setText("Parent/Student");
//        }
//    }
//
//    //Tutor and Parent/Student Switch - Stop ----------------------------------------------------------------------
//
//    public void showConfirmCodeDialog(){
//        ConfirmCodeDialog fragment = new ConfirmCodeDialog();
//
//        fragment.show(getFragmentManager(), ConfirmCodeDialog.TAG);
//    }
//
//    @Override
//    public void onConfirmCode(String message) {
//        showSnackBarMessage(message);
//    }
//
//    private void showSnackBarMessage(String message) {
//        Snackbar.make(findViewById(R.id.activity_dashboard),message,Snackbar.LENGTH_SHORT).show();
//    }
//}
