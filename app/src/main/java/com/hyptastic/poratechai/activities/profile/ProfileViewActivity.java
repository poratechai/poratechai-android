package com.hyptastic.poratechai.activities.profile;

import android.app.Activity;
import android.os.Parcel;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.ybq.android.spinkit.SpinKitView;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.CubeGrid;
import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.activities.ActionBarBackActivity;
import com.hyptastic.poratechai.activities.ActionBarProfileViewActivity;
import com.hyptastic.poratechai.adapters.profile.ProfileAdapter;
import com.hyptastic.poratechai.fragments.job_feed.parent_student.dialog.ConfirmAcceptInspectionTutorDialog;
import com.hyptastic.poratechai.fragments.profile.NavigationFragment;
import com.hyptastic.poratechai.retrofit.functions.ContractFunction;
import com.hyptastic.poratechai.retrofit.functions.SystemFunction;
import com.hyptastic.poratechai.retrofit.functions.UserFunctions;
import com.hyptastic.poratechai.retrofit.interfaces.ContractClient;
import com.hyptastic.poratechai.retrofit.interfaces.SystemClient;
import com.hyptastic.poratechai.retrofit.interfaces.UserClient;
import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
import com.hyptastic.poratechai.utils.Constants;
import com.hyptastic.poratechai.utils.Functions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

public class ProfileViewActivity extends ActionBarProfileViewActivity implements ConfirmAcceptInspectionTutorDialog.ConfirmAcceptInspectionTutorListener {
    ProfileViewActivity currentProfileViewActivity;

    ImageView profileImageView, verifiedTickImageView;
    TextView profileNameTextView, verifiedTextView;
    TabLayout profileTabs;
    ViewPager profilePager;

    ArrayList<Fragment> tutorProfileFragments, parentProfileFragments;
    String[] tutorProfileTitles = new String[]{"Profile", "Stats", "Reviews"};
    String[] parentProfileTitles = new String[]{"Profile", "Stats", "Reviews"};
    ProfileAdapter tutorProfileAdapter, parentProfileAdapter;

    boolean isTutor;
    String name, userId, matchId;
    int contractStatus;

    SpinKitView loadingSpinner = null;
    Sprite loadingSprite = null;

    private SystemFunction systemFunction;
    private UserFunctions userFunctions;
    private ContractFunction contractFunction;

    NavigationFragment.LoadListener loadListener = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_navigation_profile);

        isTutor = getIntent().getExtras().getBoolean("IS_TUTOR");
        name = getIntent().getExtras().getString("USER_NAME");
        userId = getIntent().getExtras().getString("USER_ID");
        contractStatus = getIntent().getExtras().getInt("CONTRACT_STATUS");
        matchId = getIntent().getExtras().getString("MATCH_ID");

        currentProfileViewActivity = this;

        profileImageView        = (ImageView)findViewById(R.id.profileImageView);
        profileNameTextView     = (TextView)findViewById(R.id.profileNameTextView);
        verifiedTextView        = (TextView)findViewById(R.id.verifiedTextView);
        verifiedTickImageView   = (ImageView)findViewById(R.id.verifiedTickImageView);

        profileTabs = (TabLayout)findViewById(R.id.profileTabs);
        profilePager = (ViewPager)findViewById(R.id.profilePager);

        loadingSpinner = (SpinKitView)findViewById(R.id.profileLoadingSpinKit);
        loadingSprite = new CubeGrid();
        loadingSpinner.setIndeterminateDrawable(loadingSprite);

        profilePager.setOffscreenPageLimit(3);

        systemFunction = new SystemFunction(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, SystemClient.class), this);
        userFunctions = new UserFunctions(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE,Constants.TOKEN_VALUE,UserClient.class), this);
        contractFunction = new ContractFunction(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, ContractClient.class), this);

        profileNameTextView.setText(name);
        Functions.loadProfilePicture(this, userId, new Constants.PICTURE_PACKET(this, profileImageView, null, Constants.PICTURE_PACKET.PROFILE_PICTURE_WIDTH, Constants.PICTURE_PACKET.PROFILE_PICTURE_HEIGHT));
        getVerificationStatus(userId);

        //showing accept button on the action bar
        if(isTutor && matchId.length()>0 && contractStatus<=Constants.JOB_STATUS.JOB_STATUS_CONTRACT_REVIEW){
            setAcceptButtonListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    ConfirmAcceptInspectionTutorDialog confirmAcceptInspectionTutorDialog = ConfirmAcceptInspectionTutorDialog.createNewInstance(currentProfileViewActivity, matchId);
                    confirmAcceptInspectionTutorDialog.show((currentProfileViewActivity).getFragmentManager(), "ConfirmAcceptInspectionTutorDialog");
                }
            });
        }

        loadListener = new NavigationFragment.LoadListener() {
            @Override
            public void loadFragment(boolean isStart) {
                if(isStart){
                    profilePager.setVisibility(View.INVISIBLE);

                    loadingSpinner.setVisibility(View.VISIBLE);
                    loadingSprite.start();
                }else{
                    profilePager.setVisibility(View.VISIBLE);

                    loadingSpinner.setVisibility(View.INVISIBLE);
                    loadingSprite.stop();
                }
            }

            @Override
            public int describeContents() { return 0; }

            @Override
            public void writeToParcel(Parcel parcel, int i) { }
        };

        if(isTutor){
            loadTutor(userId, contractStatus);
        }else{
            loadParentStudent(userId, contractStatus);
        }
    }

//---------------------------------------------------------------------------------------------------------------------
    //Verification Status - start
    private void getVerificationStatus(String userId){
        verifiedTextView.setText("CHECKING ...");
        verifiedTickImageView.setImageResource(android.R.color.transparent);

        userFunctions.getVerificationStatus(this, userId);
    }

    public void verificationStatusGotten(boolean isGetSuccessful, boolean isVerifiedByVerificationImages){
        if(isGetSuccessful){
            if(isVerifiedByVerificationImages){
                verifiedTextView.setText("VERIFIED");
                verifiedTickImageView.setImageResource(R.drawable.ic_verified);
            }else{
                verifiedTextView.setText("NOT VERIFIED");
                verifiedTickImageView.setImageResource(R.drawable.ic_not_verified);
            }
        }else{
            verifiedTextView.setText("CHECKING ...");
            verifiedTickImageView.setImageResource(android.R.color.transparent);
        }
    }
    //Verification Status - end
//---------------------------------------------------------------------------------------------------------------------

    public void loadTutor(String userId, int contractStatus){
        loadListener.loadFragment(true);

        tutorProfileFragments = new ArrayList<>(Arrays.asList(
                com.hyptastic.poratechai.fragments.profile.tutor.profile.ProfileFragment.createInstance(loadListener, userId, contractStatus),
                com.hyptastic.poratechai.fragments.profile.tutor.stats.StatsFragment.createInstance(userId),
                com.hyptastic.poratechai.fragments.profile.tutor.reviews.ReviewsFragment.createInstance(userId)));
        tutorProfileAdapter = new ProfileAdapter(this.getSupportFragmentManager(), tutorProfileFragments, tutorProfileTitles);

        profilePager.setAdapter(tutorProfileAdapter);
        profileTabs.setupWithViewPager(profilePager);
    }

    public void loadParentStudent(String userId, int contractStatus){
        loadListener.loadFragment(true);

        parentProfileFragments = new ArrayList<>(Arrays.asList(
                com.hyptastic.poratechai.fragments.profile.parent_student.profile.ProfileFragment.createInstance(loadListener, userId, contractStatus),
                com.hyptastic.poratechai.fragments.profile.parent_student.stats.StatsFragment.createInstance(userId),
                com.hyptastic.poratechai.fragments.profile.parent_student.reviews.ReviewsFragment.createInstance(userId)));
        parentProfileAdapter = new ProfileAdapter(this.getSupportFragmentManager(), parentProfileFragments, parentProfileTitles);

        profilePager.setAdapter(parentProfileAdapter);
        profileTabs.setupWithViewPager(profilePager);
    }

//--------------------------------------------------------------------------------------------------------------------

    @Override
    public void onAcceptInspectionTutorPositive(String matchId) {
        contractFunction.acceptTutor(currentProfileViewActivity, matchId);

        acceptButton.setEnabled(false);
    }

    public void tutorAccepted(boolean isSuccessful){
        if(isSuccessful){
            loadTutor(userId, Constants.JOB_STATUS.JOB_STATUS_INSPECTION);
        }else{
            acceptButton.setEnabled(true);
        }
    }
}
