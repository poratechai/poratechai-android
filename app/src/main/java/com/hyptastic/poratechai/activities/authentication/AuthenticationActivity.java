package com.hyptastic.poratechai.activities.authentication;


import android.content.Intent;

import android.support.v7.app.AppCompatActivity;
import android.support.design.widget.Snackbar;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.fragments.login_registration.ConfirmCodeDialog;
import com.hyptastic.poratechai.fragments.login_registration.LoginFragment;
import com.hyptastic.poratechai.fragments.login_registration.ResetPasswordDialog;

public class AuthenticationActivity extends AppCompatActivity implements ResetPasswordDialog.Listener, ConfirmCodeDialog.Listener {
    public static final String TAG = AuthenticationActivity.class.getSimpleName();

    private LoginFragment mLoginFragment;
    private ResetPasswordDialog mResetPasswordDialog;
    private ConfirmCodeDialog mConfirmCodeDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentication);

        if (savedInstanceState == null) {
            loadFragment();
        }
    }

    @Override
    public void onBackPressed() {

        int count = getFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            super.onBackPressed();
        }
        else{
            getFragmentManager().popBackStack();
        }

    }

    private void loadFragment(){
        if (mLoginFragment == null) {
            mLoginFragment = new LoginFragment();
        }
        getFragmentManager().beginTransaction().replace(R.id.fragmentFrame,mLoginFragment,LoginFragment.TAG).commit();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        String data = intent.getData().getLastPathSegment();
        Log.d(TAG, "onNewIntent: "+data);

        mResetPasswordDialog = (ResetPasswordDialog) getFragmentManager().findFragmentByTag(ResetPasswordDialog.TAG);
        mConfirmCodeDialog = (ConfirmCodeDialog) getFragmentManager().findFragmentByTag(ConfirmCodeDialog.TAG);

        if (mResetPasswordDialog != null)
            mResetPasswordDialog.setToken(data);
    }

    @Override
    public void onPasswordReset(String message) {
        showSnackBarMessage(message);
    }

    @Override
    public void onConfirmCode(String message) {
        showSnackBarMessage(message);
    }

    private void showSnackBarMessage(String message) {
        Snackbar.make(findViewById(R.id.activity_authentication),message,Snackbar.LENGTH_SHORT).show();
    }
}
