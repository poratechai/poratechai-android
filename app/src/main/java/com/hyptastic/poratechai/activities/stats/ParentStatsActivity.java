//package com.hyptastic.poratechai.activities.stats;
//
//import android.content.Context;
//import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
//import android.widget.TextView;
//
//import com.hyptastic.poratechai.R;
//import com.hyptastic.poratechai.models.users.ParentStat;
//import com.hyptastic.poratechai.retrofit.functions.StatsFunction;
//import com.hyptastic.poratechai.retrofit.interfaces.StatClient;
//import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
//import com.hyptastic.poratechai.utils.Constants;
//
//public class ParentStatsActivity extends AppCompatActivity {
//    private Context context;
//    private TextView postedJobs;
//    private TextView contractRequests;
//    private TextView rating;
//    private TextView tutorsEmployed;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_stats_parent);
//
//        context = getApplicationContext();
//        postedJobs = findViewById(R.id.postedJobs);
//        contractRequests = findViewById(R.id.contractRequestsNumber);
//        rating = findViewById(R.id.rating);
//        tutorsEmployed = findViewById(R.id.tutorsEmployed);
//
//        getStats();
//    }
//
//    private void getStats(){
//        StatClient statClient = RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, StatClient.class);
//        StatsFunction statFunction = new StatsFunction(statClient, context);
//        statFunction.getParentStats(this);
//    }
//
//    public void setStats(ParentStat stats){
//        System.out.println("stats are: " + stats.toString());
//        postedJobs.setText(String.valueOf(stats.getpostedJobs()));
//        contractRequests.setText(String.valueOf(stats.getcontractRequests()));
//        rating.setText(String.valueOf(stats.getRating()));
//        tutorsEmployed.setText(String.valueOf(stats.gettutorsEmployed()));
//    }
//}
