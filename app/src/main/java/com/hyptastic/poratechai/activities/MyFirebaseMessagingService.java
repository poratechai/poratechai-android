package com.hyptastic.poratechai.activities;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.utils.Constants;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    private static int notificationNumber = 0;

    public MyFirebaseMessagingService() {
        FirebaseMessaging.getInstance().subscribeToTopic("matches");
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.e("FirebaseMessaging", "Created");
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // ...

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {

            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            Map<String, String> data = remoteMessage.getData();

            sendNotification(data);

            if(data.containsKey("fragmentType") &&
                    Integer.parseInt(data.get("fragmentType"))==Constants.NOTIFICATION_DATA_FRAGMENT_TYPE.JOB_FEED){
                updateFeed(data);
            }

            if (true) /* Check if data needs to be processed by long running job */ {
                scheduleJob();      // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
            } else {
                handleNow();        // Handle message within 10 seconds
            }

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    private void scheduleJob() {
        // [START dispatch_job]
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
        Job myJob = dispatcher.newJobBuilder()
                .setService(MyJobService.class)
                .setTag("my-job-tag")
                .build();
        dispatcher.schedule(myJob);
        // [END dispatch_job]
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

    private void sendNotification(Map<String, String> data) {
        int fragmentType = (data.containsKey("fragmentType")?Integer.parseInt(data.get("fragmentType")):2);    //set from server's notification module
        int feedType = (data.containsKey("feedType")?Integer.parseInt(data.get("feedType")):0);

        Log.e("Notification", "Fragment: " + String.valueOf(fragmentType) + " Feed: " + String.valueOf(feedType));

        Intent intent = new Intent(this, PorateChaiActivity.class);
        intent.putExtra("fragmentType", fragmentType);
        intent.putExtra("feedType", feedType);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.pc_launcher)
                        .setContentTitle(data.get("title"))
                        .setContentText(data.get("body"))
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true)
                        .setLights(Color.parseColor("#009688"), 1000, 100)
                        .setSound(defaultSoundUri);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify((notificationNumber++) /* ID of notification */, notificationBuilder.build());
    }

    private void updateFeed(Map<String, String> data) {
        Log.e("Operation Type", data.get("operationType"));
        Log.e("Feed Type", data.get("feedType"));
        Log.e("Job/Match Id", data.get("jobIdOrMatchId"));

        int operationType = Integer.parseInt(data.get("operationType"));
        int feedType = Integer.parseInt(data.get("feedType"));
        String jobIdOrMatchId = data.get("jobIdOrMatchId");

        Intent intent = new Intent();

        switch(feedType)
        {
            case Constants.NOTIFICATION_DATA_FEED_TYPE.POSTED_JOB:
                intent.setAction("update_feed_posted_job");
                break;
            case Constants.NOTIFICATION_DATA_FEED_TYPE.ACTIVE_POSTED_JOB:
                intent.setAction("update_feed_active_posted_job");
                break;
            case Constants.NOTIFICATION_DATA_FEED_TYPE.POSTED_JOB_HISTORY:
                intent.setAction("update_feed_posted_job_history");
                break;
            case Constants.NOTIFICATION_DATA_FEED_TYPE.MATCH:
                intent.setAction("update_feed_match");
                break;
            case Constants.NOTIFICATION_DATA_FEED_TYPE.ACTIVE_JOB:
                intent.setAction("update_feed_active_job");
                break;
            case Constants.NOTIFICATION_DATA_FEED_TYPE.JOB_HISTORY:
                intent.setAction("update_feed_job_history");
                break;
        }

        intent.putExtra("operationType", operationType);
        intent.putExtra("Id", jobIdOrMatchId);

        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}
