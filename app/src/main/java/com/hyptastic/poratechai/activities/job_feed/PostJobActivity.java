package com.hyptastic.poratechai.activities.job_feed;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.SpinKitView;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.CubeGrid;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.activities.ActionBarBackActivity;
import com.hyptastic.poratechai.adapters.SpinnerAdapters;
import com.hyptastic.poratechai.fragments.job_feed.parent_student.DialogFragmentSubjects;
import com.hyptastic.poratechai.layout.FlexboxLayout;
import com.hyptastic.poratechai.retrofit.functions.JobFunction;
import com.hyptastic.poratechai.models.job_feed.JobPost;
import com.hyptastic.poratechai.retrofit.functions.SystemFunction;
import com.hyptastic.poratechai.retrofit.interfaces.JobClient;
import com.hyptastic.poratechai.retrofit.interfaces.SystemClient;
import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
import com.hyptastic.poratechai.utils.Constants;
import com.hyptastic.poratechai.utils.Functions;

import java.util.ArrayList;

import retrofit2.http.POST;

public class PostJobActivity extends ActionBarBackActivity implements DialogFragmentSubjects.DialogFragmentSubjectsCloseListener{

    private static final int REQUEST_CODE_AUTOCOMPLETE = 1;

    ConstraintLayout postJobLayout;

    Spinner categorySpinner, subCategorySpinner, classSpinner, daysPerWeekSpinner, salarySpinner;
    LinearLayout subjectsFlexBoxLayout;
    View horizontalLineTwo;
    TextView location, classToTeachTextView, subjectsTextView;
    Button enterLocationButton, postJobBtn;
    SpinKitView spinKitView;

    Sprite spinKitSprite;

    FlexboxLayout flexboxLayout = null;

    String jobTitle, category, classToTeach, subCategory, tutorGenderPreference, address;
    String[] subjects;
    int daysPerWeek, salary;
    Double latitude, longitude;

    ArrayList<String> subjectsList = new ArrayList<String>();

    SpinnerAdapters subCategorySpinnerAdapters;

    ArrayAdapter<CharSequence> categoriesSpinnerAdapter, classToTeachSpinnerAdapter, daysPerWeekSpinnerAdapter, salarySpinnerAdapter;

    JobClient jobClient = null;
    JobFunction jobFunction = null;

    SystemFunction systemFunction = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try{
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_post_edit_job);

            super.setTitle("Post Job");

            tutorGenderPreference = "any";

            postJobLayout = findViewById(R.id.postJobLayout);
            postJobLayout.setVisibility(View.INVISIBLE);

            spinKitView = findViewById(R.id.spinKitView);
            spinKitSprite = new CubeGrid();
            spinKitView.setIndeterminateDrawable(spinKitSprite);

            systemFunction = new SystemFunction(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, SystemClient.class), getApplicationContext());
            systemFunction.getSystemArrayConstants(this);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void initView(){
        try{
            postJobLayout = findViewById(R.id.postJobLayout);
            categorySpinner = findViewById(R.id.categorySpinner);
            subjectsFlexBoxLayout = findViewById(R.id.subjectsFlexBoxLayout);
            horizontalLineTwo = findViewById(R.id.horizontal_line_two);
            subCategorySpinner = findViewById(R.id.subCategorySpinner);
            classToTeachTextView = findViewById(R.id.classToTeachTextView);
            classSpinner = findViewById(R.id.classToTeachSpinner);
            subjectsTextView = findViewById(R.id.subjectsTextView);
            daysPerWeekSpinner = findViewById(R.id.daysPerWeekSpinner);
            salarySpinner = findViewById(R.id.salary);
            enterLocationButton = findViewById(R.id.locationButton);
            location = findViewById(R.id.locationTextView);
            postJobBtn = findViewById(R.id.submitButton);

            spinKitSprite.stop();
            spinKitView.setVisibility(View.INVISIBLE);

            postJobLayout.setVisibility(View.VISIBLE);

            initAdapters();

            enterLocationButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openAutocompleteActivity();
                }
            });

            postJobBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    postJobBtn.setEnabled(false);

                    if(salary==0){
                        Toast.makeText(PostJobActivity.this, "Enter salary", Toast.LENGTH_SHORT).show();
                        postJobBtn.setEnabled(true);
                    } else if(latitude==null || longitude==null){
                        Toast.makeText(PostJobActivity.this, "Enter location", Toast.LENGTH_SHORT).show();
                        postJobBtn.setEnabled(true);
                    } else if(category.equals("Academics")){
                        if(subjectsList.size()==0){
                            Toast.makeText(PostJobActivity.this, "Choose subjects", Toast.LENGTH_SHORT).show();
                            postJobBtn.setEnabled(true);
                        } else if(classToTeach.equals("")){
                            Toast.makeText(PostJobActivity.this, "Choose class to teach", Toast.LENGTH_SHORT).show();
                            postJobBtn.setEnabled(true);
                        } else{
                            jobTitle = "Need Tutor for " + classToTeach + " " + category;
                            if(!category.equals("Academics")){
                                classToTeach = "";
                                subjectsList.clear();
                            }
                            subjects = subjectsList.toArray(new String[0]);
                            if(tutorGenderPreference==null){
                                tutorGenderPreference = "any";
                            }
                            address = location.getText().toString();

                            Functions.showBlockingFragment(PostJobActivity.this, true);

                            postJob();
                        }
                    } else {
                        jobTitle = "Need Tutor for " + category;

                        classToTeach = "";
                        subjectsList.clear();
                        subjects = subjectsList.toArray(new String[0]);

                        if(tutorGenderPreference == null){
                            tutorGenderPreference = "any";
                        }
                        address = location.getText().toString();

                        Functions.showBlockingFragment(PostJobActivity.this, true);

                        postJob();
                    }
                }
            });
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void initAdapters(){
        //------------------------------------------------------------------------------------------------------------------------
        //Category Spinner
        categoriesSpinnerAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, Constants.SYSTEM_ARRAY.categories.get(Constants.SYSTEM_ARRAY.CATEGORIES_KEY));
        categoriesSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        categorySpinner.setAdapter(categoriesSpinnerAdapter);

        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                category = String.valueOf(adapterView.getItemAtPosition(i));
                if(category.equals("Academics")){
                    subCategorySpinner.setAdapter(subCategorySpinnerAdapters.getArrayAdapter("Academics"));

                    ConstraintSet constraintSet = new ConstraintSet();
                    constraintSet.clone(postJobLayout);
                    constraintSet.connect(R.id.horizontal_line_two, ConstraintSet.TOP, R.id.subjectsTextView, ConstraintSet.BOTTOM);
                    constraintSet.applyTo(postJobLayout);

                    classToTeachTextView.setVisibility(View.VISIBLE);
                    subjectsTextView.setVisibility(View.VISIBLE);
                    classSpinner.setVisibility(View.VISIBLE);
                    subjectsFlexBoxLayout.setVisibility(View.VISIBLE);

                    refreshLayoutAfterAddingSubjects();
                }
                else{
                    subCategorySpinner.setAdapter(subCategorySpinnerAdapters.getArrayAdapter(category));

                    ConstraintSet constraintSet = new ConstraintSet();
                    constraintSet.clone(postJobLayout);
                    constraintSet.connect(R.id.horizontal_line_two, ConstraintSet.TOP, R.id.subCategoryTextView, ConstraintSet.BOTTOM);
                    constraintSet.applyTo(postJobLayout);

                    classToTeachTextView.setVisibility(View.INVISIBLE);
                    subjectsTextView.setVisibility(View.INVISIBLE);
                    classSpinner.setVisibility(View.INVISIBLE);
                    subjectsFlexBoxLayout.setVisibility(View.INVISIBLE);

                    ((ConstraintLayout.LayoutParams)horizontalLineTwo.getLayoutParams()).topMargin = 25;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        //Category Spinner
        //------------------------------------------------------------------------------------------------------------------------

        //------------------------------------------------------------------------------------------------------------------------
        //Subcategory Spinner
        subCategorySpinnerAdapters = new SpinnerAdapters(this);
        subCategorySpinnerAdapters.init();

        //subCategorySpinner.setAdapter is performed in categorySpinner.setOnItemSelectedListener
        subCategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                subCategory = String.valueOf(adapterView.getItemAtPosition(i));
                subCategorySpinner.setSelection(i);
                Toast.makeText(PostJobActivity.this, "selected", Toast.LENGTH_LONG);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        //Subcategory Spinner
        //------------------------------------------------------------------------------------------------------------------------

        //------------------------------------------------------------------------------------------------------------------------
        //ClassToTeach Spinner
        classToTeachSpinnerAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, Constants.SYSTEM_ARRAY.classesToTeach.get(Constants.SYSTEM_ARRAY.CLASSES_TO_TEACH_KEY));
        classToTeachSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        classSpinner.setAdapter(classToTeachSpinnerAdapter);
        classSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                classToTeach = adapterView.getItemAtPosition(i).toString();
                classSpinner.setSelection(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        //ClassToTeach Spinner
        //------------------------------------------------------------------------------------------------------------------------

        //------------------------------------------------------------------------------------------------------------------------
        //Subject Spinner
        try{
            flexboxLayout = new FlexboxLayout(this, subjectsList, subjectsFlexBoxLayout, Constants.SCREEN_WIDTH, true);

            View.OnClickListener onClickListener = new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    DialogFragmentSubjects dialogFragmentSubjects = DialogFragmentSubjects.createInstance(
                            Constants.SYSTEM_ARRAY.subjects, subjectsList);
                    dialogFragmentSubjects.show(getSupportFragmentManager(), "Preferred Subjects");
                }
            };

            flexboxLayout.setAddOrEditListener(onClickListener);

            refreshLayoutAfterAddingSubjects();
        }catch(Exception e){
            e.printStackTrace();
        }
        //Subject Spinner
        //------------------------------------------------------------------------------------------------------------------------

        //------------------------------------------------------------------------------------------------------------------------
        //DaysPerWeek Spinner
        daysPerWeekSpinnerAdapter = ArrayAdapter.createFromResource(this, R.array.days_per_week_array, android.R.layout.simple_spinner_item);
        daysPerWeekSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        daysPerWeekSpinner.setAdapter(daysPerWeekSpinnerAdapter);
        daysPerWeekSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                daysPerWeek = i+1;
                daysPerWeekSpinner.setSelection(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        //DaysPerWeek Spinner
        //------------------------------------------------------------------------------------------------------------------------

        //------------------------------------------------------------------------------------------------------------------------
        //Salary Spinner
        salarySpinnerAdapter = ArrayAdapter.createFromResource(this, R.array.salary_array, android.R.layout.simple_spinner_item);
        salarySpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        salarySpinner.setAdapter(salarySpinnerAdapter);
        salarySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                salary = -1000 + (i+1)*Constants.SALARY_DIFFERENCE;
                salarySpinner.setSelection(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        //Salary Spinner
        //------------------------------------------------------------------------------------------------------------------------
    }

//    @Override
    public void dialogFragmentSubjectsClosed() {
        flexboxLayout.refreshFlexbox(subjectsFlexBoxLayout, subjectsList, Constants.SCREEN_WIDTH, true);

        refreshLayoutAfterAddingSubjects();
    }

    private void refreshLayoutAfterAddingSubjects(){
        subjectsFlexBoxLayout.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ((ConstraintLayout.LayoutParams)horizontalLineTwo.getLayoutParams()).topMargin = subjectsFlexBoxLayout.getMeasuredHeight() + 30;
    }

    private void postJob() {
        jobClient = RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, JobClient.class);
        jobFunction = new JobFunction(jobClient, this);

        address = address.split("\n")[1].substring(9);

        Log.d("gender preference", tutorGenderPreference);
        JobPost jobPost = new JobPost(jobTitle, category, subCategory, classToTeach, subjects, tutorGenderPreference, latitude, longitude, address, salary,0, daysPerWeek, Constants.USER_ID_VALUE);
        jobFunction.postJob(this, jobPost);
    }

    public void jobPostResponse(boolean isSuccessful){
        Functions.showBlockingFragment(PostJobActivity.this, false);

        if(isSuccessful){
            onBackPressed();
        }else{
            postJobBtn.setEnabled(true);
        }
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.anyRadioButton:
                if (checked)
                    tutorGenderPreference = "any";
                    break;
            case R.id.maleRadioButton:
                if (checked)
                    tutorGenderPreference = "m";
                    break;
            case R.id.femaleRadioButton:
                if (checked)
                    tutorGenderPreference = "f";
                    break;
        }
    }

    //Response from getting System Constants
    public void loadJobActivity(){
        initView();
    }

    //--------------------------------------------------------------------------------------------------------------------------------
    //Location - start

    private void openAutocompleteActivity() {
        try {
//            // The autocomplete activity requires Google Play Services to be available. The intent
//            // builder checks this and throws an exception if it is not the case.

            AutocompleteFilter autocompleteFilter = new AutocompleteFilter.Builder()
                    .setTypeFilter(Place.TYPE_COUNTRY)
                    .setCountry("BD")
                    .build();
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                    .setFilter(autocompleteFilter)
                    .build(this);
            startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);
        } catch (GooglePlayServicesRepairableException e) {
//            // Indicates that Google Play Services is either not installed or not up to date. Prompt
//            // the user to correct the issue.
            GoogleApiAvailability.getInstance().getErrorDialog(this, e.getConnectionStatusCode(),
                    0 /* requestCode */).show();
        } catch (GooglePlayServicesNotAvailableException e) {
            // Indicates that Google Play Services is not available and the problem is not easily
            // resolvable.
            String message = "Google Play Services is not available: " +
                    GoogleApiAvailability.getInstance().getErrorString(e.errorCode);

            Log.e("Map", message);
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Check that the result was from the autocomplete widget.
        if (requestCode == REQUEST_CODE_AUTOCOMPLETE) {
            if (resultCode == RESULT_OK) {
//                // Get the user's selected place from the Intent.
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i("Map", "Place Selected: " + place.getName());

//                // Format the place's details and display them in the TextView.
                location.setText(formatPlaceDetails(getResources(), place.getName(), place.getAddress()));
//                location.setText(place.getName(), place.getAddress());
                LatLng placeLatLng = place.getLatLng();
                latitude = placeLatLng.latitude;
                longitude = placeLatLng.longitude;
                Log.e("Map", "Lol: " + placeLatLng.latitude + " " + placeLatLng.longitude);
//
//                // Display attributions if required.
                CharSequence attributions = place.getAttributions();
                if (!TextUtils.isEmpty(attributions)) {
                    //mPlaceAttribution.setText(Html.fromHtml(attributions.toString()));
                } else {
                    //mPlaceAttribution.setText("");
                }
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                Log.e("Map", "Error: Status = " + status.toString());
            } else if (resultCode == RESULT_CANCELED) {
                // Indicates that the activity closed before a selection was made. For example if
                // the user pressed the back button.
            }
        }
    }

    private static Spanned formatPlaceDetails(Resources res, CharSequence name, CharSequence address) {
        Log.e("Map", res.getString(R.string.place_details, name, address));
        return Html.fromHtml(res.getString(R.string.place_details, name, address));

    }
    //Location - end
    //--------------------------------------------------------------------------------------------------------------------------------
}
