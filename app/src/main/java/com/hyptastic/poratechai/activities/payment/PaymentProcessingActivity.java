//package com.hyptastic.poratechai.activities.payment;
//
//import android.support.v7.app.AppCompatActivity;
//import android.os.Bundle;
//import android.view.View;
//
//import com.hyptastic.poratechai.R;
//import com.hyptastic.poratechai.activities.ActionBarBackActivity;
//import com.hyptastic.poratechai.fragments.payment.tutor.PaymentProcessingFragment;
//
//public class PaymentProcessingActivity extends AppCompatActivity{
//    PaymentProcessingFragment paymentProcessingFragment;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_payment_processing);
//
////        super.setTitle("Payment");
////        setBackButtonListener(new View.OnClickListener(){
////            @Override
////            public void onClick(View view) {
////                backButtonPressed();
////            }
////        });
//
////        paymentProcessingFragment = PaymentProcessingFragment.createNewInstance(getIntent().getExtras().getParcelable("invoiceResponse"));
////        getSupportFragmentManager().beginTransaction().replace(R.id.paymentProcessingLayout, paymentProcessingFragment).commit();
//    }
//
//    @Override
//    public void onBackPressed(){
//        backButtonPressed();
//    }
//
//    private void backButtonPressed(){
//        if(paymentProcessingFragment.canWebBrowserGoBack()){
//            paymentProcessingFragment.browserBack();
//        }else{
//            super.onBackPressed();
//        }
//    }
//}
