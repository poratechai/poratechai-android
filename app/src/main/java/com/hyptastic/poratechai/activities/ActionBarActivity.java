package com.hyptastic.poratechai.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.utils.Functions;

/**
 * Created by ISHRAK on 5/15/2018.
 */

public class ActionBarActivity extends AppCompatActivity {
    private ImageView actionBarPoints;
    private TextView actionBarTextView;
    private ImageButton refreshButton;
    private Switch switchTutorOrHire;

    protected View.OnClickListener refreshButtonClickListener;
    protected Switch.OnCheckedChangeListener switchTutorOrHireListener;

    private ActionBarActivity currentActivity;

    ActionBar actionBar = null;
    View actionBarView = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        currentActivity = this;

        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);

        actionBarView = LayoutInflater.from(this).inflate(R.layout.actionbar_basic, null);
        actionBarView.setLayoutParams(new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT));
        actionBar.setCustomView(actionBarView);

        Toolbar toolbar = (Toolbar)actionBarView.getParent();
        toolbar.setPadding(0, 0, 0, 0);
        toolbar.setContentInsetsAbsolute(0, 0);

        actionBarPoints = actionBarView.findViewById(R.id.actionbarPoints);
        actionBarTextView = actionBarView.findViewById(R.id.actionbarPointsTextView);
        refreshButton = actionBarView.findViewById(R.id.refreshButton);
        switchTutorOrHire = actionBarView.findViewById(R.id.switchTutorOrHire);

        actionBarTextView.setText(String.valueOf(30));
        setSwitch(Functions.isTutor());

        actionBarPoints.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {

            }
        });
    }

    public void onTutorParentSwitchedOverNetwork(boolean isTutor, boolean isSuccessful){
        if(isSuccessful){
            Functions.setIsTutor(isTutor);
        }else{
            switchTutorOrHire.setOnCheckedChangeListener(null);
            setSwitch(!isTutor);
            switchTutorOrHire.setOnCheckedChangeListener(switchTutorOrHireListener);
        }
    }

    public void setSwitch(boolean isTutor){
        if(isTutor==true){
            switchTutorOrHire.setChecked(true);
            switchTutorOrHire.setText("Tutor");
        }else{
            switchTutorOrHire.setChecked(false);
            switchTutorOrHire.setText("Parent/Student");
        }
    }

    protected void setActionBarPointsTextView(String points)
    {
        actionBarTextView.setText(points);
    }

    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    //For Children - start
    protected ImageButton getRefreshButton(){
        return refreshButton;
    }

    protected Switch getSwitchTutorOrHire() {
        return switchTutorOrHire;
    }
    //For Children - end

    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
}
