package com.hyptastic.poratechai.activities.splash;

/**
 * Created by User on 20-Feb-18.
 */
import android.app.Activity;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.hyptastic.poratechai.activities.PorateChaiActivity;
import com.hyptastic.poratechai.activities.authentication.AuthenticationActivity;
import com.hyptastic.poratechai.BuildConfig;
import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.retrofit.functions.SystemFunction;
import com.hyptastic.poratechai.retrofit.interfaces.SystemClient;
import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
import com.hyptastic.poratechai.utils.Constants;
import com.hyptastic.poratechai.utils.Functions;

import java.util.List;

public class SplashActivity extends AppCompatActivity {
    SystemFunction systemFunction = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler(this));

        systemFunction = new SystemFunction(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, SystemClient.class), getApplicationContext());
        systemFunction.getVersionCode(this);
    }

    //Will be called after successful retrieval of the latest android version code from PorateChai Node API
    public void checkVersionUpdate(String versionCode){
        if(BuildConfig.VERSION_CODE < Integer.parseInt(versionCode)){
            enforceUpdate();
        }else{
            loadApp();
        }
    }

    //Will be called after network error when getting the latest android version code from PorateChai Node API
    public void connectionNegative(){
        Intent updateIntent = new Intent(getApplicationContext(), NoConnectionActivity.class);
        updateIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(updateIntent);
    }

    private void enforceUpdate(){
        AlertDialog.Builder updateAppDialogBuilder = new AlertDialog.Builder(this, R.style.AppTheme_Dialog)
                .setTitle(R.string.update_title)
                .setMessage(R.string.update_message)
                .setCancelable(false)
                .setPositiveButton(R.string.update_positive, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        updateApp();
                    }
                })
                .setNegativeButton(R.string.update_negative, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        closeApp();
                    }
                });

        AlertDialog updateAppDialog = updateAppDialogBuilder.create();
        updateAppDialog.show();
    }

    private void updateApp(){
        final String packageName = getPackageName();

        try{
            Intent marketIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName));

            final List<ResolveInfo> marketApps = getPackageManager().queryIntentActivities(marketIntent, 0);

            for(ResolveInfo marketApp : marketApps){
                if(marketApp.activityInfo.applicationInfo.packageName.equals("com.android.vending")){
                    ActivityInfo marketAppActivity = marketApp.activityInfo;
                    ComponentName componentName = new ComponentName(marketAppActivity.packageName, marketAppActivity.name);

                    marketIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                    marketIntent.setComponent(componentName);
                    startActivity(marketIntent);
                    break;
                }
            }
        }catch(android.content.ActivityNotFoundException e){
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName)));
        }
    }

    private void closeApp(){
        moveTaskToBack(true);
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);
    }

    public void loadApp(){
        Functions.initSharedPreferences(getApplicationContext());

        if(Functions.isLoggedIn()){
            Functions.loadLoginDataFromSharedPreferences();

            Intent intent = new Intent(this, PorateChaiActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
        else{
            Intent intent = new Intent(this, AuthenticationActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }

        Functions.getScreenDimensions(getWindowManager());
    }

    @Override
    protected void onResume() {
        super.onResume();

        systemFunction = new SystemFunction(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, SystemClient.class), getApplicationContext());
        systemFunction.getVersionCode(this);
    }

    class UncaughtExceptionHandler implements Thread.UncaughtExceptionHandler{
        private Activity activity;

        public UncaughtExceptionHandler(Activity activity){
            this.activity = activity;
        }

        @Override
        public void uncaughtException(Thread thread, Throwable throwable) {

            //Restart App - start
//            Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//
//            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_ONE_SHOT);
//
//            AlarmManager mgr = (AlarmManager)getApplicationContext().getSystemService(Context.ALARM_SERVICE);
//            mgr.set(AlarmManager.RTC, System.currentTimeMillis()+100, pendingIntent);
//
//            activity.finish();
//            System.exit(2);
            //Restart App - end

            Log.e("Uncaught Exception", throwable.getMessage());
            throwable.printStackTrace();

            //Load data after app has stopped working - start
            Functions.initSharedPreferences(getApplicationContext());
            Functions.loadLoginDataFromSharedPreferences();
            Functions.getScreenDimensions(getWindowManager());
            //Load data after app has stopped working - end
        }
    }
}