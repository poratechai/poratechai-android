package com.hyptastic.poratechai.activities.profile;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PatternItem;
import com.hyptastic.poratechai.R;
import com.hyptastic.poratechai.activities.ActionBarBackActivity;
import com.hyptastic.poratechai.retrofit.functions.UserFunctions;
import com.hyptastic.poratechai.retrofit.interfaces.UserClient;
import com.hyptastic.poratechai.retrofit.network.RetrofitServiceGenerator;
import com.hyptastic.poratechai.utils.Constants;
import com.hyptastic.poratechai.utils.PermissionUtils;
import com.skyfishjy.library.RippleBackground;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class TutorMapActivity extends ActionBarBackActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        OnMarkerDragListener,
        OnMapReadyCallback,
        //GoogleMap.OnMarkerClickListener,
        //OnCircleClickListener,
        GoogleMap.OnMyLocationButtonClickListener,
        //GoogleMap.OnMyLocationClickListener,
        ActivityCompat.OnRequestPermissionsResultCallback, GoogleMap.OnCameraMoveListener {
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;

    android.support.v4.app.FragmentManager fragmentManager;

    // Variables declaration //////////////////////////////////////////
    private String address;

    private static Button doneButton, goBackButton;
    private static TextView radiusTextView;

    private static Double radius;
    private static Double latitude;
    private static Double longitude;

    private static LatLng center;
    private static Location mylocation = null;
    private static boolean firstTimeLocationLoaded = false;

    private static final LatLng DHAKA = new LatLng(23.810332, 90.4125181);
    private static final double DEFAULT_RADIUS_METERS = 6000;
    private static final double RADIUS_OF_EARTH_METERS = 6371009;
    private final static int REQUEST_CHECK_SETTINGS_GPS=0x1;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS=0x2;
    private final static int PLACE_PICKER_REQUEST = 3;

    private GoogleApiClient googleApiClient;
    private GoogleMap mMap;

    private List<DraggableCircle> mCircles = new ArrayList<>(1);

    // Overlay hint interface
    RelativeLayout rlOverlay;
    TextView hint3, radiusText, radiusLabel;
    RippleBackground rippleBackground3;

    UserFunctions userFunctions;

    private String location;

    //////////////////////////////////////////////////////////////////////////////

    private class DraggableCircle {
        private final Marker mCenterMarker;
        private final Marker mRadiusMarker;
        private final Circle mCircle;
        private double mRadiusMeters;

        public DraggableCircle(LatLng center, double radiusMeters) {
            mRadiusMeters = radiusMeters;
            mCenterMarker = mMap.addMarker(new MarkerOptions()
                    .position(center)
                    .title("Current position")
                    .draggable(true));
            mRadiusMarker = mMap.addMarker(new MarkerOptions()
                    .position(toRadiusLatLng(center, radiusMeters))
                    .draggable(true)
                    .icon(BitmapDescriptorFactory.defaultMarker(
                            BitmapDescriptorFactory.HUE_AZURE)));
            mCircle = mMap.addCircle(new CircleOptions()
                    .center(center)
                    .radius(radiusMeters)
                    //.strokeWidth(mStrokeWidthBar.getProgress())
                    //.strokeColor(mStrokeColorArgb)
                    //.fillColor(mFillColorArgb)
                    .clickable(true));
        }

        public boolean onMarkerMoved(Marker marker) {
            if (marker.equals(mCenterMarker)) {
                mCircle.setCenter(marker.getPosition());
                mRadiusMarker.setPosition(toRadiusLatLng(marker.getPosition(), mRadiusMeters));
                return true;

            }
            if (marker.equals(mRadiusMarker)) {
                mRadiusMeters =
                        toRadiusMeters(mCenterMarker.getPosition(), mRadiusMarker.getPosition());
                mCircle.setRadius(mRadiusMeters);

                //update overlay UI
                String text;
                //text = Double.toString(mRadiusMeters/1000) + "km";
                text = String.format("%.2f", mRadiusMeters/1000) + "km";
                radiusText.setText(text);
                //hint3.setVisibility(View.GONE);
                //rippleBackground3.setVisibility(View.GONE);
                doneButton.setVisibility(View.VISIBLE);
                //goBackButton.setVisibility(View.GONE);
                //rlOverlay.setVisibility(View.GONE);
                radiusText.setTextColor(Color.parseColor("#000000"));
                radiusLabel.setTextColor(Color.parseColor("#000000"));
                return true;
            }
            return false;
        }

        public Marker getmCenterMarker(){
            return mCenterMarker;
        }
        public Marker getmRadiusMarker() { return mRadiusMarker; }

        /*public void onStyleChange() {
            mCircle.setStrokeWidth(mStrokeWidthBar.getProgress());
            mCircle.setStrokeColor(mStrokeColorArgb);
            mCircle.setFillColor(mFillColorArgb);
        }*/

        public void setStrokePattern(List<PatternItem> pattern) {
            mCircle.setStrokePattern(pattern);
        }

        public void setClickable(boolean clickable) {
            mCircle.setClickable(clickable);
        }
    }

    private static LatLng toRadiusLatLng(LatLng center, double radiusMeters) {
        double radiusAngle = Math.toDegrees(radiusMeters / RADIUS_OF_EARTH_METERS) /
                Math.cos(Math.toRadians(center.latitude));
        return new LatLng(center.latitude, center.longitude + radiusAngle);
    }

    private static double toRadiusMeters(LatLng center, LatLng radius) {
        float[] result = new float[1];
        Location.distanceBetween(center.latitude, center.longitude,
                radius.latitude, radius.longitude, result);
        return result[0];
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutor_map);

        userFunctions = new UserFunctions(RetrofitServiceGenerator.createService(Constants.EMAIL_VALUE, Constants.TOKEN_VALUE, UserClient.class), this);

        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
        initViews();
        initHintView();
        setUpGClient();
    }

    private void initViews(){
        doneButton = (Button)findViewById(R.id.button_select);
        goBackButton = (Button)findViewById(R.id.goBackBtn);
        radiusTextView = (TextView)findViewById(R.id.tv_radius);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getRadius();
            }
        });
        goBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build(TutorMapActivity.this), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void initHintView() {
        rlOverlay = (RelativeLayout) findViewById(R.id.rlOverlay);
        hint3 = (TextView) findViewById(R.id.helpText3);
        radiusText = (TextView) findViewById(R.id.radius);
        radiusLabel = (TextView) findViewById(R.id.radiusLabel);
        rippleBackground3 = (RippleBackground) findViewById(R.id.ripple3);
        rippleBackground3.startRippleAnimation();
    }

    private void getRadius(){
        radius = mCircles.get(0).mRadiusMeters;
        center = mCircles.get(0).getmCenterMarker().getPosition();
        latitude = center.latitude;
        longitude = center.longitude;
        address = location; //set from PlacePicker onActivityResult

//        radiusTextView.setText(radius.toString() +" " + center.toString() +" " + latitude.toString() + " " + longitude.toString());

//        Toast.makeText(this, String.valueOf(radius) + " " + String.valueOf(center) + " " + String.valueOf(latitude) + " " + String.valueOf(longitude) + " " + String.valueOf(address), Toast.LENGTH_LONG).show();

        userFunctions.updateLocation(this, latitude, longitude, radius, address);
    }

    public void locationUpdatedOverNetwork(boolean isUpdateSuccessful){
        if(isUpdateSuccessful){
            Intent returnIntent = new Intent();

            returnIntent.putExtra("address", address);
            setResult(Activity.RESULT_OK, returnIntent);

            finish();
        }else{
            finish();
        }
    }

    public String getAddress(double lat, double lng) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        String add= "";
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            Address obj = addresses.get(0);
            add = obj.getLocality();
            //String add = obj.getAddressLine(0);
            add = add + ", " + obj.getCountryName();
//            add = add + "\n" + obj.getCountryCode();
//            add = add + "\n" + obj.getAdminArea();
//            add = add + "\n" + obj.getPostalCode();
//            add = add + "\n" + obj.getSubAdminArea();
//            add = add + "\n" + obj.getLocality();
//            add = add + "\n" + obj.getSubThoroughfare();

            Log.v("IGA", "Address" + add);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return add;
    }

    @Override
    public void onMapReady(GoogleMap map) {
        map.setContentDescription("Define circle region by dragging the markers");

        mMap = map;
        mMap.setOnMapClickListener(latLng -> {
            Toast.makeText(this, "heyeyey", Toast.LENGTH_SHORT).show();
        });
        mMap.setOnCameraMoveListener((GoogleMap.OnCameraMoveListener) this);
        mMap.setOnMarkerDragListener(this);

        DraggableCircle circle = new DraggableCircle(DHAKA, DEFAULT_RADIUS_METERS);
        mCircles.add(circle);

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(DHAKA, 12f));

        //mMap.setOnMyLocationButtonClickListener(this);
        //mMap.setOnMyLocationClickListener(this);
        enableMyLocation();
    }

    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION, true);
        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            //mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onCameraMove() {
        CameraPosition cameraPosition = mMap.getCameraPosition();
        LatLng position = cameraPosition.target;
        for(DraggableCircle draggableCircle: mCircles) {
            Marker marker = draggableCircle.getmRadiusMarker();
            marker.setPosition(position);
            draggableCircle.onMarkerMoved(marker);
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        //Toast.makeText(this, "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        return false;
    }

//    @Override
//    public void onMyLocationClick(@NonNull Location location) {
//        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
//        //blueLocation = latLng;
//
//        for(DraggableCircle draggableCircle: mCircles){
//            Marker marker = draggableCircle.getmCenterMarker();
//            marker.setPosition(latLng);
//            draggableCircle.onMarkerMoved(marker);
//        }
//
//        //update hint overlay interface
//        rippleBackground3.setVisibility(View.VISIBLE);
//        hint3.setVisibility(View.VISIBLE);
//        LatLng newCameraCenter = new LatLng(location.getLatitude() + 0.007, location.getLongitude() + 0.015);
//        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(newCameraCenter, 12f));
//    }


//    @Override
//    public boolean onMarkerClick(Marker marker) {
//        LatLng markerPosition = marker.getPosition();
//        //Toast.makeText(this, markerPosition.latitude + " " + markerPosition.longitude, Toast.LENGTH_LONG);
//        return false;
//    }

    @Override
    public void onMarkerDragStart(Marker marker) {
        onMarkerMoved(marker);
    }

    @Override
    public void onMarkerDrag(Marker marker) {
        onMarkerMoved(marker);
    }

    private void onMarkerMoved(Marker marker) {
        //rlOverlay.setVisibility(View.GONE);
        for (DraggableCircle draggableCircle : mCircles) {
            if (draggableCircle.onMarkerMoved(marker)) {
                break;
            }
        }
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        onMarkerMoved(marker);
    }

//    @Override
//    public void onCircleClick(Circle circle) {
//        circle.setVisible(false);
//        Log.d("Map", "Circle touched");
//    }

    private synchronized void setUpGClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage((FragmentActivity)this, 0, this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        mylocation = location;
        if(location == null) {
            Log.d("location", "location null bro");
        }
        if (mylocation != null && firstTimeLocationLoaded == false) {
            firstTimeLocationLoaded = true;
//            Double latitude=mylocation.getLatitude();
//            Double longitude=mylocation.getLongitude();
//            Log.d("location", String.valueOf(latitude));
////            latitudeTextView.setText("Latitude : "+latitude);
////            longitudeTextView.setText("Longitude : "+longitude);
//            //Or Do whatever you want with your location
//            LatLng latLng = new LatLng(latitude, longitude);
//            for(DraggableCircle draggableCircle: mCircles){
//                Marker marker = draggableCircle.getmCenterMarker();
//                marker.setPosition(latLng);
//                //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12f));
//                draggableCircle.onMarkerMoved(marker);
//
//                LatLng newCameraCenter = new LatLng(mylocation.getLatitude() + 0.007, mylocation.getLongitude() + 0.015);
//                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(newCameraCenter, 12f));
//            }
        }

    }

    @Override
    public void onConnected(Bundle bundle) {
        checkPermissions();
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Do whatever you need
        //You can display a message here
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //You can display a message here
    }

    private void getMyLocation(){
        if(googleApiClient!=null) {
            if (googleApiClient.isConnected()) {
                int permissionLocation = ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION);
                if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                    mylocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                    LocationRequest locationRequest = new LocationRequest();
                    locationRequest.setInterval(5000);
                    locationRequest.setFastestInterval(5000);
                    locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
                    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                            .addLocationRequest(locationRequest);
                    builder.setAlwaysShow(true);
                    LocationServices.FusedLocationApi
                            .requestLocationUpdates(googleApiClient, locationRequest, this);
                    PendingResult<LocationSettingsResult> result =
                            LocationServices.SettingsApi
                                    .checkLocationSettings(googleApiClient, builder.build());
                    result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

                        @Override
                        public void onResult(LocationSettingsResult result) {
                            final Status status = result.getStatus();
                            switch (status.getStatusCode()) {
                                case LocationSettingsStatusCodes.SUCCESS:
                                    // All location settings are satisfied.
                                    // You can initialize location requests here.
                                    int permissionLocation = ContextCompat
                                            .checkSelfPermission(TutorMapActivity.this,
                                                    Manifest.permission.ACCESS_COARSE_LOCATION);
                                    if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                                        enableMyLocation();
                                        mylocation = LocationServices.FusedLocationApi
                                                .getLastLocation(googleApiClient);
                                        //Log.d("location", String.valueOf(mylocation.getLatitude()));
                                    }
                                    break;
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                    // Location settings are not satisfied.
                                    // But could be fixed by showing the user a dialog.
                                    try {
                                        // Show the dialog by calling startResolutionForResult(),
                                        // and check the result in onActivityResult().
                                        // Ask to turn on GPS automatically
                                        status.startResolutionForResult(TutorMapActivity.this,
                                                REQUEST_CHECK_SETTINGS_GPS);
                                    } catch (IntentSender.SendIntentException e) {
                                        // Ignore the error.
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    // Location settings are not satisfied.
                                    // However, we have no way
                                    // to fix the
                                    // settings so we won't show the dialog.
                                    // finish();
                                    break;
                            }
                        }
                    });
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS_GPS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        getMyLocation();
                        enableMyLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        finish();
                        break;
                }
            case PLACE_PICKER_REQUEST:
                if(resultCode == RESULT_OK) {
                    Place place = PlacePicker.getPlace(this,data);
                    if(place != null) {
                        location = place.getAddress().toString();

                        String toastMsg = String.format("Place: %s", place.getName());
//                        Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();

                        LatLng latlng = place.getLatLng();
                        Double latitude=latlng.latitude;
                        Double longitude=latlng.longitude;
                        LatLng latLng = new LatLng(latitude, longitude);
                        for(DraggableCircle draggableCircle: mCircles){
                            Marker marker = draggableCircle.getmCenterMarker();
                            marker.setPosition(latLng);

                            draggableCircle.onMarkerMoved(marker);

                            LatLng newCameraCenter = new LatLng(latitude + 0.007, longitude + 0.015);
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(newCameraCenter, 12f));
                        }
                    }
                }
                break;
        }
    }

    private void checkPermissions(){
        int permissionLocation = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this,
                        listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            }
        }else{
            getMyLocation();
            enableMyLocation();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        int permissionLocation = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            getMyLocation();
            enableMyLocation();
        }
    }
}
